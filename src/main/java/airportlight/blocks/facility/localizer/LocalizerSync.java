package airportlight.blocks.facility.localizer;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class LocalizerSync  extends TileEntityMessage implements IMessageHandler<LocalizerSync, IMessage> {
    int lineCount;
    float linePitch;

    public LocalizerSync() {
        super();
    }

    public LocalizerSync(LocalizerTile tile, int lineCount, float linePitch) {
        super(tile);
        this.lineCount = lineCount;
        this.linePitch = linePitch;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.lineCount = tag.getInteger("lineCount");
        this.linePitch = tag.getFloat("linePitch");
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("lineCount", this.lineCount);
        tag.setFloat("linePitch", this.linePitch);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(LocalizerSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof LocalizerTile) {
            ((LocalizerTile) tile).setDatas(message.lineCount, message.linePitch);
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
