package airportlight.blocks.facility.localizer;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class LocalizerModel extends ModelBaseNormal<LocalizerTile> {
    protected IModelCustom model;
    protected ResourceLocation textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/common_yukikaze.png");


    public LocalizerModel() {
        super();
        model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/localizer_yukikaze.obj"));
    }

    @Override
    public void render(@NotNull LocalizerTile tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-tile.getBaseAngle(), 0.0, 1.0, 0.0);
        GL11.glTranslated(-tile.linePitch * (tile.lineCount - 1) / 2.0, 0, 0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureModel);
        if (DisplayListIDs.LocalizerBody == -1) {
            DisplayListIDs.LocalizerBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.LocalizerBody, GL11.GL_COMPILE);
            model.renderAll();
            GL11.glEndList();
        }
        for (int num = tile.lineCount - 1; num >= 0; num--) {
            GL11.glCallList(DisplayListIDs.LocalizerBody);

            GL11.glTranslatef(tile.linePitch, 0, 0);
        }
        GL11.glPopMatrix();
    }
}