package airportlight.blocks.facility.localizer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class LocalizerRenderer  extends TileEntitySpecialRenderer {
    private final LocalizerModel model = new LocalizerModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof LocalizerTile) {
            this.model.render((LocalizerTile) tE, x, y, z);
        }
    }
}