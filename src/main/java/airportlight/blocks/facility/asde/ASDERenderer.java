package airportlight.blocks.facility.asde;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class ASDERenderer extends TileEntitySpecialRenderer {
    private final ASDEModel model = new ASDEModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileASDE) {
            this.model.render((TileASDE) tE, x, y, z);
        }
    }
}