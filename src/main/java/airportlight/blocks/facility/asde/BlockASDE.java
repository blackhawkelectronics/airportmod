package airportlight.blocks.facility.asde;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockASDE extends BlockNormal {
    public BlockASDE() {
        super();
        setBlockName("ASDE");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":asde");
        setBlockBounds(-3.1F, 0F, -3.1F, 4.1F, 9F, 4.1F);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ - 2.0, (double) p_149633_3_, (double) p_149633_4_ - 2.0, (double) p_149633_2_ + 3.0, (double) p_149633_3_ + 6.0, (double) p_149633_4_ + 3.0);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TileASDE();
    }
}
