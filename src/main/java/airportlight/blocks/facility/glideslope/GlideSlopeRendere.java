package airportlight.blocks.facility.glideslope;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class GlideSlopeRendere extends TileEntitySpecialRenderer {
    private final GlideSlopeModel model = new GlideSlopeModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof GlideSlopeTile) {
            this.model.render((GlideSlopeTile) tile, x, y, z);
        }
    }

}