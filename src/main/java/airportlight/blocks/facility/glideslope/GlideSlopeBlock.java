package airportlight.blocks.facility.glideslope;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class GlideSlopeBlock extends BlockAngleLightNormal {
    public GlideSlopeBlock() {
        super();
        setBlockName("GlideSlope");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":glideslope");
        setBlockBounds(-0.5F, 0F, -0.5F, 1.5F, 16.5F, 1.5F);
    }


    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ + 0.2, p_149633_3_, p_149633_4_ + 0.2, (double) p_149633_2_ + 0.8, (double) p_149633_3_ + 2, (double) p_149633_4_ + 0.8);
    }

    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new GlideSlopeTile();
    }

}
