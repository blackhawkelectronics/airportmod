package airportlight.blocks.facility.pbb;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockNormal;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockPBB extends BlockNormal {

    public BlockPBB() {
        super();
        setBlockName("PBB");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":pbb");
        setBlockBounds(0F, 0F, 0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TilePBB) {
            player.openGui(ModAirPortLight.instance, GuiID.PBB.getID(), world, x, y, z);
            return true;
        }
        return false;
    }

    @Override
    public void onBlockPlacedBy(World p_149689_1_, int p_149689_2_, int p_149689_3_, int p_149689_4_, EntityLivingBase p_149689_5_, ItemStack p_149689_6_) {
        super.onBlockPlacedBy(p_149689_1_, p_149689_2_, p_149689_3_, p_149689_4_, p_149689_5_, p_149689_6_);
        TileEntity tile = p_149689_1_.getTileEntity(p_149689_2_, p_149689_3_, p_149689_4_);
        if (tile instanceof TilePBB) {
            TilePBB tIlePBB = (TilePBB) tile;
            tIlePBB.setDefaultSeatPos();
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World p_149668_1_, int p_149668_2_, int p_149668_3_, int p_149668_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149668_2_ + this.minX, (double) p_149668_3_ + this.minY, (double) p_149668_4_ + this.minZ, (double) p_149668_2_ + this.maxX, (double) p_149668_3_ + this.maxY, (double) p_149668_4_ + this.maxZ);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TilePBB(metadata);
    }
}
