package airportlight.blocks.facility.pbb

import airportlight.ModAirPortLight
import airportlight.modcore.PacketHandlerAPM.sendPacketServer
import airportlight.modcore.commonver.GuiID
import net.minecraft.client.Minecraft
import net.minecraft.crash.CrashReport
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.MathHelper
import net.minecraft.util.ReportedException
import net.minecraft.util.Vec3
import net.minecraft.world.World
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

/**
 * PBBの操縦席
 */
class PBBDriverSeat(world: World) : Entity(world) {
    private var speedMultiplier = 0.0
    private var riddenByPlayer: EntityPlayer? = null
    var tilePBB: TilePBB? = null
    private var tileCheckCnt: Int = 0

    /**
     * PBBの飛行機接続部分の角度をこちらに同期
     */
    var headAng = 90.0f

    init {
        setSize(1.0f, 4.0f)
        isInvisible = true
    }

    constructor(world: World, tilePBB: TilePBB) : this(world) {
        this.tilePBB = tilePBB
        setPosition(tilePBB.seatX, tilePBB.seatY, tilePBB.seatZ)
        isInvisible = true
    }

    override fun entityInit() {
        dataWatcher.addObject(PBBDataFlag.GuiOpen.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Brake.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Dismount.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.CameraReset.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Left.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Right.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Up.flagID, 0)
        dataWatcher.addObject(PBBDataFlag.Down.flagID, 0)
    }

    //dataFlagを共有 クライアントで呼び出した場合サーバー経由で共有する
    fun dataFlagUpdateSync(flag: PBBDataFlag, data: Int) {
        dataWatcher.updateObject(flag.flagID, data)
        if (worldObj.isRemote && dataWatcher.hasChanges()) {
            sendPacketServer(PBBDataFlagMessage(this, flag, data))
        }
    }


    override fun setDead() {
        if (riddenByPlayer != null) {
            riddenByPlayer = null
            dataFlagUpdateSync(PBBDataFlag.Dismount, 0)
        }
        super.setDead()
    }

    override fun onUpdate() {
        if (!worldObj.isRemote) {
            if (tileCheckCnt++ > 20) {
                //PBB本体との接続が切れたなら削除
                if (tilePBB is TilePBB) {
                    if (tilePBB!!.isInvalid) {
                        setDead()
                    }
                } else {
                    setDead()
                }
                tileCheckCnt = 0
            }
        }

        //運転処理/
        if (riddenByEntity != null && riddenByEntity is EntityPlayer) {
            val player = riddenByEntity as EntityPlayer
            //GUIに戻るフラグが立っていて他のGUIが開いていないならDriveGuiを再表示
            if (worldObj.isRemote && Minecraft.getMinecraft().currentScreen == null && dataWatcher.getWatchableObjectInt(
                    PBBDataFlag.GuiOpen.flagID
                ) == 1
            ) {
                player.openGui(
                    ModAirPortLight.instance, GuiID.PBBDrive.id, worldObj,
                    posX.toInt(), posY.toInt(), posZ.toInt()
                )
                dataWatcher.updateObject(PBBDataFlag.GuiOpen.flagID, 0)
            }

            val entityLivingBase = riddenByEntity as EntityLivingBase

            //足の角度を操作・適用
            var stepYaw = (-entityLivingBase.moveStrafing * 1.125f).toDouble()
            if (3 < abs(stepYaw)) {
                stepYaw = if (0 < stepYaw) {
                    3.0
                } else {
                    -3.0
                }
            }
            rotationYaw = MathHelper.wrapAngleTo180_double(rotationYaw + stepYaw).toFloat()
            tilePBB?.legAngDeg = rotationYaw

            //前後速度の適用
            this.speedMultiplier += entityLivingBase.moveForward.toDouble() / 100
            if (this.speedMultiplier > 0.1) {
                this.speedMultiplier = 0.1
            } else if (this.speedMultiplier < -0.1) {
                this.speedMultiplier = -0.1
            }
            stepHeight = 0.5f
        } else {
            //操縦者がいなければ速度低下
            this.speedMultiplier *= 0.5
            stepHeight = 0f
        }

        //ブレーキキーを押しているときにスピードダウン
        if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Brake.flagID) == 1) {
            this.speedMultiplier = (abs(this.speedMultiplier) - 0.005) * if (this.speedMultiplier < 0) {
                -1
            } else {
                1
            }
        }

        //速度が落ちきったら停止
        if (abs(this.speedMultiplier) < 0.005) {
            this.speedMultiplier = 0.0
        }

        //平面方向の動きを計算
        val rotationYawRad = Math.toRadians(rotationYaw.toDouble())
        motionX = -sin(rotationYawRad) * this.speedMultiplier
        motionZ = cos(rotationYawRad) * this.speedMultiplier

        //上下の動きをキー入力から計算
        motionY = 0.0
        if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Up.flagID) == 1) {
            motionY += 0.05f
        }
        if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Down.flagID) == 1) {
            motionY -= 0.05f
        }

        moveEntity(motionX, motionY, motionZ)

        if (tilePBB != null) {
            if ((prevPosX != posX ||
                        prevPosY != posY ||
                        prevPosZ != posZ)
                && !worldObj.isRemote
            ) {
                //描画用各種変数を再計算
                tilePBB!!.calcAndUpdatePBBAng()
            }

            //Headの向きを制御するキーボード操作を反映
            if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Left.flagID) == 1) {
                tilePBB!!.headAng -= 1f
                riddenByPlayer?.let { it.rotationYaw -= 1f }
            }
            if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Right.flagID) == 1) {
                tilePBB!!.headAng += 1f
                riddenByPlayer?.let { it.rotationYaw += 1f }
            }
        }


        this.speedMultiplier *= 0.98

        //プレイヤーの顔の向きリセットフラグが立っているならリセット
        if (dataWatcher.getWatchableObjectInt(PBBDataFlag.CameraReset.flagID) == 1) {
            this.riddenByEntity?.rotationYaw = this.headAng
            this.riddenByEntity?.rotationPitch = this.rotationPitch
            dataFlagUpdateSync(PBBDataFlag.CameraReset, 0)
        }

        //視点切り替え
        if (worldObj.isRemote) {
            val mc = Minecraft.getMinecraft()
            if (mc.gameSettings.keyBindTogglePerspective.isPressed) {
                if (!buttonPushing) {
                    ++mc.gameSettings.thirdPersonView
                    if (mc.gameSettings.thirdPersonView > 2) {
                        mc.gameSettings.thirdPersonView = 0
                    }
                }
                buttonPushing = true
            } else {
                buttonPushing = false
            }
        }

        //プレイヤーが座っているか・ディスマウントするかなどの制御
        if (riddenByEntity == null) {
            if (this.riddenByPlayer != null) {
                this.riddenByPlayer = null
                dataWatcher.updateObject(PBBDataFlag.Dismount.flagID, 0)
            }
        } else {
            if (dataWatcher.getWatchableObjectInt(PBBDataFlag.Dismount.flagID) == 1) {
                riddenByEntity.mountEntity(null)
                this.riddenByPlayer = null
                dataWatcher.updateObject(PBBDataFlag.Dismount.flagID, 0)
            }
        }

        super.onUpdate()
    }

    /**
     * キーの押し始め検出用
     */
    var buttonPushing = false


    override fun updateRiderPosition() {
        if (riddenByEntity != null) {
            //ヘッドの向きに追従してプレイヤー位置を移動
            val vec = Vec3.createVectorHelper(0.75, 0.0, 1.5)
            val yawRad = Math.toRadians(-headAng.toDouble()).toFloat()
            vec.rotateAroundY(yawRad)

            val offsetX = vec.xCoord
            val offsetZ = vec.zCoord

            riddenByEntity.setPosition(
                posX + offsetX,
                posY + riddenByEntity.getYOffset() + 1.75,
                posZ + offsetZ
            )
        }
    }


    override fun moveEntity(p_70091_1_: Double, p_70091_3_: Double, p_70091_5_: Double) {
        //軽量化や段差乗り越えなどの独自実装のために書き換え

        var motionX = p_70091_1_
        var motionY = p_70091_3_
        var motionZ = p_70091_5_
        worldObj.theProfiler.startSection("move")
        ySize *= 0.4f
        val d6 = motionX
        val d7 = motionY
        val d8 = motionZ
        val axisAlignedBB = boundingBox.copy()
        var list = worldObj.func_147461_a(boundingBox.addCoord(motionX, motionY, motionZ))
        for (o in list) {
            motionY = (o as AxisAlignedBB).calculateYOffset(boundingBox, motionY)
        }
        boundingBox.offset(0.0, motionY, 0.0)
        if (!field_70135_K && d7 != motionY) {
            motionZ = 0.0
            motionY = 0.0
            motionX = 0.0
        }
        val flag1 = onGround || d7 != motionY && d7 < 0.0
        var j = 0
        while (j < list.size) {
            motionX = (list[j] as AxisAlignedBB).calculateXOffset(boundingBox, motionX)
            ++j
        }
        boundingBox.offset(motionX, 0.0, 0.0)
        if (!field_70135_K && d6 != motionX) {
            motionZ = 0.0
            motionY = 0.0
            motionX = 0.0
        }
        j = 0
        while (j < list.size) {
            motionZ = (list[j] as AxisAlignedBB).calculateZOffset(boundingBox, motionZ)
            ++j
        }
        boundingBox.offset(0.0, 0.0, motionZ)
        if (!field_70135_K && d8 != motionZ) {
            motionZ = 0.0
            motionY = 0.0
            motionX = 0.0
        }
        val d10: Double
        val d11: Double
        var k: Int
        val d12: Double
        if (stepHeight > 0.0f && flag1 && (d6 != motionX || d8 != motionZ)) {
            d12 = motionX
            d10 = motionY
            d11 = motionZ
            motionX = d6
            motionY = stepHeight.toDouble()
            motionZ = d8
            val axisAlignedBB1 = boundingBox.copy()
            boundingBox.setBB(axisAlignedBB)
            list = worldObj.func_147461_a(boundingBox.addCoord(d6, motionY, d8))
            k = 0
            while (k < list.size) {
                motionY = (list[k] as AxisAlignedBB).calculateYOffset(boundingBox, motionY)
                ++k
            }
            boundingBox.offset(0.0, motionY, 0.0)
            if (!field_70135_K && d7 != motionY) {
                motionZ = 0.0
                motionY = 0.0
                motionX = 0.0
            }
            k = 0
            while (k < list.size) {
                motionX = (list[k] as AxisAlignedBB).calculateXOffset(boundingBox, motionX)
                ++k
            }
            boundingBox.offset(motionX, 0.0, 0.0)
            if (!field_70135_K && d6 != motionX) {
                motionZ = 0.0
                motionY = 0.0
                motionX = 0.0
            }
            k = 0
            while (k < list.size) {
                motionZ = (list[k] as AxisAlignedBB).calculateZOffset(boundingBox, motionZ)
                ++k
            }
            boundingBox.offset(0.0, 0.0, motionZ)
            if (!field_70135_K && d8 != motionZ) {
                motionZ = 0.0
                motionY = 0.0
                motionX = 0.0
            }
            if (!field_70135_K && d7 != motionY) {
                motionZ = 0.0
                motionY = 0.0
                motionX = 0.0
            } else {
                motionY = -stepHeight.toDouble()
                k = 0
                while (k < list.size) {
                    motionY = (list[k] as AxisAlignedBB).calculateYOffset(boundingBox, motionY)
                    ++k
                }
                boundingBox.offset(0.0, motionY, 0.0)
            }
            if (d12 * d12 + d11 * d11 >= motionX * motionX + motionZ * motionZ) {
                motionX = d12
                motionY = d10
                motionZ = d11
                boundingBox.setBB(axisAlignedBB1)
            }
        }
        worldObj.theProfiler.endSection()
        worldObj.theProfiler.startSection("rest")
        posX = (boundingBox.minX + boundingBox.maxX) / 2.0
        posY = boundingBox.minY + yOffset.toDouble() - ySize.toDouble()
        posZ = (boundingBox.minZ + boundingBox.maxZ) / 2.0
        isCollidedHorizontally = d6 != motionX || d8 != motionZ
        isCollidedVertically = d7 != motionY
        onGround = d7 != motionY && d7 < 0.0
        isCollided = isCollidedHorizontally || isCollidedVertically
        updateFallState(motionY, onGround)
        if (d6 != motionX) {
            this.motionX = 0.0
        }
        if (d7 != motionY) {
            this.motionY = 0.0
        }
        if (d8 != motionZ) {
            this.motionZ = 0.0
        }
        try {
            func_145775_I()
        } catch (throwable: Throwable) {
            val crashReport = CrashReport.makeCrashReport(throwable, "Checking entity block collision")
            val crashReportCategory = crashReport.makeCategory("Entity being checked for collision")
            addEntityCrashInfo(crashReportCategory)
            throw ReportedException(crashReport)
        }
        worldObj.theProfiler.endSection()
    }

    override fun canBeCollidedWith(): Boolean {
        return true
    }

    override fun getCollisionBorderSize(): Float {
        return 0f
    }

    override fun interactFirst(player: EntityPlayer): Boolean {
        //誰も乗っていなければ乗車
        if (riddenByEntity == null || riddenByEntity !is EntityPlayer || riddenByEntity === player) {
            player.mountEntity(this)
            if (!worldObj.isRemote) {
                //操縦用のGuiを開く
                player.openGui(
                    ModAirPortLight.instance, GuiID.PBBDrive.id, worldObj,
                    posX.toInt(), posY.toInt(), posZ.toInt()
                )
            }
            this.riddenByPlayer = player
            player.rotationYaw = headAng
        }
        return true
    }


    override fun readEntityFromNBT(nbtTagCompound: NBTTagCompound) {
        this.headAng = nbtTagCompound.getFloat("headAng")
    }

    override fun writeEntityToNBT(nbtTagCompound: NBTTagCompound) {
        nbtTagCompound.setFloat("headAng", this.headAng)
    }
}
