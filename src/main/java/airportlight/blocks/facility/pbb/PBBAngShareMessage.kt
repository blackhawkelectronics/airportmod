package airportlight.blocks.facility.pbb

import airportlight.util.TileEntityMessage
import airportlight.util.currentWorld
import airportlight.util.packetErrorLog
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.tileentity.TileEntity
import kotlin.properties.Delegates

/**
 * PBBの各所の角度・長さなどの情報をクライアントへ共有
 */
class PBBAngShareMessage : TileEntityMessage {
    private var footerPitch: Float by Delegates.notNull()
    private var footerYaw: Float by Delegates.notNull()
    private var bridgeLong: Float by Delegates.notNull()
    private var headYaw: Float by Delegates.notNull()
    private var legAng: Float by Delegates.notNull()
    private var legLong: Float by Delegates.notNull()
    private var seatX: Double by Delegates.notNull()
    private var seatY: Double by Delegates.notNull()
    private var seatZ: Double by Delegates.notNull()

    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()

    constructor(
        tileEntity: TilePBB,
        footerPitch: Float,
        footerYaw: Float,
        bridgeLong: Float,
        headYaw: Float,
        legAng: Float,
        legLong: Float,
        seatX: Double,
        seatY: Double,
        seatZ: Double
    ) : super(tileEntity as TileEntity) {
        this.footerPitch = footerPitch
        this.footerYaw = footerYaw
        this.bridgeLong = bridgeLong
        this.headYaw = headYaw
        this.legAng = legAng
        this.legLong = legLong
        this.seatX = seatX
        this.seatY = seatY
        this.seatZ = seatZ
    }

    override fun write(buf: ByteBuf) {
        buf.writeFloat(this.footerPitch)
        buf.writeFloat(this.footerYaw)
        buf.writeFloat(this.bridgeLong)
        buf.writeFloat(this.headYaw)
        buf.writeFloat(this.legAng)
        buf.writeFloat(this.legLong)
        buf.writeDouble(this.seatX)
        buf.writeDouble(this.seatY)
        buf.writeDouble(this.seatZ)
    }

    override fun read(buf: ByteBuf) {
        this.footerPitch = buf.readFloat()
        this.footerYaw = buf.readFloat()
        this.bridgeLong = buf.readFloat()
        this.headYaw = buf.readFloat()
        this.legAng = buf.readFloat()
        this.legLong = buf.readFloat()
        this.seatX = buf.readDouble()
        this.seatY = buf.readDouble()
        this.seatZ = buf.readDouble()
    }

    companion object : IMessageHandler<PBBAngShareMessage, IMessage> {
        override fun onMessage(message: PBBAngShareMessage, ctx: MessageContext): IMessage? {
            if (!ctx.currentWorld.getChunkFromBlockCoords(message.pos.x, message.pos.z).isChunkLoaded) return null
            val tile = message.getTileEntity(ctx) as? TilePBB
                ?: return packetErrorLog("${message.pos} is not TilePBB")
            tile.setAngPitchYaw(
                message.footerPitch,
                message.footerYaw,
                message.bridgeLong,
                message.headYaw,
                message.legAng,
                message.legLong,
                message.seatX,
                message.seatY,
                message.seatZ
            )

            return null
        }
    }
}
