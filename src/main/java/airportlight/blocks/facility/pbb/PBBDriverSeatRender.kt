package airportlight.blocks.facility.pbb

import net.minecraft.client.renderer.entity.RenderEntity
import net.minecraft.entity.Entity

/**
 * PBB操縦用座席のEntityが写り込まないようにするためのレンダー
 */
class PBBDriverSeatRender : RenderEntity() {
    override fun doRender(
        p_76986_1_: Entity,
        p_76986_2_: Double,
        p_76986_4_: Double,
        p_76986_6_: Double,
        p_76986_8_: Float,
        p_76986_9_: Float
    ) {
    }
}