package airportlight.blocks.facility.pbb;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class PBBModel extends ModelBaseNormal<TilePBB> {
    protected IModelCustom bridgeFooter, bridgeCenter, bridgehead;
    protected IModelCustom bridgeFooterLong, bridgeCenterLong, bridgeheadLong;
    protected final ResourceLocation textureGray, textureBlack;
    protected final IModelCustom footerbody, footerpanel, footerroatebody;
    protected final IModelCustom headbody, headPanel;
    protected final IModelCustom legbody, legyaw, legwheel;

    public PBBModel() {
        this.textureGray = new ResourceLocation(ModAirPortLight.DOMAIN, "gray.png");
        this.textureBlack = new ResourceLocation(ModAirPortLight.DOMAIN, "black.png");
        this.footerbody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/footerbody.obj"));
        this.footerpanel = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/footerpanel.obj"));
        this.footerroatebody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/footerroatebody.obj"));
        this.bridgeFooter = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgefooter.obj"));
        this.bridgeCenter = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgecenter.obj"));
        this.bridgehead = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgehead.obj"));
        this.headbody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/headbody.obj"));
        this.headPanel = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/headpanel.obj"));
        this.legbody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/legbody.obj"));
        this.legyaw = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/legyaw.obj"));
        this.legwheel = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/legwheel.obj"));


        this.bridgeFooterLong = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgefooterlong.obj"));
        this.bridgeCenterLong = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgecenterlong.obj"));
        this.bridgeheadLong = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/pbb/bridgeheadlong.obj"));
    }

    protected void renderBridgeFooter(boolean useLongType) {
        if (useLongType) {
            if (DisplayListIDs.PBBBridgeFooterLong == -1) {
                DisplayListIDs.PBBBridgeFooterLong = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeFooterLong, GL11.GL_COMPILE);
                this.bridgeFooterLong.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeFooterLong);
        } else {
            if (DisplayListIDs.PBBBridgeFooter == -1) {
                DisplayListIDs.PBBBridgeFooter = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeFooter, GL11.GL_COMPILE);
                this.bridgeFooter.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeFooter);
        }
    }

    protected void renderBridgeCenter(boolean useLongType) {
        if (useLongType) {
            if (DisplayListIDs.PBBBridgeCenterLong == -1) {
                DisplayListIDs.PBBBridgeCenterLong = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeCenterLong, GL11.GL_COMPILE);
                this.bridgeCenterLong.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeCenterLong);
        } else {
            if (DisplayListIDs.PBBBridgeCenter == -1) {
                DisplayListIDs.PBBBridgeCenter = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeCenter, GL11.GL_COMPILE);
                this.bridgeCenter.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeCenter);
        }
    }

    protected void renderBridgeHead(boolean useLongType) {
        if (useLongType) {
            if (DisplayListIDs.PBBBridgeHeadLong == -1) {
                DisplayListIDs.PBBBridgeHeadLong = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeHeadLong, GL11.GL_COMPILE);
                this.bridgeheadLong.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeHeadLong);
        } else {
            if (DisplayListIDs.PBBBridgeHead == -1) {
                DisplayListIDs.PBBBridgeHead = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.PBBBridgeHead, GL11.GL_COMPILE);
                this.bridgehead.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.PBBBridgeHead);
        }
    }

    @Override
    public void render(@NotNull TilePBB tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y + 1.0, z + 0.5);
        int meta = tile.getWorldObj().getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord);
        int baseAngle = -(meta - 2) * 90;
        if (baseAngle >= 180) {
            baseAngle = -180;
        }
        GL11.glRotated(baseAngle, 0, 1, 0);
        GL11.glTranslated(0.5, 0, 0);


        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGray);

        GL11.glTranslated(0, 0, 1.5);

        //建屋に繋がる部分
        if (DisplayListIDs.PBBFooterbody == -1) {
            DisplayListIDs.PBBFooterbody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBFooterbody, GL11.GL_COMPILE);
            this.footerbody.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PBBFooterbody);


        //建屋側の回転部分とパネルを描画
        double footerYaw = tile.getFooterYawAng();
        double footerYawAngOffset = footerYaw - baseAngle;

        if (DisplayListIDs.PBBFooterPanel == -1) {
            DisplayListIDs.PBBFooterPanel = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBFooterPanel, GL11.GL_COMPILE);
            this.footerpanel.renderAll();
            GL11.glEndList();
        }

        int hasPanel = 0;
        for (double i = footerYawAngOffset; -90 < i; i -= 15) {
            GL11.glCallList(DisplayListIDs.PBBFooterPanel);
            GL11.glRotated(+15, 0, 1, 0);
            hasPanel++;
        }
        GL11.glRotated(90, 0, 1, 0);
        for (double i = footerYawAngOffset; i < 90; i += 15) {
            GL11.glCallList(DisplayListIDs.PBBFooterPanel);
            GL11.glRotated(+15, 0, 1, 0);
            hasPanel++;
        }
        if (0 < hasPanel) {
            GL11.glRotated(-15 * hasPanel - 90, 0, 1, 0);
        }

        GL11.glRotated(-180 + footerYawAngOffset, 0, 1, 0);

        if (DisplayListIDs.PBBFooterRoatebody == -1) {
            DisplayListIDs.PBBFooterRoatebody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBFooterRoatebody, GL11.GL_COMPILE);
            this.footerroatebody.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PBBFooterRoatebody);

        GL11.glRotated(180, 0, 1, 0);
        GL11.glTranslated(0, 0, 1.25);

        //ブリッジの根元側
        double bridgePitch = tile.getBridgePitch();
        GL11.glRotated(bridgePitch, -1, 0, 0);

        boolean useLongType = tile.getUseLongType();
        renderBridgeFooter(useLongType);

        double bridgeLong = tile.getBridgeLong();
        double bridgeLongC = bridgeLong * 0.66;

        //ブリッジの中間
        GL11.glTranslated(0, 0, bridgeLongC);

        renderBridgeCenter(useLongType);

        //ブリッジの先端側
        GL11.glTranslated(0, 0, -bridgeLongC + bridgeLong);

        renderBridgeHead(useLongType);

        //ブリッジの足
        double legLong = tile.getLegLong();
        GL11.glTranslated(0, -legLong, -2.5);
        if (DisplayListIDs.PBBLegbody == -1) {
            DisplayListIDs.PBBLegbody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBLegbody, GL11.GL_COMPILE);
            this.legbody.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PBBLegbody);

        //足の回転部分
        if (DisplayListIDs.PBBLegYaw == -1) {
            DisplayListIDs.PBBLegYaw = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBLegYaw, GL11.GL_COMPILE);
            this.legyaw.renderAll();
            GL11.glEndList();
        }

        double legAngDiff = -footerYaw - tile.getLegAngDeg();

        GL11.glRotated(-bridgePitch, -1, 0, 0);
        GL11.glRotated(legAngDiff, 0, 1, 0);
        GL11.glCallList(DisplayListIDs.PBBLegYaw);

        //タイヤ
        GL11.glTranslated(0, -0.4, 0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureBlack);
        if (DisplayListIDs.PBBLegWheel == -1) {
            DisplayListIDs.PBBLegWheel = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBLegWheel, GL11.GL_COMPILE);
            this.legwheel.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PBBLegWheel);
        GL11.glTranslated(0, 0.4, 0);

        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGray);
        GL11.glRotated(-legAngDiff, 0, 1, 0);
        GL11.glRotated(bridgePitch, -1, 0, 0);

        GL11.glTranslated(0, legLong, 2.5);


        //ブリッジヘッド側の回転部分
        double headAng = tile.getHeadAng();
        double headAngleOffset = -footerYaw - headAng;

        GL11.glRotated(headAngleOffset, 0, 1, 0);

        if (DisplayListIDs.PBBHeadbody == -1) {
            DisplayListIDs.PBBHeadbody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBHeadbody, GL11.GL_COMPILE);
            this.headbody.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PBBHeadbody);


        if (DisplayListIDs.PBBHeadPanel == -1) {
            DisplayListIDs.PBBHeadPanel = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PBBHeadPanel, GL11.GL_COMPILE);
            this.headPanel.renderAll();
            GL11.glEndList();
        }

        hasPanel = 0;
        if (headAngleOffset > 180) {
            headAngleOffset -= 360;
        }
        for (double i = headAngleOffset; -55 < i; i -= 15) {
            GL11.glCallList(DisplayListIDs.PBBHeadPanel);
            GL11.glRotated(-15, 0, 1, 0);
            hasPanel++;
        }
        GL11.glRotated(-100, 0, 1, 0);
        for (double i = headAngleOffset; i < 52; i += 15) {
            GL11.glCallList(DisplayListIDs.PBBHeadPanel);
            GL11.glRotated(-15, 0, 1, 0);
            hasPanel++;
        }
        if (0 < hasPanel) {
            GL11.glRotated(-15 * hasPanel - 90, 0, 1, 0);
        }


        GL11.glPopMatrix();
    }
}
