package airportlight.blocks.facility.pbb

import airportlight.modcore.config.APMKeyConfig
import airportlight.modcore.gui.ContainerAirPort
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiIngameMenu
import net.minecraft.client.gui.inventory.GuiContainer
import net.minecraft.client.settings.KeyBinding
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.Display
import java.util.*

class PBBDriverGui : GuiContainer {
    private var pbbDriverSeat: PBBDriverSeat? = null
    private var player: EntityPlayer? = null
    private var tilePBB: TilePBB? = null

    constructor() : super(ContainerAirPort()) {
        allowUserInput = true
    }

    constructor(player: EntityPlayer) : super(ContainerAirPort()) {
        this.player = player
        val seat = (player.ridingEntity as? PBBDriverSeat)

        this.tilePBB = seat?.tilePBB
        if (player.ridingEntity is PBBDriverSeat) {
            pbbDriverSeat = player.ridingEntity as PBBDriverSeat
        }
        allowUserInput = true
    }

    override fun initGui() {
        mc.mouseHelper.grabMouseCursor()
    }

    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            //操作説明の背景
            drawGradientRect(width - 100, height - 170, width - 10, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, p_146976_2_: Int, p_146976_3_: Int) {
        //操作説明一覧
        val settings = Minecraft.getMinecraft().gameSettings
        fontRendererObj.drawString(
            "Move       : "
                    + Keyboard.getKeyName(settings.keyBindForward.keyCode) + ", "
                    + Keyboard.getKeyName(settings.keyBindBack.keyCode),
            width - 95, height - 160, -1, false
        )
        fontRendererObj.drawString(
            ("Strafing    : "
                    + Keyboard.getKeyName(settings.keyBindLeft.keyCode) + ", "
                    + Keyboard.getKeyName(settings.keyBindRight.keyCode)), width - 95, height - 150, -1, false
        )
        fontRendererObj.drawString(
            "Brake      : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake)),
            width - 95,
            height - 140,
            -1,
            false
        )

        fontRendererObj.drawString(
            "Head Rotate : "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadLeft)) + ", "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadRight)),
            width - 95,
            height - 120,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Head Height : "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadUp)) + ", "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadDown)),
            width - 95,
            height - 110,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Cam Reset  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)),
            width - 95,
            height - 70,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Dismount   : " + Keyboard.getKeyName(settings.keyBindSneak.keyCode),
            width - 95,
            height - 60,
            -1,
            false
        )
    }

    override fun onGuiClosed() {
        //プレイヤーが乗っていないなら、Guiを復帰時に再び開く設定を解除
        if (pbbDriverSeat!!.riddenByEntity == null) {
            pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.GuiOpen, 0)
        }
        //プレイヤーを下ろすかのフラグを状況によって設定
        if (Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.keyCode)) {
            pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.Dismount, 1)
        } else {
            pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.Dismount, 0)
        }

        //その他設定をリセット
        pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.Brake, 0)
    }

    override fun keyTyped(p_73869_1_: Char, p_73869_2_: Int) {
        if (p_73869_2_ == 1) {
            mc.displayGuiScreen(GuiIngameMenu()) //closeScreen();
            Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor()
            return
        }
        //移動などのMinecraft通常の処理を呼び出すための処置
        val settings = Minecraft.getMinecraft().gameSettings
        if (Keyboard.isKeyDown(settings.keyBindSneak.keyCode)) {
            player!!.isInvisible = false
            player!!.mountEntity(null)
            player!!.closeScreen()
            return
        }
        if (Keyboard.getEventKeyState()) {
            val keyID = Keyboard.getEventKey()
            if (keyID == settings.keyBindCommand.keyCode) {
                KeyBinding.onTick(keyID)
                pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.GuiOpen, 1)
                player!!.closeScreen()
                Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor()
                return
            }
            val keyIDs: List<Int> = ArrayList(
                listOf(
                    settings.keyBindInventory.keyCode,
                    settings.keyBindChat.keyCode,
                    settings.keyBindTogglePerspective.keyCode,
                    settings.keyBindSmoothCamera.keyCode,
                    settings.field_152395_am.keyCode
                )
            )
            if (keyIDs.contains(keyID)) {
                KeyBinding.onTick(keyID)
                if (keyID == settings.keyBindInventory.keyCode || keyID == settings.keyBindChat.keyCode) {
                    pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.GuiOpen, 1)
                    Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor()
                }
                return
            }
            //cameraの向きをリセット
            if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)) {
                pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.CameraReset, 1)
            }
        }
    }

    override fun updateScreen() {
        super.updateScreen()
        val flag = Display.isActive()
        //マウスの動きに合わせてプレイヤーの顔の動きが変わるように
        if (flag) {
            mc.mouseHelper.mouseXYChange()
            val f1 = mc.gameSettings.mouseSensitivity * 0.6f + 0.2f
            val f2 = f1 * f1 * f1 * 8.0f
            val f3 = mc.mouseHelper.deltaX.toFloat() * f2
            val f4 = mc.mouseHelper.deltaY.toFloat() * f2
            var b0: Byte = 1
            if (mc.gameSettings.invertMouse) {
                b0 = -1
            }
            mc.thePlayer.setAngles(f3, f4 * b0.toFloat())
        }

        //各種操作ボタンを押しているかの情報を送信
        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Brake,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Left,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadLeft))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Right,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadRight))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Up,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadUp))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Down,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadDown))) {
                1
            } else {
                0
            }
        )

        //ゲームプレイのキーボード入力をプレイに反映できるように
        val settings = Minecraft.getMinecraft().gameSettings
        val keyIDStates: List<Int> = ArrayList(
            listOf(
                settings.keyBindForward.keyCode,
                settings.keyBindBack.keyCode,
                settings.keyBindRight.keyCode,
                settings.keyBindLeft.keyCode,
                settings.keyBindPlayerList.keyCode
            )
        )
        for (stateKey: Int in keyIDStates) {
            KeyBinding.setKeyBindState(stateKey, Keyboard.isKeyDown(stateKey))
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
