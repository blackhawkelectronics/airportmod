package airportlight.blocks.facility.pbb

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.normal.TileNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.block.BlockAir
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.MathHelper
import net.minecraft.util.Vec3
import net.minecraft.world.World
import kotlin.math.atan2
import kotlin.math.hypot

open class TilePBB() : TileNormal() {

    constructor(meta: Int) : this() {
        headAng = -(meta - 2) * 90f - 90
    }

    private var posInitialized = false

    var useLongType: Boolean = false

    /**
     * 操縦座席用のEntity
     */
    var seat: PBBDriverSeat? = null
    var seatEntityID: Int = -1
    var seatX: Double = Double.NaN
    var seatY: Double = Double.NaN
    var seatZ: Double = Double.NaN

    //挙動計算用
    /**
     * 操舵角度
     */
    var legAngDeg = 0.0f
        set(value) {
            hasChange = field != value
            field = value
        }

    //描画用計算結果
    /**
     * 直線部分の角度
     */
    var footerYawAng = 0.0f
        private set

    /**
     * 直線部分の上下方向の角度
     * */
    var bridgePitch = -3.0f

    /**
     * 直線部分の長さ
     */
    var bridgeLong = 0f

    /**
     * 飛行機との接続部分の角度
     */
    var headAng = 90f
        set(value) {
            val newValue = MathHelper.wrapAngleTo180_float(value)
            hasChange = field != newValue
            field = newValue
            seat?.headAng = newValue
        }

    /**
     * 足の長さ
     */
    var legLong = 0.25f

    var hasChange = false

    override fun updateEntity() {
        if (!worldObj.isRemote) {
            if (seat == null) {
                //seatとの接続がなければ生成して設定
                seat = PBBDriverSeat(worldObj, this)
                seat!!.rotationYaw = headAng
                worldObj.spawnEntityInWorld(seat)
                seatEntityID = seat!!.entityId
                worldObj.notifyBlockChange(this.xCoord, this.yCoord, this.zCoord, this.blockType)

                calcAndUpdatePBBAng()
            }
            if (hasChange) {
                sendSyncMsg()
                this.markDirty()
                hasChange = false
            }
        } else {
            if (seat == null && seatEntityID >= 0 && worldObj != null) {
                //クライアント側で運転用座席との接続が切れているなら設定
                val entity = this.worldObj.getEntityByID(seatEntityID)
                if (entity is PBBDriverSeat) {
                    seat = entity
                    seat!!.tilePBB = this
                    calcAndUpdatePBBAng()
                }
            }
        }
    }

    protected open fun setDefaultSeatPos() {
        if (useLongType) {
            when (getBlockMetadata()) {
                0 -> {
                    seatX = xCoord + 0.5 - 27.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 - 28.5
                }
                1 -> {
                    seatX = xCoord + 0.5 + 28.5
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 - 27.0
                }
                2 -> {
                    seatX = xCoord + 0.5 + 27.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 + 28.5
                }
                3 -> {
                    seatX = xCoord + 0.5 - 28.5
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 + 27.0
                }
            }
        } else {
            when (getBlockMetadata()) {
                0 -> {
                    seatX = xCoord + 0.5 - 18.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 - 19.0
                }
                1 -> {
                    seatX = xCoord + 0.5 + 19.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 - 18.0
                }
                2 -> {
                    seatX = xCoord + 0.5 + 18.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 + 19.0
                }
                3 -> {
                    seatX = xCoord + 0.5 - 19.0
                    seatY = yCoord - 0.25
                    seatZ = zCoord + 0.5 + 18.0
                }
            }
        }
    }

    /**
     * 運転席とPBB接続部の位置関係から描画用の角度情報などを計算
     */
    fun calcAndUpdatePBBAng() {
        if (seat != null) {
            val baseAngle = -(getBlockMetadata() - 2) * 90.0
            val footerDiff = Vec3.createVectorHelper(0.5, 0.0, 1.5)
            footerDiff.rotateAroundY(Math.toRadians(baseAngle).toFloat())

            val diffX = seat!!.posX - (this.xCoord + 0.5 + footerDiff.xCoord)
            val diffY = seat!!.posY - this.yCoord
            val diffZ = seat!!.posZ - (this.zCoord + 0.5 + footerDiff.zCoord)
            val diffAngYaw = -Math.toDegrees(atan2(-diffX, diffZ))
            val diffXZ = hypot(diffX, diffZ)
            val diffAngPitch = Math.toDegrees(atan2(diffY, diffXZ))
            footerYawAng = diffAngYaw.toFloat()
            bridgePitch = diffAngPitch.toFloat()
            bridgeLong = hypot(diffY, diffXZ).toFloat() - 1.25f
            var nextY = MathHelper.floor_double(seat!!.posY)
            var block: Block?
            while (true) {
                block = this.worldObj.getBlock(
                    MathHelper.floor_double(seat!!.posX),
                    nextY,
                    MathHelper.floor_double(seat!!.posZ)
                )
                if (block == null || block !is BlockAir || nextY < 0) {
                    break
                }
                nextY--
            }

            val grandHeight: Double = if (block != null) {
                nextY - 1 + block.blockBoundsMaxY
            } else {
                nextY.toDouble()
            }

            legLong = ((seat!!.posY - grandHeight) * 0.85).toFloat()

            seatX = seat!!.posX
            seatY = seat!!.posY
            seatZ = seat!!.posZ
            hasChange = true
        }
    }

    override fun setWorldObj(world: World) {
        worldObj = world
        if (seat != null) {
            seat!!.setWorld(worldObj)
        }
    }

    fun posInit() {
        if (!posInitialized) {
            val meta = getWorldObj().getBlockMetadata(xCoord, yCoord, zCoord)
            footerYawAng = (-(meta - 2) * 90).toFloat()
            footerYawAng += 45.0f
            posInitialized = true
        }
    }

    @SideOnly(Side.CLIENT)
    override fun getRenderBoundingBox(): AxisAlignedBB {
        return AxisAlignedBB.getBoundingBox(
            (xCoord - 100).toDouble(),
            (yCoord - 10).toDouble(),
            (zCoord - 100).toDouble(),
            (xCoord + 100).toDouble(),
            (yCoord + 10).toDouble(),
            (zCoord + 100).toDouble()
        )
    }

    private fun sendSyncMsg() {
        if (!this.worldObj.isRemote) {
            PacketHandlerAPM.sendPacketAround(
                PBBAngShareMessage(
                    this,
                    this.footerYawAng,
                    this.bridgePitch,
                    this.bridgeLong,
                    this.headAng,
                    this.legAngDeg,
                    this.legLong,
                    this.seatX,
                    this.seatY,
                    this.seatZ
                ),
                this,
                500.0
            )
        }
    }

    fun resetPos() {
        setDefaultSeatPos()
        this.seat?.setPosition(this.seatX, this.seatY, this.seatZ)
        calcAndUpdatePBBAng()
        this.markDirty()
    }

    fun setAngPitchYaw(
        footerPitch: Float,
        footerYaw: Float,
        bridgeLong: Float,
        headYaw: Float,
        legAng: Float,
        legLong: Float,
        seatX: Double,
        seatY: Double,
        seatZ: Double
    ) {
        this.footerYawAng = footerPitch
        this.bridgePitch = footerYaw
        this.bridgeLong = bridgeLong
        this.headAng = headYaw
        this.legAngDeg = legAng
        this.legLong = legLong
        this.seatX = seatX
        this.seatY = seatY
        this.seatZ = seatZ
        this.markDirty()
        this.posInitialized = true
    }

    fun setData(useLongType: Boolean): Boolean {
        if (this.useLongType != useLongType) {
            this.useLongType = useLongType
            return true
        }
        return false
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        this.legAngDeg = nbt.getFloat("legAngDeg")
        this.headAng = nbt.getFloat("headAng")
        this.seatEntityID = nbt.getInteger("seatEntityID")
        this.seatX = nbt.getDouble("seatX")
        this.seatY = nbt.getDouble("seatY")
        this.seatZ = nbt.getDouble("seatZ")
        this.useLongType = nbt.getBoolean("useLongType")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setFloat("legAngDeg", legAngDeg)
        nbt.setFloat("headAng", headAng)
        nbt.setInteger("seatEntityID", seatEntityID)
        nbt.setDouble("seatX", seatX)
        nbt.setDouble("seatY", seatY)
        nbt.setDouble("seatZ", seatZ)
        nbt.setBoolean("useLongType", useLongType)
    }
}
