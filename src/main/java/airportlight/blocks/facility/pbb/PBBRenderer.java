package airportlight.blocks.facility.pbb;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

@SideOnly(Side.CLIENT)
public class PBBRenderer extends TileEntitySpecialRenderer {
    private final PBBModel modePBB = new PBBModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TilePBB) {
            this.modePBB.render((TilePBB) tE, x, y, z);
        }
    }

}