package airportlight.blocks.facility.psr;

import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import org.jetbrains.annotations.NotNull;

public class PSRDataShare extends TileEntityMessage implements IMessageHandler<PSRDataShare, IMessage> {
    boolean enabled = false;
    float angle = 0f;

    @SuppressWarnings("unused")
    public PSRDataShare() {
    }

    public PSRDataShare(boolean enabled, float angle) {
        this.enabled = enabled;
        this.angle = angle;
    }

    @Override
    public void write(@NotNull ByteBuf buf) {
        buf.writeBoolean(enabled);
        buf.writeFloat(angle);
    }

    @Override
    public void read(@NotNull ByteBuf buf) {
        enabled = buf.readBoolean();
        angle = buf.readFloat();
    }


    @Override
    public IMessage onMessage(PSRDataShare message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TilePSR) {
            TilePSR tilePSR = ((TilePSR) tile);

            if (ctx.side.isClient()) {
                tilePSR.rotateEnabled = message.enabled;
                tilePSR.headAngleDeg = message.angle;
            }
        }
        return null;
    }
}
