package airportlight.blocks.facility.psr;

import airportlight.ModAirPortLight;
import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockPSR extends BlockAngleLightNormal {
    public BlockPSR() {
        super();
        setBlockName("PSR");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":psr");
        setBlockBounds(-1F, 0F, -1F, 2F, 4F, 2F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int hitX, float hitY, float hitZ, float tick) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TilePSR) {
            TilePSR tilePSR = (TilePSR) tile;
            tilePSR.rotateEnabled = !tilePSR.rotateEnabled;
            if (!world.isRemote) {
                PacketHandlerAPM.sendPacketPlayersLoadingTheBlock(new PSRDataShare(tilePSR.rotateEnabled, tilePSR.headAngleDeg), tilePSR);
            }
            return true;
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ - 1, p_149633_3_, p_149633_4_ - 1, (double) p_149633_2_ + 2, (double) p_149633_3_ + 5, (double) p_149633_4_ + 2);
    }

    public boolean InversionBaseAngle() {
        return true;
    }

    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TilePSR();
    }
}
