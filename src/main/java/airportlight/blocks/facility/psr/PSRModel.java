package airportlight.blocks.facility.psr;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormalLight;
import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class PSRModel extends ModelBaseNormalLight<TilePSR> {
    protected IModelCustom model;
    protected final ResourceLocation textureModel;

    public PSRModel() {
        super();
        this.model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/psr_yukikaze.obj"));
        this.textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/psr_yukikaze.png");
    }

    @Override
    protected void ModelBodyRender() {
        if (DisplayListIDs.PSRBody == -1) {
            DisplayListIDs.PSRBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PSRBody, GL11.GL_COMPILE);
            model.renderPart("body");
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PSRBody);
    }

    protected void ModelHeadRender() {
        if (DisplayListIDs.PSRHead == -1) {
            DisplayListIDs.PSRHead = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PSRHead, GL11.GL_COMPILE);
            model.renderOnly("ssr", "psr");
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PSRHead);
    }

    @Override
    public void render(@NotNull TilePSR tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glDisable(GL11.GL_CULL_FACE);
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-tile.getBaseAngle(), 0.0, 1.0, 0.0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureModel);
        ModelBodyRender();
        GL11.glRotated(tile.getBaseAngle() - tile.headAngleDeg, 0.0, 1.0, 0.0);
        ModelHeadRender();
        GL11.glPopMatrix();
    }
}
