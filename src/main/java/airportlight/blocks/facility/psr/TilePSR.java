package airportlight.blocks.facility.psr;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import org.jetbrains.annotations.NotNull;

public class TilePSR extends TileAngleLightNormal {
    boolean rotateEnabled = false;
    float headAngleDeg = 0f;
    float angleSpeed = 4.5f;

    public TilePSR() {
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - 7,
                yCoord,
                zCoord - 7,
                xCoord + 8,
                yCoord + 12,
                zCoord + 8
        );
    }

    @Override
    public void updateEntity() {
        if (rotateEnabled) {
            headAngleDeg += angleSpeed;
            if (headAngleDeg >= 360) {
                headAngleDeg -= 360;
                if (!worldObj.isRemote) {
                    PacketHandlerAPM.sendPacketPlayersLoadingTheBlock(new PSRDataShare(true, this.headAngleDeg), this);
                }
            }
        }
    }

    @Override
    public void setBaseAngleWithOffset(@NotNull EntityLivingBase entityLivingBase, boolean inversionBaseAngle, int step) {
        super.setBaseAngleWithOffset(entityLivingBase, inversionBaseAngle, step);
        this.headAngleDeg = (float) this.getBaseAngle();
    }
}
