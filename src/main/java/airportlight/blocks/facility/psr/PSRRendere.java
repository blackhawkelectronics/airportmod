package airportlight.blocks.facility.psr;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class PSRRendere extends TileEntitySpecialRenderer {
    private final PSRModel model = new PSRModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof TilePSR) {
            this.model.render((TilePSR) tile, x, y, z);
        }
    }

}