package airportlight.blocks.light.taxiwayedgelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.ReUseModelDataBank;
import airportlight.modcore.normal.ModelBaseNormalStatic;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class TaxiwayEdgeLightModel extends ModelBaseNormalStatic<TileTaxiwayEdgeLight> implements IUseWeightModel {
    protected ResourceLocation textureBlew = new ResourceLocation(ModAirPortLight.DOMAIN, "blew.png");
    protected ResourceLocation textureOrange = new ResourceLocation(ModAirPortLight.DOMAIN, "orange.png");

    public TaxiwayEdgeLightModel() {
        super();
        ReUseModelDataBank.ModelInit();
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        ReUseModelDataBank.reloadModels(UseWeightModel);
    }

    @Override
    protected void ModelBodyRender() {
        ReUseModelDataBank.setDisplayList();
        GL11.glCallList(DisplayListIDs.EdgeBody);
    }

    @Override
    protected void renderLight(EntityPlayer player, TileEntity tile, double dist, float worldLight) {
        boolean lightON = worldLight < 0.5;
        if (dist > 8 && lightON) {
            dist = 1 + (dist - 8) / 10;
            double dist1 = 1 / dist;
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureBlew);
            GL11.glTranslated(0.0, 0.21, 0.0);
            GL11.glScaled(dist, dist, dist);
            GL11.glCallList(DisplayListIDs.EdgeLightON);
            GL11.glScaled(dist1, dist1, dist1);
            GL11.glTranslated(0.0, -0.21, 0.0);
        } else {
            GL11.glTranslated(0.0, 0.21, 0.0);
            if (lightON) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureBlew);
                GL11.glCallList(DisplayListIDs.EdgeLightON);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureBlew);
                GL11.glCallList(DisplayListIDs.EdgeLightOFF);
            }
            GL11.glTranslated(0.0, -0.21, 0.0);
        }
    }
}