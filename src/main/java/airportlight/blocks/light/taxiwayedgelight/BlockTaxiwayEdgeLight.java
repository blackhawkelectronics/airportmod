package airportlight.blocks.light.taxiwayedgelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockTaxiwayEdgeLight extends BlockNormal {
    public BlockTaxiwayEdgeLight() {
        super();
        setBlockName("TaxiwayEdgeLight");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":taxiwayedgelight");
        setBlockBounds(0.2F, 0F, 0.2F, 0.8F, 0.3F, 0.8F);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ + 0.2, p_149633_3_, (double) p_149633_4_ + 0.2, (double) p_149633_2_ + 0.8, (double) p_149633_3_ + 0.3, (double) p_149633_4_ + 0.8);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TileTaxiwayEdgeLight();
    }
}
