package airportlight.blocks.light.runwayedgelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockRunwayEdgeLight extends BlockNormal {
    public BlockRunwayEdgeLight() {
        super();
        setBlockName("RunwayEdgeLight");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":runwayedgelight");
        setBlockBounds(0.2F, 0F, 0.2F, 0.8F, 0.3F, 0.8F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TileRunwayEdgeLight) {
            ((TileRunwayEdgeLight) tile).invert(world);
            return true;
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ + 0.2, p_149633_3_, (double) p_149633_4_ + 0.2, (double) p_149633_2_ + 0.8, (double) p_149633_3_ + 0.3, (double) p_149633_4_ + 0.8);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TileRunwayEdgeLight();
    }
}

