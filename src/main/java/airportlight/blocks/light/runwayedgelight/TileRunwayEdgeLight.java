package airportlight.blocks.light.runwayedgelight;

import airportlight.modcore.normal.ILightModeInvert;
import airportlight.modcore.normal.TileNormal;
import net.minecraft.nbt.NBTTagCompound;

public class TileRunwayEdgeLight extends TileNormal implements ILightModeInvert {
    public TileRunwayEdgeLight() {
    }

    private boolean lightModeNormal = true;

    public boolean getLightModeNormal() {
        return this.lightModeNormal;
    }

    @Override
    public boolean getInvert() {
        return lightModeNormal;
    }

    @Override
    public void setInvert(boolean invert) {
        this.lightModeNormal = invert;
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setBoolean("lightNormal", this.lightModeNormal);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.lightModeNormal = p_145839_1_.getBoolean("lightNormal");
    }
}
