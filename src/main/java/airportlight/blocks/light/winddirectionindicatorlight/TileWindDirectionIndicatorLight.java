package airportlight.blocks.light.winddirectionindicatorlight;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;

public class TileWindDirectionIndicatorLight extends TileAngleLightNormal {
    public TileWindDirectionIndicatorLight() {
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - 3,
                yCoord,
                zCoord - 3,
                xCoord + 3,
                yCoord + 3.3,
                zCoord + 3
        );
    }
}
