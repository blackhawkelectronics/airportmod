package airportlight.blocks.light.winddirectionindicatorlight;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class WindDirectionIndicatorLightRenderer extends TileEntitySpecialRenderer {
    private final WindDirectionIndicatorLightModel model = new WindDirectionIndicatorLightModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof TileWindDirectionIndicatorLight) {
            this.model.render((TileWindDirectionIndicatorLight) tile, x, y, z);
        }
    }
}
