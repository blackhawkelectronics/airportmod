package airportlight.blocks.light.runwaytouchdownzonelight;

import airportlight.modcore.normal.ILightModeInvert;
import airportlight.modcore.normal.TileAngleLightNormal;
import net.minecraft.nbt.NBTTagCompound;

public class TileRunwayTouchdownZoneLight extends TileAngleLightNormal implements ILightModeInvert {
    public TileRunwayTouchdownZoneLight() {
    }


    private boolean lightModeNormal = true;

    public boolean getLightModeNormal() {
        return this.lightModeNormal;
    }

    @Override
    public boolean getInvert() {
        return lightModeNormal;
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setBoolean("lightNormal", this.lightModeNormal);
    }

    @Override
    public void setInvert(boolean invert) {
        this.lightModeNormal = invert;
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.lightModeNormal = p_145839_1_.getBoolean("lightNormal");
    }
}
