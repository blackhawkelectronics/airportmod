package airportlight.blocks.light.runwaytouchdownzonelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

public class RunwayTouchdownZoneLightModel extends AngleLightModelBase<TileRunwayTouchdownZoneLight> implements IUseWeightModel {
    protected IModelCustom modelBase;
    protected ResourceLocation textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");

    public RunwayTouchdownZoneLightModel() {
        super();
        IModelCustom useModel;
        if (APMConfig.UseWeightModel) {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwaytouchdownzonelight.obj"));
        } else {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightrunwaytouchdownzonelight.obj"));
        }
        this.modelBase = useModel;
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        IModelCustom useModel;
        if (UseWeightModel) {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwaytouchdownzonelight.obj"));
        } else {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightrunwaytouchdownzonelight.obj"));
        }
        this.modelBase = useModel;
    }

    @Override
    protected void ModelBodyRender() {
        if (DisplayListIDs.TouchdownZone == -1) {
            DisplayListIDs.TouchdownZone = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.TouchdownZone, GL11.GL_COMPILE);
            modelBase.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.TouchdownZone);
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        boolean lightON = worldLight < 0.5 && ((TileRunwayTouchdownZoneLight) tile).getLightModeNormal();
        if (dist > 8 && lightON) {
            if (Math.abs(MathHelper.wrapAngleTo180_double(tile.getBackAngle()) - playerAng) < 45) {
                dist = 1 + (dist - 8) / 8;
                double dist1 = 1 / dist;
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
                GL11.glScaled(dist, dist, dist);
                renderREDLON();
                GL11.glScaled(dist1, dist1, dist1);
            }
        } else {
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
            if (lightON) {
                renderLightON_LED();
            } else {
                renderLightOFF_LED();
            }
        }
    }
}
