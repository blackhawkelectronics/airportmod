package airportlight.blocks.light.aerodromebeacon;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import java.nio.FloatBuffer;

public class AerodromeBeaconModel extends ModelBaseNormal<TileAerodromeBeacon> {
    protected final ResourceLocation textureGray, textureGreen, textureWhite;
    protected final IModelCustom modelBody, modelHead, modelLight;

    public AerodromeBeaconModel() {
        this.modelBody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "aerodromebeaconlightbody.obj"));
        this.modelHead = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "aerodromebeaconlighthead.obj"));
        this.modelLight = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "aerodromebeaconlightlenz.obj"));
        this.textureGray = new ResourceLocation(ModAirPortLight.DOMAIN, "gray.png");
        this.textureGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "aqua.png");
        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");
    }

    private static FloatBuffer toFloatBuffer(float[] values) {
        FloatBuffer floats = BufferUtils.createFloatBuffer(4).put(values);
        floats.flip();
        return floats;
    }

    @Override
    public void render(@NotNull TileAerodromeBeacon tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        if (DisplayListIDs.AerodromeBody == -1) {
            DisplayListIDs.AerodromeBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.AerodromeBody, GL11.GL_COMPILE);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGray);
            this.modelBody.renderAll();
            GL11.glTranslated(0, 8.5, 0);
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.AerodromeBody);
        GL11.glRotated(tile.getLightAng(), 0, 1, 0);

        float worldLight = tile.getWorldObj().getSunBrightnessFactor(1.0F);
        if (worldLight < 0.5) {
            EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
            double dx = tile.xCoord - player.posX + 0.5;
            double dy = tile.yCoord - player.posY;
            double dz = tile.zCoord - player.posZ + 0.5;
            double dist = Math.sqrt(dx * dx + dy * dy + dz * dz);
            double dist1 = 1 / dist;
            if (dist > 10) {
                dist = 1 + (dist - 10) / 60;
                GL11.glScaled(dist, dist, dist);

                renderHeadLightON();

                GL11.glScaled(dist1, dist1, dist1);
            } else {
                renderHeadLightON();
            }
        } else {
            if (DisplayListIDs.AerodromeLightOFF == -1) {
                DisplayListIDs.AerodromeLightOFF = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.AerodromeLightOFF, GL11.GL_COMPILE);
                this.modelHead.renderAll();
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
                this.modelLight.renderAll();
                GL11.glRotated(90, 0, 1, 0);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
                this.modelLight.renderAll();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.AerodromeLightOFF);
        }
        GL11.glTranslated(0, -8.5, 0);
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{0, 0, 0, 1}));
        GL11.glPopMatrix();
    }

    private void renderHeadLightON() {
        if (DisplayListIDs.AerodromeLightON == -1) {
            DisplayListIDs.AerodromeLightON = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.AerodromeLightON, GL11.GL_COMPILE);
            this.modelHead.renderAll();

            GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{1f, 1f, 1f, 1f}));
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
            render(240, this.modelLight);
            GL11.glRotated(90, 0, 1, 0);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
            render(240, this.modelLight);
            GL11.glRotated(-90, 0, 1, 0);
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.AerodromeLightON);
    }


    public void render(int light, IModelCustom obj) {
        if (obj instanceof WavefrontObject) {
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(light);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) obj).groupObjects) {
                groupObject.render(tessellator);
            }
            tessellator.draw();
        } else {
            obj.renderAll();
        }
    }
}
