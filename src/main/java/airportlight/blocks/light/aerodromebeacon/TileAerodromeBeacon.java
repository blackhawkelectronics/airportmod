package airportlight.blocks.light.aerodromebeacon;

import airportlight.modcore.normal.TileNormal;

public class TileAerodromeBeacon extends TileNormal {
    double lightAng = 0;

    public TileAerodromeBeacon() {
    }

    public void updateEntity() {
        if (this.getWorldObj().getSunBrightnessFactor(1.0F) < 0.5) {
            lightAng -= 3.6;
        } else {
            lightAng = 0;
        }
    }

    public double getLightAng() {
        return this.lightAng;
    }
}
