package airportlight.blocks.light.papi;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

public class TilePAPI extends TileAngleLightNormal {
    enum PAPIMode {
        Auto(0),
        L(-0.056f),    //2°30′=0.0436
        LC(-0.028f),    //2°50 = 0.04887
        CR(0.028f),    //3°10′=0.05527
        R(0.056f);    //3°30′=0.06109
        private final double colorSwitchAngBase;

        PAPIMode(double ang) {
            this.colorSwitchAngBase = ang;
        }

        public double getColorSwitchAngBase(double glideSlopeAngRad) {
            return glideSlopeAngRad + this.colorSwitchAngBase;
        }

    }

    public final PAPIMode mode;
    public float glideSlopeAngDeg = 5f;
    public float glideSlopeAngRad = 0.087f;
    public float papiWideInterval = 3f;

    public TilePAPI() {
        this.mode = PAPIMode.R;
    }

    public TilePAPI(PAPIMode mode) {
        this.mode = mode;
    }

    public boolean setData(float glideSlopeAng, float papiWideInterval) {
        if (this.glideSlopeAngDeg != glideSlopeAng || this.papiWideInterval != papiWideInterval) {
            this.glideSlopeAngDeg = glideSlopeAng;
            this.glideSlopeAngRad = (float) Math.toRadians(this.glideSlopeAngDeg);
            this.papiWideInterval = papiWideInterval;
            return true;
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - papiWideInterval * 2,
                yCoord,
                zCoord - papiWideInterval * 2,
                xCoord + papiWideInterval * 2,
                yCoord + 2,
                zCoord + papiWideInterval * 2
        );
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setFloat("glideSlopeAng", glideSlopeAngDeg);
        p_145841_1_.setFloat("papiWideInterval", papiWideInterval);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        glideSlopeAngDeg = p_145839_1_.getFloat("glideSlopeAng");
        glideSlopeAngRad = (float) Math.toRadians(this.glideSlopeAngDeg);
        papiWideInterval = p_145839_1_.getFloat("papiWideInterval");
    }
}
