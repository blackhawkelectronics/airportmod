package airportlight.blocks.light.papi;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class PAPIGui extends GuiContainer {
    private static final int biTotalLineCount = 30;
    private static final int biLinePitch = 31;

    private TilePAPI tile;
    private int inputID;
    private StringInputGuiButton buttonGlideSlopeAng;
    private StringInputGuiButton buttonPapiWideInterval;

    private String scrapString;
    private float glideSlopeAng;
    private float papiWideInterval;

    public PAPIGui() {
        super(new ContainerAirPort());
    }

    public PAPIGui(TilePAPI tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.glideSlopeAng = tile.glideSlopeAngDeg;
        this.papiWideInterval = tile.papiWideInterval;
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        enterAndSend();
    }

    public void enterAndSend() {
        boolean isChanged = this.tile.setData(this.glideSlopeAng, this.papiWideInterval);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new PAPISync(this.tile, this.glideSlopeAng, this.papiWideInterval));
        }
    }

    @Override
    public void initGui() {
        buttonGlideSlopeAng = new StringInputGuiButton(biTotalLineCount, this.width / 2 - 45, this.height / 2 - 35, 80, 20, String.format("%.2f", tile.glideSlopeAngDeg));
        this.buttonList.add(buttonGlideSlopeAng);

        if (tile.mode == TilePAPI.PAPIMode.Auto) {
            buttonPapiWideInterval = new StringInputGuiButton(biLinePitch, this.width / 2 - 45, this.height / 2 - 5, 80, 20, String.format("%.2f", tile.papiWideInterval));
            this.buttonList.add(buttonPapiWideInterval);
        } else {
            papiWideInterval = -1;
        }
    }

    protected void actionPerformed(GuiButton button) {
        if (button.id == biTotalLineCount || button.id == biLinePitch) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        }
    }

    private void EnterSettings() {
        if (this.inputID == biTotalLineCount) {
            if (scrapString.isEmpty()) {
                this.buttonGlideSlopeAng.displayString = String.valueOf(tile.glideSlopeAngDeg);
            } else {
                this.buttonGlideSlopeAng.displayString = scrapString;
            }
            this.glideSlopeAng = Float.parseFloat(this.buttonGlideSlopeAng.displayString);
        } else if (this.inputID == biLinePitch) {
            if (scrapString.isEmpty()) {
                this.buttonPapiWideInterval.displayString = String.format("%.2f", tile.papiWideInterval);
            } else {
                this.buttonPapiWideInterval.displayString = String.format("%.2f", Float.parseFloat(scrapString));
            }
            this.papiWideInterval = Float.parseFloat(this.buttonPapiWideInterval.displayString);
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.fontRendererObj.drawString("Glide Slope Ang (Deg)  : ", this.width / 2 - 140, this.height / 2 - 30, -1, false);
        if (buttonPapiWideInterval != null) {
            this.fontRendererObj.drawString("PAPI Wide Interval (m) : ", this.width / 2 - 140, this.height / 2, -1, false);
        }
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biTotalLineCount) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.length() > 0) {
                    this.glideSlopeAng = Float.parseFloat(scrapString);
                } else {
                    this.glideSlopeAng = 5;
                    this.scrapString = "5";
                }
                enterAndSend();
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (scrapString.length() < 2 && Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonGlideSlopeAng.displayString = scrapString;
        } else if (inputID == biLinePitch) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.trim().length() > 0) {
                    this.papiWideInterval = Float.parseFloat(scrapString);
                    scrapString = String.format("%.2f", papiWideInterval);
                } else {
                    this.papiWideInterval = 15;
                    this.scrapString = "15";
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                int strLength = scrapString.length();
                boolean flag = false;
                if (strLength == 0 && Character.isDigit(eventChar)) {
                    flag = true;
                } else if (strLength == 1 && eventChar == '.') {
                    flag = true;
                } else if (strLength == 2 || strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true;
                    }
                }
                if (flag) {
                    scrapString += eventChar;
                }
            }
            this.buttonPapiWideInterval.displayString = scrapString;
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}

