package airportlight.blocks.light.papi;

import airportlight.ModAirPortLight;
import airportlight.blocks.light.papi.TilePAPI.PAPIMode;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.util.MathHelperAirPort;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class PAPIModel extends AngleLightModelBase<TilePAPI> {
    protected final ResourceLocation textureGray, textureRed, textureWhite;
    protected final IModelCustom model, modelFaaLight;

    public PAPIModel() {
        this.model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/papi_kiukiki.obj"));//papibody_kaka.obj"));
        this.modelFaaLight = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "papilight_kaka.obj"));
        this.textureGray = new ResourceLocation(ModAirPortLight.DOMAIN, "gray.png");
        this.textureRed = new ResourceLocation(ModAirPortLight.DOMAIN, "red.png");
        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");
    }

    @Override
    public void render(@NotNull TilePAPI tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-tile.getBaseAngle(), 0.0, 1.0, 0.0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureGray);
        EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
        double dx = tile.xCoord - player.posX + 0.5;
        double dy = tile.yCoord - player.posY;
        double dz = tile.zCoord - player.posZ + 0.5;
        double posAng = -Math.atan2(dz, dx);
        double lightAng = Math.toRadians(-tile.getBaseAngle() + 90);
        double dXZ = Math.hypot(dx, dz);
        double diffAng = posAng - lightAng;
        double angledDist = Math.cos(diffAng) * dXZ;
        double dist = Math.hypot(angledDist, dy);
        boolean useFarModel = dist > 30 && Math.abs(MathHelperAirPort.wrapAngleToPI_double(diffAng)) < (Math.PI / 4);
        if (tile.mode == PAPIMode.Auto) {
            GL11.glTranslatef(tile.papiWideInterval * -1.5f, 0, 0);
            ModelBodyRender();
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            ModelBodyRender();
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            ModelBodyRender();
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            ModelBodyRender();
            GL11.glTranslatef(tile.papiWideInterval * -3, 0, 0);
            renderAngleLight(player, (TilePAPI) tile, PAPIMode.L, dist, useFarModel);
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            renderAngleLight(player, (TilePAPI) tile, PAPIMode.LC, dist, useFarModel);
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            renderAngleLight(player, (TilePAPI) tile, PAPIMode.CR, dist, useFarModel);
            GL11.glTranslatef(tile.papiWideInterval, 0, 0);
            renderAngleLight(player, (TilePAPI) tile, PAPIMode.R, dist, useFarModel);
        } else {
            ModelBodyRender();
            renderAngleLight(player, (TilePAPI) tile, tile.mode, dist, useFarModel);
        }
        GL11.glPopMatrix();
    }

    @Override
    protected void ModelBodyRender() {
        if (DisplayListIDs.PAPIBody == -1) {
            DisplayListIDs.PAPIBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PAPIBody, GL11.GL_COMPILE);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGray);
            this.model.renderPart("body");
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PAPIBody);
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
    }

    protected void renderAngleLight(EntityPlayer player, TilePAPI tile, PAPIMode mode, double dist, boolean useFarModel) {
        double dy = player.posY - (tile.yCoord + 0.5);

        double distPitch = Math.atan2(dy, dist);
        if (distPitch <= mode.getColorSwitchAngBase(tile.glideSlopeAngRad)) {
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureRed);
        } else {
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
        }

        if (useFarModel) {
            dist = 1 + (dist - 30) / 7;
            double dist1 = 1 / dist;
            GL11.glTranslated(0, 0.45, 0.4986);
            GL11.glScaled(dist, dist, dist);
            drawLightFar();
            GL11.glScaled(dist1, dist1, dist1);
            GL11.glTranslated(0, -0.45, -0.4986);
        } else {
            drawLightNear();
        }
    }

    private void drawLightFar() {
        if (DisplayListIDs.PAPILightFar == -1) {
            DisplayListIDs.PAPILightFar = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PAPILightFar, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) this.modelFaaLight).groupObjects) {
                groupObject.render(tessellator);
            }
            tessellator.draw();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PAPILightFar);
    }

    private void drawLightNear() {
        if (DisplayListIDs.PAPILightNear == -1) {
            DisplayListIDs.PAPILightNear = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.PAPILightNear, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) this.model).groupObjects) {
                if (groupObject.name.equals("light")) {
                    groupObject.render(tessellator);
                }
            }
            tessellator.draw();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.PAPILightNear);
    }


}
