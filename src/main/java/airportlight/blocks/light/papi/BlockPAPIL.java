package airportlight.blocks.light.papi;

import airportlight.ModAirPortLight;
import airportlight.blocks.light.papi.TilePAPI.PAPIMode;
import airportlight.modcore.normal.TileAngleLightNormal;
import net.minecraft.world.World;

public class BlockPAPIL extends BlockPAPI{

    public BlockPAPIL() {
        super();
        setBlockTextureName(ModAirPortLight.DOMAIN + ":papil");
        setBlockName("PAPIL");
    }

    //region ブロック情報関係
    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TilePAPI(PAPIMode.L);
    }
}
