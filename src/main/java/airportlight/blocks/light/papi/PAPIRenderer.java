package airportlight.blocks.light.papi;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

@SideOnly(Side.CLIENT)
public class PAPIRenderer extends TileEntitySpecialRenderer {
	private final PAPIModel modelElClearBoard = new PAPIModel();

	@Override
	public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
		// モデルの表示
		if(tE instanceof TilePAPI){
            this.modelElClearBoard.render((TilePAPI) tE, x, y, z);
        }
	}

}