package airportlight.blocks.light.apronlighting.lightair

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.block.BlockAir
import net.minecraft.block.ITileEntityProvider
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import java.util.*

class BlockLightAir : BlockAir(), ITileEntityProvider {
    init {
        setLightLevel(1.0F)
        setBlockName("lightAir")
        isBlockContainer = true
        tickRandomly = true
    }

    override fun onBlockAdded(world: World, x: Int, y: Int, z: Int) {
        val light = world.getBlockMetadata(x, y, z)
        setLightLevel(light / 15.0f)
    }

    @SideOnly(Side.CLIENT)
    override fun randomDisplayTick(world: World, x: Int, y: Int, z: Int, random: Random) {
        val tile = world.getTileEntity(x, y, z)
        if (tile is TileLightAir) {
            tile.check()
        }
    }

    override fun updateTick(world: World, x: Int, y: Int, z: Int, rand: Random) {
        val tile = world.getTileEntity(x, y, z)
        if (tile is TileLightAir) {
            tile.check()
        }
    }

    override fun breakBlock(
        world: World,
        x: Int,
        y: Int,
        z: Int,
        block: Block?,
        meta: Int
    ) {
        super.breakBlock(world, x, y, z, block, meta)
        world.removeTileEntity(x, y, z)
    }

    override fun onBlockEventReceived(
        world: World,
        x: Int,
        y: Int,
        z: Int,
        p_149696_5_: Int,
        p_149696_6_: Int
    ): Boolean {
        super.onBlockEventReceived(world, x, y, z, p_149696_5_, p_149696_6_)
        val tileEntity = world.getTileEntity(x, y, z)
        return tileEntity?.receiveClientEvent(p_149696_5_, p_149696_6_) ?: false
    }

    override fun createNewTileEntity(p_149915_1_: World, p_149915_2_: Int): TileEntity {
        return TileLightAir()
    }
}