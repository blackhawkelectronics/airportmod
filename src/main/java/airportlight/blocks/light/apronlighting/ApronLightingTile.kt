package airportlight.blocks.light.apronlighting

import airportlight.blocks.light.apronlighting.lightair.TileLightAir
import airportlight.modcore.AirportEventManager
import airportlight.modcore.InstanceList
import airportlight.modcore.normal.TileAngleLightNormal
import airportlight.util.Vec3I
import net.minecraft.block.material.Material
import net.minecraft.init.Blocks
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.MathHelper
import java.util.*
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin
import kotlin.properties.Delegates

class ApronLightingTile : TileAngleLightNormal() {
    var lightBlocks = LinkedHashSet<Vec3I>()

    private var yawArray0: Array<Double> by Delegates.notNull()
    private var yawArray1: Array<Double> by Delegates.notNull()
    private var yawArrays: Array<Array<Double>> by Delegates.notNull()
    private var pitchArray: Array<Double> by Delegates.notNull()
    var phasePitch = -1
    var phaseYaw = -1

    override fun updateEntity() {
        if (phasePitch >= 0) {
            for (i in 0..1) {
                setLightBlocks(phasePitch, phaseYaw)
                phaseYaw++
                if (phaseYaw >= yawArrays[phasePitch].size) {
                    phaseYaw = 0
                    phasePitch++
                    if (phasePitch >= pitchArray.size) {
                        phasePitch = -1
                        phaseYaw = -1
                        break
                    }
                }
            }
        }
    }

    fun startSetLightBlocks(yaw: Double) {
        lightBlocks.clear()
        yawArray0 = Array<Double>(15) { yaw - 0.131 * 6 + 0.131 * it }
        yawArray1 = Array<Double>(25) { yaw - 0.066 * 12 + 0.066 * it }
        yawArrays = arrayOf(yawArray0, yawArray1, yawArray1)
        val pitch = Math.toRadians(-15.0)
        pitchArray = arrayOf(
            pitch,
            pitch + 0.087,
            pitch + 0.175
        )
        phasePitch = 0
        phaseYaw = 0
    }

    private fun setLightBlocks(phasePitch: Int, phaseYaw: Int) {
        val dPitch = pitchArray[phasePitch]
        val yawArray = yawArrays[phasePitch]
        val dYaw = yawArray[phaseYaw]
        val vxz = 1 * cos(dPitch)
        val vx = vxz * cos(dYaw)
        val vz = vxz * sin(dYaw)
        val vy = 1 * sin(dPitch)

        var dx = this.xCoord + 0.5
        var dy = this.yCoord + 0.5 + 16
        var dz = this.zCoord + 0.5

        var flagHit = false

        for (i in 120 downTo 5) {
            dx += vx
            dy += vy
            dz += vz

            val x = MathHelper.floor_double(dx)
            val y = MathHelper.floor_double(dy)
            val z = MathHelper.floor_double(dz)
            val xyz = Vec3I(x, y, z)

            if (!flagHit) {
                val block = worldObj.getBlock(x, y, z)
                if (block == Blocks.air) {
                    if (!lightBlocks.contains(xyz)) {
                        worldObj.setBlock(x, y, z, InstanceList.blockLightAir, min(i / 5, 13), 3)
                        val tile = worldObj.getTileEntity(x, y, z)
                        if (tile is TileLightAir) {
                            tile.parent = Vec3I(this.xCoord, this.yCoord, this.zCoord)
                        }
                        lightBlocks.add(xyz)
                    }
                } else if (block.material != Material.air) {
                    flagHit = true
                }
            }
        }
    }

    fun breakLights() {
//        for (xyz in lightBlocks) {
//            if (worldObj.getBlock(xyz.x, xyz.y, xyz.z) == InstanceList.blockLightAir) {
//                worldObj.setBlockToAir(xyz.x, xyz.y, xyz.z)
//            }
//        }
        val pair = Pair(Vec3I(this.xCoord, this.yCoord, this.zCoord), lightBlocks)
        AirportEventManager.removeLights.getOrDefault(this.worldObj, LinkedList()).add(pair)
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("lightBLocksSize", lightBlocks.size)
        for ((index, xyz) in lightBlocks.withIndex()) {
            nbt.setInteger("lightBlocks-${index}X", xyz.x)
            nbt.setInteger("lightBlocks-${index}Y", xyz.y)
            nbt.setInteger("lightBlocks-${index}Z", xyz.z)
        }
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        val size = nbt.getInteger("lightBLocksSize")
        for (index in 0 until size) {
            lightBlocks.add(
                Vec3I(
                    nbt.getInteger("lightBlocks-${index}X"),
                    nbt.getInteger("lightBlocks-${index}Y"),
                    nbt.getInteger("lightBlocks-${index}Z")
                )
            )
        }
    }
}
