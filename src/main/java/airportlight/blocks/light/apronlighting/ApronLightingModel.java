package airportlight.blocks.light.apronlighting;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormalLight;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class ApronLightingModel extends ModelBaseNormalLight<ApronLightingTile> {
    protected IModelCustom model;
    protected ResourceLocation textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/common_yukikaze.png");


    public ApronLightingModel() {
        super();
        model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/apronlighting_yukikaze.obj"));
    }

    /**
     * 通常と違う描画実装をするためここでは描画準備のみ
     */
    @Override
    protected void ModelBodyRender() {
        if (DisplayListIDs.ApronLightingBody == -1) {
            DisplayListIDs.ApronLightingBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ApronLightingBody, GL11.GL_COMPILE);
            model.renderOnly("body", "ramp.body");
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.ApronLightingBody);
    }

    @Override
    public void render(@NotNull ApronLightingTile tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-((ApronLightingTile) tile).getBaseAngle(), 0.0, 1.0, 0.0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureModel);
        ModelBodyRender();
        if (DisplayListIDs.ApronLightingRamp == -1) {
            DisplayListIDs.ApronLightingRamp = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ApronLightingRamp, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) this.model).groupObjects) {
                if (groupObject.name.equals("ramp.body.light")) {
                    groupObject.render(tessellator);
                }
            }
            tessellator.draw();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.ApronLightingRamp);
        GL11.glPopMatrix();
    }
}