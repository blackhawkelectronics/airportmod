package airportlight.blocks.light.runwaycenterlinelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.ReUseModelDataBank;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RunwayCenterLineLightModel extends AngleLightModelBase<TileRunwayCenterLineLight> implements IUseWeightModel {
    protected ResourceLocation textureRed = new ResourceLocation(ModAirPortLight.DOMAIN, "red.png");
    protected ResourceLocation textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");

    public RunwayCenterLineLightModel() {
        super();
        ReUseModelDataBank.ModelInit();
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        ReUseModelDataBank.instance.readModel(UseWeightModel);
    }

    @Override
    protected void ModelBodyRender() {
        ReUseModelDataBank.setDisplayList();
        GL11.glCallList(DisplayListIDs.CenterLineBody);
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        boolean lightON = worldLight < 0.5;
        if (dist > 8 && lightON) {
            dist = 1 + (dist - 8) / 10;
            double dist1 = 1 / dist;
            if (((TileRunwayCenterLineLight) tile).getLightModeNormal()) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureRed);
            }
            GL11.glScaled(dist, dist, dist);
            renderREDLON();
            GL11.glScaled(dist1, dist1, dist1);
        } else {
            if (((TileRunwayCenterLineLight) tile).getLightModeNormal()) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureRed);
            }
            if (lightON) {
                renderLightON_LED();
                GL11.glRotated(180, 0, 1, 0);
                renderLightON_LED();
            } else {
                renderLightOFF_LED();
                GL11.glRotated(180, 0, 1, 0);
                renderLightOFF_LED();
            }
        }
    }
}