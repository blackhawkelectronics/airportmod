package airportlight.blocks.light.runwaycenterlinelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

@SideOnly(Side.CLIENT)
public class RunwayCenterLineLightRenderer extends TileEntitySpecialRenderer {
    private final RunwayCenterLineLightModel modelElClearBoard = ModelSwitcherDataBank.registerModelClass(new RunwayCenterLineLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileRunwayCenterLineLight) {
            this.modelElClearBoard.render((TileRunwayCenterLineLight) tE, x, y, z);
        }
    }

}