package airportlight.blocks.light.runwaydistancemarkerlight;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import org.jetbrains.annotations.NotNull;

public class TileRunwayDistanceMarkerLight extends TileAngleLightNormal {
    int numL = 1;
    int numR = 9;

    public TileRunwayDistanceMarkerLight() {
    }

    public int getNumL() {
        return this.numL;
    }

    public int getNumR() {
        return this.numR;
    }

    public boolean setDatas(int textL, int textR) {
        boolean isChange = false;
        if (this.numL != textL || this.numR != textR) {
            this.numL = textL;
            this.numR = textR;
            isChange = true;
        }
        return isChange;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord,
                yCoord,
                zCoord,
                xCoord + 1,
                yCoord + 2,
                zCoord + 1
        );
    }

    @Override
    public void writeToNBT(@NotNull NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setInteger("numL", this.numL);
        p_145841_1_.setInteger("numR", this.numR);
    }

    @Override
    public void readFromNBT(@NotNull NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.numL = p_145839_1_.getInteger("numL");
        this.numR = p_145839_1_.getInteger("numR");
    }
}
