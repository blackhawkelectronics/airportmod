package airportlight.blocks.light.runwaydistancemarkerlight;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.StatCollector;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class RunwayDistanceMarkerGui extends GuiContainer {
    private final int biNumL = 30;
    private final int biNumR = 40;

    private TileRunwayDistanceMarkerLight tile;
    private int inputID;
    private StringInputGuiButton buttonNumL;
    private StringInputGuiButton buttonNumR;

    private int tempNumR;
    private int tempNumL;

    private int scrapNum = 0;

    public RunwayDistanceMarkerGui() {
        super(new ContainerAirPort());
    }

    public RunwayDistanceMarkerGui(TileRunwayDistanceMarkerLight tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempNumR = tile.getNumR();
        this.tempNumL = tile.getNumL();
    }

    @Override
    public void onGuiClosed() {
        enterSettings();
        enterAndSend();
    }

    @Override
    public void initGui() {
        buttonNumL = new StringInputGuiButton(biNumL, this.width / 3 - 20, this.height / 2 + 10, 40, 20, String.valueOf(this.tempNumL));
        buttonNumR = new StringInputGuiButton(biNumR, (int) (this.width * 0.667 - 20), this.height / 2 + 10, 40, 20, String.valueOf(this.tempNumR));

        this.buttonList.add(buttonNumL);
        this.buttonList.add(buttonNumR);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case biNumL:
            case biNumR:
                enterSettings();
                this.inputID = button.id;
                this.scrapNum = 0;
                button.displayString = "0";
                break;

        }
    }

    private void enterSettings() {
        if (this.inputID == biNumL || this.inputID == biNumR) {
            if (scrapNum == 0) {
                if (this.inputID == biNumL) {
                    this.buttonNumL.displayString = String.valueOf(tile.numL);
                } else {
                    this.buttonNumR.displayString = String.valueOf(tile.numR);
                }
            } else {
                if (this.inputID == biNumL) {
                    this.buttonNumL.displayString = String.valueOf(scrapNum);
                } else {
                    this.buttonNumR.displayString = String.valueOf(scrapNum);
                }
            }

            this.tempNumR = Integer.parseInt(this.buttonNumR.displayString);
            this.tempNumL = Integer.parseInt(this.buttonNumL.displayString);
        }
    }

    public void enterAndSend() {
        boolean isChanged = this.tile.setDatas(this.tempNumL, this.tempNumR);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new RunwayDistanceMarkerSync(this.tile, this.tempNumL, this.tempNumR));
        }
    }

    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        GL11.glPushMatrix();
        String name = StatCollector.translateToLocal("tile.RunwayDistanceMarkerLight.name");
        this.fontRendererObj.drawString(name, this.width / 2 - fontRendererObj.getStringWidth(name) / 2, this.height / 2 - 30, -1, false);
        this.fontRendererObj.drawString("Left", this.width / 3 - 10, this.height / 2, -1, false);
        this.fontRendererObj.drawString("Right", (int) (this.width * 0.667 - 10), this.height / 2, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biNumL || inputID == biNumR) {
            if (keyID == Keyboard.KEY_RETURN) {
                if (inputID == biNumL) {
                    this.tempNumL = scrapNum;
                } else {
                    this.tempNumR = scrapNum;
                }
                this.inputID = 0;
                enterAndSend();
            } else if (keyID == Keyboard.KEY_BACK) {
                scrapNum = 0;
            } else {
                if (Character.isLetter(eventChar) || Character.isDigit(eventChar)) {
                    scrapNum = eventChar - '0';
                }
            }
            if (inputID == biNumL) {
                this.buttonNumL.displayString = String.valueOf(scrapNum);
            } else {
                this.buttonNumR.displayString = String.valueOf(scrapNum);
            }
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
