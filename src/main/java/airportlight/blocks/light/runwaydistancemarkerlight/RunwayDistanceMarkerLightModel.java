package airportlight.blocks.light.runwaydistancemarkerlight;

import airportlight.ModAirPortLight;
import airportlight.font.fontobj.FontModel;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

public class RunwayDistanceMarkerLightModel extends AngleLightModelBase<TileRunwayDistanceMarkerLight> {
    protected IModelCustom model;
    protected final ResourceLocation textureModel;
    protected final ResourceLocation textureWhite;

    public RunwayDistanceMarkerLightModel() {
        super();
        this.model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwaydistancemarkerlight.obj"));
        this.textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "orange-black.png");
        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");
    }

    @Override
    protected void ModelBodyRender() {
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureModel);
        if (DisplayListIDs.RunwayDistanceMarkerLight == -1) {
            DisplayListIDs.RunwayDistanceMarkerLight = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.RunwayDistanceMarkerLight, GL11.GL_COMPILE);
            model.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.RunwayDistanceMarkerLight);
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureWhite);
        boolean lightON = worldLight < 0.5;
        double scale = 2;
        double scale1 = 1 / scale;
        GL11.glTranslated(0.25, 1.25, 0.15);
        GL11.glScaled(scale, scale, scale);
        GL11.glRotated(60, 0, 1, 0);
        if (lightON) {
            FontModel.render(((TileRunwayDistanceMarkerLight) tile).numR, 240);
        } else {
            FontModel.render(((TileRunwayDistanceMarkerLight) tile).numR);
        }
        GL11.glRotated(-60, 0, 1, 0);
        GL11.glTranslated(-0.25, 0, 0);
        GL11.glRotated(60, 0, -1, 0);
        if (lightON) {
            FontModel.render(((TileRunwayDistanceMarkerLight) tile).numL, 240);
        } else {
            FontModel.render(((TileRunwayDistanceMarkerLight) tile).numL);
        }
        GL11.glRotated(-60, 0, -1, 0);
        GL11.glScaled(scale1, scale1, scale1);
        GL11.glTranslated(-0.25, -1.25, -0.15);
    }
}
