package airportlight.blocks.light.runwaydistancemarkerlight;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class RunwayDistanceMarkerSync extends TileEntityMessage implements IMessageHandler<RunwayDistanceMarkerSync, IMessage> {
    int numL, numR;

    public RunwayDistanceMarkerSync() {
        super();
    }

    public RunwayDistanceMarkerSync(TileRunwayDistanceMarkerLight tile, int numL, int numR) {
        super(tile);
        this.numL = numL;
        this.numR = numR;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.numL = tag.getInteger("numL");
        this.numR = tag.getInteger("numR");
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("numL", this.numL);
        tag.setInteger("numR", this.numR);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(RunwayDistanceMarkerSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileRunwayDistanceMarkerLight) {
            ((TileRunwayDistanceMarkerLight) tile).setDatas(message.numL, message.numR);
            if (ctx.side.isServer()) {
                tile.markDirty();
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
