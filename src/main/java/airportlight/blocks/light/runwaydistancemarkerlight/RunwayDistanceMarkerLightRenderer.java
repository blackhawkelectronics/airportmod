package airportlight.blocks.light.runwaydistancemarkerlight;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class RunwayDistanceMarkerLightRenderer extends TileEntitySpecialRenderer {
    private final RunwayDistanceMarkerLightModel model = new RunwayDistanceMarkerLightModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof TileRunwayDistanceMarkerLight) {
            this.model.render((TileRunwayDistanceMarkerLight) tile, x, y, z);
        }
    }

}
