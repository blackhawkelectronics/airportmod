package airportlight.blocks.light.overrunareaedgelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class OverrunAreaEdgeLightRenderer extends TileEntitySpecialRenderer {
    private final OverrunAreaEdgeLightModel model = ModelSwitcherDataBank.registerModelClass(new OverrunAreaEdgeLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileOverrunAreaEdgeLight) {
            this.model.render((TileOverrunAreaEdgeLight) tE, x, y, z);
        }
    }
}