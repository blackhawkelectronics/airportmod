package airportlight.blocks.light.endlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockEndLight extends BlockAngleLightNormal {
    public BlockEndLight() {
        super();
        setBlockName("EndLight");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":end");
        setBlockBounds(0F, 0F, 0F, 1F, 0.75F, 1F);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ + 0.2, p_149633_3_, (double) p_149633_4_ + 0.2, (double) p_149633_2_ + 0.8, (double) p_149633_3_ + 0.2, (double) p_149633_4_ + 0.8);
    }

    @Override
    public TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TileEndLight();
    }
}
