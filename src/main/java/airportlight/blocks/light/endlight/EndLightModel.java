package airportlight.blocks.light.endlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.util.IUseWeightModel;
import airportlight.util.MathHelperAirPort;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class EndLightModel extends AngleLightModelBase<TileEndLight> implements IUseWeightModel {
    protected IModelCustom modelBase;
    protected ResourceLocation textureGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "green.png");
    protected ResourceLocation textureRed = new ResourceLocation(ModAirPortLight.DOMAIN, "red.png");
    protected ResourceLocation textureDarkGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "darkgreen.png");
    protected ResourceLocation textureDarkRed = new ResourceLocation(ModAirPortLight.DOMAIN, "darkred.png");
    protected ResourceLocation textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");


    public EndLightModel() {
        super();
        IModelCustom model;
        if (APMConfig.UseWeightModel) {
            model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwayendlights.obj"));
        } else {
            model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightrunwayendlight.obj"));
        }
        this.modelBase = model;
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        IModelCustom model;
        if (UseWeightModel) {
            model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwayendlights.obj"));
        } else {
            model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightrunwayendlight.obj"));
        }
        this.modelBase = model;
    }

    @Override
    protected void ModelBodyRender() {
        modelBase.renderAll();
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        boolean lightON = worldLight < 0.5;
        if (dist > 8 && lightON) {
            dist = 1 + (dist - 8) / 6;

            playerAng = MathHelperAirPort.wrapAngleTo360_double(playerAng);

            double dist1 = 1 / dist;
            if (Math.abs(MathHelper.wrapAngleTo180_double(MathHelperAirPort.wrapAngleTo360_double(tile.getBaseAngle()) - playerAng)) < 45) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
                GL11.glScaled(dist, dist, dist);
                render(240, this.modelREDL);
                GL11.glScaled(dist1, dist1, dist1);
            } else if (Math.abs(MathHelper.wrapAngleTo180_double(MathHelperAirPort.wrapAngleTo360_double(tile.getBackAngle()) - playerAng)) < 45) {
                GL11.glRotated(180, 0, 1, 0);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureRed);
                GL11.glScaled(dist, dist, dist);
                render(240, this.modelREDL);
                GL11.glScaled(dist1, dist1, dist1);
            }
        } else {

            GL11.glTranslated(0, 0, 0.1);
            if (lightON) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureRed);
                render(150, this.modelREDL);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureDarkRed);
                this.modelREDL.renderAll();
            }
            GL11.glTranslated(0, 0, -0.1);


            GL11.glRotated(180, 0, 1, 0);


            if (lightON) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
                GL11.glTranslated(0.1, 0, 0.07);
                render(240, this.modelREDL);
                GL11.glTranslated(-0.2, 0, 0);
                render(240, this.modelREDL);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureDarkGreen);
                GL11.glTranslated(0.1, 0, 0.07);
                this.modelREDL.renderAll();
                GL11.glTranslated(-0.2, 0, 0);
                this.modelREDL.renderAll();
            }
            GL11.glTranslated(0.1, 0, -0.07);


            if (false) {//!lightON) { //TODO 使用する滑走路の方向によって灯火の方向を変更できるようにしたとき開放する。
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureDarkGreen);
                GL11.glTranslated(0.1, 0, 0.07);
                this.modelREDL.renderAll();
                GL11.glTranslated(-0.2, 0, 0);
                this.modelREDL.renderAll();
                GL11.glTranslated(0.1, 0, -0.07);
                GL11.glRotated(180, 0, 1, 0);
                GL11.glTranslated(0, 0, 0.1);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureDarkRed);
                this.modelREDL.renderAll();
                GL11.glTranslated(0, 0, -0.1);
            }
        }
    }
}