package airportlight.blocks.light.endlight;

import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

@SideOnly(Side.CLIENT)
public class EndLightRenderer extends TileEntitySpecialRenderer {
	private final EndLightModel modelElClearBoard = ModelSwitcherDataBank.registerModelClass(new EndLightModel());

	@Override
	public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
		// モデルの表示
		if (tE instanceof TileEndLight) {
			this.modelElClearBoard.render((TileEndLight) tE, x, y, z);
		}
	}

}