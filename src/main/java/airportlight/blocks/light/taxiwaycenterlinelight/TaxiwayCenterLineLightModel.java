package airportlight.blocks.light.taxiwaycenterlinelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.ReUseModelDataBank;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class TaxiwayCenterLineLightModel extends AngleLightModelBase<TileTaxiwayCenterLineLight> implements IUseWeightModel {
    protected ResourceLocation textureGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "green.png");
    protected ResourceLocation textureYellow = new ResourceLocation(ModAirPortLight.DOMAIN, "yellow.png");
    protected ResourceLocation textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");

    public TaxiwayCenterLineLightModel() {
        super();
        ReUseModelDataBank.ModelInit();
    }


    @Override
    public void readModel(boolean UseWeightModel) {
        ReUseModelDataBank.reloadModels(UseWeightModel);
    }

    @Override
    protected void ModelBodyRender() {
        ReUseModelDataBank.setDisplayList();
        GL11.glCallList(DisplayListIDs.CenterLineBody);
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        boolean lightON = worldLight < 0.5;
        if (dist > 8 && lightON) {
            dist = 1 + (dist - 8) / 10;
            double dist1 = 1 / dist;
            if (((TileTaxiwayCenterLineLight) tile).getLightModeNormal()) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureYellow);
            }
            GL11.glScaled(dist, dist, dist);
            renderREDLON();
            GL11.glScaled(dist1, dist1, dist1);
        } else {
            if (((TileTaxiwayCenterLineLight) tile).getLightModeNormal()) {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureGreen);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureYellow);
            }
            if (lightON) {
                renderLightON_LED();
                GL11.glRotated(180, 0, 1, 0);
                renderLightON_LED();
            } else {
                renderLightOFF_LED();
                GL11.glRotated(180, 0, 1, 0);
                renderLightOFF_LED();
            }
        }
    }
}
