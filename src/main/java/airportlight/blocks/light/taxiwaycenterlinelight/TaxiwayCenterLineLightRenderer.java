package airportlight.blocks.light.taxiwaycenterlinelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class TaxiwayCenterLineLightRenderer extends TileEntitySpecialRenderer {
    private final TaxiwayCenterLineLightModel model = ModelSwitcherDataBank.registerModelClass(new TaxiwayCenterLineLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileTaxiwayCenterLineLight) {
            this.model.render((TileTaxiwayCenterLineLight) tE, x, y, z);
        }
    }

}