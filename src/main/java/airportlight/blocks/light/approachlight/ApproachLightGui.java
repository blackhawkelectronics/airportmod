package airportlight.blocks.light.approachlight;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class ApproachLightGui extends GuiContainer {
    private static final int biTotalLineCount = 30;
    private static final int biLinePitch = 31;

    private ApproachLightTile tile;
    private int inputID;
    private StringInputGuiButton buttonLineCount;
    private StringInputGuiButton buttonLinePitch;

    private String scrapString;
    private int tempLineCount;
    private float tempLinePitch;

    public ApproachLightGui() {
        super(new ContainerAirPort());
    }

    public ApproachLightGui(ApproachLightTile tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempLineCount = tile.lineCount;
        this.tempLinePitch = tile.linePitch;
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        boolean isChanged = this.tile.setDatas(this.tempLineCount, this.tempLinePitch);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new ApproachLightSync(this.tile, this.tempLineCount, this.tempLinePitch));
        }
    }


    @Override
    public void initGui() {
        buttonLineCount = new StringInputGuiButton(biTotalLineCount, this.width / 2 - 70, this.height / 2 - 35, String.valueOf(tile.lineCount));
        buttonLinePitch = new StringInputGuiButton(biLinePitch, this.width / 2 - 70, this.height / 2 - 5, 80, 20, String.format("%.2f", tile.linePitch));

        this.buttonList.add(buttonLineCount);
        this.buttonList.add(buttonLinePitch);
    }

    protected void actionPerformed(GuiButton button) {
        if (button.id == biTotalLineCount || button.id == biLinePitch) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        }
    }

    private void EnterSettings() {
        if (this.inputID == biTotalLineCount) {
            if (scrapString.isEmpty()) {
                this.buttonLineCount.displayString = String.valueOf(tile.lineCount);
            } else {
                this.buttonLineCount.displayString = scrapString;
            }
            this.tempLineCount = Integer.parseInt(this.buttonLineCount.displayString);
        } else if (this.inputID == biLinePitch) {
            if (scrapString.isEmpty()) {
                this.buttonLinePitch.displayString = String.format("%.2f", tile.linePitch);
            } else {
                this.buttonLinePitch.displayString = String.format("%.2f", Float.parseFloat(scrapString));
            }
            this.tempLinePitch = Float.parseFloat(this.buttonLinePitch.displayString);
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.fontRendererObj.drawString("lineCount  : ", this.width / 2 - 140, this.height / 2 - 30, -1, false);
        this.fontRendererObj.drawString("linePitch  : ", this.width / 2 - 140, this.height / 2, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biTotalLineCount) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.length() > 0) {
                    this.tempLineCount = Integer.parseInt(scrapString);
                } else {
                    this.tempLineCount = 5;
                    this.scrapString = "5";
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (scrapString.length() < 2 && Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonLineCount.displayString = scrapString;
        } else if (inputID == biLinePitch) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.trim().length() > 0) {
                    this.tempLinePitch = Float.parseFloat(scrapString);
                    scrapString = String.format("%.2f", tempLinePitch);
                } else {
                    this.tempLinePitch = 15;
                    this.scrapString = "15";
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                int strLength = scrapString.length();
                boolean flag = false;
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true;
                } else if (strLength <= 2 && !scrapString.endsWith(".") && eventChar == '.') {
                    flag = true;
                } else if (scrapString.contains(".") && (strLength - scrapString.indexOf(".")) < 3 && strLength < 5) {
                    if (Character.isDigit(eventChar)) {
                        flag = true;
                    }
                }
                if (flag) {
                    scrapString += eventChar;
                }
            }
            this.buttonLinePitch.displayString = scrapString;
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}

