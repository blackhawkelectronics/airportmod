package airportlight.blocks.light.approachlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;

public class ApproachLightModel extends AngleLightModelBase<ApproachLightTile> {
    protected GroupObject baseModel;
    protected GroupObject lightModel;
    protected GroupObject flashlightModel;
    protected ResourceLocation textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/aproachlight_yukikaze.png");
    protected ResourceLocation textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");


    public ApproachLightModel() {
        super();
        IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/approachlight_yukikaze.obj"));

        for (GroupObject groupObject : ((WavefrontObject) model).groupObjects) {
            if ("base".equalsIgnoreCase(groupObject.name)) {
                baseModel = groupObject;
            } else if ("light".equalsIgnoreCase(groupObject.name)) {
                lightModel = groupObject;
            } else if ("flashlight".equalsIgnoreCase(groupObject.name)) {
                flashlightModel = groupObject;
            }
        }
    }

    /**
     * 通常と違う描画実装をするためここでは描画準備のみ
     */
    @Override
    protected void ModelBodyRender() {
        if (DisplayListIDs.ApproachLightBody == -1) {
            DisplayListIDs.ApproachLightBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ApproachLightBody, GL11.GL_COMPILE);
            baseModel.render();
            GL11.glEndList();
        }
    }

    private void renderLightWithScale(double dist, double dist1, int light) {
        GL11.glScaled(dist, dist, dist);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(GL11.GL_TRIANGLES);
        tessellator.setBrightness(240);
        tessellator.setColorRGBA(255, 255, 255, light);
        lightModel.render(tessellator);
        tessellator.draw();
        GL11.glScaled(dist1, dist1, dist1);
    }

    private void renderflashLightWithScale(double dist, double dist1, int light) {
        GL11.glScaled(dist, dist, dist);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(GL11.GL_TRIANGLES);
        tessellator.setBrightness(light);
        tessellator.setColorRGBA(255, 255, 255, light);
        flashlightModel.render(tessellator);
        tessellator.draw();
        GL11.glScaled(dist1, dist1, dist1);
    }


    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        GL11.glPushMatrix();
        boolean lightON = worldLight < 0.5;
        double playerDist = dist;
        double playerPosAng = playerAng;
        ApproachLightTile alTile = (ApproachLightTile) tile;
        for (int num = alTile.lineCount - 1; num >= 0; num--) {
            Vec3 linepos = Vec3.createVectorHelper(0, 0, alTile.linePitch * (alTile.lineCount - num - 1));
            linepos.rotateAroundY((float) Math.toRadians(alTile.getBaseAngle()));
            linepos = linepos.addVector(tile.xCoord + 0.5, tile.yCoord, tile.zCoord + 0.5);

            double dx = linepos.xCoord - player.posX + 0.5;
            double dy = linepos.yCoord - player.posY;
            double dz = linepos.zCoord - player.posZ + 0.5;
            playerDist = Math.sqrt(dx * dx + dy * dy + dz * dz);
            playerPosAng = -Math.toDegrees(Math.atan2(dx, dz));

            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureModel);
            GL11.glCallList(DisplayListIDs.ApproachLightBody);

            GL11.glPushMatrix();
            if (playerDist > 8 && lightON && Math.abs(MathHelper.wrapAngleTo180_double(tile.getBackAngle()) - playerPosAng) < 45) {
                long systemTime = System.currentTimeMillis();

                GL11.glTranslatef(0, 2.16f, 0);
                double lightScale = 1 + (playerDist - 10) / 10;
                double lightScale1 = 1 / lightScale;
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);

                renderLightWithScale(lightScale, lightScale1, 240);//C

                GL11.glTranslatef(1, 0, 0);
                renderLightWithScale(lightScale, lightScale1, 240);//CR

                GL11.glTranslatef(1, 0, 0);
                renderLightWithScale(lightScale, lightScale1, 240);//R

                GL11.glTranslatef(-3, 0, 0);
                renderLightWithScale(lightScale, lightScale1, 240);//LC

                GL11.glTranslatef(-1, 0, 0);
                renderLightWithScale(lightScale, lightScale1, 240);//L

                GL11.glTranslatef(1.5f, 0.04f, 0);

                int light = alTile.lightValue(systemTime, num);
                lightScale = 1 + (playerDist - 8) / (18 - light / 19.0);
                lightScale1 = 1 / lightScale;
                light = light / 2 + 120;
                flashlightModel.render();
                renderflashLightWithScale(lightScale, lightScale1, light);
            } else {
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
                if (lightON) {
                    long systemTime = System.currentTimeMillis();

                    GL11.glTranslatef(0, 2.16f, 0);
                    FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);

                    renderLightWithScale(1, 1, 240);//C

                    GL11.glTranslatef(1, 0, 0);
                    renderLightWithScale(1, 1, 240);//CR

                    GL11.glTranslatef(1, 0, 0);
                    renderLightWithScale(1, 1, 240);//R

                    GL11.glTranslatef(-3, 0, 0);
                    renderLightWithScale(1, 1, 240);//LC

                    GL11.glTranslatef(-1, 0, 0);
                    renderLightWithScale(1, 1, 240);//L

                    GL11.glTranslatef(1.5f, 0.04f, 0);

                    int light = alTile.lightValue(systemTime, num);
                    renderflashLightWithScale(1, 1, light);
                } else {
                    GL11.glTranslatef(0, 2.16f, 0);
                    lightModel.render();//C

                    GL11.glTranslatef(1, 0, 0);
                    lightModel.render();//CR

                    GL11.glTranslatef(1, 0, 0);
                    lightModel.render();//R

                    GL11.glTranslatef(-3, 0, 0);
                    lightModel.render();//LC

                    GL11.glTranslatef(-1, 0, 0);
                    lightModel.render();//L

                    GL11.glTranslatef(1.5f, 0.04f, 0);
                    flashlightModel.render();
                }
            }
            GL11.glPopMatrix();
            GL11.glTranslatef(0, 0, ((ApproachLightTile) tile).linePitch);
        }
        GL11.glPopMatrix();
    }
}