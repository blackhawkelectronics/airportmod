package airportlight.blocks.light.obstaclelight;

import airportlight.modcore.normal.TileNormal;
import net.minecraft.nbt.NBTTagCompound;
import org.jetbrains.annotations.NotNull;

public class TileObstacleLight extends TileNormal {
    private int light = 0;
    private int cnt = 0;
    public EnumObstacleLightColorSet lightMode = EnumObstacleLightColorSet.MediumBlinkingRed;

    public TileObstacleLight() {
    }

    public void updateEntity() {
        if (this.worldObj.getSunBrightnessFactor(1.0F) < 0.5) {
            light = lightMode.getLightModel().getLight(cnt, light);
            cnt++;
            if (cnt >= lightMode.getMaxCnt()) {
                cnt = 0;
            }
        } else {
            cnt = 0;
            light = 0;
        }
    }

    public int getLight() {
        return light;
    }

    public boolean setDatas(EnumObstacleLightColorSet lightMode) {
        boolean isChange = false;
        if (this.lightMode != lightMode) {
            this.lightMode = lightMode;
            isChange = true;
        }
        return isChange;
    }


    @Override
    public void writeToNBT(@NotNull NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setInteger("lightMode", this.lightMode.getMode());
    }

    @Override
    public void readFromNBT(@NotNull NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.lightMode = EnumObstacleLightColorSet.Companion.getFromMode(p_145839_1_.getInteger("lightMode"));
    }
}
