package airportlight.blocks.light.obstaclelight;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.normal.ModelBaseNormal;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import java.nio.FloatBuffer;

public class ObstacleLightModel extends ModelBaseNormal<TileObstacleLight> implements IUseWeightModel {
    protected final ResourceLocation textureRed;
    protected GroupObject modelBody = null, modelLight = null;

    public ObstacleLightModel() {
        IModelCustom useModel;
        if (APMConfig.UseWeightModel) {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/obstaclelight.obj"));
        } else {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightobstaclelight.obj"));
        }

        for (GroupObject groupObject : ((WavefrontObject) useModel).groupObjects) {
            if (groupObject.name.equals("Body")) {
                modelBody = groupObject;
            } else if (groupObject.name.equals("Light")) {
                modelLight = groupObject;
            }
        }

        this.textureRed = new ResourceLocation(ModAirPortLight.DOMAIN, "red.png");
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        IModelCustom useModel;
        if (UseWeightModel) {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/obstaclelight.obj"));
        } else {
            useModel = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightobstaclelight.obj"));
        }
        for (GroupObject groupObject : ((WavefrontObject) useModel).groupObjects) {
            if (groupObject.name.equals("Body")) {
                modelBody = groupObject;
            } else if (groupObject.name.equals("Light")) {
                modelLight = groupObject;
            }
        }
    }

    private static FloatBuffer toFloatBuffer(float[] values) {
        FloatBuffer floats = BufferUtils.createFloatBuffer(4).put(values);
        floats.flip();
        return floats;
    }

    @Override
    public void render(@NotNull TileObstacleLight tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(tile.lightMode.getColorTexture());
        if (DisplayListIDs.ObstacleBody == -1) {
            DisplayListIDs.ObstacleBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ObstacleBody, GL11.GL_COMPILE);
            this.modelBody.render();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.ObstacleBody);
        GL11.glTranslated(0, 0.19, 0);

        EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
        float worldLight = tile.getWorldObj().getSunBrightnessFactor(1.0F);

        if (worldLight < 0.5) {
            double dx = tile.xCoord - player.posX + 0.5;
            double dy = tile.yCoord - player.posY;
            double dz = tile.zCoord - player.posZ + 0.5;
            double dist = Math.sqrt(dx * dx + dy * dy + dz * dz);
            if (dist > 10) {
                dist = 1 + (dist - 10) / 100 * Math.log(tile.getLight());
                double dist1 = 1 / dist;

                this.modelLight.render();
                GL11.glScaled(dist, dist, dist);

                GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{0.25f, 0.25f, 0.25f, 1f}));
                renderHeadLightON(tile);

                GL11.glScaled(dist1, dist1, dist1);
            } else {
                GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{0, 0, 0, 1}));
                renderHeadLightON(tile);
            }
        } else {
            if (DisplayListIDs.ObstacleLightOFF == -1) {
                DisplayListIDs.ObstacleLightOFF = GL11.glGenLists(1);
                GL11.glNewList(DisplayListIDs.ObstacleLightOFF, GL11.GL_COMPILE);
                this.modelLight.render();
                GL11.glEndList();
            }
            GL11.glCallList(DisplayListIDs.ObstacleLightOFF);
        }
        GL11.glTranslated(0, -0.1834, 0);
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{0, 0, 0, 1}));
        GL11.glPopMatrix();
    }

    private void renderHeadLightON(TileObstacleLight tile) {
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(tile.lightMode.getColorTexture());
        if (DisplayListIDs.ObstacleLightOFF == -1) {
            DisplayListIDs.ObstacleLightOFF = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ObstacleLightOFF, GL11.GL_COMPILE);
            this.modelLight.render();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.ObstacleLightOFF);
        render(tile.getLight(), this.modelLight);
    }

    public void render(int light, GroupObject obj) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(GL11.GL_TRIANGLES);
        tessellator.setBrightness(light);
        tessellator.setColorRGBA(255, 255, 255, light);
        obj.render(tessellator);
        tessellator.draw();
    }
}
