package airportlight.blocks.light.obstaclelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class ObstacleLightRenderer extends TileEntitySpecialRenderer {
    private final ObstacleLightModel model = ModelSwitcherDataBank.registerModelClass(new ObstacleLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileObstacleLight) {
            this.model.render((TileObstacleLight) tE, x, y, z);
        }
    }

}