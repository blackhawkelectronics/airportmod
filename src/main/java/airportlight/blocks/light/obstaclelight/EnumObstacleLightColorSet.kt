package airportlight.blocks.light.obstaclelight

import airportlight.ModAirPortLight
import net.minecraft.util.ResourceLocation
import kotlin.math.max
import kotlin.math.min

/**
 * @param mode EnumID
 * @param maxCnt Max Light Cnt
 * @param lightModel Nou Light Value
 */
enum class EnumObstacleLightColorSet(val mode: Int, val maxCnt: Int, val colorTexture: ResourceLocation, val lightModel: LightModel) {
    LowConstantRed(0, 0, ResourceLocation(ModAirPortLight.DOMAIN, "red.png"), object : LightModel {
        override fun getLight(cnt: Int, nowLight: Int): Int {
            return 150
        }
    }),
    MediumBlinkingRed(1, 40, ResourceLocation(ModAirPortLight.DOMAIN, "red.png"), object : LightModel {
        override fun getLight(cnt: Int, nowLight: Int): Int {
            val newLight: Int = if (cnt <= 20) {
                200
            } else if (cnt < 30) {
                nowLight - 13
            } else if (cnt < 40) {
                nowLight + 43
            } else {
                200
            }
            return max(0, min(newLight, 183))
        }
    }),
    HighFlashingWhite(2, 20, ResourceLocation(ModAirPortLight.DOMAIN, "white.png"), object : LightModel {
        override fun getLight(cnt: Int, nowLight: Int): Int {
            val newLight: Int = if (cnt <= 14) {
                0
            } else if (cnt < 16) {
                nowLight + 120
            } else if (cnt < 18) {
                240
            } else if (cnt < 20) {
                nowLight - 120
            } else {
                0
            }
            return max(0, min(newLight, 240))
        }
    }),

    ;

    interface LightModel {
        /**
         * @param cnt Now Light Cnt
         * @param nowLight Nou Light Value
         * @return New Light Value
         */
        fun getLight(cnt: Int, nowLight: Int): Int
    }

    companion object {
        fun getFromMode(mode: Int): EnumObstacleLightColorSet {
            return EnumObstacleLightColorSet.values()[mode]
        }
    }
}
