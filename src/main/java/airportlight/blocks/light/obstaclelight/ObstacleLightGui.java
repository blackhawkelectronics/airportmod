package airportlight.blocks.light.obstaclelight;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.EnumSimpleButton;
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListVertical;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;

public class ObstacleLightGui extends GuiContainer {
    private static final int biColor = 40;
    private TileObstacleLight tile;
    private EnumSimpleButtonListVertical<EnumObstacleLightColorSet> buttonColors;

    private EnumObstacleLightColorSet tempLightMode;

    public ObstacleLightGui() {
        super(new ContainerAirPort());
    }

    public ObstacleLightGui(TileObstacleLight tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempLightMode = tile.lightMode;
    }

    @Override
    public void onGuiClosed() {
        enterAndSend();
    }


    public void enterAndSend() {
        boolean isChanged = this.tile.setDatas(this.tempLightMode);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new ObstacleLightSync(this.tile));
        }
    }

    @Override
    public void initGui() {
        buttonColors = new EnumSimpleButtonListVertical<>(biColor, this.width / 2 - 70, this.height / 2 - 25, EnumObstacleLightColorSet.class, 120,
                enumObstacleLightColorSet -> StatCollector.translateToLocal("enumObstacleLightColorSet." + enumObstacleLightColorSet.name())
        );
        buttonColors.selectButton(tempLightMode);
        buttonList.addAll(buttonColors.getButtonList());
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == biColor) {
            EnumSimpleButton<EnumObstacleLightColorSet> enumButton = (EnumSimpleButton<EnumObstacleLightColorSet>) button;
            EnumObstacleLightColorSet lightMode = enumButton.getValue();
            this.tempLightMode = lightMode;
            buttonColors.selectButton(lightMode);
        }
    }

    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 50, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.fontRendererObj.drawString("Light Mode  : ", this.width / 2 - 130, this.height / 2 - 40, -1, false);
        GL11.glPopMatrix();
    }
}
