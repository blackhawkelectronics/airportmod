package airportlight.blocks.markings.gloundsine;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class GroundSineRenderer extends TileEntitySpecialRenderer {
    private final GroundSineModel model = new GroundSineModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof GroundSineTile) {
            this.model.render((GroundSineTile) tile, x, y, z);
        }
    }
}
