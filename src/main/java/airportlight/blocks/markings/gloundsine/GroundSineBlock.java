package airportlight.blocks.markings.gloundsine;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class GroundSineBlock extends BlockAngleLightNormal {
    public GroundSineBlock() {
        super();
        setBlockName("GroundSine");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":groundsine");
        setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.05F, 1.0F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        player.openGui(ModAirPortLight.instance, GuiID.GroundSine.getID(), world, x, y, z);
        return true;
    }

    public boolean InversionBaseAngle() {
        return true;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_, p_149633_3_, p_149633_4_, (double) p_149633_2_ + 1, (double) p_149633_3_ + 0.25, (double) p_149633_4_ + 1);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World p_149668_1_, int p_149668_2_, int p_149668_3_, int p_149668_4_) {
        return null;
    }

    @Override
    public TileAngleLightNormal createNewAngleTileEntity(World p_149915_1_, int p_149915_2_) {
        return new GroundSineTile();
    }
}
