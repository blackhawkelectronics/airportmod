package airportlight.blocks.markings.gloundsine;

import airportlight.blocks.markings.guidepanel.EnumColorSet;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

public class GroundSineTile extends TileAngleLightNormal {
    String text = "18R";
    EnumColorSet color = EnumColorSet.Red_White;

    private float width = 1;
    private float height = 1;

    float widthOffset = 0.0f;
    float heightOffset = 0.0f;

    public GroundSineTile() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text.trim();
    }

    public EnumColorSet getColor() {
        return this.color;
    }

    public void setColor(EnumColorSet color) {
        this.color = color;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setSize(float width, float height) {
        this.width = width;
        this.height = height;
        maxsize = Math.max(this.width, this.height) + 1;
    }

    public boolean setDatas(String text, EnumColorSet color, float width, float height, float widthOffset, float heightOffset) {
        boolean isChange = false;
        if (!this.text.endsWith(text) || this.color != color || this.width != width || this.height != height || this.widthOffset != widthOffset || this.heightOffset != heightOffset) {
            this.text = text;
            this.color = color;
            this.width = width;
            this.height = height;
            this.widthOffset = widthOffset;
            this.heightOffset = heightOffset;

            maxsize = Math.max(this.width, this.height) + 1;
            isChange = true;
        }
        return isChange;
    }

    private float maxsize = 1;

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - maxsize,
                yCoord,
                zCoord - maxsize,
                xCoord + maxsize,
                yCoord + 0.25,
                zCoord + maxsize
        );
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setString("text", this.text);
        p_145841_1_.setInteger("color", this.color.mode);
        p_145841_1_.setFloat("width", this.width);
        p_145841_1_.setFloat("height", this.height);
        p_145841_1_.setFloat("widthOffset", this.widthOffset);
        p_145841_1_.setFloat("heightOffset", this.heightOffset);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.text = p_145839_1_.getString("text");
        this.color = EnumColorSet.getFromMode(p_145839_1_.getInteger("color"));
        this.width = p_145839_1_.getFloat("width");
        this.height = p_145839_1_.getFloat("height");
        this.widthOffset = p_145839_1_.getFloat("widthOffset");
        this.heightOffset = p_145839_1_.getFloat("heightOffset");

    }
}
