package airportlight.blocks.markings.gloundsine;

import airportlight.blocks.markings.guidepanel.EnumColorSet;
import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import org.jetbrains.annotations.NotNull;

public class GroundSineSync extends TileEntityMessage implements IMessageHandler<GroundSineSync, IMessage> {
    String text;
    EnumColorSet color;
    float width, height;
    float widthOffset, heightOffset;

    public GroundSineSync() {
        super();
    }

    public GroundSineSync(GroundSineTile tile) {
        super(tile);
        this.text = tile.text;
        this.color = tile.color;
        this.width = tile.getWidth();
        this.height = tile.getHeight();
        this.widthOffset = tile.widthOffset;
        this.heightOffset = tile.heightOffset;
    }

    @Override
    public void write(@NotNull ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("text", this.text);
        tag.setInteger("color", this.color.mode);
        tag.setFloat("width", this.width);
        tag.setFloat("height", this.height);
        tag.setFloat("widthOffset", this.widthOffset);
        tag.setFloat("heightOffset", this.heightOffset);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public void read(@NotNull ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.text = tag.getString("text");
        this.color = EnumColorSet.getFromMode(tag.getInteger("color"));
        this.width = tag.getFloat("width");
        this.height = tag.getFloat("height");
        this.widthOffset = tag.getFloat("widthOffset");
        this.heightOffset = tag.getFloat("heightOffset");
    }

    @Override
    public IMessage onMessage(GroundSineSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof GroundSineTile) {
            ((GroundSineTile) tile).setDatas(message.text, message.color, message.width, message.height, message.widthOffset, message.heightOffset);
            tile.markDirty();
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
