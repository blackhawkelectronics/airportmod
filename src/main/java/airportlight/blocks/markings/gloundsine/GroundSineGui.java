package airportlight.blocks.markings.gloundsine;

import airportlight.blocks.markings.guidepanel.EnumColorSet;
import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.util.Collections;

public class GroundSineGui extends GuiContainer {
    private static final int biText = 30;
    private static final int biColor = 40;
    private static final int biWidth = 1;
    private static final int biHeight = 2;
    private static final int biWidthOffset = 11;
    private static final int biHeightOffset = 12;


    private GroundSineTile tile;
    private int inputID;
    private StringInputGuiButton buttonText;
    private GuiButton[] buttonColors;
    private StringInputGuiButton buttonWidth;
    private StringInputGuiButton buttonHeight;
    private StringInputGuiButton buttonWidthOffset;
    private StringInputGuiButton buttonHeightOffset;

    private String scrapString;
    private String tempText;
    private EnumColorSet tempColor;
    private float tempWidth;
    private float tempHeight;
    private float tempWidthOffset;
    private float tempHeightOffset;

    public GroundSineGui() {
        super(new ContainerAirPort());
    }

    public GroundSineGui(GroundSineTile tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempText = tile.getText();
        this.tempColor = tile.getColor();
        this.tempWidth = tile.getWidth();
        this.tempHeight = tile.getHeight();
        this.tempWidthOffset = tile.widthOffset;
        this.tempHeightOffset = tile.heightOffset;
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        enterAndSend();
    }

    public void enterAndSend() {
        boolean isChanged = this.tile.setDatas(this.tempText, this.tempColor, tempWidth, tempHeight, tempWidthOffset, tempHeightOffset);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new GroundSineSync(this.tile));
        }
    }


    @Override
    public void initGui() {
        buttonText = new StringInputGuiButton(biText, this.width / 2 - 70, this.height / 2 - 55, tile.getText());
        this.buttonList.add(buttonText);

        buttonColors = new GuiButton[EnumColorSet.values().length];
        int i = 0;
        for (EnumColorSet color : EnumColorSet.values()) {
            buttonColors[i] = new GuiButton(biColor + i, this.width / 2 - 70 + i * 55, this.height / 2 - 25, 50, 20, color.name());
            i++;
        }
        this.buttonColors[tile.getColor().mode].enabled = false;

        Collections.addAll(this.buttonList, buttonColors);

        buttonWidth = new StringInputGuiButton(biWidth, this.width / 2 - 70, this.height / 2 + 10, 50, 20, String.valueOf(tile.getWidth()));
        buttonHeight = new StringInputGuiButton(biHeight, this.width / 2 - 70, this.height / 2 + 30, 50, 20, String.valueOf(tile.getHeight()));
        buttonWidthOffset = new StringInputGuiButton(biWidthOffset, this.width / 2 + 60, this.height / 2 + 10, 50, 20, String.valueOf(tile.widthOffset));
        buttonHeightOffset = new StringInputGuiButton(biHeightOffset, this.width / 2 + 60, this.height / 2 + 30, 50, 20, String.valueOf(tile.heightOffset));
        this.buttonList.add(buttonWidth);
        this.buttonList.add(buttonHeight);
        this.buttonList.add(buttonWidthOffset);
        this.buttonList.add(buttonHeightOffset);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == biText || button.id == biWidth || button.id == biHeight || button.id == biWidthOffset || button.id == biHeightOffset) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        } else if (biColor <= button.id && button.id < biColor + buttonColors.length) {
            for (GuiButton bs : buttonColors) {
                bs.enabled = true;
            }
            button.enabled = false;
            tempColor = EnumColorSet.getFromMode(button.id - biColor);
            enterAndSend();
        }
    }

    private void EnterSettings() {
        if (this.inputID == biText) {
            if (scrapString.isEmpty()) {
                this.buttonText.displayString = tile.getText();
            } else {
                this.buttonText.displayString = scrapString;
            }
            this.tempText = this.buttonText.displayString;
        } else if (this.inputID == biWidth) {
            if (scrapString.isEmpty() || scrapString.equals("-")) {
                this.buttonWidth.displayString = "1.0";
            } else {
                float size = Float.parseFloat(scrapString);
                size = Math.min(0.25f, Math.max(size, 20));
                this.buttonWidth.displayString = String.format("%.2f", size);
            }
        } else if (this.inputID == biHeight) {
            if (scrapString.isEmpty() || scrapString.equals("-")) {
                this.buttonHeight.displayString = "1.0";
            } else {
                float size = Float.parseFloat(scrapString);
                size = Math.min(0.25f, Math.max(size, 20));
                this.buttonHeight.displayString = String.format("%.2f", size);
            }
        } else if (this.inputID == biWidthOffset) {
            if (scrapString.isEmpty() || scrapString.equals("-")) {
                this.buttonWidthOffset.displayString = "0.0";
            } else {
                float size = Float.parseFloat(scrapString);
                size = Math.min(-20, Math.max(size, 20));
                this.buttonWidthOffset.displayString = String.format("%.2f", size);
            }
        } else if (this.inputID == biHeightOffset) {
            if (scrapString.isEmpty() || scrapString.equals("-")) {
                this.buttonHeightOffset.displayString = "0.0";
            } else {
                float size = Float.parseFloat(scrapString);
                size = Math.min(-20, Math.max(size, 20));
                this.buttonHeightOffset.displayString = String.format("%.2f", size);
            }
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.fontRendererObj.drawString("Text   : ", this.width / 2 - 120, this.height / 2 - 50, -1, false);
        this.fontRendererObj.drawString("Color  : ", this.width / 2 - 120, this.height / 2 - 20, -1, false);
        this.fontRendererObj.drawString("Width  : ", this.width / 2 - 120, this.height / 2 + 15, -1, false);
        this.fontRendererObj.drawString("Height : ", this.width / 2 - 120, this.height / 2 + 35, -1, false);
        this.fontRendererObj.drawString("Width  Offset : ", this.width / 2 - 0, this.height / 2 + 15, -1, false);
        this.fontRendererObj.drawString("Height Offset : ", this.width / 2 - 0, this.height / 2 + 35, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biText) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                this.tempText = scrapString;
                enterAndSend();
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (Character.isLetter(eventChar) || Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonText.displayString = scrapString;
        } else if (inputID == biWidth || inputID == biHeight || inputID == biWidthOffset || inputID == biHeightOffset) {
            if (keyID == Keyboard.KEY_RETURN) {
                if (0 < scrapString.length()) {
                    float size = Float.parseFloat(scrapString);
                    if (inputID == biWidth) {
                        size = Math.max(0.25f, Math.min(size, 20));
                        this.tempWidth = size;
                        scrapString = String.format("%.2f", tempWidth);
                        buttonWidth.displayString = scrapString;
                    } else if (inputID == biHeight) {
                        size = Math.max(0.25f, Math.min(size, 20));
                        this.tempHeight = size;
                        scrapString = String.format("%.2f", tempHeight);
                        buttonHeight.displayString = scrapString;
                    } else if (inputID == biWidthOffset) {
                        size = Math.max(-20, Math.min(size, 20));
                        this.tempWidthOffset = size;
                        scrapString = String.format("%.2f", tempWidthOffset);
                        buttonWidthOffset.displayString = scrapString;
                    } else if (inputID == biHeightOffset) {
                        size = Math.max(-20, Math.min(size, 20));
                        this.tempHeightOffset = size;
                        scrapString = String.format("%.2f", tempHeightOffset);
                        buttonHeightOffset.displayString = scrapString;
                    }
                    this.inputID = 0;
                    enterAndSend();
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (scrapString.isEmpty() && (keyID == Keyboard.KEY_MINUS || keyID == Keyboard.KEY_SUBTRACT)) {
                    scrapString += eventChar;
                } else if (scrapString.contains(".")) {
                    int index = scrapString.indexOf(".");
                    if (scrapString.length() < index + 3 && Character.isDigit(eventChar)) {
                        scrapString += eventChar;
                    }
                } else {
                    if (scrapString.length() > 0 && keyID == Keyboard.KEY_DECIMAL) {
                        scrapString += eventChar;
                    } else if (scrapString.length() < 2 && Character.isDigit(eventChar)) {
                        scrapString += eventChar;
                    }
                }
            }
            if (inputID == biWidth) {
                this.buttonWidth.displayString = scrapString;
            } else if (inputID == biHeight) {
                this.buttonHeight.displayString = scrapString;
            } else if (inputID == biWidthOffset) {
                this.buttonWidthOffset.displayString = scrapString;
            } else if (inputID == biHeightOffset) {
                this.buttonHeightOffset.displayString = scrapString;
            }
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
