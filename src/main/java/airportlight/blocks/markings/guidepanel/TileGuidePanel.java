package airportlight.blocks.markings.guidepanel;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

public class TileGuidePanel extends TileAngleLightNormal {
    String text = "18R";
    EnumColorSet color = EnumColorSet.Red_White;

    public TileGuidePanel() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text.trim();
    }

    public EnumColorSet getColor() {
        return this.color;
    }

    public void setColor(EnumColorSet color) {
        this.color = color;
    }

    public boolean setDatas(String text, EnumColorSet color) {
        boolean isChange = false;
        if (!this.text.equals(text) || this.color != color) {
            this.text = text;
            this.color = color;
            isChange = true;
        }
        return isChange;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - 1,
                yCoord,
                zCoord - 1,
                xCoord + 2,
                yCoord + 2.5,
                zCoord + 2
        );
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setString("text", this.text);
        p_145841_1_.setInteger("color", this.color.mode);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        this.text = p_145839_1_.getString("text");
        this.color = EnumColorSet.getFromMode(p_145839_1_.getInteger("color"));
    }
}
