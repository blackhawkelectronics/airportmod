package airportlight.blocks.markings.guidepanel;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class GuidePanelRenderer extends TileEntitySpecialRenderer {
    private final GuidePanelModel model = new GuidePanelModel();

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float tick) {
        // モデルの表示
        if (tile instanceof TileGuidePanel) {
            this.model.render((TileGuidePanel) tile, x, y, z);
        }
    }

}
