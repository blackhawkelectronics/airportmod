package airportlight.blocks.markings.guidepanel;

import airportlight.ModAirPortLight;
import airportlight.font.lwjgfont.LWJGFont;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GuidePanelModel extends AngleLightModelBase<TileGuidePanel> implements IUseWeightModel {
    protected IModelCustom model;
    protected final ResourceLocation textureModel;

    public GuidePanelModel() {
        super();
        IModelCustom m;
        if (APMConfig.UseWeightModel) {
            m = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/guidepanel_yukikaze.obj"));
        } else {
            m = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/guidepanel_yukikaze.obj"));
        }
        this.model = m;
        this.textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/guidepanel_yukikaze.png");
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        IModelCustom m;
        if (UseWeightModel) {
            m = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/guidepanel_yukikaze.obj"));
        } else {
            m = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/guidepanel_yukikaze.obj"));
        }
        this.model = m;
    }

    @Override
    protected void ModelBodyRender() {
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureModel);
        model.renderPart("body");
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        boolean lightON = worldLight < 0.5;
        String text = ((TileGuidePanel) tile).getText();

        EnumColorSet color = ((TileGuidePanel) tile).getColor();
        LWJGFont useFont = color.font;
        float wide = useFont.stringWidth(text);
        double scale = 53.0 / wide * 0.04;
        if (scale > 0.04) {
            scale = 0.04;
        }
        double scale1 = 1 / scale;

        FMLClientHandler.instance().getClient().renderEngine.bindTexture(color.texture);

        if (lightON) {
            render(240, this.model, "panel");
        } else {
            this.model.renderPart("panel");
        }
        float height = useFont.getLineHeight() / 2f;
        GL11.glTranslated(0.0, 1.4, 0.2);

        GL11.glScaled(scale, scale, scale);
        try {
            useFont.drawString(text, -wide / 2f, -height, 0, 200);
        } catch (IOException e) {
            e.printStackTrace();
        }
        GL11.glScaled(scale1, scale1, scale1);
        //GL11.glRotated(90, 0, -1, 0);
        GL11.glTranslated(0, -height, -0.2);
    }
}
