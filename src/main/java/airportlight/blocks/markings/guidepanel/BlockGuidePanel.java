package airportlight.blocks.markings.guidepanel;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockGuidePanel extends BlockAngleLightNormal {
    public BlockGuidePanel() {
        super();
        setBlockName("GuidePanel");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":guidepanel");
        setBlockBounds(0F, 0F, 0F, 1F, 2F, 1F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        player.openGui(ModAirPortLight.instance, GuiID.GuidePanel.getID(), world, x, y, z);
        return true;
    }

    public boolean InversionBaseAngle() {
        return true;
    }


    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ + 0.2, p_149633_3_, p_149633_4_ + 0.2, (double) p_149633_2_ + 0.8, (double) p_149633_3_ + 2, (double) p_149633_4_ + 0.8);
    }

    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TileGuidePanel();
    }
}
