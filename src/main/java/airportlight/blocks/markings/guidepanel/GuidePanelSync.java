package airportlight.blocks.markings.guidepanel;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class GuidePanelSync extends TileEntityMessage implements IMessageHandler<GuidePanelSync, IMessage> {
    String text;
    EnumColorSet color;

    public GuidePanelSync() {
        super();
    }

    public GuidePanelSync(TileGuidePanel tile, String text, EnumColorSet color) {
        super(tile);
        this.text = text;
        this.color = color;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.text = tag.getString("text");
        this.color = EnumColorSet.getFromMode(tag.getInteger("color"));
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("text", this.text);
        tag.setInteger("color", this.color.mode);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(GuidePanelSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileGuidePanel) {
            ((TileGuidePanel) tile).setDatas(message.text, message.color);
            if (ctx.side.isServer()) {
                tile.markDirty();
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
