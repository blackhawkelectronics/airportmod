package airportlight.blocks.markings.guidepanel;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuidePanelGui extends GuiContainer {
    private static final int biText = 30;
    private static final int biSides = 40;


    private TileGuidePanel tile;
    private int inputID;
    private StringInputGuiButton buttonText;
    private GuiButton[] buttonColors;

    private String scrapString;

    private String tempText;
    private EnumColorSet tempColor;

    public GuidePanelGui() {
        super(new ContainerAirPort());
    }

    public GuidePanelGui(TileGuidePanel tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempText = tile.getText();
        this.tempColor = tile.getColor();
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        enterAndSend();
    }

    public void enterAndSend() {
        boolean isChanged = this.tile.setDatas(this.tempText, this.tempColor);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new GuidePanelSync(this.tile, this.tempText, this.tempColor));
        }
    }


    @Override
    public void initGui() {
        buttonText = new StringInputGuiButton(biText, this.width / 2 - 70, this.height / 2 - 35, tile.getText());

        buttonColors = new GuiButton[EnumColorSet.values().length];
        int i = 0;
        for (EnumColorSet color : EnumColorSet.values()) {
            buttonColors[i] = new GuiButton(biSides + i, this.width / 2 - 70 + i * 55, this.height / 2 - 5, 50, 20, color.name());
            i++;
        }
        this.buttonColors[tile.getColor().mode].enabled = false;

        this.buttonList.add(buttonText);
        for (GuiButton button : buttonColors) {
            this.buttonList.add(button);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == biText) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        } else if (biSides <= button.id && button.id < biSides + buttonColors.length) {
            for (GuiButton bs : buttonColors) {
                bs.enabled = true;
            }
            button.enabled = false;
            tempColor = EnumColorSet.getFromMode(button.id - biSides);
            enterAndSend();
        }
    }

    private void EnterSettings() {
        if (this.inputID == biText) {
            if (scrapString.isEmpty()) {
                this.buttonText.displayString = tile.getText();
            } else {
                this.buttonText.displayString = scrapString;
            }
            this.tempText = this.buttonText.displayString;
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.fontRendererObj.drawString("Text   : ", this.width / 2 - 120, this.height / 2 - 30, -1, false);
        this.fontRendererObj.drawString("Color  : ", this.width / 2 - 120, this.height / 2, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biText) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                this.tempText = scrapString;
                enterAndSend();
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (Character.isLetter(eventChar) || Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonText.displayString = scrapString;
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
