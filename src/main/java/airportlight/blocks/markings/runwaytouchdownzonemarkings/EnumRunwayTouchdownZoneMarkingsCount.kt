package airportlight.blocks.markings.runwaytouchdownzonemarkings

enum class EnumRunwayTouchdownZoneMarkingsCount(val cnt: Int) {
    C1(1),
    C2(2),
    C3(3),
    ;

    companion object {
        fun getFromMode(mode: Int): EnumRunwayTouchdownZoneMarkingsCount {
            return if (mode > 0) {
                EnumRunwayTouchdownZoneMarkingsCount.values()[mode - 1]
            } else {
                C3
            }
        }
    }
}
