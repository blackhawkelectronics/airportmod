package airportlight.blocks.markings.runwaytouchdownzonemarkings

import airportlight.ModAirPortLight
import airportlight.modcore.normal.ModelBaseNormal
import cpw.mods.fml.client.FMLClientHandler
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import org.lwjgl.opengl.GL11

class RunwayTouchdownZoneMarkingsModel : ModelBaseNormal<RunwayTouchdownZoneMarkingsTile>() {
    protected val textureWhite: ResourceLocation = ResourceLocation(ModAirPortLight.DOMAIN, "white.png")
    protected val model18: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(ModAirPortLight.DOMAIN, "models/1-8_22-5_bar.obj"))
    protected val model30: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(ModAirPortLight.DOMAIN, "models/3_22-5_bar.obj"))

    override fun render(tile: RunwayTouchdownZoneMarkingsTile, x: Double, y: Double, z: Double) {
        val lineCnt = tile.lineCnt.cnt
        if (lineCnt > 0) {
            GL11.glPushMatrix()
            GL11.glTranslated(x + 0.5, y, z + 0.5)
            val ang = tile.baseAngle.toInt()
            GL11.glRotated(-ang.toDouble(), 0.0, 1.0, 0.0)
            FMLClientHandler.instance().client.renderEngine.bindTexture(textureWhite)
            val interval = 1.5 + 1.8
            val innerInterval = if (lineCnt == 1) {
                tile.centerInterval / 2.0 + 1.5
            } else {
                tile.centerInterval / 2.0 + 0.9
            }

            GL11.glTranslated(innerInterval, 0.0, 0.0)
            if (lineCnt == 1) {
                model30.renderAll()
            } else {
                model18.renderAll()
            }

            for (i in 1 until lineCnt) {
                GL11.glTranslated(interval, 0.0, 0.0)
                model18.renderAll()
            }

            GL11.glTranslated(-interval * (lineCnt - 1) - innerInterval, 0.0, 0.0)

            GL11.glTranslated(-innerInterval, 0.0, 0.0)
            if (lineCnt == 1) {
                model30.renderAll()
            } else {
                model18.renderAll()
            }

            for (i in 1 until lineCnt) {
                GL11.glTranslated(-interval, 0.0, 0.0)
                model18.renderAll()
            }
            GL11.glTranslated(interval * (lineCnt - 1) + innerInterval, 0.0, 0.0)

            if (tile.lineLong > 30) {
                GL11.glTranslated(0.0, 0.0, -tile.lineLong + 30.0)

                GL11.glTranslated(innerInterval, 0.0, 0.0)
                if (lineCnt == 1) {
                    model30.renderAll()
                } else {
                    model18.renderAll()
                }

                for (i in 1 until lineCnt) {
                    GL11.glTranslated(interval, 0.0, 0.0)
                    model18.renderAll()
                }

                GL11.glTranslated(-interval * (lineCnt - 1) - innerInterval, 0.0, 0.0)

                GL11.glTranslated(-innerInterval, 0.0, 0.0)
                if (lineCnt == 1) {
                    model30.renderAll()
                } else {
                    model18.renderAll()
                }

                for (i in 1 until lineCnt) {
                    GL11.glTranslated(-interval, 0.0, 0.0)
                    model18.renderAll()
                }
                GL11.glTranslated(interval * (lineCnt - 1) + innerInterval, 0.0, 0.0)
            }

            GL11.glPopMatrix()
        }
    }
}
