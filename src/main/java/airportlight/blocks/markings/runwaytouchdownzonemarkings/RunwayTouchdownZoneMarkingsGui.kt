package airportlight.blocks.markings.runwaytouchdownzonemarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.custombutton.EnumSimpleButton
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListHorizontal
import airportlight.modcore.gui.custombutton.StringInputGuiButton
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.inventory.GuiContainer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import kotlin.math.max
import kotlin.math.min

class RunwayTouchdownZoneMarkingsGui : GuiContainer {
    private val biRunwayWide = 20
    private val biLong = 31
    private val biCenterInterval = 32

    private var tile: RunwayTouchdownZoneMarkingsTile? = null
    private var inputID = 0
    private lateinit var buttonRunwayWides: EnumSimpleButtonListHorizontal<EnumRunwayTouchdownZoneMarkingsCount>
    private var buttonLineLong: StringInputGuiButton? = null
    private var buttonCenterInterval: StringInputGuiButton? = null

    private var tempRunwayWide = EnumRunwayTouchdownZoneMarkingsCount.C3
    private var templong = 0f
    private var tempCenterInterval = 0f
    private var scrapString = ""

    constructor() : super(ContainerAirPort()) {}
    constructor(tile: RunwayTouchdownZoneMarkingsTile) : super(ContainerAirPort()) {
        this.tile = tile
        tempRunwayWide = tile.lineCnt
        templong = tile.lineLong
        tempCenterInterval = tile.centerInterval
    }

    override fun onGuiClosed() {
        enterSettings()
        val isChanged = tile!!.setInfo(tempRunwayWide, templong, tempCenterInterval)
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(RunwayTouchdownZoneMarkingsSync(tile!!, tempRunwayWide, templong, tempCenterInterval))
        }
    }


    override fun initGui() {
        buttonRunwayWides =
            EnumSimpleButtonListHorizontal<EnumRunwayTouchdownZoneMarkingsCount>(
                biRunwayWide,
                width / 2 - 70,
                height / 2 - 35,
                40,
                20,
                EnumRunwayTouchdownZoneMarkingsCount::class.java
            )
        buttonLineLong = StringInputGuiButton(biLong, width / 2 - 70, height / 2 - 5, 80, 20, String.format("%.1f", tile!!.lineLong))
        buttonLineLong!!.enabled = false
        buttonCenterInterval = StringInputGuiButton(biCenterInterval, width / 2 - 70, height / 2 + 25, 80, 20, String.format("%.1f", tile!!.centerInterval))
        buttonList.addAll(buttonRunwayWides.buttonList)
        //buttonList.add(buttonLineLong)
        buttonList.add(buttonCenterInterval)
        buttonRunwayWides.selectButton(tempRunwayWide)
    }

    override fun actionPerformed(button: GuiButton) {
        if (button.id == biRunwayWide) {
            inputID = button.id
            tempRunwayWide = (button as EnumSimpleButton<*>).value as EnumRunwayTouchdownZoneMarkingsCount
            enterSettings()
            inputID = button.id
            scrapString = ""
            val isChanged = tile!!.setInfo(tempRunwayWide, templong, tempCenterInterval)
            if (isChanged) {
                PacketHandlerAPM.sendPacketServer(RunwayTouchdownZoneMarkingsSync(tile!!, tempRunwayWide, templong, tempCenterInterval))
            }
        } else if (button.id == biLong || button.id == biCenterInterval) {
            enterSettings()
            inputID = button.id
            scrapString = ""
            button.displayString = ""
        }
    }

    private fun enterSettings() {
        if (inputID == biRunwayWide) {
            buttonRunwayWides.selectButton(tempRunwayWide)
        } else if (inputID == biLong) {
            if (scrapString.isEmpty()) {
                buttonLineLong!!.displayString = String.format("%.1f", tile!!.lineLong)
            } else {
                buttonLineLong!!.displayString = String.format("%.1f", scrapString.toFloat())
            }
            templong = max(22.5f, min(buttonLineLong!!.displayString.toFloat(), 22.5f))
        } else if (inputID == biCenterInterval) {
            if (scrapString.isEmpty()) {
                buttonCenterInterval!!.displayString = String.format("%.1f", tile!!.centerInterval)
            } else {
                buttonCenterInterval!!.displayString = String.format("%.1f", scrapString.toFloat())
            }
            tempCenterInterval = max(12f, min(buttonCenterInterval!!.displayString.toFloat(), 22.5f))
        }
    }


    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            drawGradientRect(40, 40, width - 40, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        GL11.glPushMatrix()
        fontRendererObj.drawString("Line Count  : ", width / 2 - 140, height / 2 - 30, -1, false)
        fontRendererObj.drawString("Line Long  : ", width / 2 - 140, height / 2, -1, false)
        fontRendererObj.drawString("25", width / 2 - 35, height / 2, -1, false)
        fontRendererObj.drawString("Center Interval  : ", width / 2 - 140, height / 2 + 30, -1, false)
        GL11.glPopMatrix()
    }

    override fun keyTyped(eventChar: Char, keyID: Int) {
        if (keyID == 1) {
            mc.thePlayer.closeScreen()
            return
        }
        if (inputID == biLong) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    templong = max(22.5f, min(scrapString.toFloat(), 22.5f))
                    scrapString = String.format("%.1f", templong)
                } else {
                    templong = 30f
                    scrapString = "30"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempRunwayWide, templong, tempCenterInterval)
                if (isChanged) {
                    PacketHandlerAPM.sendPacketServer(RunwayTouchdownZoneMarkingsSync(tile!!, tempRunwayWide, templong, tempCenterInterval))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                } else if (strLength == 2 && eventChar == '.') {
                    flag = true
                } else if (strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true
                    }
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonLineLong!!.displayString = scrapString
        } else if (inputID == biCenterInterval) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    tempCenterInterval = max(12f, min(scrapString.toFloat(), 22.5f))
                    scrapString = String.format("%.1f", tempCenterInterval)
                } else {
                    tempCenterInterval = 18f
                    scrapString = "18"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempRunwayWide, templong, tempCenterInterval)
                if (isChanged) {
                    PacketHandlerAPM.sendPacketServer(RunwayTouchdownZoneMarkingsSync(tile!!, tempRunwayWide, templong, tempCenterInterval))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                } else if (strLength == 2 && eventChar == '.') {
                    flag = true
                } else if (strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true
                    }
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonCenterInterval!!.displayString = scrapString
        } else {
            super.keyTyped(eventChar, keyID)
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
