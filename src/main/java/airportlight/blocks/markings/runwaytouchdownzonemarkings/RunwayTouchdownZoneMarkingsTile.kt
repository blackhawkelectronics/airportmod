package airportlight.blocks.markings.runwaytouchdownzonemarkings

import airportlight.modcore.normal.TileAngleLightNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB

class RunwayTouchdownZoneMarkingsTile : TileAngleLightNormal() {
    var lineCnt = EnumRunwayTouchdownZoneMarkingsCount.C3
    var lineLong = 22.5f //22.5
    var centerInterval = 18f // 12~22.5

    fun setInfo(lineCnt: EnumRunwayTouchdownZoneMarkingsCount, lineLong: Float, centerInterval: Float): Boolean {
        if (this.lineCnt != lineCnt || this.lineLong != lineLong || this.centerInterval != centerInterval) {
            this.lineCnt = lineCnt
            this.lineLong = lineLong
            this.centerInterval = centerInterval
            return true
        }
        return false
    }


    @SideOnly(Side.CLIENT)
    override fun getRenderBoundingBox(): AxisAlignedBB? {
        return AxisAlignedBB.getBoundingBox(
            (xCoord - lineLong * 1.5), yCoord.toDouble(), (zCoord - lineLong * 1.5), (xCoord + lineLong * 1.5), yCoord + 0.2, (zCoord + lineLong * 1.5)
        )
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        lineCnt = EnumRunwayTouchdownZoneMarkingsCount.getFromMode(nbt.getInteger("lineCnt"))
        lineLong = nbt.getFloat("lineLong")
        centerInterval = nbt.getFloat("centerInterval")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("lineCnt", lineCnt.cnt)
        nbt.setFloat("lineLong", lineLong)
        nbt.setFloat("centerInterval", centerInterval)
    }
}
