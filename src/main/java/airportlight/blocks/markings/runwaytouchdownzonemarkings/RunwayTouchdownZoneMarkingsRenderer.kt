package airportlight.blocks.markings.runwaytouchdownzonemarkings

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity

class RunwayTouchdownZoneMarkingsRenderer : TileEntitySpecialRenderer() {
    val model: RunwayTouchdownZoneMarkingsModel = RunwayTouchdownZoneMarkingsModel()

    override fun renderTileEntityAt(tile: TileEntity, x: Double, y: Double, z: Double, tick: Float) {
        // モデルの表示
        if (tile is RunwayTouchdownZoneMarkingsTile) {
            this.model.render(tile, x, y, z)
        }
    }
}
