package airportlight.blocks.markings.runwayaimingpointmarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.nbt.NBTTagCompound

class RunwayAimingPointMarkingsSync : TileEntityMessage {
    lateinit var lineLong: EnumRunwayAimingPointMarkingsLong
    var lineWide = 0f
    var centerInterval = 0f

    @Suppress("unused")
    constructor()

    constructor(tile: RunwayAimingPointMarkingsTile, lineCnt: EnumRunwayAimingPointMarkingsLong, lineLong: Float, centerInterval: Float) : super(tile) {
        this.lineLong = lineCnt
        this.lineWide = lineLong
        this.centerInterval = centerInterval
    }

    override fun read(buf: ByteBuf) {
        val tag = ByteBufUtils.readTag(buf)
        this.lineLong = EnumRunwayAimingPointMarkingsLong.getFromType(tag.getInteger("lineLong"))
        this.lineWide = tag.getFloat("lineWide")
        this.centerInterval = tag.getFloat("centerInterval")
    }

    override fun write(buf: ByteBuf) {
        val tag = NBTTagCompound()
        tag.setInteger("lineLong", this.lineLong.type)
        tag.setFloat("lineWide", this.lineWide)
        tag.setFloat("centerInterval", this.centerInterval)
        ByteBufUtils.writeTag(buf, tag)
    }

    companion object : IMessageHandler<RunwayAimingPointMarkingsSync, IMessage> {
        override fun onMessage(message: RunwayAimingPointMarkingsSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is RunwayAimingPointMarkingsTile) {
                tile.setInfo(message.lineLong, message.lineWide, message.centerInterval)
                if (ctx.side.isServer) {
                    message.tile = tile
                    PacketHandlerAPM.sendPacketAll(message)
                }
            }
            return null
        }
    }
}
