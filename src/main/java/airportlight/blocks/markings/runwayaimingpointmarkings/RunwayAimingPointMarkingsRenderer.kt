package airportlight.blocks.markings.runwayaimingpointmarkings

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity

class RunwayAimingPointMarkingsRenderer : TileEntitySpecialRenderer() {
    val model: RunwayAimingPointMarkingsModel = RunwayAimingPointMarkingsModel()

    override fun renderTileEntityAt(tile: TileEntity, x: Double, y: Double, z: Double, tick: Float) {
        // モデルの表示
        if (tile is RunwayAimingPointMarkingsTile) {
            this.model.render(tile, x, y, z)
        }
    }
}

