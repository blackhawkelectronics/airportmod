package airportlight.blocks.markings.runwayaimingpointmarkings

import airportlight.modcore.normal.TileAngleLightNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB

class RunwayAimingPointMarkingsTile : TileAngleLightNormal() {
    var lineLong = EnumRunwayAimingPointMarkingsLong.L45
    var lineWide = 6f //6~10*60, 4~10*45
    var centerInterval = 18f // 12~22.5

    fun setInfo(lineLong: EnumRunwayAimingPointMarkingsLong, lineWide: Float, centerInterval: Float): Boolean {
        if (this.lineLong != lineLong || this.lineWide != lineWide || this.centerInterval != centerInterval) {
            this.lineLong = lineLong
            this.lineWide = lineWide
            this.centerInterval = centerInterval
            return true
        }
        return false
    }


    @SideOnly(Side.CLIENT)
    override fun getRenderBoundingBox(): AxisAlignedBB? {
        return AxisAlignedBB.getBoundingBox(
            (xCoord - lineLong.long * 1.5),
            yCoord.toDouble(),
            (zCoord - lineLong.long * 1.5),
            (xCoord + lineLong.long * 1.5),
            yCoord + 0.2,
            (zCoord + lineLong.long * 1.5)
        )
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        lineLong = EnumRunwayAimingPointMarkingsLong.getFromType(nbt.getInteger("lineLong"))
        lineWide = nbt.getFloat("lineWide")
        centerInterval = nbt.getFloat("centerInterval")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("lineLong", lineLong.type)
        nbt.setFloat("lineWide", lineWide)
        nbt.setFloat("centerInterval", centerInterval)
    }
}
