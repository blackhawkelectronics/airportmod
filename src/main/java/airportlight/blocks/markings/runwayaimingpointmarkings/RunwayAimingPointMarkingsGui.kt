package airportlight.blocks.markings.runwayaimingpointmarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.custombutton.EnumSimpleButton
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListHorizontal
import airportlight.modcore.gui.custombutton.StringInputGuiButton
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.inventory.GuiContainer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import kotlin.math.max
import kotlin.math.min

class RunwayAimingPointMarkingsGui : GuiContainer {
    private val biRunwayWide = 20
    private val biLineWide = 31
    private val biCenterInterval = 32

    private var tile: RunwayAimingPointMarkingsTile? = null
    private var inputID = 0
    private lateinit var buttonLineLong: EnumSimpleButtonListHorizontal<EnumRunwayAimingPointMarkingsLong>
    private var buttonLineWide: StringInputGuiButton? = null
    private var buttonCenterInterval: StringInputGuiButton? = null

    private var tempLineLong = EnumRunwayAimingPointMarkingsLong.L60
    private var tempLineWide = 0f
    private var tempCenterInterval = 0f
    private var scrapString = ""

    constructor() : super(ContainerAirPort()) {}
    constructor(tile: RunwayAimingPointMarkingsTile) : super(ContainerAirPort()) {
        this.tile = tile
        tempLineLong = tile.lineLong
        tempLineWide = tile.lineWide
        tempCenterInterval = tile.centerInterval
    }

    override fun onGuiClosed() {
        enterSettings()
        val isChanged = tile!!.setInfo(tempLineLong, tempLineWide, tempCenterInterval)
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(RunwayAimingPointMarkingsSync(tile!!, tempLineLong, tempLineWide, tempCenterInterval))
        }
    }


    override fun initGui() {
        buttonLineLong =
            EnumSimpleButtonListHorizontal(
                biRunwayWide,
                width / 2 - 70,
                height / 2 - 35,
                40,
                20,
                EnumRunwayAimingPointMarkingsLong::class.java
            )
        buttonLineWide = StringInputGuiButton(biLineWide, width / 2 - 70, height / 2 - 5, 80, 20, String.format("%.1f", tile!!.lineWide))
        buttonCenterInterval = StringInputGuiButton(biCenterInterval, width / 2 - 70, height / 2 + 25, 80, 20, String.format("%.1f", tile!!.centerInterval))
        buttonList.addAll(buttonLineLong.buttonList)
        buttonList.add(buttonLineWide)
        buttonList.add(buttonCenterInterval)
        buttonLineLong.selectButton(tempLineLong)
    }

    override fun actionPerformed(button: GuiButton) {
        if (button.id == biRunwayWide) {
            inputID = button.id
            tempLineLong = (button as EnumSimpleButton<*>).value as EnumRunwayAimingPointMarkingsLong
            enterSettings()
            inputID = button.id
            scrapString = ""
            val isChanged = tile!!.setInfo(tempLineLong, tempLineWide, tempCenterInterval)
            if (isChanged) {
                PacketHandlerAPM.sendPacketServer(RunwayAimingPointMarkingsSync(tile!!, tempLineLong, tempLineWide, tempCenterInterval))
            }
        } else if (button.id == biLineWide || button.id == biCenterInterval) {
            enterSettings()
            inputID = button.id
            scrapString = ""
            button.displayString = ""
        }
    }

    private fun enterSettings() {
        if (inputID == biRunwayWide) {
            buttonLineLong.selectButton(tempLineLong)
        } else if (inputID == biLineWide) {
            if (scrapString.isEmpty()) {
                buttonLineWide!!.displayString = String.format("%.1f", tile!!.lineWide)
            } else {
                buttonLineWide!!.displayString = String.format("%.1f", scrapString.toFloat())
            }
            tempLineWide = max(4f, min(buttonLineWide!!.displayString.toFloat(), 10f))
        } else if (inputID == biCenterInterval) {
            if (scrapString.isEmpty()) {
                buttonCenterInterval!!.displayString = String.format("%.1f", tile!!.centerInterval)
            } else {
                buttonCenterInterval!!.displayString = String.format("%.1f", scrapString.toFloat())
            }
            tempCenterInterval = max(6f, min(buttonCenterInterval!!.displayString.toFloat(), 22.5f))
        }
    }


    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            drawGradientRect(40, 40, width - 40, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        GL11.glPushMatrix()
        fontRendererObj.drawString("Line Long  : ", width / 2 - 140, height / 2 - 30, -1, false)
        fontRendererObj.drawString("Line Wide  : ", width / 2 - 140, height / 2, -1, false)
        fontRendererObj.drawString("Center Interval  : ", width / 2 - 140, height / 2 + 30, -1, false)
        GL11.glPopMatrix()
    }

    override fun keyTyped(eventChar: Char, keyID: Int) {
        if (keyID == 1) {
            mc.thePlayer.closeScreen()
            return
        }
        if (inputID == biLineWide) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    tempLineWide = max(4f, min(scrapString.toFloat(), 10f))
                    scrapString = String.format("%.1f", tempLineWide)
                } else {
                    tempLineWide = 6f
                    scrapString = "6.0"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempLineLong, tempLineWide, tempCenterInterval)
                if (isChanged) {
                    PacketHandlerAPM.sendPacketServer(RunwayAimingPointMarkingsSync(tile!!, tempLineLong, tempLineWide, tempCenterInterval))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                } else if (strLength == 2 && eventChar == '.') {
                    flag = true
                } else if (strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true
                    }
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonLineWide!!.displayString = scrapString
        } else if (inputID == biCenterInterval) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    tempCenterInterval = max(6f, min(buttonCenterInterval!!.displayString.toFloat(), 22.5f))
                    scrapString = String.format("%.1f", tempCenterInterval)
                } else {
                    tempLineWide = 18f
                    scrapString = "18.0"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempLineLong, tempLineWide, tempCenterInterval)
                if (isChanged) {
                    PacketHandlerAPM.sendPacketServer(RunwayAimingPointMarkingsSync(tile!!, tempLineLong, tempLineWide, tempCenterInterval))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                } else if (strLength == 2 && eventChar == '.') {
                    flag = true
                } else if (strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true
                    }
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonCenterInterval!!.displayString = scrapString
        } else {
            super.keyTyped(eventChar, keyID)
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}

