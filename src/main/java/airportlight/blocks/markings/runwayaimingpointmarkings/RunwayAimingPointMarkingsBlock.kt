package airportlight.blocks.markings.runwayaimingpointmarkings

import airportlight.ModAirPortLight
import airportlight.modcore.commonver.GuiID
import airportlight.modcore.normal.BlockAngleLightNormal
import airportlight.modcore.normal.TileAngleLightNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World

class RunwayAimingPointMarkingsBlock : BlockAngleLightNormal() {
    init {
        setBlockName("RunwayAimingPointMarkings")
        setBlockTextureName(ModAirPortLight.DOMAIN + ":runwayaimingpoint")
        setBlockBounds(0.0f, 0f, 0.0f, 1.0f, 0.2f, 1.0f)
    }

    @SideOnly(Side.CLIENT)
    override fun getSelectedBoundingBoxFromPool(p_149633_1_: World?, p_149633_2_: Int, p_149633_3_: Int, p_149633_4_: Int): AxisAlignedBB? {
        return AxisAlignedBB.getBoundingBox(
            p_149633_2_.toDouble(),
            p_149633_3_.toDouble(),
            p_149633_4_.toDouble(),
            p_149633_2_.toDouble() + 1.0,
            p_149633_3_.toDouble() + 0.2,
            p_149633_4_.toDouble() + 1.0
        )
    }

    override fun onBlockActivated(
        world: World?,
        x: Int,
        y: Int,
        z: Int,
        player: EntityPlayer,
        p_149727_6_: Int,
        p_149727_7_: Float,
        p_149727_8_: Float,
        p_149727_9_: Float
    ): Boolean {
        player.openGui(ModAirPortLight.instance, GuiID.RunwayAimingPointMarkings.id, world, x, y, z)
        return true
    }

    override fun onBlockPlacedBy(world: World, x: Int, y: Int, z: Int, entity: EntityLivingBase?, itemStack: ItemStack?) {
        super.onBlockPlacedBy(world, x, y, z, entity, itemStack)
        val tile = world.getTileEntity(x, y, z)
        if (tile is RunwayAimingPointMarkingsTile) {
            tile.setInfo(tile.lineLong, tile.lineWide, tile.centerInterval)
        }
    }

    override fun InversionBaseAngle(): Boolean {
        return true
    }

    override fun createNewAngleTileEntity(world: World, metadata: Int): TileAngleLightNormal? {
        return RunwayAimingPointMarkingsTile()
    }
}
