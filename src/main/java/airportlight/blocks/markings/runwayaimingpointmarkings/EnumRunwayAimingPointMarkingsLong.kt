package airportlight.blocks.markings.runwayaimingpointmarkings

enum class EnumRunwayAimingPointMarkingsLong(val type: Int, val long: Float) {
    L60(0, 60f),
    L45(1, 45f),
    ;

    companion object {
        fun getFromType(type: Int): EnumRunwayAimingPointMarkingsLong {
            return EnumRunwayAimingPointMarkingsLong.values()[type]
        }
    }
}
