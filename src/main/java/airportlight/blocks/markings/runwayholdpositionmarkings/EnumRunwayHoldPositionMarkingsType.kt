package airportlight.blocks.markings.runwayholdpositionmarkings

enum class EnumRunwayHoldPositionMarkingsType(val type: Int) {
    RunwayHoldPosition(0),
    ILSHoldPosition(1)
    ;

    companion object {
        fun getFromMode(type: Int): EnumRunwayHoldPositionMarkingsType {
            return EnumRunwayHoldPositionMarkingsType.values()[type]
        }
    }
}
