package airportlight.blocks.markings.runwayholdpositionmarkings

import airportlight.ModAirPortLight
import airportlight.modcore.normal.ModelBaseNormal
import cpw.mods.fml.client.FMLClientHandler
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import org.lwjgl.opengl.GL11

class RunwayHoldPositionMarkingsModel2 : ModelBaseNormal<RunwayHoldPositionMarkingsTile>() {
    protected val textureWhite: ResourceLocation = ResourceLocation(ModAirPortLight.DOMAIN, "white.png")
    protected val model: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(ModAirPortLight.DOMAIN, "models/runwayholdpositionmarkings2.obj"))

    override fun render(tile: RunwayHoldPositionMarkingsTile, x: Double, y: Double, z: Double) {
        GL11.glPushMatrix()

        GL11.glTranslated(x + 0.5, y, z + 0.5)
        val ang = tile.baseAngle.toInt()
        GL11.glRotated(-ang.toDouble(), 0.0, 1.0, 0.0)
        FMLClientHandler.instance().client.renderEngine.bindTexture(textureWhite)
        val lineWide = tile.lineWide
        val interval = 1.0
        val oneSidelineWide = (lineWide - 1) / 2

        model.renderAll()

        for (i in 1..oneSidelineWide) {
            GL11.glTranslated(interval, 0.0, 0.0)
            if (i % 4 == 0) {
                model.renderAll()
            } else {
                model.renderOnly("lineL", "lineR")
            }
        }

        if (lineWide > 2 && lineWide % 2 == 0) {
            GL11.glTranslated(interval, 0.0, 0.0)
            model.renderOnly("lineL")
            GL11.glTranslated(-interval, 0.0, 0.0)
        }

        GL11.glTranslated(-interval * (oneSidelineWide), 0.0, 0.0)

        for (i in 1..oneSidelineWide) {
            GL11.glTranslated(-interval, 0.0, 0.0)
            if (i % 4 == 0) {
                model.renderAll()
            } else {
                model.renderOnly("lineL", "lineR")
            }
        }

        if (lineWide > 2 && lineWide % 2 == 0) {
            GL11.glTranslated(-interval, 0.0, 0.0)
            model.renderOnly("lineR")
            GL11.glTranslated(interval, 0.0, 0.0)
        }

        GL11.glTranslated(interval * (oneSidelineWide), 0.0, 0.0)

        GL11.glPopMatrix()
    }
}
