package airportlight.blocks.markings.runwayholdpositionmarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.custombutton.EnumSimpleButton
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListHorizontal
import airportlight.modcore.gui.custombutton.StringInputGuiButton
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.inventory.GuiContainer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import kotlin.math.max
import kotlin.math.min

class RunwayHoldPositionMarkingsGui : GuiContainer {
    private val biLineType = 30
    private val biLineWide = 31

    private var tile: RunwayHoldPositionMarkingsTile? = null
    private var inputID = 0
    private lateinit var buttonLineTypes: EnumSimpleButtonListHorizontal<EnumRunwayHoldPositionMarkingsType>
    private var buttonLineWide: StringInputGuiButton? = null

    private var tempLineType = EnumRunwayHoldPositionMarkingsType.RunwayHoldPosition
    private var tempLineWide = 0
    private var scrapString = ""

    constructor() : super(ContainerAirPort()) {}
    constructor(tile: RunwayHoldPositionMarkingsTile) : super(ContainerAirPort()) {
        this.tile = tile
        tempLineType = tile.lineType
        tempLineWide = tile.lineWide
    }

    override fun onGuiClosed() {
        enterSettings()
        val isChanged = tile!!.setInfo(tempLineType, tempLineWide)
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(RunwayHoldPositionMarkingsSync(tile!!, tempLineType, tempLineWide))
        }
    }


    override fun initGui() {
        buttonLineTypes =
            EnumSimpleButtonListHorizontal(
                biLineType,
                width / 2 - 70,
                height / 2 - 35,
                80,
                20,
                EnumRunwayHoldPositionMarkingsType::class.java
            )
        buttonLineWide = StringInputGuiButton(biLineWide, width / 2 - 70, height / 2 - 5, 80, 20, String.format("%d", tile!!.lineWide))
        buttonList.addAll(buttonLineTypes.buttonList)
        buttonList.add(buttonLineWide)
        buttonLineTypes.selectButton(tempLineType)
    }

    override fun actionPerformed(button: GuiButton) {
        if (button.id == biLineType) {
            inputID = button.id
            tempLineType = (button as EnumSimpleButton<*>).value as EnumRunwayHoldPositionMarkingsType
            enterSettings()
            inputID = button.id
            scrapString = ""
            val isChanged = tile!!.setInfo(tempLineType, tempLineWide)
            if (isChanged) {
                PacketHandlerAPM.sendPacketServer(RunwayHoldPositionMarkingsSync(tile!!, tempLineType, tempLineWide))
            }
        } else if (button.id == biLineWide) {
            enterSettings()
            inputID = button.id
            scrapString = ""
            button.displayString = ""
        }
    }

    private fun enterSettings() {
        if (inputID == biLineType) {
            buttonLineTypes.selectButton(tempLineType)
        } else if (inputID == biLineWide) {
            if (scrapString.isEmpty()) {
                buttonLineWide!!.displayString = String.format("%d", tile!!.lineWide)
            } else {
                buttonLineWide!!.displayString = String.format("%d", scrapString.toInt())
            }
            tempLineWide = max(5, min(buttonLineWide!!.displayString.toInt(), 30))
        }
    }


    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            drawGradientRect(40, 40, width - 40, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        GL11.glPushMatrix()
        fontRendererObj.drawString("Line Wide  : ", width / 2 - 140, height / 2, -1, false)
        GL11.glPopMatrix()
    }

    override fun keyTyped(eventChar: Char, keyID: Int) {
        if (keyID == 1) {
            mc.thePlayer.closeScreen()
            return
        }
        if (inputID == biLineWide) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    tempLineWide = scrapString.toInt()
                    scrapString = String.format("%d", tempLineWide)
                } else {
                    tempLineWide = 15
                    scrapString = "15"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempLineType, tempLineWide)
                if (isChanged) {
                    PacketHandlerAPM.sendPacketServer(RunwayHoldPositionMarkingsSync(tile!!, tempLineType, tempLineWide))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonLineWide!!.displayString = scrapString
        } else {
            super.keyTyped(eventChar, keyID)
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
