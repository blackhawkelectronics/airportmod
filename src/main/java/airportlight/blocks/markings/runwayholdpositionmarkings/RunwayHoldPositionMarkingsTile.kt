package airportlight.blocks.markings.runwayholdpositionmarkings

import airportlight.modcore.normal.TileAngleLightNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB

class RunwayHoldPositionMarkingsTile : TileAngleLightNormal() {
    var lineType = EnumRunwayHoldPositionMarkingsType.RunwayHoldPosition
    var lineWide = 15

    fun setInfo(lineType: EnumRunwayHoldPositionMarkingsType, lineWide: Int): Boolean {
        if (this.lineType != lineType || this.lineWide != lineWide) {
            this.lineType = lineType
            this.lineWide = lineWide
            return true
        }
        return false
    }


    @SideOnly(Side.CLIENT)
    override fun getRenderBoundingBox(): AxisAlignedBB? {
        return AxisAlignedBB.getBoundingBox(
            (xCoord - lineWide * 1.5), yCoord.toDouble(), (zCoord - lineWide * 1.5), (xCoord + lineWide * 1.5), yCoord + 0.2, (zCoord + lineWide * 1.5)
        )
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        lineType = EnumRunwayHoldPositionMarkingsType.getFromMode(nbt.getInteger("lineType"))
        lineWide = nbt.getInteger("lineWide")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("lineType", lineType.type)
        nbt.setInteger("lineWide", lineWide)
    }
}
