package airportlight.blocks.markings.runwaynumber;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class RunwayNumberRenderer extends TileEntitySpecialRenderer {
    private final RunwayNumberModel model = new RunwayNumberModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileRunwayNumber) {
            this.model.render((TileRunwayNumber) tE, x, y, z);
        }
    }
}
