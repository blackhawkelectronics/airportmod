package airportlight.blocks.markings.runwaynumber;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.util.MathHelperAirPort;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockRunwayNumber extends BlockAngleLightNormal {
    public BlockRunwayNumber() {
        super();
        setBlockName("RunwayNumber");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":runwaynumber");
        setBlockBounds(0.0F, 0F, 0.0F, 1.0F, 0.2F, 1.0F);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox(p_149633_2_, p_149633_3_, p_149633_4_, (double) p_149633_2_ + 1.0, (double) p_149633_3_ + 0.2, (double) p_149633_4_ + 1.0);
    }

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        player.openGui(ModAirPortLight.instance, GuiID.RunwayNumber.getID(), world, x, y, z);
        return true;
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack) {
        super.onBlockPlacedBy(world, x, y, z, entity, itemStack);
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TileRunwayNumber) {
            TileRunwayNumber tileR = ((TileRunwayNumber) tile);
            int angle = MathHelperAirPort.floor360(tileR.getBaseAngle());
            tileR.setInfo(angle, (angle + 5) / 10, tileR.side, tileR.use18mSize);
        }
    }

    @Override
    public boolean InversionBaseAngle() {
        return true;
    }

    @Override
    public TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TileRunwayNumber();
    }
}
