package airportlight.blocks.markings.runwaynumber;

import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modcore.normal.TileAngleLightNormal;
import airportlight.util.MathHelperAirPort;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import org.jetbrains.annotations.NotNull;

public class TileRunwayNumber extends TileAngleLightNormal {
    public int strAng = 0;
    public int ten = 0;
    public int one = 0;
    public EnumRunwaySide side = EnumRunwaySide.NON;

    public boolean use18mSize = false;

    public boolean setInfo(int baseAngle, int strAng, EnumRunwaySide side, boolean use18mSize) {
        int baseAngle360 = MathHelperAirPort.floor360(baseAngle);
        strAng %= 36;
        if (strAng == 0) {
            strAng = 36;
        }
        if (this.getBaseAngle() != baseAngle360 || this.strAng != strAng || !this.side.equals(side) || this.use18mSize != use18mSize) {
            this.setBaseAngle(baseAngle360);
            this.strAng = strAng;
            this.ten = strAng / 10;
            this.one = strAng % 10;
            this.side = side;
            this.use18mSize = use18mSize;
            return true;
        }
        return false;
    }

    @Override
    public void setBaseAngleWithOffset(EntityLivingBase entityLivingBase, boolean inversionBaseAngle, int step) {
        super.setBaseAngleWithOffset(entityLivingBase, inversionBaseAngle, step);
        int angle = MathHelperAirPort.floor360(this.getBaseAngle());
        this.setInfo(angle, (angle + 5) / 10, this.side, this.use18mSize);
    }


    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - 24,
                yCoord,
                zCoord - 24,
                xCoord + 25,
                yCoord + 0.2,
                zCoord + 25
        );
    }

    @Override
    public void readFromNBT(@NotNull NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        strAng = p_145839_1_.getInteger("strAng");
        strAng %= 100;
        this.ten = strAng / 10;
        this.one = strAng % 10;
        side = EnumRunwaySide.getEnum(p_145839_1_.getString("side").trim());
        use18mSize = p_145839_1_.getBoolean("use18mSize");
    }

    @Override
    public void writeToNBT(@NotNull NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setInteger("strAng", strAng);
        p_145841_1_.setString("side", side.name());
        p_145841_1_.setBoolean("use18mSize", use18mSize);
    }
}
