package airportlight.blocks.markings.runwaythresholdmarkings

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity

class RunwayThresholdMarkingsRenderer : TileEntitySpecialRenderer() {
    val model: RunwayThresholdMarkingsModel = RunwayThresholdMarkingsModel()

    override fun renderTileEntityAt(tile: TileEntity, x: Double, y: Double, z: Double, tick: Float) {
        // モデルの表示
        if (tile is RunwayThresholdMarkingsTile) {
            this.model.render(tile, x, y, z)
        }
    }
}
