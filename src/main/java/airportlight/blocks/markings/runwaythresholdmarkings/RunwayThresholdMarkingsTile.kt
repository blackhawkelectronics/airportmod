package airportlight.blocks.markings.runwaythresholdmarkings

import airportlight.modcore.normal.TileAngleLightNormal
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB

class RunwayThresholdMarkingsTile : TileAngleLightNormal() {
    var runwayWide = EnumRunwayThresholdMarkingsWide.W30
    var lineLong = 30f

    fun setInfo(runwayWide: EnumRunwayThresholdMarkingsWide, lineLong: Float): Boolean {
        if (this.runwayWide != runwayWide || this.lineLong != lineLong) {
            this.runwayWide = runwayWide
            this.lineLong = lineLong
            return true
        }
        return false
    }


    @SideOnly(Side.CLIENT)
    override fun getRenderBoundingBox(): AxisAlignedBB? {
        return AxisAlignedBB.getBoundingBox(
            (xCoord - lineLong * 1.5), yCoord.toDouble(), (zCoord - lineLong * 1.5), (xCoord + lineLong * 1.5), yCoord + 0.2, (zCoord + lineLong * 1.5)
        )
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        runwayWide = EnumRunwayThresholdMarkingsWide.getFromMode(nbt.getInteger("runwayWide"))
        lineLong = nbt.getFloat("lineLong")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("runwayWide", runwayWide.mode)
        nbt.setFloat("lineLong", lineLong)
    }
}
