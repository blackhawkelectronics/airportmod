package airportlight.blocks.markings.runwaythresholdmarkings

enum class EnumRunwayThresholdMarkingsWide(val mode: Int) {
    W15(0),
    W25(1),
    W30(2),
    W45(3),
    W60(4),
    ;

    companion object {
        fun getFromMode(mode: Int): EnumRunwayThresholdMarkingsWide {
            return EnumRunwayThresholdMarkingsWide.values()[mode]
        }
    }
}
