package airportlight.blocks.markings.runwaythresholdmarkings

import airportlight.modcore.PacketHandlerAPM.sendPacketServer
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.custombutton.EnumSimpleButton
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListHorizontal
import airportlight.modcore.gui.custombutton.StringInputGuiButton
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.inventory.GuiContainer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import kotlin.math.max
import kotlin.math.min

class RunwayThresholdMarkingsGui : GuiContainer {
    private val biRunwayWide = 20
    private val biLong = 31

    private var tile: RunwayThresholdMarkingsTile? = null
    private var inputID = 0
    private lateinit var buttonRunwayWides: EnumSimpleButtonListHorizontal<EnumRunwayThresholdMarkingsWide>
    private var buttonLinePitch: StringInputGuiButton? = null

    private var tempRunwayWide = EnumRunwayThresholdMarkingsWide.W30
    private var templong = 0f
    private var scrapString = ""

    constructor() : super(ContainerAirPort()) {}
    constructor(tile: RunwayThresholdMarkingsTile) : super(ContainerAirPort()) {
        this.tile = tile
        tempRunwayWide = tile.runwayWide
        templong = tile.lineLong
    }

    override fun onGuiClosed() {
        enterSettings()
        val isChanged = tile!!.setInfo(tempRunwayWide, templong)
        if (isChanged) {
            sendPacketServer(RunwayThresholdMarkingsSync(tile!!, tempRunwayWide, templong))
        }
    }


    override fun initGui() {
        buttonRunwayWides =
            EnumSimpleButtonListHorizontal(
                biRunwayWide,
                width / 2 - 70,
                height / 2 - 35,
                40,
                20,
                EnumRunwayThresholdMarkingsWide::class.java
            )
        buttonLinePitch = StringInputGuiButton(biLong, width / 2 - 70, height / 2 - 5, 80, 20, String.format("%.1f", tile!!.lineLong))
        buttonList.addAll(buttonRunwayWides.buttonList)
        buttonList.add(buttonLinePitch)
        buttonRunwayWides.selectButton(tempRunwayWide)
    }

    override fun actionPerformed(button: GuiButton) {
        if (button.id == biRunwayWide) {
            inputID = button.id
            tempRunwayWide = (button as EnumSimpleButton<*>).value as EnumRunwayThresholdMarkingsWide
            enterSettings()
            inputID = button.id
            scrapString = ""
            val isChanged = tile!!.setInfo(tempRunwayWide, templong)
            if (isChanged) {
                sendPacketServer(RunwayThresholdMarkingsSync(tile!!, tempRunwayWide, templong))
            }
        } else if (button.id == biLong) {
            enterSettings()
            inputID = button.id
            scrapString = ""
            button.displayString = ""
        }
    }

    private fun enterSettings() {
        if (inputID == biRunwayWide) {
            buttonRunwayWides.selectButton(tempRunwayWide)
        } else if (inputID == biLong) {
            if (scrapString.isEmpty()) {
                buttonLinePitch!!.displayString = String.format("%.1f", tile!!.lineLong)
            } else {
                buttonLinePitch!!.displayString = String.format("%.1f", scrapString.toFloat())
            }
            templong = max(30f, min(buttonLinePitch!!.displayString.toFloat(), 60f))
        }
    }


    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            drawGradientRect(40, 40, width - 40, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        GL11.glPushMatrix()
        fontRendererObj.drawString("Runway Wide  : ", width / 2 - 140, height / 2 - 30, -1, false)
        fontRendererObj.drawString("Line Long  : ", width / 2 - 140, height / 2, -1, false)
        GL11.glPopMatrix()
    }

    override fun keyTyped(eventChar: Char, keyID: Int) {
        if (keyID == 1) {
            mc.thePlayer.closeScreen()
            return
        }
        if (inputID == biLong) {
            if (keyID == Keyboard.KEY_RETURN) {
                inputID = 0
                if (scrapString.trim { it <= ' ' }.isNotEmpty()) {
                    templong = scrapString.toFloat()
                    scrapString = String.format("%.1f", templong)
                } else {
                    templong = 30f
                    scrapString = "30"
                }

                enterSettings()
                val isChanged = tile!!.setInfo(tempRunwayWide, templong)
                if (isChanged) {
                    sendPacketServer(RunwayThresholdMarkingsSync(tile!!, tempRunwayWide, templong))
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (scrapString.isNotEmpty()) {
                    scrapString = scrapString.substring(0, scrapString.length - 1)
                }
            } else {
                val strLength = scrapString.length
                var flag = false
                if (strLength <= 1 && Character.isDigit(eventChar)) {
                    flag = true
                } else if (strLength == 2 && eventChar == '.') {
                    flag = true
                } else if (strLength == 3) {
                    if (Character.isDigit(eventChar)) {
                        flag = true
                    }
                }
                if (flag) {
                    scrapString += eventChar
                }
            }
            buttonLinePitch!!.displayString = scrapString
        } else {
            super.keyTyped(eventChar, keyID)
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
