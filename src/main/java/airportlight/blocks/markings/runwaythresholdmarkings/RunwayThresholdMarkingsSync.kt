package airportlight.blocks.markings.runwaythresholdmarkings

import airportlight.modcore.PacketHandlerAPM.sendPacketAll
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.nbt.NBTTagCompound

class RunwayThresholdMarkingsSync : TileEntityMessage {
    lateinit var runwayWide: EnumRunwayThresholdMarkingsWide
    var lineLong = 0f

    @Suppress("unused")
    constructor()

    constructor(tile: RunwayThresholdMarkingsTile, runwayWide: EnumRunwayThresholdMarkingsWide, long: Float) : super(tile) {
        this.runwayWide = runwayWide
        this.lineLong = long
    }

    override fun read(buf: ByteBuf) {
        val tag = ByteBufUtils.readTag(buf)
        this.runwayWide = EnumRunwayThresholdMarkingsWide.getFromMode(tag.getInteger("runwayWide"))
        this.lineLong = tag.getFloat("lineLong")
    }

    override fun write(buf: ByteBuf) {
        val tag = NBTTagCompound()
        tag.setInteger("runwayWide", this.runwayWide.mode)
        tag.setFloat("lineLong", this.lineLong)
        ByteBufUtils.writeTag(buf, tag)
    }

    companion object : IMessageHandler<RunwayThresholdMarkingsSync, IMessage> {
        override fun onMessage(message: RunwayThresholdMarkingsSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is RunwayThresholdMarkingsTile) {
                tile.setInfo(message.runwayWide, message.lineLong)
                if (ctx.side.isServer) {
                    message.tile = tile
                    sendPacketAll(message)
                }
            }
            return null
        }
    }
}
