package airportlight.modcore.command

import airportlight.modcore.config.APMConfig
import airportlight.modsystem.navigation.autopilot.EntityAutopilotCarrier
import airportlight.radar.RadarData
import airportlight.radar.StripData
import airportlight.radar.system.RadarSystemServer
import airportlight.radar.system.syncmessage.RadarNewStripDataSerToCli
import net.minecraft.command.CommandBase
import net.minecraft.command.ICommandSender
import net.minecraft.util.ChatComponentText

/**
 * コマンド処理 Debug用
 */
class CommandAPL : CommandBase() {
    override fun getCommandName(): String {
        return "APL"
    }

    private val APEntity = "APEntity"
    private val AllKill = "AllKill"
    private val List = "List"
    private val Kill = "Kill"

    private val NLT = "NLT"
    private val R = "R"
    private val Remove = "Remove"
    private val A = "A"
    private val Add = "Add"

    private val RadarReset = "RadarReset"
    private val Reset = "Reset"

    private val EntityTypeNameList = "EntityTypeNameList"

    override fun getRequiredPermissionLevel(): Int {
        return 2
    }

    override fun processCommand(sender: ICommandSender, var2: Array<String>) {
        if (var2[0] == APEntity && var2.size > 1) {
            if (var2[1] == AllKill) {
                val list = sender.entityWorld.getLoadedEntityList()
                for (obj in list) {
                    if (obj is EntityAutopilotCarrier) {
                        obj.setDead()
                    }
                }
                sender.addChatMessage(ChatComponentText("Auto Pilot Carrier All Killed"))
            } else if (var2[1] == List) {
                val list = sender.entityWorld.getLoadedEntityList()
                sender.addChatMessage(ChatComponentText("== Auto Pilot Carrier List =="))
                for (obj in list) {
                    if (obj is EntityAutopilotCarrier) {
                        val eac = obj
                        sender.addChatMessage(ChatComponentText(eac.entityId.toString() + " : " + eac.posX.toInt() + ", " + eac.posY.toInt() + ", " + eac.posZ.toInt()))
                    }
                }
                sender.addChatMessage(ChatComponentText("== List End =="))
            } else if (var2[1] == Kill) {
                if (var2.size > 2) {
                    val entityID = var2[2].toInt()
                    val entity = sender.getEntityWorld().getEntityByID(entityID)
                    (entity as? EntityAutopilotCarrier)?.setDead()
                    sender.addChatMessage(ChatComponentText("Auto Pilot Carrier ($entityID) Killed"))
                }
            }
        } else if (var2[0] == EntityTypeNameList) {
            if (APMConfig.noneLookType != null) {
                sender.addChatMessage(ChatComponentText("------- EntityTypeNameList -------"))
                sender.addChatMessage(ChatComponentText("## Not Added ##"))
                for (name in RadarSystemServer.typeNameList) {
                    if (!(APMConfig.noneLookType!!.contains(name))) {
                        sender.addChatMessage(ChatComponentText(name))
                    }
                }
                sender.addChatMessage(ChatComponentText("## Added To List ##"))
                for (name in RadarSystemServer.typeNameList) {
                    if (APMConfig.noneLookType!!.contains(name)) {
                        sender.addChatMessage(ChatComponentText(name))
                    }
                }
                sender.addChatMessage(ChatComponentText("------- End -------"))
            }
        } else if (var2[0] == NLT) {   //Non Lock Type レーダーに表示しないEntityタイプのリスト
            if (3 <= var2.size) {
                if (var2[1] == R || var2[1] == Remove) {
                    APMConfig.removeNoneLookType(var2[2])
                    sender.addChatMessage(ChatComponentText(" APM : Radar Non Lock Type Remove ${var2[2]}"))

                } else if (var2[1] == A || var2[1] == Add) {
                    APMConfig.addNoneLookType(var2[2])
                    sender.addChatMessage(ChatComponentText(" APM : Radar Non Lock Type Add ${var2[2]}"))
                }
            } else {
                if (APMConfig.noneLookType != null) {
                    sender.addChatMessage(ChatComponentText("-----------------------"))
                    sender.addChatMessage(ChatComponentText("APM NonLock Entity Type List"))
                    sender.addChatMessage(ChatComponentText("-----------------------"))

                    var str = StringBuilder()
                    val it = APMConfig.noneLookType!!.iterator()
                    var i = 1
                    while (it.hasNext()) {
                        if (i != 1) {
                            str.append(", ")
                        }
                        str.append(it.next())
                        if (i % 6 == 0) {
                            sender.addChatMessage(ChatComponentText(" $str"))
                            str = StringBuilder()
                        }
                        i++
                    }
                    sender.addChatMessage(ChatComponentText(" $str"))
                    sender.addChatMessage(ChatComponentText("-----------------------"))
                } else {
                    sender.addChatMessage(ChatComponentText("APM NonLock Entity Type List Was Not Init"))
                }
            }
        } else if (var2[0] == RadarReset) {
            if (var2[1] == Reset) {
                RadarSystemServer.resetRadarStripData()
                RadarSystemServer.sendMessageToRadarReceivers(
                    RadarNewStripDataSerToCli(HashMap<Int, RadarData>(), HashMap<Int, StripData>())
                )
            }
        }
    }

    override fun addTabCompletionOptions(p_71516_1_: ICommandSender, vararg arg: String): List<*>? {
        if (arg.size == 1) {
            return getListOfStringsMatchingLastWord(
                arg,
                APEntity, NLT, RadarReset, EntityTypeNameList
            )
        } else if (arg.size == 2) {
            if (arg[0] == APEntity) {
                return getListOfStringsMatchingLastWord(
                    arg,
                    List, Kill, AllKill
                )
            } else if (arg[0] == NLT) {
                return getListOfStringsMatchingLastWord(
                    arg,
                    Remove, Add
                )
            }
        }
        return null
    }

    override fun getCommandUsage(p_71518_1_: ICommandSender): String {
        return "APL:getCommandUsage."
    }
}