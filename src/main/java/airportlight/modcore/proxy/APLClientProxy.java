package airportlight.modcore.proxy;

import airportlight.blocks.facility.asde.ASDERenderer;
import airportlight.blocks.facility.asde.TileASDE;
import airportlight.blocks.facility.glideslope.GlideSlopeRendere;
import airportlight.blocks.facility.glideslope.GlideSlopeTile;
import airportlight.blocks.facility.localizer.LocalizerRenderer;
import airportlight.blocks.facility.localizer.LocalizerTile;
import airportlight.blocks.facility.pbb.PBBDriverSeat;
import airportlight.blocks.facility.pbb.PBBDriverSeatRender;
import airportlight.blocks.facility.pbb.PBBRenderer;
import airportlight.blocks.facility.pbb.TilePBB;
import airportlight.blocks.facility.psr.PSRRendere;
import airportlight.blocks.facility.psr.TilePSR;
import airportlight.blocks.light.aerodromebeacon.AerodromeBeaconRenderer;
import airportlight.blocks.light.aerodromebeacon.TileAerodromeBeacon;
import airportlight.blocks.light.approachlight.ApproachLightRenderer;
import airportlight.blocks.light.approachlight.ApproachLightTile;
import airportlight.blocks.light.apronlighting.ApronLightingRenderer;
import airportlight.blocks.light.apronlighting.ApronLightingTile;
import airportlight.blocks.light.endlight.EndLightRenderer;
import airportlight.blocks.light.endlight.TileEndLight;
import airportlight.blocks.light.obstaclelight.ObstacleLightRenderer;
import airportlight.blocks.light.obstaclelight.TileObstacleLight;
import airportlight.blocks.light.overrunareaedgelight.OverrunAreaEdgeLightRenderer;
import airportlight.blocks.light.overrunareaedgelight.TileOverrunAreaEdgeLight;
import airportlight.blocks.light.papi.PAPIRenderer;
import airportlight.blocks.light.papi.TilePAPI;
import airportlight.blocks.light.runwaycenterlinelight.RunwayCenterLineLightRenderer;
import airportlight.blocks.light.runwaycenterlinelight.TileRunwayCenterLineLight;
import airportlight.blocks.light.runwaydistancemarkerlight.RunwayDistanceMarkerLightRenderer;
import airportlight.blocks.light.runwaydistancemarkerlight.TileRunwayDistanceMarkerLight;
import airportlight.blocks.light.runwayedgelight.RunwayEdgeLightRenderer;
import airportlight.blocks.light.runwayedgelight.TileRunwayEdgeLight;
import airportlight.blocks.light.runwaytouchdownzonelight.RunwayTouchdownZoneLightRenderer;
import airportlight.blocks.light.runwaytouchdownzonelight.TileRunwayTouchdownZoneLight;
import airportlight.blocks.light.taxiwaycenterlinelight.TaxiwayCenterLineLightRenderer;
import airportlight.blocks.light.taxiwaycenterlinelight.TileTaxiwayCenterLineLight;
import airportlight.blocks.light.taxiwayedgelight.TaxiwayEdgeLightRenderer;
import airportlight.blocks.light.taxiwayedgelight.TileTaxiwayEdgeLight;
import airportlight.blocks.light.winddirectionindicatorlight.TileWindDirectionIndicatorLight;
import airportlight.blocks.light.winddirectionindicatorlight.WindDirectionIndicatorLightRenderer;
import airportlight.blocks.markings.gloundsine.GroundSineRenderer;
import airportlight.blocks.markings.gloundsine.GroundSineTile;
import airportlight.blocks.markings.guidepanel.GuidePanelRenderer;
import airportlight.blocks.markings.guidepanel.TileGuidePanel;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsRenderer;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsTile;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsRenderer;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsTile;
import airportlight.blocks.markings.runwaynumber.RunwayNumberRenderer;
import airportlight.blocks.markings.runwaynumber.TileRunwayNumber;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsRenderer;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsTile;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsRenderer;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsTile;
import airportlight.modcore.KeyInputObserver;
import airportlight.modcore.ReUseModelDataBank;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.modsystem.navigation.NavReceiverGui;
import airportlight.modsystem.navigation.ils.CommandILSCli;
import airportlight.modsystem.navigation.vordme.TileVORDME;
import airportlight.modsystem.navigation.vordme.VORDMERenderer;
import airportlight.towingcar.EntityTowingCar;
import airportlight.towingcar.RenderTowingCar;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.ClientCommandHandler;

public class APLClientProxy extends APLProxy {
    private NavReceiverGui ilsGui;
    Minecraft mc;

    @Override
    public void init() {
        ilsGui = new NavReceiverGui();
        mc = Minecraft.getMinecraft();
        ClientCommandHandler.instance.registerCommand(new CommandILSCli());
        KeyInputObserver.registerClientActions();
    }

    @Override
    public void registerRenderer() {
        ModelSwitcherDataBank.registerModelClass(ReUseModelDataBank.instance);
        // レーダーブロックのカスタムレンダリングの登録

        //facility
        ClientRegistry.bindTileEntitySpecialRenderer(TileAerodromeBeacon.class, new AerodromeBeaconRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(ApproachLightTile.class, new ApproachLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEndLight.class, new EndLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(GlideSlopeTile.class, new GlideSlopeRendere());
        ClientRegistry.bindTileEntitySpecialRenderer(TileObstacleLight.class, new ObstacleLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileOverrunAreaEdgeLight.class, new OverrunAreaEdgeLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TilePAPI.class, new PAPIRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TilePBB.class, new PBBRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileRunwayCenterLineLight.class, new RunwayCenterLineLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileRunwayDistanceMarkerLight.class, new RunwayDistanceMarkerLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileRunwayEdgeLight.class, new RunwayEdgeLightRenderer());

        //light
        ClientRegistry.bindTileEntitySpecialRenderer(TileRunwayTouchdownZoneLight.class, new RunwayTouchdownZoneLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileTaxiwayCenterLineLight.class, new TaxiwayCenterLineLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileTaxiwayEdgeLight.class, new TaxiwayEdgeLightRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileVORDME.class, new VORDMERenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileASDE.class, new ASDERenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(LocalizerTile.class, new LocalizerRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(ApronLightingTile.class, new ApronLightingRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TilePSR.class, new PSRRendere());
        ClientRegistry.bindTileEntitySpecialRenderer(TileWindDirectionIndicatorLight.class, new WindDirectionIndicatorLightRenderer());

        //markings
        ClientRegistry.bindTileEntitySpecialRenderer(GroundSineTile.class, new GroundSineRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileGuidePanel.class, new GuidePanelRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(RunwayAimingPointMarkingsTile.class, new RunwayAimingPointMarkingsRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(RunwayHoldPositionMarkingsTile.class, new RunwayHoldPositionMarkingsRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileRunwayNumber.class, new RunwayNumberRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(RunwayThresholdMarkingsTile.class, new RunwayThresholdMarkingsRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(RunwayTouchdownZoneMarkingsTile.class, new RunwayTouchdownZoneMarkingsRenderer());

        RenderingRegistry.registerEntityRenderingHandler(EntityTowingCar.class, new RenderTowingCar());
        RenderingRegistry.registerEntityRenderingHandler(PBBDriverSeat.class, new PBBDriverSeatRender());
    }

    @Override
    public void onRenderTickPost() {
        if (mc != null && mc.thePlayer != null) {
            ilsGui.drawGui(this.mc.thePlayer, this.mc.gameSettings.thirdPersonView != 0);
        } else {
            mc = Minecraft.getMinecraft();
        }
    }
}
