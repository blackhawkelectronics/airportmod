package airportlight.modcore.proxy;

public abstract class APLProxy {
    public void init() {
    }

    public void onRenderTickPost() {
    }

    public void registerRenderer() {
    }
}
