package airportlight.modcore.config

import airportlight.ModAirPortLight
import airportlight.modcore.DisplayListIDs
import airportlight.modcore.ReUseModelDataBank
import airportlight.modsystem.ModelSwitcherDataBank
import airportlight.modsystem.navigation.ils.CommandILSManager
import cpw.mods.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent
import cpw.mods.fml.common.event.FMLPreInitializationEvent
import cpw.mods.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.common.config.Configuration

object APMConfig {
    lateinit var config: Configuration

    @kotlin.jvm.JvmField
    val GENERAL = "general"

    var ApproachAng = 5
        set(angDeg: Int) {
            val prop = config[GENERAL, "ApproachAng", 5]
            prop.set(angDeg)
            syncConfig()
        }


    @kotlin.jvm.JvmField
    var UseWeightModel: Boolean = true

    var ILSWideAngRad = 0.35
        set(angRad: Double) {
            val prop = config[GENERAL, "ILSWideAng", 0.35]
            prop.set(angRad)
            syncConfig()
        }

    var noneLookType: Set<String>? = null
        private set

    @JvmStatic
    fun loadConfig(event: FMLPreInitializationEvent) {
        try {
            // net.minecraftforge.common.config.Configurationのインスタンスを生成する。
            config = Configuration(event.suggestedConfigurationFile, "1.0.1", true)
            // 初期化する。
            initConfig()
            // コンフィグファイルの内容を変数と同期させる。
            syncConfig()
        } catch (exception: Exception) {
            try {
                event.suggestedConfigurationFile.delete()
                // net.minecraftforge.common.config.Configurationのインスタンスを生成する。
                config = Configuration(event.suggestedConfigurationFile, "1.0.1", true)
                // 初期化する。
                initConfig()
                // コンフィグファイルの内容を変数と同期させる。
                syncConfig()
            } catch (exception2: Exception) {
                throw RuntimeException(exception2)
            }
        }
    }

    /**
     * コンフィグを初期化する。
     */
    private fun initConfig() {
        // カテゴリのコメントなどを設定する。
        // General
        config.addCustomCategoryComment(GENERAL, "A settings of Airport Mod.")
    }


    /**
     * コンフィグを同期する。
     */
    private fun syncConfig() {
        //ApproachAng のSetterで再起ループが起きるので手動実装
        val approachAng = config.getInt("ApproachAng", GENERAL, 5, 0, 90, "PAPI/ILS ApproachAng.")
        val prop = config[GENERAL, "ApproachAng", 5]
        prop.set(approachAng)

        CommandILSManager.ilsApproachAngDeg = ApproachAng.toDouble()
        CommandILSManager.ilsApproachAngRad = Math.toRadians(
            CommandILSManager.ilsApproachAngDeg
        )
        UseWeightModel = config.getBoolean("UseWeightModel", GENERAL, true, "Use Weight Model.")
        CommandILSManager.ilsWideAngRad =
            config.getFloat("ILSWideAng", GENERAL, 0.35f, 0.001f, 1.57f, "ILS Enabled Ang.").toDouble()
        noneLookType = config.getStringList(
            "undetectedEntities",
            GENERAL,
            defaultUndetectedEntities,
            "entities who radar shouldn't see"
        ).toSet()
        config.save()
    }


    object EventHandler {
        @SubscribeEvent
        fun onConfigChangedEvent(e: OnConfigChangedEvent) {
            if (e.modID == ModAirPortLight.MOD_ID) {
                val oldUseWeightModel = UseWeightModel
                syncConfig()
                if (UseWeightModel != oldUseWeightModel) {
                    DisplayListIDs.IDInit()
                    ReUseModelDataBank.noninit = true
                    ModelSwitcherDataBank.switchModel(UseWeightModel)
                }
            }
        }
    }


    private val defaultUndetectedEntities = arrayOf(
        "Pig",
        "Cow",
        "MushroomCow",
        "Sheep",
        "Chicken",
        "Squid",
        "Wolf",
        "Ozelot",
        "EntityHorse",
        "EntitySnowman",
        "VillagerGolem",
        "Villager",
        "Bat",
        "Zombie",
        "Skeleton",
        "Creeper",
        "Spider",
        "Slime",
        "Enderman",
        "CaveSpider",
        "Silverfish",
        "Witch",
        "PigZombie",
        "Ghast",
        "LavaSlime",
        "Blaze",
        "AegisSystemMod.Seat",
        "RTM.RTM.E.Floor",
        "RTM.RTM.E.Bogie",
        "RTM.RTM.E.Motorman",
        "RTM.RTM.E.ATC",
        "RTM.RTM.E.TrainDetector",
        "RTM.RTM.E.BumpingPost",
        "RTM.RTM.E.Bullet",
        "mcheli.MCH.E.Bullet",
        "mcheli.MCH.E.Flare",
    )

    /**
     * NoneLookType に追加
     * @return true:追加成功 false:追加済み
     */
    fun addNoneLookType(newName: String): Boolean {
        val prop = config[GENERAL, "undetectedEntities", defaultUndetectedEntities]
        val base = prop.stringList
        if (!base.contains(newName)) {
            val new = base.copyOf(base.size + 1)
            new[new.lastIndex] = newName
            prop.set(new)
            syncConfig()
            return true
        }
        return false
    }

    fun removeNoneLookType(removeName: String) {
        val prop = config[GENERAL, "undetectedEntities", defaultUndetectedEntities]
        var oldLen = 0
        while (prop.stringList.size != oldLen) {
            val base = prop.stringList
            val new = arrayOfNulls<String>(base.size - 1)
            val index = base.indexOf(removeName)
            System.arraycopy(base, 0, new, 0, index)
            System.arraycopy(base, index + 1, new, index, new.size - index)
            prop.set(new)
            oldLen = new.size
        }
        syncConfig()
    }
}