package airportlight.modcore.config;

import org.lwjgl.input.Keyboard;

import java.util.HashMap;

public class APMKeyConfig {
    public static final String str_CameraReset = "Drive_CameraReset";
    public static final String str_Brake = "Drive_Brake";

    public static final String str_Towing_PickUp = "TowingCarDrive_PickUp";
    public static final String str_Towing_Release = "TowingCarDrive_Release";
    public static final String str_Towing_Open = "TowingCarDrive_Open";
    public static final String str_Towing_Close = "TowingCarDrive_Close";

    public static final String str_PBB_HeadLeft = "PBB_Left";
    public static final String str_PBB_HeadRight = "PBB_Right";
    public static final String str_PBB_HeadUp = "PBB_UP";
    public static final String str_PBB_HeadDown = "PBB_Down";
    private static final HashMap<String, Integer> hash = new HashMap<String, Integer>();

    public static void setKeyConfig(String name, int id) {
        hash.put(name, id);
    }

    public static int getKeyidFromName(String name) {
        return hash.get(name);
    }

    public static void setKeyConfigDefaults() {
        APMKeyConfig.setKeyConfig(str_CameraReset, Keyboard.KEY_LCONTROL);
        APMKeyConfig.setKeyConfig(str_Brake, Keyboard.KEY_SPACE);

        APMKeyConfig.setKeyConfig(str_Towing_Close, Keyboard.KEY_C);
        APMKeyConfig.setKeyConfig(str_Towing_Open, Keyboard.KEY_O);
        APMKeyConfig.setKeyConfig(str_Towing_PickUp, Keyboard.KEY_P);
        APMKeyConfig.setKeyConfig(str_Towing_Release, Keyboard.KEY_R);

        APMKeyConfig.setKeyConfig(str_PBB_HeadLeft, Keyboard.KEY_LEFT);
        APMKeyConfig.setKeyConfig(str_PBB_HeadRight, Keyboard.KEY_RIGHT);
        APMKeyConfig.setKeyConfig(str_PBB_HeadUp, Keyboard.KEY_UP);
        APMKeyConfig.setKeyConfig(str_PBB_HeadDown, Keyboard.KEY_DOWN);
    }
}