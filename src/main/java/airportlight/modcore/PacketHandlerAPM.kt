package airportlight.modcore

import airportlight.blocks.facility.localizer.LocalizerSync
import airportlight.blocks.facility.pbb.PBBAngShareMessage
import airportlight.blocks.facility.pbb.PBBDataFlagMessage
import airportlight.blocks.facility.pbb.PBBDataSyncMessage
import airportlight.blocks.facility.psr.PSRDataShare
import airportlight.blocks.light.approachlight.ApproachLightSync
import airportlight.blocks.light.obstaclelight.ObstacleLightSync
import airportlight.blocks.light.papi.PAPISync
import airportlight.blocks.light.runwaydistancemarkerlight.RunwayDistanceMarkerSync
import airportlight.blocks.markings.gloundsine.GroundSineSync
import airportlight.blocks.markings.guidepanel.GuidePanelSync
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsSync
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsSync
import airportlight.blocks.markings.runwaynumber.SyncRunwayNumberData
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsSync
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsSync
import airportlight.landingpoint.LandingPointSync
import airportlight.modcore.normal.LightInvertShareMessage
import airportlight.modsystem.navigation.autopilot.*
import airportlight.modsystem.navigation.frequency.RequestFreqManagerData
import airportlight.modsystem.navigation.frequency.SendToFreqManagerData
import airportlight.modsystem.navigation.navsetting.MessageReqNavSetting
import airportlight.modsystem.navigation.vordme.VORDMESync
import airportlight.radar.artsdisplay.ArtsDisplayDataSync
import airportlight.radar.artsdisplay.ArtsDisplayTileSync
import airportlight.radar.system.syncmessage.RadarDataUpdateSerToCli
import airportlight.radar.system.syncmessage.RadarNewStripDataSerToCli
import airportlight.radar.system.syncmessage.StripDataMakeDeleteSync
import airportlight.radar.system.syncmessage.StripDataWriteSync
import airportlight.towingcar.TowingDataFlagMessage
import airportlight.util.Logger
import cpw.mods.fml.common.network.NetworkRegistry
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.relauncher.Side
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.ChunkCoordIntPair
import net.minecraft.world.WorldServer

object PacketHandlerAPM {
    private val INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("AirportMod")

    @JvmStatic
    fun init() {
        registerMessage(TowingDataFlagMessage::class.java, TowingDataFlagMessage::class.java, 0x01, Side.SERVER)
        registerMessage(SyncRunwayNumberData::class.java, SyncRunwayNumberData::class.java, 0x02, Side.SERVER)
        registerMessage(LandingPointSync::class.java, LandingPointSync::class.java, 0x03, Side.SERVER)
        registerMessage(RequestFreqManagerData::class.java, RequestFreqManagerData::class.java, 0x04, Side.SERVER)
        registerMessage(MessageAPRollSetting::class.java, MessageAPRollSetting::class.java, 0x06, Side.SERVER)
        registerMessage(MessageAPEngageSetting::class.java, MessageAPEngageSetting::class.java, 0x07, Side.SERVER)
        registerMessage(MessageAPPitchSetting::class.java, MessageAPPitchSetting::class.java, 0x08, Side.SERVER)
        registerMessage(MessageAPThrottleSetting::class.java, MessageAPThrottleSetting::class.java, 0x09, Side.SERVER)
        registerMessage(MessageEntityUseFreqData::class.java, MessageEntityUseFreqData::class.java, 0x0a, Side.SERVER)
        registerMessage(MessageReqNavSetting::class.java, MessageReqNavSetting::class.java, 0x0b, Side.SERVER)
        registerMessage(MessageAPCourseSetting::class.java, MessageAPCourseSetting::class.java, 0x0c, Side.SERVER)
        registerMessage(VORDMESync::class.java, VORDMESync::class.java, 0x0d, Side.SERVER)
        registerMessage(GuidePanelSync::class.java, GuidePanelSync::class.java, 0x0f, Side.SERVER)
        registerMessage(ApproachLightSync::class.java, ApproachLightSync::class.java, 0x10, Side.SERVER)
        registerMessage(LocalizerSync::class.java, LocalizerSync::class.java, 0x11, Side.SERVER)
        registerMessage(StripDataWriteSync::class.java, StripDataWriteSync::class.java, 0x14, Side.SERVER)
        registerMessage(ArtsDisplayTileSync, ArtsDisplayTileSync::class.java, 0x15, Side.SERVER)
        registerMessage(ArtsDisplayDataSync, ArtsDisplayDataSync::class.java, 0x16, Side.SERVER)
        registerMessage(StripDataMakeDeleteSync, StripDataMakeDeleteSync::class.java, 0x17, Side.SERVER)
        registerMessage(PBBDataFlagMessage, PBBDataFlagMessage::class.java, 0x18, Side.SERVER)
        registerMessage(PBBAngShareMessage, PBBAngShareMessage::class.java, 0x19, Side.SERVER)
        registerMessage(RunwayDistanceMarkerSync::class.java, RunwayDistanceMarkerSync::class.java, 0x20, Side.SERVER)
        registerMessage(GroundSineSync::class.java, GroundSineSync::class.java, 0x21, Side.SERVER)
        registerMessage(PAPISync::class.java, PAPISync::class.java, 0x22, Side.SERVER)
        registerMessage(PBBDataSyncMessage::class.java, PBBDataSyncMessage::class.java, 0x23, Side.SERVER)
        registerMessage(PSRDataShare::class.java, PSRDataShare::class.java, 0x24, Side.SERVER)
        registerMessage(RunwayThresholdMarkingsSync, RunwayThresholdMarkingsSync::class.java, 0x25, Side.SERVER)
        registerMessage(RunwayTouchdownZoneMarkingsSync, RunwayTouchdownZoneMarkingsSync::class.java, 0x26, Side.SERVER)
        registerMessage(RunwayHoldPositionMarkingsSync, RunwayHoldPositionMarkingsSync::class.java, 0x27, Side.SERVER)
        registerMessage(RunwayAimingPointMarkingsSync, RunwayAimingPointMarkingsSync::class.java, 0x28, Side.SERVER)
        registerMessage(ObstacleLightSync::class.java, ObstacleLightSync::class.java, 0x29, Side.SERVER)


        registerMessage(SyncRunwayNumberData::class.java, SyncRunwayNumberData::class.java, 0x02, Side.CLIENT)
        registerMessage(LandingPointSync::class.java, LandingPointSync::class.java, 0x03, Side.CLIENT)
        registerMessage(SendToFreqManagerData::class.java, SendToFreqManagerData::class.java, 0x05, Side.CLIENT)
        registerMessage(MessageAPRollSetting::class.java, MessageAPRollSetting::class.java, 0x06, Side.CLIENT)
        registerMessage(MessageAPEngageSetting::class.java, MessageAPEngageSetting::class.java, 0x7, Side.CLIENT)
        registerMessage(MessageAPPitchSetting::class.java, MessageAPPitchSetting::class.java, 0x08, Side.CLIENT)
        registerMessage(MessageAPThrottleSetting::class.java, MessageAPThrottleSetting::class.java, 0x09, Side.CLIENT)
        registerMessage(MessageEntityUseFreqData::class.java, MessageEntityUseFreqData::class.java, 0x0a, Side.CLIENT)
        registerMessage(MessageAPCourseSetting::class.java, MessageAPCourseSetting::class.java, 0x0c, Side.CLIENT)
        registerMessage(VORDMESync::class.java, VORDMESync::class.java, 0x0d, Side.CLIENT)
        registerMessage(LightInvertShareMessage::class.java, LightInvertShareMessage::class.java, 0x0e, Side.CLIENT)
        registerMessage(GuidePanelSync::class.java, GuidePanelSync::class.java, 0x0f, Side.CLIENT)
        registerMessage(ApproachLightSync::class.java, ApproachLightSync::class.java, 0x10, Side.CLIENT)
        registerMessage(LocalizerSync::class.java, LocalizerSync::class.java, 0x11, Side.CLIENT)
        registerMessage(RadarDataUpdateSerToCli::class.java, RadarDataUpdateSerToCli::class.java, 0x12, Side.CLIENT)
        registerMessage(RadarNewStripDataSerToCli::class.java, RadarNewStripDataSerToCli::class.java, 0x13, Side.CLIENT)
        registerMessage(StripDataWriteSync::class.java, StripDataWriteSync::class.java, 0x14, Side.CLIENT)
        registerMessage(ArtsDisplayTileSync, ArtsDisplayTileSync::class.java, 0x15, Side.CLIENT)
        registerMessage(ArtsDisplayDataSync, ArtsDisplayDataSync::class.java, 0x16, Side.CLIENT)
        registerMessage(StripDataMakeDeleteSync, StripDataMakeDeleteSync::class.java, 0x17, Side.CLIENT)
        registerMessage(PBBDataFlagMessage, PBBDataFlagMessage::class.java, 0x18, Side.CLIENT)
        registerMessage(PBBAngShareMessage, PBBAngShareMessage::class.java, 0x19, Side.CLIENT)
        registerMessage(RunwayDistanceMarkerSync::class.java, RunwayDistanceMarkerSync::class.java, 0x20, Side.CLIENT)
        registerMessage(GroundSineSync::class.java, GroundSineSync::class.java, 0x21, Side.CLIENT)
        registerMessage(PAPISync::class.java, PAPISync::class.java, 0x22, Side.CLIENT)
        registerMessage(PBBDataSyncMessage::class.java, PBBDataSyncMessage::class.java, 0x23, Side.CLIENT)
        registerMessage(PSRDataShare::class.java, PSRDataShare::class.java, 0x24, Side.CLIENT)
        registerMessage(RunwayThresholdMarkingsSync, RunwayThresholdMarkingsSync::class.java, 0x25, Side.CLIENT)
        registerMessage(RunwayTouchdownZoneMarkingsSync, RunwayTouchdownZoneMarkingsSync::class.java, 0x26, Side.CLIENT)
        registerMessage(RunwayHoldPositionMarkingsSync, RunwayHoldPositionMarkingsSync::class.java, 0x27, Side.CLIENT)
        registerMessage(RunwayAimingPointMarkingsSync, RunwayAimingPointMarkingsSync::class.java, 0x28, Side.CLIENT)
        registerMessage(ObstacleLightSync::class.java, ObstacleLightSync::class.java, 0x29, Side.CLIENT)
    }

    private fun <REQ : IMessage, REPLY : IMessage?> registerMessage(
        messageHandler: Class<out IMessageHandler<REQ, REPLY>>,
        requestMessageType: Class<REQ>,
        discriminator: Int,
        sendTo: Side
    ) {
        INSTANCE.registerMessage(messageHandler, requestMessageType, discriminator, sendTo)
    }

    private fun <REQ : IMessage, REPLY : IMessage?> registerMessage(
        messageHandler: IMessageHandler<in REQ, out REPLY>,
        requestMessageType: Class<REQ>,
        discriminator: Int,
        sendTo: Side
    ) {
        INSTANCE.registerMessage(messageHandler, requestMessageType, discriminator, sendTo)
    }

    @JvmStatic
    fun sendPacketServer(message: IMessage) {
        INSTANCE.sendToServer(message)
    }

    @JvmStatic
    fun sendPacketAll(message: IMessage) {
        INSTANCE.sendToAll(message)
    }

    @JvmStatic
    fun sendPacketEPM(message: IMessage, EPM: EntityPlayerMP) {
        INSTANCE.sendTo(message, EPM)
    }

    @JvmStatic
    fun sendPacketAround(message: IMessage, dimension: Int, x: Double, y: Double, z: Double, range: Double) {
        INSTANCE.sendToAllAround(message, TargetPoint(dimension, x, y, z, range))
    }

    @JvmStatic
    fun sendPacketAround(message: IMessage, tileEntity: TileEntity, range: Double) {
        INSTANCE.sendToAllAround(
            message, TargetPoint(
                tileEntity.worldObj.provider.dimensionId,
                tileEntity.xCoord + 0.5,
                tileEntity.yCoord + 0.5,
                tileEntity.zCoord + 0.5, range
            )
        )
    }

    @JvmStatic
    fun getLoadingPlayersAt(world: WorldServer, chunkX: Int, chunkZ: Int): ArrayList<EntityPlayerMP> {

        val ret = ArrayList<EntityPlayerMP>()
        for (player in world.playerEntities) {
            if (player is EntityPlayerMP) {
                if (player.loadedChunks.contains(ChunkCoordIntPair(chunkX, chunkZ))) {
                    ret.add(player)
                }
            }
        }
        return ret
    }

    @JvmStatic
    fun sendPacketPlayers(message: IMessage, players: Iterable<EntityPlayerMP>) {
        for (player in players) {
            sendPacketEPM(message, player)
        }
    }

    @JvmStatic
    fun sendPacketPlayersLoadingTheBlock(message: IMessage, blockTile: TileEntity) {
        val world = blockTile.worldObj
        if (world !is WorldServer) {
            Logger.error("sendPacketPlayersLoadingTheBlock called for client world with \$message for \${block.vec}")
            return
        }
        sendPacketPlayers(message, getLoadingPlayersAt(world, blockTile.xCoord / 16, blockTile.zCoord / 16))
    }
}
