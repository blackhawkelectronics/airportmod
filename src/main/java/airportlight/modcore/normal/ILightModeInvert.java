package airportlight.modcore.normal;

import airportlight.modcore.PacketHandlerAPM;
import net.minecraft.world.World;

/**
 * Use For TileEntity
 */
public interface ILightModeInvert {
    default void invert(World world) {
        if (!world.isRemote) {
            setInvert(!getInvert());
            PacketHandlerAPM.sendPacketAll(new LightInvertShareMessage(this));
        }
    }

    boolean getInvert();

    void setInvert(boolean invert);
}
