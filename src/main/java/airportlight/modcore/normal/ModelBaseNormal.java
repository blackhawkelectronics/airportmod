package airportlight.modcore.normal;

import airportlight.ModAirPortLight;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.jetbrains.annotations.NotNull;

@SideOnly(Side.CLIENT)
public abstract class ModelBaseNormal<T extends TileEntity> extends ModelBase {
    protected final TextureManager textureManager;
    protected ResourceLocation textureGray = new ResourceLocation(ModAirPortLight.DOMAIN, "gray.png");
    //protected final ResourceLocation textureGray, textureGreen, textureRed, textureWhite, textureDarkGreen, textureDarkRed, textureBlew, textureOrange, textureYellow;

    public ModelBaseNormal() {
        this.textureManager = Minecraft.getMinecraft().getTextureManager();
//        this.textureGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "Green.png");
//        this.textureRed = new ResourceLocation(ModAirPortLight.DOMAIN, "Red.png");
//        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "White.png");
//        this.textureDarkGreen = new ResourceLocation(ModAirPortLight.DOMAIN, "DarkGreen.png");
//        this.textureDarkRed = new ResourceLocation(ModAirPortLight.DOMAIN, "DarkRed.png");
//        this.textureBlew = new ResourceLocation(ModAirPortLight.DOMAIN, "Blew.png");
//        this.textureOrange = new ResourceLocation(ModAirPortLight.DOMAIN, "Orange.png");
//        this.textureYellow = new ResourceLocation(ModAirPortLight.DOMAIN, "Yellow.png");
    }

    abstract public void render(@NotNull T tile, double x, double y, double z);
}