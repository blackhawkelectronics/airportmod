package airportlight.modcore.normal;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public abstract class AngleLightModelBase<T extends TileAngleLightNormal> extends ModelBaseNormalLight<T> {
    @Override
    public void render(@NotNull T tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-((TileAngleLightNormal) tile).getBaseAngle(), 0.0, 1.0, 0.0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureGray);
        ModelBodyRender();
        EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
        double dx = tile.xCoord - player.posX + 0.5;
        double dy = tile.yCoord - player.posY;
        double dz = tile.zCoord - player.posZ + 0.5;
        double dist = Math.sqrt(dx * dx + dy * dy + dz * dz);
        float worldLight = tile.getWorldObj().getSunBrightnessFactor(1.0f);
        double ang = -Math.toDegrees(Math.atan2(dx, dz));
        renderAngleLight(player, (TileAngleLightNormal) tile, dist, worldLight, ang);
        GL11.glPopMatrix();
    }

    abstract protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng);
}
