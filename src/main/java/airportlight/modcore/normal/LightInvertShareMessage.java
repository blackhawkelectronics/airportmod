package airportlight.modcore.normal;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

public class LightInvertShareMessage extends TileEntityMessage implements IMessageHandler<LightInvertShareMessage, IMessage> {
    boolean invertValue;

    public LightInvertShareMessage() {
    }

    public LightInvertShareMessage(ILightModeInvert iinvert) {
        super((TileEntity) iinvert);
        invertValue = iinvert.getInvert();
    }

    @Override
    public void read(ByteBuf buf) {
        invertValue = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeBoolean(invertValue);
    }

    @Override
    public IMessage onMessage(LightInvertShareMessage message, MessageContext ctx) {
        TileEntity tileEntity = message.getTileEntity(ctx);
        if (tileEntity instanceof ILightModeInvert) {
            ((ILightModeInvert) tileEntity).setInvert(message.invertValue);
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
