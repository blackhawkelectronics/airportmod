package airportlight.modcore.normal;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

import java.util.Objects;

@SideOnly(Side.CLIENT)
public abstract class ModelBaseNormalStatic<T extends TileEntity> extends ModelBaseNormalLight<T> {
    @Override
    public void render(@NotNull T tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        textureManager.bindTexture(this.textureGray);
        this.ModelBodyRender();
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{1f, 1f, 1f, 1f}));
        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        assert player != null;
        double dx = tile.xCoord - player.posX + 0.5;
        double dy = tile.yCoord - player.posY;
        double dz = tile.zCoord - player.posZ + 0.5;
        double dist = Math.sqrt(dx * dx + dy * dy + dz * dz);
        float worldLight = Objects.requireNonNull(tile.getWorldObj()).getSunBrightnessFactor(1.0f);
        ;
        renderLight(player, tile, dist, worldLight);
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, toFloatBuffer(new float[]{0, 0, 0, 1}));
        GL11.glPopMatrix();
    }

    protected abstract void renderLight(EntityPlayer player, TileEntity tile, double dist, float worldLight);
}