package airportlight.modcore;

import org.lwjgl.opengl.GL11;

public class DisplayListIDs {
    public static int AerodromeBody = -1;
    public static int AerodromeLightON = -1;
    public static int AerodromeLightOFF = -1;

    public static int ApproachLightBody = -1;

    public static int ApronLightingBody = -1;
    public static int ApronLightingRamp = -1;

    public static int ASDEBody = -1;

    public static int GlideSlopeBody = -1;

    public static int LocalizerBody = -1;

    public static int ObstacleBody = -1;
    public static int ObstacleLightOFF = -1;

    public static int PAPIBody = -1;
    public static int PAPILightNear = -1;
    public static int PAPILightFar = -1;

    public static int PBBFooterbody = -1;
    public static int PBBFooterRoatebody = -1;
    public static int PBBFooterPanel = -1;
    public static int PBBBridgeFooter = -1;
    public static int PBBBridgeCenter = -1;
    public static int PBBBridgeHead = -1;
    public static int PBBBridgeFooterLong = -1;
    public static int PBBBridgeCenterLong = -1;
    public static int PBBBridgeHeadLong = -1;
    public static int PBBHeadbody = -1;
    public static int PBBHeadPanel = -1;
    public static int PBBLegbody = -1;
    public static int PBBLegYaw = -1;
    public static int PBBLegWheel = -1;

    public static int PSRBody = -1;
    public static int PSRHead = -1;

    public static int REDLON = -1;

    public static int CenterLineBody = -1;
    public static int CenterLineLightLEDON = -1;
    public static int CenterLineLightLEDOFF = -1;

    public static int RunwayDistanceMarkerLight = -1;

    public static int EdgeBody = -1;
    public static int EdgeLightON = -1;
    public static int EdgeLightOFF = -1;

    public static int TouchdownZone = -1;

    public static int VORDME = -1;

    public static void IDInit() {
        GL11.glDeleteLists(AerodromeBody, 1);
        AerodromeBody = -1;
        GL11.glDeleteLists(AerodromeLightON, 1);
        AerodromeLightON = -1;
        GL11.glDeleteLists(AerodromeLightOFF, 1);
        AerodromeLightOFF = -1;

        GL11.glDeleteLists(ASDEBody, 1);
        ASDEBody = -1;
        GL11.glDeleteLists(GlideSlopeBody, 1);
        GlideSlopeBody = -1;
        GL11.glDeleteLists(LocalizerBody, 1);
        LocalizerBody = -1;

        GL11.glDeleteLists(ObstacleBody, 1);
        ObstacleBody = -1;
        GL11.glDeleteLists(ObstacleLightOFF, 1);
        ObstacleLightOFF = -1;

        GL11.glDeleteLists(PAPIBody, 1);
        PAPIBody = -1;
        GL11.glDeleteLists(PAPILightNear, 1);
        PAPILightNear = -1;
        GL11.glDeleteLists(PAPILightFar, 1);
        PAPILightFar = -1;

        GL11.glDeleteLists(PBBFooterbody, 1);
        PBBFooterbody = -1;
        GL11.glDeleteLists(PBBFooterRoatebody, 1);
        PBBFooterRoatebody = -1;
        GL11.glDeleteLists(PBBFooterPanel, 1);
        PBBFooterPanel = -1;
        GL11.glDeleteLists(PBBBridgeFooter, 1);
        PBBBridgeFooter = -1;
        GL11.glDeleteLists(PBBBridgeCenter, 1);
        PBBBridgeCenter = -1;
        GL11.glDeleteLists(PBBBridgeHead, 1);
        PBBBridgeHead = -1;
        GL11.glDeleteLists(PBBBridgeFooterLong, 1);
        PBBBridgeFooterLong = -1;
        GL11.glDeleteLists(PBBBridgeCenterLong, 1);
        PBBBridgeCenterLong = -1;
        GL11.glDeleteLists(PBBBridgeHeadLong, 1);
        PBBBridgeHeadLong = -1;
        GL11.glDeleteLists(PBBHeadbody, 1);
        PBBHeadbody = -1;
        GL11.glDeleteLists(PBBHeadPanel, 1);
        PBBHeadPanel = -1;
        GL11.glDeleteLists(PBBLegYaw, 1);
        PBBLegYaw = -1;
        GL11.glDeleteLists(PBBLegWheel, 1);
        PBBLegWheel = -1;

        GL11.glDeleteLists(PSRBody, 1);
        PSRBody = -1;

        GL11.glDeleteLists(PSRHead, 1);
        PSRHead = -1;

        GL11.glDeleteLists(REDLON, 1);
        REDLON = -1;

        GL11.glDeleteLists(CenterLineBody, 1);
        CenterLineBody = -1;
        GL11.glDeleteLists(CenterLineLightLEDON, 1);
        CenterLineLightLEDON = -1;
        GL11.glDeleteLists(CenterLineLightLEDOFF, 1);
        CenterLineLightLEDOFF = -1;

        GL11.glDeleteLists(EdgeBody, 1);
        EdgeBody = -1;
        GL11.glDeleteLists(EdgeLightON, 1);
        EdgeLightON = -1;
        GL11.glDeleteLists(EdgeLightOFF, 1);
        EdgeLightOFF = -1;

        GL11.glDeleteLists(TouchdownZone, 1);
        TouchdownZone = -1;

        GL11.glDeleteLists(VORDME, 1);
        VORDME = -1;
    }
}
