package airportlight.modcore.gui.Indicator

import net.minecraft.client.Minecraft
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class BarIndicator(xPosition: Double, yPosition: Double, size: Double, indicator: Boolean = false) :
    Indicator(xPosition, yPosition, size, indicator) {
    private val indicatorTextures = ResourceLocation("airportlight:textures/gui/ap_indicator.png")
    override fun drawButton(mc: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        mc.textureManager.bindTexture(indicatorTextures)
        val yOffset = if (indicator) {
            128
        } else {
            0
        }

        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        GL11.glTranslated(xPos, yPos, 0.0)
        GL11.glScaled(scale, scale, scale)
        drawTexturedModalRect(0, 0, 0, yOffset, 255, 60)
        GL11.glPopMatrix()
    }
}