package airportlight.modcore.gui

import airportlight.modcore.gui.custombutton.IdoWheel
import net.minecraft.client.gui.inventory.GuiContainer
import net.minecraft.inventory.Container
import org.lwjgl.input.Mouse
import java.util.*

abstract class GuiContainerAirPort(container: Container) : GuiContainer(container) {
    protected var doWheels: ArrayList<IdoWheel> = ArrayList<IdoWheel>()

    override fun initGui() {
        super.initGui()
        doWheels.clear()
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        val dWheel = Mouse.getDWheel()
        for (item in doWheels) {
            item.doWheel(mouseX, mouseY, dWheel)
        }
    }
}