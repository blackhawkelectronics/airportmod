package airportlight.modcore.gui.custombutton

interface IdoWheel {
    fun doWheel(mouseX: Int, mouseY: Int, dWheel: Int)
}