package airportlight.modcore.gui.custombutton;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class SwitchButton extends GuiButton {
    private final int size;
    private final double scale;
    private final double scale1;
    protected static final ResourceLocation btSwitch = new ResourceLocation("airportlight:textures/gui/switcher.png");

    public SwitchButton(int id, int x, int y, int xSize) {
        super(id, x, y, 95, 61, "");
        this.size = xSize;
        scale = (double) size / 95;
        scale1 = 1 / scale;
        this.width = this.size;
        this.height = (int) (61 * scale);
    }

    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_) {
        p_146112_1_.getTextureManager().bindTexture(btSwitch);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslated(this.xPosition, this.yPosition, 0);
        GL11.glScaled(scale, scale, scale);
        this.drawTexturedModalRect(0, 0, 0, 0, 95, 61);
        GL11.glScaled(scale1, scale1, scale1);
        GL11.glTranslated(-this.xPosition, -this.yPosition, 0);
        this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
    }
}
