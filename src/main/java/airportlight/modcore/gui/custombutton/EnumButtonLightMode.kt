package airportlight.modcore.gui.custombutton

enum class EnumButtonLightMode(val engage: Boolean) {
    OFF(false), StandBy(false), ON(false);

    companion object {
        fun getFromBoolean(value: Boolean): EnumButtonLightMode {
            return if (value) {
                ON
            } else {
                OFF
            }
        }
    }
}