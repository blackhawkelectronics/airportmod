package airportlight.modcore.gui.custombutton

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

abstract class EnumModeButton(
    id: Int,
    x: Int,
    y: Int,
    private val size: Int,
    var lightMode: EnumButtonLightMode = EnumButtonLightMode.OFF
) : GuiButton(id, x, y, size, size, "") {
    protected abstract val txOffset: Int
    protected abstract val tyOffset: Int
    protected abstract fun getTexturue(lightMode: EnumButtonLightMode): ResourceLocation
    protected abstract fun setEnabled(enabled: Boolean): EnumModeButton

    private val scale: Double = size.toDouble() / 116
    private val scale1: Double = 1 / scale

    init {
        width = size
        height = (111 * scale).toInt()
    }


    override fun drawButton(p_146112_1_: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        p_146112_1_.textureManager.bindTexture(getTexturue(lightMode))

        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        field_146123_n =
            p_146112_2_ >= xPosition && p_146112_3_ >= yPosition && p_146112_2_ < xPosition + width && p_146112_3_ < yPosition + height
        GL11.glEnable(GL11.GL_BLEND)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        GL11.glTranslated(xPosition.toDouble(), yPosition.toDouble(), 0.0)
        GL11.glScaled(scale, scale, scale)
        drawTexturedModalRect(0, 0, 116 * txOffset, 111 * tyOffset, 116, 111)
        GL11.glTranslated(0.0, 0.0, 10.0)
        if (!enabled) {
            drawGradientRect(0, 0, 116, 111, -0x5fefeff0, -0x5fafafb0)
        }
        GL11.glTranslated(0.0, 0.0, -10.0)
        GL11.glScaled(scale1, scale1, scale1)
        GL11.glTranslated(-xPosition.toDouble(), -yPosition.toDouble(), 0.0)
        mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_)
        GL11.glPopMatrix()
    }
}