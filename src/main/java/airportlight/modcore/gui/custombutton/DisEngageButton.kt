package airportlight.modcore.gui.custombutton

import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class DisEngageButton(id: Int, x: Double, y: Double, private val size: Double) :
    GuiButtonDouble(id, x, y, size, 90 * size / 255, "") {
    companion object {
        protected val btSwitch = ResourceLocation("airportlight:textures/gui/ap_disengage.png")
    }

    private val scale: Double = size.toDouble() / 255
    private val scale1: Double


    init {
        scale1 = 1 / scale
    }

    fun setEnabled(enabled: Boolean): DisEngageButton {
        this.enabled = enabled
        return this
    }

    override fun drawButton(p_146112_1_: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        p_146112_1_.textureManager.bindTexture(btSwitch)
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        field_146123_n =
            p_146112_2_ >= xPosD && p_146112_3_ >= yPosD && p_146112_2_ < xPosD + width && p_146112_3_ < yPosD + height
        GL11.glEnable(GL11.GL_BLEND)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        GL11.glTranslated(xPosD.toDouble(), yPosD.toDouble(), 0.0)
        GL11.glScaled(scale, scale, scale)
        drawTexturedModalRect(0, 0, 0, 0, 255, 90)
        if (!enabled) {
            drawGradientRect(0, 0, 255, 90, -0x5fefeff0, -0x5fafafb0)
        }
        GL11.glScaled(scale1, scale1, scale1)
        GL11.glTranslated(-xPosD.toDouble(), -yPosD.toDouble(), 0.0)
        mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_)
        GL11.glPopMatrix()
    }

}