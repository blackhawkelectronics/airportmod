package airportlight.modcore.gui.custombutton

import airportlight.modsystem.navigation.autopilot.EnumEngageThrottleMode
import airportlight.util.Pair
import net.minecraft.util.ResourceLocation
import java.util.*

class ThrottleModeButton(
    id: Int,
    x: Int,
    y: Int,
    size: Int,
    val mode: EnumEngageThrottleMode,
    lightMode: EnumButtonLightMode = EnumButtonLightMode.OFF
) : EnumModeButton(id, x, y, size, lightMode) {

    companion object {
        val btSwitch_ON = ResourceLocation("airportlight:textures/gui/ap_throttle_on.png")
        val btSwitch_StandBy = ResourceLocation("airportlight:textures/gui/ap_roll_standby.png")
        val btSwitch_OFF = ResourceLocation("airportlight:textures/gui/ap_throttle_off.png")

        val bottonTextureIndexMap =
            EnumMap<EnumEngageThrottleMode, Pair<Int, Int>>(
                EnumEngageThrottleMode::class.java
            )

        init {
            bottonTextureIndexMap[EnumEngageThrottleMode.N1] =
                Pair(0, 0)
            bottonTextureIndexMap[EnumEngageThrottleMode.SPEED] =
                Pair(1, 0)
            bottonTextureIndexMap[EnumEngageThrottleMode.CMD] =
                Pair(0, 1)
            bottonTextureIndexMap[EnumEngageThrottleMode.CWS] =
                Pair(1, 1)
        }
    }

    override fun getTexturue(lightMode: EnumButtonLightMode): ResourceLocation {
        return when (lightMode) {
            EnumButtonLightMode.OFF -> btSwitch_OFF
            EnumButtonLightMode.StandBy -> btSwitch_StandBy
            EnumButtonLightMode.ON -> btSwitch_ON
        }
    }

    public override fun setEnabled(enabled: Boolean): ThrottleModeButton {
        this.enabled = enabled
        return this
    }

    override val txOffset: Int
    override val tyOffset: Int

    init {
        val offset = bottonTextureIndexMap[mode]
        txOffset = offset!!.key
        tyOffset = offset.value
    }
}