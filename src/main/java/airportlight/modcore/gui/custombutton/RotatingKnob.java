package airportlight.modcore.gui.custombutton;

import airportlight.ModAirPortLight;
import airportlight.util.Consumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RotatingKnob extends GuiButton implements IdoWheel {
    public static int OutSize = 189;
    public static int InSize = 111;
    public static double offset = (double) 255 / 2;
    public static double kd = ((double) InSize) / OutSize;
    protected static final ResourceLocation buttonTexturesIn = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/knobin.png");
    protected static final ResourceLocation buttonTexturesOut = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/knobout.png");

    private final int size;
    private final double scale;
    private final double scale1;
    private int outAngle = 0;
    private int inAngle = 0;
    private final Consumer onValueChange;

    public RotatingKnob(int id, int x, int y, int size, Consumer onValueChange) {
        super(id, x, y, size, size, "");
        this.size = size;
        scale = (double) size / 255;
        scale1 = 1 / scale;
        this.onValueChange = onValueChange;
    }

    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_) {
        GL11.glPushMatrix();
        FontRenderer fontrenderer = p_146112_1_.fontRenderer;
        p_146112_1_.getTextureManager().bindTexture(buttonTexturesOut);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.field_146123_n = p_146112_2_ >= this.xPosition - size && p_146112_3_ >= this.yPosition - size && p_146112_2_ < this.xPosition + size && p_146112_3_ < this.yPosition + size;
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslated(this.xPosition, this.yPosition, 0);
        GL11.glScaled(scale, scale, scale);
        p_146112_1_.getTextureManager().bindTexture(buttonTexturesOut);
        GL11.glRotated(this.outAngle, 0, 0, 1);
        GL11.glTranslated(-offset, -offset, 0);
        this.drawTexturedModalRect(0, 0, 0, 0, 255, 255);
        GL11.glTranslated(offset, offset, 0);
        GL11.glRotated(-this.outAngle, 0, 0, 1);
        p_146112_1_.getTextureManager().bindTexture(buttonTexturesIn);
        GL11.glRotated(this.inAngle, 0, 0, 1);
        GL11.glScaled(kd, kd, kd);
        GL11.glTranslated(-offset, -offset, 0);
        this.drawTexturedModalRect(0, 0, 0, 0, 255, 255);
        GL11.glTranslated(offset, offset, 0);
        GL11.glScaled(1 / kd, 1 / kd, 1 / kd);
        GL11.glRotated(-this.inAngle, 0, 0, 1);
        this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);

        GL11.glScaled(scale1, scale1, scale1);
        GL11.glPopMatrix();
    }

    @Override
    public void doWheel(int mouseX, int mouseY, int dWheel) {
        if (enabled) {
            double dx = this.xPosition - mouseX;
            double dy = this.yPosition - mouseY;
            double dxy2 = dx * dx + dy * dy;
            double outrange = this.size * this.size * 0.5 * 0.5;
            if (dxy2 < outrange) {
                if (dWheel != 0) {
                    if (dWheel < 0) {
                        dWheel = -1;
                    } else {
                        dWheel = 1;
                    }
                    int ret;
                    double inrange = outrange * kd * kd;
                    if (dxy2 < inrange) {
                        ret = dWheel;
                        this.inAngle += dWheel * 18;
                        this.inAngle %= 360;
                    } else {
                        ret = dWheel * 20;
                        this.outAngle += dWheel * 18;
                        this.outAngle %= 360;
                    }
                    this.onValueChange.accept(ret);
                }
            }
        }
    }

    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        double dx = this.xPosition - mouseX;
        double dy = this.yPosition - mouseY;
        double dxy2 = dx * dx + dy * dy;
        double outrange = this.size * this.size * 0.5 * 0.5;
        boolean ret = this.enabled && this.visible && dxy2 < outrange;
        if (ret) {
            double inrange = outrange * kd * kd;
            int dWheel;
            if (0 < dx) {
                dWheel = -1;
            } else {
                dWheel = 1;
            }
            if (dxy2 < inrange) {
                this.onValueChange.accept(dWheel);
                this.inAngle += dWheel * 15;
                this.inAngle %= 360;
            } else {
                this.onValueChange.accept(dWheel * 20);
                this.outAngle += dWheel * 18;
                this.outAngle %= 360;
            }
        }
        return ret;
    }
}
