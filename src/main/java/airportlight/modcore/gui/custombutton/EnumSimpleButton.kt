package airportlight.modcore.gui.custombutton

import net.minecraft.client.gui.GuiButton

class EnumSimpleButton<E : Enum<E>> : GuiButton {
    val value: E

    constructor(id: Int, x: Int, y: Int, with: Int, value: E, displayString: String = value.name) : super(id, x, y, with, 20, displayString) {
        this.value = value
    }

    constructor(id: Int, x: Int, y: Int, value: E, displayString: String = value.name) : super(id, x, y, 80, 20, displayString) {
        this.value = value
    }
}
