package airportlight.modcore.gui.custombutton

import java.util.*

class EnumSimpleButtonListVertical<E : Enum<E>>(
    val id: Int,
    val x: Int,
    val y: Int,
    enum: Class<E>,
    with: Int = 80,
    displayStringGetter: DisplayStringGetter<E> = object : DisplayStringGetter<E> {
        override fun getDisplayStringFromEnum(enum: E): String {
            return enum.name
        }
    }
) {
    val buttonList = ArrayList<EnumSimpleButton<E>>()

    init {
        for (value in EnumSet.allOf(enum)) {
            buttonList.add(EnumSimpleButton(id, x, y + 20 * value.ordinal, with, value, displayStringGetter.getDisplayStringFromEnum(value)))
        }
    }

    fun selectButton(select: E) {
        for (button in buttonList) {
            button.enabled = button.value != select
        }
    }

    interface DisplayStringGetter<E : Enum<E>> {
        fun getDisplayStringFromEnum(enum: E): String
    }
}
