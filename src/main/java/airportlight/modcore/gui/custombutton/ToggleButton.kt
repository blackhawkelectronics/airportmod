package airportlight.modcore.gui.custombutton

import airportlight.util.Consumer
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class ToggleButton(
    id: Int,
    x: Double,
    y: Double,
    private val size: Double,
    switchUp: Boolean = false,
    val indicator: Consumer? = null
) : GuiButtonDouble(id, x, y, size / 2, size, "") {
    companion object {
        protected val btSwitch = ResourceLocation("airportlight:textures/gui/toggle.png")
    }

    var switchUp: Boolean = false
        set(value) {
            field = value
            indicator?.accept(
                if (value) {
                    1
                } else {
                    0
                }
            )
        }

    private val scale: Double = size.toDouble() / 255
    private val scale1: Double


    init {
        this.switchUp = switchUp
        scale1 = 1 / scale
    }

    override fun drawButton(p_146112_1_: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        p_146112_1_.textureManager.bindTexture(btSwitch)
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        field_146123_n =
            p_146112_2_ >= xPosD && p_146112_3_ >= yPosD && p_146112_2_ < xPosD + width && p_146112_3_ < yPosD + height
        GL11.glEnable(GL11.GL_BLEND)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        val reverse = if (switchUp) {
            180.0
        } else {
            0.0
        }
        GL11.glTranslated(xPosD.toDouble(), yPosD.toDouble(), 0.0)
        GL11.glScaled(scale, scale, scale)
        GL11.glTranslated(63.5, 127.5, 0.0)
        GL11.glRotated(reverse, 0.0, 0.0, 1.0)
        GL11.glTranslated(-63.5, -127.5, 0.0)
        drawTexturedModalRect(0, 0, 0, 0, 127, 255)
        if (!enabled) {
            drawGradientRect(0, 0, 127, 255, -0x5fefeff0, -0x5fafafb0)
        }
        GL11.glTranslated(63.5, 127.5, 0.0)
        GL11.glRotated(-reverse, 0.0, 0.0, 1.0)
        GL11.glTranslated(-63.5, -127.5, 0.0)
        GL11.glScaled(scale1, scale1, scale1)
        GL11.glTranslated(-xPosD.toDouble(), -yPosD.toDouble(), 0.0)
        mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_)
        GL11.glPopMatrix()
    }

}