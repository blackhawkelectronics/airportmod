package airportlight.radar.artsdisplay

import airportlight.modcore.PacketHandlerAPM
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf

/**
 * ArtsDisplayBlockのデータ共有
 */
class ArtsDisplayTileSync : TileEntityMessage {
    private lateinit var airportICAO: String

    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()
    constructor(tile: ArtsDisplayTile, airportICAO: String) : super(tile) {
        this.airportICAO = airportICAO
    }


    override fun read(buf: ByteBuf) {
        this.airportICAO = ByteBufUtils.readUTF8String(buf)
    }

    override fun write(buf: ByteBuf) {
        ByteBufUtils.writeUTF8String(buf, this.airportICAO)
    }

    companion object : IMessageHandler<ArtsDisplayTileSync, IMessage> {
        override fun onMessage(message: ArtsDisplayTileSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is ArtsDisplayTile) {
                tile.airportICAO = message.airportICAO
                if (ctx.side == Side.SERVER) {
                    PacketHandlerAPM.sendPacketAll(ArtsDisplayTileSync(tile, message.airportICAO))
                    tile.markDirty()
                } else {
                    tile.isChange = true
                }
            }
            return null
        }
    }
}
