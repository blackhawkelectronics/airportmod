package airportlight.radar.artsdisplay

import airportlight.modcore.PacketHandlerAPM
import airportlight.radar.ATCType
import airportlight.radar.RadarData
import airportlight.radar.StripData
import airportlight.radar.StripDataType
import airportlight.radar.system.RadarSystemClient
import airportlight.radar.system.RadarSystemServer
import airportlight.util.TileEntityMessage
import airportlight.util.guiScreen
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf

/**
 * StripListGuiからの操作共有用
 */
class ArtsDisplayDataSync : TileEntityMessage {
    private var entityID: Int = 0
    private var dataType: Int = 0
    private var srtObj0: String = ""
    private var srtObj1: String = ""


    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()

    private constructor(tile: ArtsDisplayTile, entityID: Int, dataType: Int, strObj0: String, strObj1: String) : super(tile) {
        this.entityID = entityID
        this.dataType = dataType
        this.srtObj0 = strObj0
        this.srtObj1 = strObj1
    }

    override fun write(buf: ByteBuf) {
        buf.writeInt(this.entityID)
        buf.writeInt(this.dataType)
        ByteBufUtils.writeUTF8String(buf, srtObj0)
        ByteBufUtils.writeUTF8String(buf, srtObj1)
    }

    override fun read(buf: ByteBuf) {
        this.entityID = buf.readInt()
        this.dataType = buf.readInt()
        this.srtObj0 = ByteBufUtils.readUTF8String(buf)
        this.srtObj1 = ByteBufUtils.readUTF8String(buf)
    }


    companion object : IMessageHandler<ArtsDisplayDataSync, IMessage> {
        private const val typeATC = 1
        private const val typeAddStrip = 2
        private const val typeDelStrip = 3

        private fun getStrip(entityID: Int, ctx: MessageContext): StripData? {
            return if (ctx.side.isClient) {
                RadarSystemClient.getStripData(entityID)
            } else {
                RadarSystemServer.getStripData(entityID)
            }
        }

        private fun getRadarData(entityID: Int, ctx: MessageContext): RadarData? {
            return if (ctx.side.isClient) {
                RadarSystemClient.getRadarData(entityID)
            } else {
                RadarSystemServer.getRadarData(entityID)
            }
        }

        override fun onMessage(message: ArtsDisplayDataSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is ArtsDisplayTile) {
                var shareMessage: IMessage? = null
                when (message.dataType) {
                    typeATC -> {
                        val controllerName = message.srtObj0
                        val atcType = ATCType.valueOf(message.srtObj1)
                        val stripData = getStrip(message.entityID, ctx)
                        stripData?.atcType = atcType
                        val radarData = getRadarData(message.entityID, ctx)
                        radarData?.setATCType(controllerName, atcType)
                        if (ctx.side.isServer) {
                            shareMessage = syncATCType(tile, message.entityID, controllerName, atcType)
                        } else {
                            stripData?.updatedList?.add(StripDataType.AtcType)
                            (ctx.guiScreen as? ArtsDisplayGui)?.stripListGui?.syncByEntityIDs(setOf(message.entityID))
                        }
                    }
                    typeAddStrip -> {
                        if (ctx.side.isServer) {
                            val radarData = getRadarData(message.entityID, ctx)
                            if (radarData != null) {
                                RadarSystemServer.makeAndAddStripData(message.entityID, radarData, tile.airportICAO)
                            }
                        }
                    }
                    typeDelStrip -> {
                        val stripData = getStrip(message.entityID, ctx)
                        if (stripData != null) {
                            if (ctx.side.isServer) {
                                RadarSystemServer.delStripData(stripData)
                                shareMessage = syncDelStrip(tile, message.entityID)
                            } else {
                                RadarSystemClient.deleteStripSerToCli(listOf(message.entityID))
                            }
                        }
                    }
                }

                if (ctx.side == Side.SERVER && shareMessage != null) {
                    PacketHandlerAPM.sendPacketAll(shareMessage)
                }
            }
            return null
        }

        //region DataType別メッセージコンストラクタ
        fun syncATCType(tile: ArtsDisplayTile, entityID: Int, controllerName: String, atcType: ATCType): ArtsDisplayDataSync {
            return ArtsDisplayDataSync(tile, entityID, typeATC, controllerName, atcType.name)
        }

        fun syncAddStrip(tile: ArtsDisplayTile, entityID: Int): ArtsDisplayDataSync {
            return ArtsDisplayDataSync(tile, entityID, typeAddStrip, "", "")
        }

        fun syncDelStrip(tile: ArtsDisplayTile, entityID: Int): ArtsDisplayDataSync {
            return ArtsDisplayDataSync(tile, entityID, typeDelStrip, "", "")
        }
        //endregion
    }
}
