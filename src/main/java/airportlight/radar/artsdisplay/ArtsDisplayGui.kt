package airportlight.radar.artsdisplay

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.GuiContainerAirPort
import airportlight.modcore.gui.custombutton.EnumSimpleButton
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListVertical
import airportlight.modcore.gui.custombutton.SelectKnob
import airportlight.radar.ATCType
import airportlight.radar.RadarData
import airportlight.radar.guiparts.GuiPartsRadar
import airportlight.radar.striplistgui.StripListGui
import airportlight.radar.system.RadarSystemClient
import airportlight.util.Consumer
import airportlight.util.GLTool
import airportlight.util.Vec2D
import airportlight.util.floorStep
import cpw.mods.fml.relauncher.Side
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiTextField
import org.lwjgl.input.Mouse
import org.lwjgl.opengl.GL11
import kotlin.math.*


class ArtsDisplayGui(val tile: ArtsDisplayTile) : GuiContainerAirPort(ContainerAirPort()) {
    //Guiのパネル関連
    /**
     * Radar描画用機能
     */
    private val guiPartsRadar: GuiPartsRadar by lazy { GuiPartsRadar(this.fontRendererObj, tile) }

    /**
     * StripDataリスト表示用外部Gui
     */
    var stripListGui: StripListGui? = null

    //ボタン・テキストフィールド関連
    /**
     * ATCType選択ボタン郡
     */
    private lateinit var atcButtons: EnumSimpleButtonListVertical<ATCType>

    /**
     * 表示スケール切り替えノブ
     */
    private lateinit var scaleNob: SelectKnob
    val ranges = arrayOf(50, 100, 500, 1000, 5000, 10000)
    private val rangeSets =
        arrayOf(Pair(50, 10), Pair(100, 10), Pair(500, 100), Pair(1000, 100), Pair(5000, 1000), Pair(10000, 1000))

    /**
     * 空港名設定用テキストフィールド
     */
    private lateinit var airportNameField: GuiTextField

    //現状の設定関係
    /**
     * 現在の表示の中心
     */
    private var viewCenter: Vec2D = Vec2D(0.0, 0.0)

    /**
     * 現在の表示の中心
     */
    var range: Int = ranges[1]

    //tempデータ
    /**
     * 選択中のRadarData
     */
    private var clickTarget: RadarData? = null


    private val atcTypeSelect = 1
    private val addStrip = 3

    init {
        if (tile.worldObj.isRemote) {
            //StripListGuiを生成し表示
            stripListGui = StripListGui(Side.CLIENT, tile, Minecraft.getMinecraft(), this)
            stripListGui!!.start()

            //保存されている設定を読み出し
            viewCenter = tile.viewCenter
            range = tile.range
        }
    }

    override fun initGui() {
        super.initGui()
        this.buttonList.clear()

        //AtcType選択ボタン郡
        atcButtons = EnumSimpleButtonListVertical<ATCType>(atcTypeSelect, width - 110, 10, ATCType::class.java)
        this.buttonList.addAll(atcButtons.buttonList)

        //空港名の設定テキストフィールド
        this.airportNameField = GuiTextField(
            this.fontRendererObj,
            10,
            10,
            80,
            this.fontRendererObj.FONT_HEIGHT
        )
        this.airportNameField.maxStringLength = 10
        this.airportNameField.setTextColor(16777215)
        airportNameField.text = tile.airportICAO

        //描画範囲のセレクタ
        val rangesStr = Array<String>(ranges.size) {
            ranges[it].toString()
        }
        //現在選択中の表示レンジからノブの位置を計算
        val rangeIndex = ranges.indexOf(range).let {
            if (it < 0) {
                0
            } else {
                it
            }
        }
        scaleNob = SelectKnob(
            2, width - 50, height - 50, 30,
            rangesStr,
            rangeIndex,
            object : Consumer() {
                override fun accept(valIndex: Int) {
                    range = ranges[valIndex]
                }
            })
        buttonList.add(scaleNob)
        doWheels.add(scaleNob)

        //操作用のボタン類
        buttonList.add(GuiButton(addStrip, width - 110, 110, 80, 20, "Add Strip"))
    }

    override fun onGuiClosed() {
        super.onGuiClosed()
        //StripListGuiを閉じる
        stripListGui?.finish()
        stripListGui = null

        //設定を保存
        tile.viewCenter = viewCenter
        tile.range = range
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        super.drawGuiContainerBackgroundLayer(p_146976_1_, mouseX, mouseY)
        //変更フラグが立ったら更新を反映
        if (tile.isChange) {
            airportNameField.text = tile.airportICAO
            tile.isChange = false
        }

        val scale = getRangeScale()

        GL11.glPushMatrix()
        //画面の中心に移動
        GL11.glTranslated(width / 2.0, height / 2.0, 0.0)
        //ヒュウ時の中心を画面の中心とするように移動
        GL11.glTranslated(-viewCenter.x * scale, -viewCenter.y * scale, 0.0)

        //同心円を描画
        drawCircles(scale)

        //レーダーを描画
        guiPartsRadar.draw(scale)

        clickTarget?.let {
            GLTool.drawCircle(it.nowPos.x * scale, it.nowPos.z * scale, 10.0, false)
        }

        GL11.glPopMatrix()
//        GL11.glTranslated(viewCenter.x * scale, viewCenter.y * scale * scale, 0.0)
//        GL11.glTranslated(-width / 2.0, -height / 2.0, 0.0)
        this.airportNameField.drawTextBox()
    }

    private val glColor170 = GLTool.glColor(170, 170, 170, 170)
    private val glColor100 = GLTool.glColor(100, 100, 100, 100)


    /**
     * 同心円を描画
     * @param scale
     * @see getRangeScale()
     */
    private fun drawCircles(scale: Double) {
        //同心円の間隔
        val step = rangeSets[scaleNob.valIndex].second

        val range = this.range
        //画面の移動量から表示すべき一番遠い地点までの距離を計算
        val dPosX = this.viewCenter.x - tile.xCoord
        val dPosZ = this.viewCenter.y - tile.zCoord
        val diffSqrt = hypot(dPosX, dPosZ)

        val maxCircleRange = diffSqrt.floorStep(step) + range * 2
        for (r in step..maxCircleRange step step) {
            //10本ごとに濃い線・太い線
            //5本ごとに少し濃い線
            val bound = r % (step * 10) == 0
            val halfBound = r % (step * 5) == 0
            val (color, stringColor) = when {
                bound -> {
                    Pair(170f, glColor170)
                }
                halfBound -> {
                    Pair(150f, glColor100)
                }
                else -> {
                    Pair(100f, glColor100)
                }
            }
            GL11.glLineWidth(
                if (bound) {
                    4f
                } else {
                    2f
                }
            )

            //円を描画
            GLTool.drawCircle(
                tile.xCoord.toDouble() * scale,
                tile.zCoord.toDouble() * scale,
                r.toDouble() * scale, color, color, color, color, false
            )

            if (bound || halfBound) {
                //画面の移動方向から距離表示をする方向を算出
                val ang = atan2(dPosZ, dPosX)
                val x = cos(ang)
                val z = sin(ang)
                //距離を示す数字を表示
                fontRendererObj.drawString(
                    r.toString(),
                    (tile.xCoord.toDouble() * scale + r * scale * x).roundToInt(),
                    (tile.zCoord.toDouble() * scale + r * scale * z).roundToInt(),
                    stringColor,
                    false
                )
            }
        }

        GL11.glColor4f(0.39f, 0.39f, 0.39f, 0.39f)
        //方位を示す直線を描画
        val x0 = (tile.xCoord.toDouble() * scale)
        val z0 = (tile.zCoord.toDouble() * scale)
        for (a in 0 until 360 step 30) {
            val x1 = x0 + cos(Math.toRadians(a.toDouble())) * maxCircleRange * scale
            val z1 = z0 + sin(Math.toRadians(a.toDouble())) * maxCircleRange * scale
            GLTool.line(x0, z0, x1, z1, 2.0f)
        }
    }

    /**
     * マウスの座標をノーマライズ
     */
    private fun getMouseWorldPos(): Vec2D {
        val mc = Minecraft.getMinecraft()
        return Vec2D(
            Mouse.getX().toDouble() * width / mc.displayWidth,
            height - Mouse.getY().toDouble() * height / mc.displayHeight - 1
        )
    }

    private var prevMouse = getMouseWorldPos()
    private var mouseDown = false
    override fun handleMouseInput() {
        val button = Mouse.getEventButton()
        val pos = getMouseWorldPos()
        handleInputVanilla(pos, button)

        when {
            Mouse.getEventButtonState() -> {
                if (button != 0) return
                mouseDown = true
            }
            button == -1 -> {
                if (mouseDown) {
                    //ボタンを押して移動中なら表示の中心位置を移動
                    this.viewCenter += (pos - prevMouse) / -getRangeScale()
                }
            }
            else -> {
                if (button != 0) return
                mouseDown = false
            }
        }
        prevMouse = pos
    }

    //region バニラのInputVanilla

    private var eventButton = 0
    private var lastMouseEvent: Long = 0
    private var clickCnt = 0

    /**
     * バニラのInputVanillaを上書きしているので処理を呼び出し
     */
    private fun handleInputVanilla(posIn: Vec2D, button: Int) {
        if (Mouse.getEventButtonState()) {
            if (mc.gameSettings.touchscreen && clickCnt++ > 0) {
                return
            }
            eventButton = button
            lastMouseEvent = Minecraft.getSystemTime()
            mouseClicked(posIn.x.toInt(), posIn.y.toInt(), eventButton)
        } else if (button != -1) {
            if (mc.gameSettings.touchscreen && --clickCnt > 0) {
                return
            }
            eventButton = -1
            mouseMovedOrUp(posIn.x.toInt(), posIn.y.toInt(), button)
        } else if (eventButton != -1 && lastMouseEvent > 0L) {
            val l = Minecraft.getSystemTime() - lastMouseEvent
            mouseClickMove(posIn.x.toInt(), posIn.y.toInt(), eventButton, l)
        }
    }
    //endregion

    private var wasButtonClicked: Boolean = false
    override fun mouseClicked(x: Int, y: Int, mouseEvent: Int) {
        super.mouseClicked(x, y, mouseEvent)

        //Field類がクリックされていないか確認
        airportNameField.mouseClicked(x, y, mouseEvent)


        if (!airportNameField.isFocused) {
            if (!wasButtonClicked) {
                //クリックした瞬間ならクリックしているRadarDataを検索
                clickTarget = searchMouseTarget(Vec2D(x.toDouble(), y.toDouble()), getRangeScale())
            }
            wasButtonClicked = false
        } else {
            wasButtonClicked = true
        }
    }

    override fun keyTyped(p_73869_1_: Char, p_73869_2_: Int) {
        if (airportNameField.isFocused) {
            //空港名テキストフィールドの入力
            this.airportNameField.textboxKeyTyped(p_73869_1_, p_73869_2_)
            PacketHandlerAPM.sendPacketServer(ArtsDisplayTileSync(tile, airportNameField.text))
        } else {
            super.keyTyped(p_73869_1_, p_73869_2_)
        }
    }


    override fun actionPerformed(button: GuiButton) {
        when (button.id) {
            atcTypeSelect -> {
                if (clickTarget != null) {
                    val esButton = (button as? EnumSimpleButton<ATCType>)
                    esButton?.let {
                        PacketHandlerAPM.sendPacketServer(
                            ArtsDisplayDataSync.syncATCType(
                                this.tile,
                                clickTarget!!.entityID,
                                this.tile.airportICAO,
                                esButton.value
                            )
                        )
                    }
                }
            }
            addStrip -> {
                if (clickTarget != null) {
                    PacketHandlerAPM.sendPacketServer(
                        ArtsDisplayDataSync.syncAddStrip(
                            this.tile,
                            clickTarget!!.entityID
                        )
                    )
                }
            }
        }
        wasButtonClicked = true
    }

    /**
     * マウスがある場所のRadarDataを検索
     */
    private fun searchMouseTarget(mouse: Vec2D, scale: Double): RadarData? {
        //マウスの座標を表示中のワールドの座標に変換
        val mousePointX = (mouse.x - this.width / 2) / scale + viewCenter.x
        val mousePointZ = (mouse.y - this.height / 2) / scale + viewCenter.y

        var tempDiff = 50.0
        var tempData: RadarData? = null

        for (radarData in RadarSystemClient.radarDataList.values) {
            val diffX = radarData.nowPos.x - mousePointX
            val diffZ = radarData.nowPos.z - mousePointZ
            val diffXZ = diffX * diffX + diffZ * diffZ
            if (diffXZ < tempDiff) {
                tempDiff = diffXZ
                tempData = radarData
            }
        }

        return tempData
    }


    /**
     * レーダー表示のスケールを現在の表示範囲設定より計算
     */
    private fun getRangeScale(): Double {
        return 100.0 / range
    }
}
