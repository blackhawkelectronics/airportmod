package airportlight.radar.artsdisplay

import airportlight.modcore.normal.TileNormal
import airportlight.util.Vec2D
import net.minecraft.nbt.NBTTagCompound
import kotlin.properties.Delegates

class ArtsDisplayTile : TileNormal() {
    var airportICAO: String = "RJAA"
    var range: Int = 100
    var viewCenter: Vec2D by Delegates.notNull<Vec2D>()
    var isChange: Boolean = false
    override fun validate() {
        super.validate()
        this.viewCenter = Vec2D(this)
    }


    override fun writeToNBT(p_145841_1_: NBTTagCompound) {
        super.writeToNBT(p_145841_1_)
        p_145841_1_.setString("airportICAO", airportICAO)
    }

    override fun readFromNBT(p_145839_1_: NBTTagCompound) {
        super.readFromNBT(p_145839_1_)
        airportICAO = p_145839_1_.getString("airportICAO")
    }
}
