package airportlight.radar.artsdisplay;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ArtsDisplayBlock extends BlockContainer {
    public ArtsDisplayBlock() {
        super(Material.rock);
        setBlockName("ArtsDisplay");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":arts");
        setCreativeTab(ModAirPortLight.AirPortLightTabs);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof ArtsDisplayTile) {
            player.openGui(ModAirPortLight.instance, GuiID.Arts.getID(), world, x, y, z);
            return true;
        }
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new ArtsDisplayTile();
    }
}
