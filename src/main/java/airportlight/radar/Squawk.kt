package airportlight.radar

class Squawk(num: Int) {
    val squawk: Int

    init {
        val s1 = num % 10 and 7
        val s2 = num % 100 and 7
        val s3 = num % 1000 and 7
        val s4 = num % 10000 and 7
        squawk = s4 * 1000 + s3 * 100 + s2 * 10 + s1
    }

    override fun toString(): String {
        return String.format("%04d", squawk)
    }
}