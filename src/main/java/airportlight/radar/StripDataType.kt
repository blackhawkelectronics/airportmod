package airportlight.radar

enum class StripDataType(val id: Int) {
    //初期設定内容 変更なし
    DepartureAirport(1),
    CallSign(2),
    Squawk(3),
    AircraftType(4),
    AircraftEquipment(5),


    //初期設定内容　変更あり
    AlternativeAirport(11),
    ArrivalAirport(12),
    FlightType(13),
    Route(14),
    CruiseSpeed(15),


    //ATC メモ
    InstructedAltitude(21),
    TempAltitude(22),
    InstructedHeading(23),
    InstructedSpeed(24),
    MemoLong(25),
    MemoShort(26),
    AtcType(27),
    ;

    companion object {
        fun getType(id: Int): StripDataType {
            for (value in values()) {
                if (value.id == id) {
                    return value
                }
            }
            throw IllegalArgumentException("StripDataType id not find. Input:$id")
        }
    }
}