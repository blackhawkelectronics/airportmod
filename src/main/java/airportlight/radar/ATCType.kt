package airportlight.radar

/**
 * ATCの管理状況
 */
enum class ATCType(val id: Int) {
    NotUnderMyControl(1), UnderMyControl(2), Departure(3), Arrival(4);

    companion object {
        fun valueOf(id: Int): ATCType {
            for (value in values()) {
                if (value.id == id) {
                    return value
                }
            }
            throw IllegalArgumentException("ATCType id out of range.  input=$id")
        }
    }
}