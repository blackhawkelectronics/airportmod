package airportlight.radar.guiparts

import java.awt.GridBagConstraints
import java.awt.Insets

/**
 * GridBagConstraintsを使いやすいように改造
 */
class CustomGridBagConstraints(gridx: Int, gridy: Int, gridwidth: Int, gridheight: Int, weightx: Double = 1.0) :
    GridBagConstraints() {
    init {
        this.fill = BOTH
        this.gridx = gridx
        this.gridy = gridy
        this.gridwidth = gridwidth
        this.gridheight = gridheight
        this.insets = Insets(0, 0, 0, 0)
        this.weightx = weightx
        this.weighty = 1.0
    }

    fun update(gridx: Int, gridy: Int, gridwidth: Int, gridheight: Int, weightx: Double = 1.0) {
        this.fill = BOTH
        this.gridx = gridx
        this.gridy = gridy
        this.gridwidth = gridwidth
        this.gridheight = gridheight
        this.insets = Insets(0, 0, 0, 0)
        this.weightx = weightx
        this.weighty = 1.0
    }
}