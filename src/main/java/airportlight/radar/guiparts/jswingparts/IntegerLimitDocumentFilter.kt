package airportlight.radar.guiparts.jswingparts

import javax.swing.text.AttributeSet
import javax.swing.text.BadLocationException
import javax.swing.text.Document
import javax.swing.text.DocumentFilter

/**
 * テキストフィールドなどの入力制限処置実装用
 */
internal class IntegerLimitDocumentFilter(private val IntegerOnly: Boolean = false, private val limit: Int = 0) :
    DocumentFilter() {
    @Throws(BadLocationException::class)
    override fun insertString(
        fb: FilterBypass,
        offset: Int, string: String?, attr: AttributeSet?
    ) {
        if (string == null) {
            return
        } else {
            replace(fb, offset, 0, string, attr)
        }
    }

    @Throws(BadLocationException::class)
    override fun remove(fb: FilterBypass, offset: Int, length: Int) {
        replace(fb, offset, length, "", null)
    }

    @Throws(BadLocationException::class)
    override fun replace(
        fb: FilterBypass, offset: Int, length: Int,
        text: String?, attrs: AttributeSet?
    ) {
        val doc: Document = fb.document
        val currentLength: Int = doc.length
        val currentContent: String = doc.getText(0, currentLength)
        val before = currentContent.substring(0, offset)
        val after = currentContent.substring(length + offset, currentLength)
        val newValue = before + (text ?: "") + after
        if (IntegerOnly) {
            checkInput(newValue, offset)
        }
        val newOffset = offset.coerceAtMost(limit)
        val totalLength = offset + (text?.length ?: 0)
        if (limit < totalLength) {
            fb.replace(newOffset, currentLength - limit, "", attrs)
        } else {
            fb.replace(offset, length, text, attrs)
        }
    }

    @Throws(BadLocationException::class)
    private fun checkInput(proposedValue: String, offset: Int): Int {
        var newValue = 0
        if (proposedValue.isNotEmpty()) {
            newValue = try {
                proposedValue.toInt()
            } catch (e: NumberFormatException) {
                throw BadLocationException(proposedValue, offset)
            }
        }
        return newValue
    }
}