package airportlight.radar.guiparts

import airportlight.modsystem.navigation.ils.ILSData
import airportlight.modsystem.navigation.ils.NavFreqManager
import airportlight.radar.ATCType
import airportlight.radar.RadarData
import airportlight.radar.artsdisplay.ArtsDisplayTile
import airportlight.radar.system.RadarSettingVar
import airportlight.radar.system.RadarSystemClient
import airportlight.util.GLTool
import net.minecraft.client.gui.FontRenderer
import net.minecraft.util.MathHelper
import org.lwjgl.opengl.GL11
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class GuiPartsRadar(private val fontRenderer: FontRenderer, val tile: ArtsDisplayTile) : GuiParts {
    override fun draw(scale: Double) {
        val sqrtScale = sqrt(scale)
        GL11.glPushMatrix()
        for (radarData in RadarSystemClient.radarDataList.values) {
            if (radarData.isLost()) {
                continue
            }
            //stripDataごとに描画
            //ATCタイプから角度や描画色を決定
            val (angle, r, g, b, a) = getDataSet(radarData)
            val color = GLTool.glColor((r * 255).toInt(), (g * 255).toInt(), (b * 255).toInt(), (a * 255).toInt())

            GL11.glColor4d(r, g, b, a)

            val nowPosX = radarData.nowPos.x * scale
            val nowPosZ = radarData.nowPos.z * scale
            GLTool.drawCircle(nowPosX, nowPosZ, 2.0, true)
            GLTool.line(
                nowPosX,
                nowPosZ,
                nowPosX + radarData.nowVec.x * sqrtScale,
                nowPosZ + radarData.nowVec.z * sqrtScale,
                1.0f
            )

            for (i in 1 until RadarSettingVar.logSize step 2) {
                radarData.posLog[i]?.let {
                    val posX = it.x * scale
                    val posZ = it.z * scale
                    if (posX != nowPosX || posZ != nowPosZ) {
                        GLTool.line(posX - 5, posZ, posX + 5, posZ, 1.5f)
                    }
                }
            }

            if (radarData.getATCType(tile.airportICAO) != ATCType.NotUnderMyControl) {
                GLTool.line(nowPosX - 6 + angle, nowPosZ - 4 - angle, nowPosX + 6, nowPosZ - 4 + angle, 1f)
                GLTool.line(nowPosX - 6 + angle, nowPosZ - 4 - angle, nowPosX + 0 - angle, nowPosZ + 6, 1f)
                GLTool.line(nowPosX + 0 - angle, nowPosZ + 6, nowPosX + 6, nowPosZ - 4 + angle, 1f)
            }

            GLTool.line(nowPosX - 3, nowPosZ - 3, nowPosX - 12, nowPosZ - 12, 1f)

            val s0 = (radarData.stripData?.callSign ?: "") + " " + (radarData.stripData?.memoShort ?: "")
            val sAlt = if (radarData.stripData != null) {
                String.format("%03d", radarData.stripData!!.tempAltitude)
            } else {
                ""
            }
            val s1 = sAlt +
                    radarData.altitudeChange +
                    String.format("%03.0f", radarData.nowAltitude)
            val moreInfo = radarData.getATCType(tile.airportICAO) != ATCType.NotUnderMyControl
            val moreInfoOffset = if (moreInfo) {
                20
            } else {
                0
            }
            val infoX = MathHelper.floor_double(nowPosX - 50)
            val infoZ = MathHelper.floor_double(nowPosZ - 10 - fontRenderer.FONT_HEIGHT)
            fontRenderer.drawString(s0, infoX, infoZ - 10 - moreInfoOffset, color, false)
            fontRenderer.drawString(s1, infoX, infoZ - 0 - moreInfoOffset, color, false)

            if (moreInfo) {
                val s2 = String.format("%03d", radarData.nowSpeed) + " " + (radarData.stripData?.arrivalAirport ?: "")
                val s3 = String.format(
                    "%03d",
                    (radarData.stripData?.instructedSpeed ?: 0)
                ) + " " + (radarData.stripData?.instructedHeading ?: "")
                fontRenderer.drawString(s2, infoX, infoZ - 10, color, false)
                fontRenderer.drawString(s3, infoX, infoZ - 0, color, false)
            }
        }

        val color = 0.7
        GL11.glColor4d(color, color, color, color)
        val color255 = (color * 255).toInt()
        val glColor = GLTool.glColor(color255, color255, color255, color255)
        for ((navFreqID, navDataList) in NavFreqManager.getNavInfo()) {
            for (navDataSet in navDataList) {
                val pos = navDataSet.key
                val navData = navDataSet.value
                val posX = pos.x * scale
                val posZ = pos.z * scale
                if (navData is ILSData) {
                    val size = 150 * scale
                    val sizeS = 145 * scale
                    val rad = navData.localizerAngRad - (Math.PI * 1.5)
                    val pRx = cos(rad + 0.1) * size
                    val pRy = sin(rad + 0.1) * size
                    val pCx = cos(rad) * sizeS
                    val pCy = sin(rad) * sizeS
                    val pLx = cos(rad - 0.1) * size
                    val pLy = sin(rad - 0.1) * size

                    GL11.glTranslated(posX, posZ, 0.0)
                    GL11.glDisable(GL11.GL_TEXTURE_2D)
                    GL11.glBegin(GL11.GL_LINE_LOOP)
                    GL11.glVertex2d(pRx, pRy)
                    GL11.glVertex2d(0.0, 0.0)
                    GL11.glVertex2d(pCx, pCy)
                    GL11.glEnd()
                    GL11.glBegin(GL11.GL_LINE_LOOP)
                    GL11.glVertex2d(pCx, pCy)
                    GL11.glVertex2d(0.0, 0.0)
                    GL11.glVertex2d(pLx, pLy)
                    GL11.glEnd()
                    GL11.glEnable(GL11.GL_TEXTURE_2D)
                    if (navData.name != "") {
                        val infoX = -fontRenderer.getStringWidth(navData.name) / 2
                        val infoZ = -2 - fontRenderer.FONT_HEIGHT
                        fontRenderer.drawString(
                            "[" + navData.name + " " + navData.frequencyID + "]",
                            infoX,
                            infoZ,
                            glColor,
                            false
                        )
                    }
                    GL11.glTranslated(-posX, -posZ, 0.0)

                } else {
                    GL11.glTranslated(posX, posZ, 0.0)
                    GLTool.drawCircle(0.0, 0.0, 1.2, true)
                    GL11.glDisable(GL11.GL_TEXTURE_2D)
                    GL11.glBegin(GL11.GL_LINE_LOOP)
                    GL11.glVertex2d(4.0, 0.0)
                    GL11.glVertex2d(2.0, 3.46)
                    GL11.glVertex2d(-2.0, 3.46)
                    GL11.glVertex2d(-4.0, 0.0)
                    GL11.glVertex2d(-2.0, -3.46)
                    GL11.glVertex2d(2.0, -3.46)
                    GL11.glEnd()
                    GL11.glEnable(GL11.GL_TEXTURE_2D)
                    if (navData.name != "") {
                        val infoX = -fontRenderer.getStringWidth(navData.name) / 2
                        val infoZ = -3 - fontRenderer.FONT_HEIGHT
                        fontRenderer.drawString(
                            "[" + navData.name + " " + navData.frequencyID + "]",
                            infoX,
                            infoZ,
                            glColor,
                            false
                        )
                    }
                    GL11.glTranslated(-posX, -posZ, 0.0)
                }
            }
        }

        GL11.glPopMatrix()
        //色設定がpush/popで治らないので手動でリセット
        GL11.glColor4d(1.0, 1.0, 1.0, 1.0)
    }


    //ATCTypeごとの設定返却用
    data class ATCTypeDataSet(
        val angle: Int,
        val r: Double,
        val g: Double,
        val b: Double,
        val a: Double = 1.0
    )

    //ATCTypeごとのDataSet
    private val datasetNotUnderMyControl = ATCTypeDataSet(0, 0.7, 0.7, 0.7, 0.7)
    private val datasetUnderMyControl = ATCTypeDataSet(0, 1.0, 1.0, 1.0)
    private val datasetDeparture = ATCTypeDataSet(-1, 0.0, 1.0, 0.77)
    private val datasetArrival = ATCTypeDataSet(1, 1.0, 0.77, 0.0)

    /**
     * ATCタイプから角度や描画色を決定
     * */
    private fun getDataSet(radarData: RadarData): ATCTypeDataSet {
        return when (radarData.getATCType(tile.airportICAO)) {
            ATCType.NotUnderMyControl -> datasetNotUnderMyControl
            ATCType.UnderMyControl -> datasetUnderMyControl
            ATCType.Departure -> datasetDeparture
            ATCType.Arrival -> datasetArrival
        }
    }
}
