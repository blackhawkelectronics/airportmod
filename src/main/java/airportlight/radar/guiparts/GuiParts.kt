package airportlight.radar.guiparts

interface GuiParts {
    fun draw(scale: Double)
}