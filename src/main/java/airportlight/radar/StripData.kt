package airportlight.radar

import io.netty.buffer.Unpooled
import java.io.DataInput
import java.io.DataOutput

class StripData(val entityID: Int) {
    /**
     * レーダーデータから新規作成
     */
    constructor(radarData: RadarData, controllerName: String) : this(radarData.entityID) {
        this.radarData = radarData
        this.atcType = radarData.getATCType(controllerName)
        this.callSign = "mc" + getNextID()
    }

    /**
     * メッセージからの復元
     */
    constructor(
        entityID: Int,
        atcType: ATCType
    ) : this(entityID) {
        this.atcType = atcType
    }

    //初期設定内容 変更なし
    // 名前　Stripでの番号　レーダーでの番号
    var DepartureAirport: String = ""//出発空港
    var callSign // s1 b4 コールサイン
            : String = "mc0000"
    var squawk: Squawk = Squawk(1200)
    var aircraftType //s2
            : String = ""
    var aircraftEquipment //航空機が持つ機能　s5
            : String = ""


    //初期設定内容　変更あり
    var AlternativeAirport: String = ""// 代替空港
    var arrivalAirport //s12 到着空港
            : String = ""
    var flightType = FlightType.VFR
    var route //飛行経路 s9
            : String = ""
    var cruiseSpeed //予定飛行速度 s4
            = 0


    //ATC メモ
    var instructedAltitude //指示した高度　s6
            : Int = 280
    var tempAltitude //一時的に指示した高度 s7 r2
            : Int = 250
    var instructedHeading //指示したポイントまたは方位 s10 r4
            : String = "000"
    var instructedSpeed //指示した速度
            = 0
    var memoLong //s11
            : String = ""
    var memoShort //r1
            : String = ""

    var atcType: ATCType = ATCType.NotUnderMyControl

    var radarData: RadarData? = null

    //管理用データ
    var updatedList = HashSet<StripDataType>()


    init {
    }

    /**
     * データタイプから現在の設定を取得
     */
    fun getStringInType(stripDataType: StripDataType): String {
        when (stripDataType) {
            StripDataType.DepartureAirport -> return this.DepartureAirport
            StripDataType.CallSign -> return this.callSign
            StripDataType.Squawk -> return this.squawk.squawk.toString()
            StripDataType.AircraftType -> return this.aircraftType
            StripDataType.AircraftEquipment -> return this.aircraftEquipment
            StripDataType.AlternativeAirport -> return this.AlternativeAirport
            StripDataType.ArrivalAirport -> return this.arrivalAirport
            StripDataType.FlightType -> return this.flightType.name
            StripDataType.Route -> return this.route
            StripDataType.CruiseSpeed -> return String.format("%03d", this.cruiseSpeed)
            StripDataType.InstructedAltitude -> return String.format("%03d", this.instructedAltitude)
            StripDataType.TempAltitude -> return String.format("%03d", this.tempAltitude)
            StripDataType.InstructedHeading -> return this.instructedHeading
            StripDataType.InstructedSpeed -> return String.format("%03d", this.instructedSpeed)
            StripDataType.MemoLong -> return this.memoLong
            StripDataType.MemoShort -> return this.memoShort
            StripDataType.AtcType -> return this.atcType.name
        }
    }

    /**
     * stripDataを書き出し
     * @param writeStripData 各種設定内容・ATCメモを書き込むか　Radarデータは常時書き込み
     */
    fun writeData(out: DataOutput, writeStripData: Boolean = true) {
        out.writeInt(entityID)
        out.writeBoolean(writeStripData)

        //レーダーデータ
        out.writeUTF(atcType.name)


        if (writeStripData) {
            val buf = Unpooled.buffer()

            //初期設定内容 変更なし
            out.writeUTF(DepartureAirport)
            out.writeUTF(callSign)
            out.writeInt(squawk.squawk)
            out.writeUTF(aircraftType)
            out.writeUTF(aircraftEquipment)


            //初期設定内容　変更あり
            out.writeUTF(AlternativeAirport)
            out.writeUTF(arrivalAirport)
            out.writeUTF(flightType.name)
            out.writeUTF(route)
            out.writeInt(cruiseSpeed)


            //ATC メモ
            out.writeInt(instructedAltitude)
            out.writeInt(tempAltitude)
            out.writeUTF(instructedHeading)
            out.writeInt(instructedSpeed)
            out.writeUTF(memoLong)
            out.writeUTF(memoShort)
        }
    }


    companion object {
        fun readData(input: DataInput): StripData {
            val entityID = input.readInt()
            val readStripData = input.readBoolean()

            //レーダーデータ
            val atcType = ATCType.valueOf(input.readUTF())
            val retStrip = StripData(entityID, atcType)


            if (readStripData) {
                //初期設定内容 変更なし
                retStrip.DepartureAirport = input.readUTF()
                retStrip.callSign = input.readUTF()
                retStrip.squawk = Squawk(input.readInt())
                retStrip.aircraftType = input.readUTF()
                retStrip.aircraftEquipment = input.readUTF()


                //初期設定内容　変更あり
                retStrip.AlternativeAirport = input.readUTF()
                retStrip.arrivalAirport = input.readUTF()
                retStrip.flightType = FlightType.valueOf(input.readUTF())
                retStrip.route = input.readUTF()
                retStrip.cruiseSpeed = input.readInt()


                //ATC メモ
                retStrip.instructedAltitude = input.readInt()
                retStrip.tempAltitude = input.readInt()
                retStrip.instructedHeading = input.readUTF()
                retStrip.instructedSpeed = input.readInt()
                retStrip.memoLong = input.readUTF()
                retStrip.memoShort = input.readUTF()
            }
            return retStrip
        }

        /**
         * コールサイン自動生成時の数字用
         */
        private var nextID = 1

        /**
         * コールサイン自動背性
         */
        fun getNextID(): String {
            val ret = String.format("%04d", nextID)
            nextID++
            if (nextID > 9999) {
                nextID = 0
            }
            return ret
        }
    }
}
