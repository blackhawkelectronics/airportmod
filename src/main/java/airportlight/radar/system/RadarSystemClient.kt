package airportlight.radar.system

import airportlight.radar.RadarData
import airportlight.radar.StripData
import airportlight.radar.artsdisplay.ArtsDisplayGui
import net.minecraft.client.Minecraft
import java.util.*
import kotlin.collections.HashMap

object RadarSystemClient {
    //region 情報保持変数
    /**
     * スプリットデータリスト
     */
    @JvmField
    var stripList = HashMap<Int, StripData>()

    /**
     * レーダーデータリスト
     */
    @kotlin.jvm.JvmField
    val radarDataList = java.util.HashMap<Int, RadarData>()

    //endregion

    //region サーバーからのストリップ情報更新
    /**
     * サーバーからのストリップの追加を反映
     */
    fun addStripSerToCli(
        addStrip: List<StripData>
    ) {
        //stripListに反映
        val addMap = HashMap<Int, StripData>()
        for (strip in addStrip) {
            stripList[strip.entityID] = strip
            addMap[strip.entityID] = strip
        }
        for (strip in addStrip) {
            val radarData = getRadarData(strip.entityID)
            if (radarData != null) {
                strip.radarData = radarData
                radarData.stripData = strip
            }
        }

        //パネルに反映
        val mc = Minecraft.getMinecraft()
        if (mc.currentScreen is ArtsDisplayGui) {
            if (addMap.isNotEmpty()) {
                (mc.currentScreen as ArtsDisplayGui).stripListGui?.addStrip(addMap)
            }
        }
    }

    /**
     * サーバーからのストリップの削除情報を反映
     */
    fun deleteStripSerToCli(
        delStrip: List<Int>
    ) {
        //stripListに反映
        for (entityID in delStrip) {
            val stripData = stripList[entityID]
            stripData?.radarData?.stripData = null
            stripList.remove(entityID)
        }

        //パネルに反映
        val mc = Minecraft.getMinecraft()
        if (mc.currentScreen is ArtsDisplayGui) {
            if (delStrip.isNotEmpty()) {
                (mc.currentScreen as ArtsDisplayGui).stripListGui?.delStrip(delStrip)
            }
        }
    }

    //endregion

    /**
     * サーバーからのレーダーデータアップデートを反映
     */
    fun updateRadarDataSerToCli(
        addRadarData: List<RadarData>,
        updateRadarData: List<RadarData>,
        delRadar: List<Int>
    ) {
        val addMap = HashMap<Int, RadarData>()
        for (data in addRadarData) {
            radarDataList[data.entityID] = data
            addMap[data.entityID] = data
        }

        for (data in updateRadarData) {
            if (radarDataList[data.entityID] != null) {
                radarDataList[data.entityID]!!.updateRadarDataSerToCli(data)
            } else {
                radarDataList[data.entityID] = data
            }
        }

        for (entityID in delRadar) {
            radarDataList.remove(entityID)
        }

        val updateSet = HashSet<Int>()
        for (data in updateRadarData) {
            updateSet.add(data.entityID)
        }
    }

    /**
     * レーダー画面を開いたときのレーダーデータ・ストリップデータ受け取り
     */
    fun newDataSerToCli(
        newRadar: List<RadarData>,
        newData: List<StripData>
    ) {
        radarDataList.clear()
        for (radar in newRadar) {
            radarDataList[radar.entityID] = radar
        }

        stripList.clear()
        for (strip in newData) {
            stripList[strip.entityID] = strip
        }

        for (strip in newData) {
            val radarData = getRadarData(strip.entityID)
            if (radarData != null) {
                strip.radarData = radarData
                radarData.stripData = strip
            }
        }
    }

    fun getStripData(entityID: Int): StripData? {
        return stripList[entityID]
    }

    fun getRadarData(entityID: Int): RadarData? {
        return radarDataList[entityID]
    }
}