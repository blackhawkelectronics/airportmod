package airportlight.radar.system.syncmessage

import airportlight.radar.StripData
import airportlight.radar.system.RadarSystemClient
import airportlight.util.*
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf

class StripDataMakeDeleteSync : IMessage {
    private lateinit var addStrip: List<StripData>

    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()

    constructor(
        addStrip: List<StripData>
    ) {
        this.addStrip = addStrip
    }

    override fun toBytes(buf: ByteBuf) {
        buf.outputStream()
            .gzip()
            .forData()
            .use { dos ->
                dos.writeList(this.addStrip) { it.writeData(dos) }
            }
    }

    override fun fromBytes(buf: ByteBuf) {
        buf.readBytes(buf.readableBytes()).array()
            .inputStream()
            .gunzip()
            .toData()
            .use { dis ->
                this.addStrip = dis.readList { StripData.readData(dis) }
            }
    }

    companion object : IMessageHandler<StripDataMakeDeleteSync, IMessage> {
        override fun onMessage(message: StripDataMakeDeleteSync, ctx: MessageContext): IMessage? {
            RadarSystemClient.addStripSerToCli(
                message.addStrip
            )
            return null
        }
    }
}