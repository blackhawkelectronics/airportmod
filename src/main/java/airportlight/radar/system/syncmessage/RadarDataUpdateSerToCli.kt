package airportlight.radar.system.syncmessage

import airportlight.radar.RadarData
import airportlight.radar.system.RadarSystemClient
import airportlight.util.*
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf

/**
 * レーダーの更新をサーバーからレーダー画面を開いているクライアントへ共有
 * (c) 2020 anatawa12
 * edit kuma
 */
class RadarDataUpdateSerToCli : IMessage, IMessageHandler<RadarDataUpdateSerToCli, IMessage> {
    private lateinit var addRadarData: List<RadarData>
    private lateinit var updateRadarData: List<RadarData>
    private lateinit var delRadarData: List<Int>

    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()

    constructor(
        addRadarData: List<RadarData>,
        updateRadarData: List<RadarData>,
        delRadarData: List<Int>
    ) {
        this.addRadarData = addRadarData
        this.updateRadarData = updateRadarData
        this.delRadarData = delRadarData
    }

    override fun toBytes(buf: ByteBuf) {
        buf.outputStream()
            .gzip()
            .forData()
            .use { dos ->
                dos.writeList(this.addRadarData) { it.writeData(dos) }
                dos.writeList(this.updateRadarData) { it.writeData(dos) }
                dos.writeList(this.delRadarData) { dos.writeInt(it) }
            }
    }

    override fun fromBytes(buf: ByteBuf) {
        buf.readBytes(buf.readableBytes()).array()
            .inputStream()
            .gunzip()
            .toData()
            .use { dis ->
                this.addRadarData = dis.readList { RadarData.readData(dis) }
                this.updateRadarData = dis.readList { RadarData.readData(dis) }
                this.delRadarData = dis.readList { dis.readInt() }
            }
    }


    override fun onMessage(message: RadarDataUpdateSerToCli, ctx: MessageContext): IMessage? {
        RadarSystemClient.updateRadarDataSerToCli(
            message.addRadarData,
            message.updateRadarData,
            message.delRadarData
        )
        return null
    }
}