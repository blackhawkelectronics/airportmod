package airportlight.towingcar;

public enum GateAction {
    NON(0), GateOpen(1), PickUp(2), Release(3), GateClose(4);

    private static GateAction[] values;
    public final int modeID;

    GateAction(int modeID) {
        this.modeID = modeID;
    }

    static GateAction getModeFromNum(int modeNum) {
        if (values == null) {
            values = new GateAction[]{NON, GateOpen, PickUp, Release, GateClose};
        }
        return values[modeNum];
    }
}
