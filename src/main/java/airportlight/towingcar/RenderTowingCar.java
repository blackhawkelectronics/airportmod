package airportlight.towingcar;

import airportlight.ModAirPortLight;
import net.minecraft.client.renderer.entity.RenderEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

public class RenderTowingCar extends RenderEntity {
    private final ResourceLocation textureYellow = new ResourceLocation(ModAirPortLight.DOMAIN, "yellow.png");
    private final ResourceLocation textureGray = new ResourceLocation(ModAirPortLight.DOMAIN, "gray.png");
    private final IModelCustom modelMissile;

    public RenderTowingCar() {
        this.modelMissile = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/mototokspacer250.obj"));
    }


    @Override
    public void doRender(Entity entity, double x, double y, double z, float yaw, float p_76986_9_) {
        if (entity instanceof EntityTowingCar) {
            GL11.glPushMatrix();
            GL11.glTranslated(x, y, z);
            GL11.glRotatef(-yaw, 0.0F, 1.0F, 0.0F);
            this.bindTexture(textureYellow);
            this.modelMissile.renderPart("Body");
            this.bindTexture(textureGray);
            GL11.glTranslated(0.0, ((EntityTowingCar) entity).liftHeight, 0);
            this.modelMissile.renderPart("Hand");
            GL11.glTranslated(0.85, 0.083, 0.75);
            GL11.glRotated(((EntityTowingCar) entity).gateAng, 0, 1, 0);
            this.modelMissile.renderPart("Gate");
            GL11.glPopMatrix();
        }
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        return textureYellow;
    }
}
