package airportlight.towingcar;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;

public class TowingDataFlagMessage implements IMessage, IMessageHandler<TowingDataFlagMessage, IMessage> {
    int towingCarEntityID;
    TowingDataFlag flag;
    int data;

    public TowingDataFlagMessage() {
    }

    public TowingDataFlagMessage(EntityTowingCar towingCar, TowingDataFlag flag, int data) {
        this.towingCarEntityID = towingCar.getEntityId();
        this.flag = flag;
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.towingCarEntityID = buf.readInt();
        this.flag = TowingDataFlag.getFlagFromMemberID(buf.readInt());
        this.data = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.towingCarEntityID);
        buf.writeInt(this.flag.memberID);
        buf.writeInt(this.data);
    }

    @Override
    public IMessage onMessage(TowingDataFlagMessage message, MessageContext ctx) {
        Entity entity = ctx.getServerHandler().playerEntity.worldObj.getEntityByID(message.towingCarEntityID);
        if (entity != null) {
            entity.getDataWatcher().updateObject(message.flag.flagID, message.data);
        }
        return null;
    }
}
