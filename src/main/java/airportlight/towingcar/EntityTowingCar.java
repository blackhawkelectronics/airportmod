package airportlight.towingcar;

import airportlight.ModAirPortLight;
import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.GuiID;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;

import java.util.Iterator;
import java.util.List;

public class EntityTowingCar extends Entity {
    public double liftHeight = 0;
    public double gateAng = 0;
    public GateMode gateMode = GateMode.Closed;
    private GateMode _gateModeBuff = GateMode.Closed;
    public GateAction gateAction = GateAction.NON;
    private double speedMultiplier = 0;
    private Entity pickupEntity;
    private double pickupDistance = 0;

    public EntityTowingCar(World p_i1582_1_) {
        super(p_i1582_1_);
        setSize(0.5f, 0.5f);
    }

    public EntityTowingCar(World p_i1705_1_, double p_i1705_2_, double p_i1705_4_, double p_i1705_6_) {
        this(p_i1705_1_);
        this.setPosition(p_i1705_2_, p_i1705_4_ + (double) this.yOffset, p_i1705_6_);
        this.motionX = 0.0D;
        this.motionY = 0.0D;
        this.motionZ = 0.0D;
        this.prevPosX = p_i1705_2_;
        this.prevPosY = p_i1705_4_;
        this.prevPosZ = p_i1705_6_;
        setSize(0.5f, 0.5f);
    }

    @Override
    protected void entityInit() {
        this.dataWatcher.addObject(TowingDataFlag.GuiOpen.flagID, 0);
        this.dataWatcher.addObject(TowingDataFlag.GateAction.flagID, 0);
        this.dataWatcher.addObject(TowingDataFlag.Brake.flagID, 0);
        this.dataWatcher.addObject(TowingDataFlag.Dismount.flagID, 0);
        this.dataWatcher.addObject(TowingDataFlag.GateMode.flagID, 0);
    }

    public void DataFlagUpdateSync(TowingDataFlag flag, int data) {
        this.dataWatcher.updateObject(flag.flagID, data);
        if (this.worldObj.isRemote && this.dataWatcher.hasChanges()) {
            PacketHandlerAPM.sendPacketServer(new TowingDataFlagMessage(this, flag, data));
        }
    }

    @Override
    public void setDead() {
        if (this.riddenByPlayer != null) {
            this.riddenByPlayer.setInvisible(false);
            this.riddenByPlayer = null;
            this.DataFlagUpdateSync(TowingDataFlag.Dismount, 0);
        }
        if (this.riddenByEntity == null) {
            if (this.dataWatcher.getWatchableObjectInt(TowingDataFlag.Dismount.flagID) == 1) {
                this.riddenByEntity.setInvisible(false);
                this.riddenByEntity.mountEntity(null);
                this.riddenByPlayer = null;
                this.DataFlagUpdateSync(TowingDataFlag.Dismount, 0);
            }
        }
        super.setDead();
    }

    public void onUpdate() {
        //運転処理/
        if (this.riddenByEntity != null && this.riddenByEntity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) this.riddenByEntity;
            //GUIに戻るフラグが立っていて
            if (this.worldObj.isRemote && Minecraft.getMinecraft().currentScreen == null && this.dataWatcher.getWatchableObjectInt(TowingDataFlag.GuiOpen.flagID) == 1) {
                player.openGui(ModAirPortLight.instance, GuiID.TowingCarDriver.getID(), this.worldObj, (int) this.posX, (int) this.posY, (int) this.posZ);
                this.dataWatcher.updateObject(TowingDataFlag.GuiOpen.flagID, 0);
            }

            int gaid = this.dataWatcher.getWatchableObjectInt(TowingDataFlag.GateAction.flagID);
            this.gateAction = GateAction.getModeFromNum(gaid);
            int gmid = this.dataWatcher.getWatchableObjectInt(TowingDataFlag.GateMode.flagID);
            this.gateMode = GateMode.getModeFromNum(gmid);


            if (this.gateMode == GateMode.Closed && this.gateAction == GateAction.GateOpen) {
                this.gateMode = GateMode.Opening;
                this.gateAction = GateAction.NON;
            } else if (this.gateMode == GateMode.Opened && this.gateAction == GateAction.PickUp) {
                float yawRad = (float) Math.toRadians(this.rotationYaw);
                double offsetX = 1.5 * Math.cos(yawRad) + this.posX;
                double offsetZ = 1.5 * Math.sin(yawRad) + this.posZ;
                double range = TowingCarSetting.SearchRange;
                AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(offsetX - range, this.posY - range, offsetZ - range, offsetX + range, this.posY + range, offsetZ + range);
                List list = worldObj.getEntitiesWithinAABBExcludingEntity(this, aabb);

                if (list != null && !list.isEmpty()) {
                    Iterator iterator = list.iterator();

                    double longBuff = range * range * 6;
                    Entity target = null;
                    while (iterator.hasNext()) {
                        Entity entity = (Entity) iterator.next();
                        if (entity.ridingEntity == null && !TowingCarSetting.pickUpIgnore.contains(EntityList.getEntityString(entity))) {
                            double longsq = entity.getDistanceSq(this.posX, this.posY, this.posZ);
                            if (longsq < longBuff) {
                                longBuff = longsq;
                                target = entity;
                            }
                        }
                    }
                    if (target != null) {
                        this.pickupEntity = target;
                        this.pickupDistance = MathHelper.sqrt_double(longBuff);
                        this.gateMode = GateMode.PickUp;
                        ((EntityPlayer) this.riddenByEntity).addChatMessage(new ChatComponentText("PickUp'ed " + EntityList.getEntityString(pickupEntity) + ((this.worldObj.isRemote) ? "(Cli)" : "(Ser)")));
                    }
                }
                this.gateAction = GateAction.NON;
            } else if (this.gateMode == GateMode.CanRun && this.gateAction == GateAction.Release) {
                this.gateMode = GateMode.LiftDown;
                this.gateAction = GateAction.NON;
            } else if (this.gateMode == GateMode.Opened && this.gateAction == GateAction.GateClose) {
                this.gateMode = GateMode.Closing;
            }

            if (!this.worldObj.isRemote && this.gateAction != GateAction.NON) {
                this.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.NON.modeID);
            }


            EntityLivingBase entitylivingbase = (EntityLivingBase) this.riddenByEntity;
            double stepYaw = -entitylivingbase.moveStrafing * 1.125F;
            if (3 < Math.abs(stepYaw)) {
                if (0 < stepYaw) {
                    stepYaw = 3;
                } else {
                    stepYaw = -3;
                }
            }
            this.rotationYaw = (float) MathHelper.wrapAngleTo180_double(this.rotationYaw + stepYaw);
            entitylivingbase.rotationYaw = (float) MathHelper.wrapAngleTo180_double(entitylivingbase.rotationYaw + stepYaw);
            this.speedMultiplier += (double) entitylivingbase.moveForward / 100;
            if (this.speedMultiplier > 0.1) {
                this.speedMultiplier = 0.1;
            } else if (this.speedMultiplier < -0.1) {
                this.speedMultiplier = -0.1;
            }
            this.stepHeight = 0.5F;
        } else {
            this.speedMultiplier *= 0.5;
            this.stepHeight = 0F;
        }

        switch (gateMode) {
            case Closed:
            case Opened:
            case CanRun:
                break;
            case Closing:
            case PickUp:
                if (0 < this.gateAng) {
                    gateAng -= 2;
                } else {
                    gateAng = 0;
                    if (gateMode == GateMode.Closing) {
                        this.gateMode = GateMode.Closed;
                    } else {
                        this.gateMode = GateMode.LiftUp;
                    }
                }
                break;
            case LiftUp:
                if (liftHeight < 0.2) {
                    liftHeight += 0.0125;
                } else {
                    liftHeight = 0.2;
                    this.gateMode = GateMode.CanRun;
                }
                break;
            case LiftDown:
                if (0 < liftHeight) {
                    liftHeight -= 0.0125;
                } else {
                    liftHeight = 0;
                    this.gateMode = GateMode.Opening;
                }
                break;
            case Opening:
                if (this.gateAng < 90) {
                    gateAng += 2;
                } else {
                    gateAng = 90;
                    if (this.pickupEntity != null) {
                        this.pickupEntity.prevRotationYaw = this.pickupEntity.rotationYaw;
                        this.pickupEntity = null;
                        this.pickupDistance = 0;
                    }
                    this.gateMode = GateMode.Opened;
                }
                break;
        }
        if (!worldObj.isRemote && this.gateMode != _gateModeBuff) {
            this.dataWatcher.updateObject(TowingDataFlag.GateMode.flagID, this.gateMode.modeID);
            _gateModeBuff = gateMode;
        }

        //ブレーキキーを押しているときにスピードダウン
        if (this.dataWatcher.getWatchableObjectInt(TowingDataFlag.Brake.flagID) == 1) {
            this.speedMultiplier *= 0.75;
        }
        if (Math.abs(this.speedMultiplier) < 0.005) {
            this.speedMultiplier = 0;
        }

        double rotationYawRad = Math.toRadians(this.rotationYaw);
        this.motionX = -Math.sin(rotationYawRad) * this.speedMultiplier;
        this.motionZ = Math.cos(rotationYawRad) * this.speedMultiplier;
        this.motionY -= 0.0245;

        this.moveEntity(this.motionX, this.motionY, this.motionZ);

        if (this.pickupEntity != null) {
            float yawRad = (float) Math.toRadians(this.rotationYaw);
            double pX = -pickupDistance * Math.sin(yawRad);
            double pZ = pickupDistance * Math.cos(yawRad);
            this.pickupEntity.setPosition(this.posX + pX, this.posY + this.liftHeight + this.pickupEntity.getYOffset(), this.posZ + pZ);
            this.pickupEntity.rotationPitch = this.rotationPitch;

            if (this.motionX != 0 || this.motionZ != 0) {
                double pickYawRad = Math.toRadians(this.pickupEntity.rotationYaw);

                double angDiff = Math.atan2(-this.motionX, this.motionZ) - pickYawRad;
                double vec = Math.sqrt(this.motionX * this.motionX + this.motionZ * this.motionZ);
                double moveStrafing = vec * Math.sin(angDiff);
                if (moveStrafing != 0) {
                    double strafingYawRad = moveStrafing / TowingCarSetting.WheelBase;
                    pickYawRad += strafingYawRad * 2;
                }

                this.pickupEntity.rotationYaw = (float) Math.toDegrees(pickYawRad);
            }
        }

        this.speedMultiplier *= 0.98;

        //視点切り替え
        if (this.worldObj.isRemote) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.gameSettings.keyBindTogglePerspective.isPressed()) {
                if (!buttonPushing) {
                    ++mc.gameSettings.thirdPersonView;

                    if (mc.gameSettings.thirdPersonView > 2) {
                        mc.gameSettings.thirdPersonView = 0;
                    }
                }
                buttonPushing = true;
            } else {
                buttonPushing = false;
            }
        }

        if (this.riddenByEntity == null) {
            if (this.riddenByPlayer != null) {
                this.riddenByPlayer.setInvisible(false);
                this.riddenByPlayer = null;
                this.DataFlagUpdateSync(TowingDataFlag.Dismount, 0);
            }
        } else {
            if (this.dataWatcher.getWatchableObjectInt(TowingDataFlag.Dismount.flagID) == 1) {
                this.riddenByEntity.setInvisible(false);
                this.riddenByEntity.mountEntity(null);
                this.riddenByPlayer = null;
                this.DataFlagUpdateSync(TowingDataFlag.Dismount, 0);
            }
        }
    }

    boolean buttonPushing = false;


    @Override
    public void updateRiderPosition() {
        if (this.riddenByEntity != null) {
            float yawRad = (float) Math.toRadians(this.rotationYaw);
            double offsetX = 2.5 * Math.sin(yawRad);
            double offsetZ = -2.5 * Math.cos(yawRad);
            this.riddenByEntity.setPosition(this.posX + offsetX, this.posY + this.getMountedYOffset() + this.riddenByEntity.getYOffset(), this.posZ + offsetZ);
        }
    }

    @Override
    public void moveEntity(double p_70091_1_, double p_70091_3_, double p_70091_5_) {
        this.worldObj.theProfiler.startSection("move");
        this.ySize *= 0.4F;
        double d3 = this.posX;
        double d4 = this.posY;
        double d5 = this.posZ;

        double d6 = p_70091_1_;
        double d7 = p_70091_3_;
        double d8 = p_70091_5_;
        AxisAlignedBB axisalignedbb = this.boundingBox.copy();

        List list = this.worldObj.func_147461_a(this.boundingBox.addCoord(p_70091_1_, p_70091_3_, p_70091_5_));

        for (Object o : list) {
            p_70091_3_ = ((AxisAlignedBB) o).calculateYOffset(this.boundingBox, p_70091_3_);
        }

        this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);

        if (!this.field_70135_K && d7 != p_70091_3_) {
            p_70091_5_ = 0.0D;
            p_70091_3_ = 0.0D;
            p_70091_1_ = 0.0D;
        }

        boolean flag1 = this.onGround || d7 != p_70091_3_ && d7 < 0.0D;
        int j;

        for (j = 0; j < list.size(); ++j) {
            p_70091_1_ = ((AxisAlignedBB) list.get(j)).calculateXOffset(this.boundingBox, p_70091_1_);
        }

        this.boundingBox.offset(p_70091_1_, 0.0D, 0.0D);

        if (!this.field_70135_K && d6 != p_70091_1_) {
            p_70091_5_ = 0.0D;
            p_70091_3_ = 0.0D;
            p_70091_1_ = 0.0D;
        }

        for (j = 0; j < list.size(); ++j) {
            p_70091_5_ = ((AxisAlignedBB) list.get(j)).calculateZOffset(this.boundingBox, p_70091_5_);
        }

        this.boundingBox.offset(0.0D, 0.0D, p_70091_5_);

        if (!this.field_70135_K && d8 != p_70091_5_) {
            p_70091_5_ = 0.0D;
            p_70091_3_ = 0.0D;
            p_70091_1_ = 0.0D;
        }

        double d10;
        double d11;
        int k;
        double d12;

        if (this.stepHeight > 0.0F && flag1 && (d6 != p_70091_1_ || d8 != p_70091_5_)) {
            d12 = p_70091_1_;
            d10 = p_70091_3_;
            d11 = p_70091_5_;
            p_70091_1_ = d6;
            p_70091_3_ = this.stepHeight;
            p_70091_5_ = d8;
            AxisAlignedBB axisalignedbb1 = this.boundingBox.copy();
            this.boundingBox.setBB(axisalignedbb);
            list = this.worldObj.func_147461_a(this.boundingBox.addCoord(d6, p_70091_3_, d8));

            for (k = 0; k < list.size(); ++k) {
                p_70091_3_ = ((AxisAlignedBB) list.get(k)).calculateYOffset(this.boundingBox, p_70091_3_);
            }

            this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);

            if (!this.field_70135_K && d7 != p_70091_3_) {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            for (k = 0; k < list.size(); ++k) {
                p_70091_1_ = ((AxisAlignedBB) list.get(k)).calculateXOffset(this.boundingBox, p_70091_1_);
            }

            this.boundingBox.offset(p_70091_1_, 0.0D, 0.0D);

            if (!this.field_70135_K && d6 != p_70091_1_) {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            for (k = 0; k < list.size(); ++k) {
                p_70091_5_ = ((AxisAlignedBB) list.get(k)).calculateZOffset(this.boundingBox, p_70091_5_);
            }

            this.boundingBox.offset(0.0D, 0.0D, p_70091_5_);

            if (!this.field_70135_K && d8 != p_70091_5_) {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            }

            if (!this.field_70135_K && d7 != p_70091_3_) {
                p_70091_5_ = 0.0D;
                p_70091_3_ = 0.0D;
                p_70091_1_ = 0.0D;
            } else {
                p_70091_3_ = -this.stepHeight;

                for (k = 0; k < list.size(); ++k) {
                    p_70091_3_ = ((AxisAlignedBB) list.get(k)).calculateYOffset(this.boundingBox, p_70091_3_);
                }

                this.boundingBox.offset(0.0D, p_70091_3_, 0.0D);
            }

            if (d12 * d12 + d11 * d11 >= p_70091_1_ * p_70091_1_ + p_70091_5_ * p_70091_5_) {
                p_70091_1_ = d12;
                p_70091_3_ = d10;
                p_70091_5_ = d11;
                this.boundingBox.setBB(axisalignedbb1);
            }
        }

        this.worldObj.theProfiler.endSection();
        this.worldObj.theProfiler.startSection("rest");
        this.posX = (this.boundingBox.minX + this.boundingBox.maxX) / 2.0D;
        this.posY = this.boundingBox.minY + (double) this.yOffset - (double) this.ySize;
        this.posZ = (this.boundingBox.minZ + this.boundingBox.maxZ) / 2.0D;
        this.isCollidedHorizontally = d6 != p_70091_1_ || d8 != p_70091_5_;
        this.isCollidedVertically = d7 != p_70091_3_;
        this.onGround = d7 != p_70091_3_ && d7 < 0.0D;
        this.isCollided = this.isCollidedHorizontally || this.isCollidedVertically;
        this.updateFallState(p_70091_3_, this.onGround);

        if (d6 != p_70091_1_) {
            this.motionX = 0.0D;
        }

        if (d7 != p_70091_3_) {
            this.motionY = 0.0D;
        }

        if (d8 != p_70091_5_) {
            this.motionZ = 0.0D;
        }


        try {
            this.func_145775_I();
        } catch (Throwable throwable) {
            CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Checking entity block collision");
            CrashReportCategory crashreportcategory = crashreport.makeCategory("Entity being checked for collision");
            this.addEntityCrashInfo(crashreportcategory);
            throw new ReportedException(crashreport);
        }


        this.worldObj.theProfiler.endSection();
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public float getCollisionBorderSize() {
        return 3F;
    }

    @Override
    public boolean attackEntityFrom(DamageSource p_70097_1_, float p_70097_2_) {
        if (this.isEntityInvulnerable()) {
            return false;
        } else {
            if (!this.worldObj.isRemote && !this.isDead) {
                this.setBeenAttacked();
                this.setDead();
            }
            return true;
        }
    }

    private EntityPlayer riddenByPlayer = null;

    @Override
    public boolean interactFirst(EntityPlayer p_130002_1_) {
        if (this.riddenByEntity == null || !(this.riddenByEntity instanceof EntityPlayer) || this.riddenByEntity == p_130002_1_) {
            p_130002_1_.mountEntity(this);
            if (!this.worldObj.isRemote) {
                float yawRad = (float) Math.toRadians(this.rotationYaw);
                double offsetX = 2.5 * Math.sin(yawRad);
                double offsetZ = -2.5 * Math.cos(yawRad);
                p_130002_1_.setLocationAndAngles(this.posX + offsetX, this.posY + this.getMountedYOffset() + this.riddenByEntity.getYOffset(), this.posZ + offsetZ, this.rotationYaw, this.rotationPitch);
                if (p_130002_1_ instanceof EntityPlayerMP) {
                    ((EntityPlayerMP) p_130002_1_).playerNetServerHandler.setPlayerLocation(this.posX, this.posY, this.posZ, this.rotationYaw, this.rotationPitch);
                }
                p_130002_1_.openGui(ModAirPortLight.instance, GuiID.TowingCarDriver.getID(), this.worldObj, (int) this.posX, (int) this.posY, (int) this.posZ);
            }
            this.riddenByPlayer = p_130002_1_;
            this.riddenByPlayer.setInvisible(true);
        }
        return true;
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound p_70037_1_) {
//        this.gateMode = GateMode.getModeFromNum(p_70037_1_.getInteger("gateMode"));
//        this.gateAction = GateAction.getModeFromNum(p_70037_1_.getInteger("gateAction"));
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound p_70014_1_) {
//        p_70014_1_.setInteger("gateMode", this.gateMode.modeID);
//        p_70014_1_.setInteger("gateAction", this.gateAction.modeID);
    }

}
