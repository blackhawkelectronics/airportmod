package airportlight.towingcar;

import airportlight.modcore.config.APMKeyConfig;
import airportlight.modcore.gui.ContainerAirPort;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TowingCarDriverGui extends GuiContainer {
    private EntityTowingCar towingCar;
    private EntityPlayer player;

    public TowingCarDriverGui() {
        super(new ContainerAirPort());
        //super(new ContainerAirPort());
        this.allowUserInput = true;
    }

    public TowingCarDriverGui(EntityPlayer player) {
        super(new ContainerAirPort());
        //super(new ContainerAirPort());
        this.player = player;
        if (player.ridingEntity instanceof EntityTowingCar) {
            this.towingCar = (EntityTowingCar) player.ridingEntity;
            player.rotationYaw = this.towingCar.rotationYaw;
        }
        this.allowUserInput = true;
    }

    public void initGui() {
        this.mc.mouseHelper.grabMouseCursor();
    }

    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(this.width - 100, this.height - 170, this.width - 10, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {

        GameSettings settings = Minecraft.getMinecraft().gameSettings;
        this.fontRendererObj.drawString("Move       : "
                        + Keyboard.getKeyName(settings.keyBindForward.getKeyCode()) + ", "
                        + Keyboard.getKeyName(settings.keyBindBack.getKeyCode()),
                this.width - 95, this.height - 160, -1, false);
        this.fontRendererObj.drawString("Strafing    : "
                + Keyboard.getKeyName(settings.keyBindLeft.getKeyCode()) + ", "
                + Keyboard.getKeyName(settings.keyBindRight.getKeyCode()), this.width - 95, this.height - 150, -1, false);
        this.fontRendererObj.drawString("Brake      : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake)), this.width - 95, this.height - 140, -1, false);
        this.fontRendererObj.drawString("Gate Open  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Open)), this.width - 95, this.height - 120, -1, false);
        this.fontRendererObj.drawString("PickUp     : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_PickUp)), this.width - 95, this.height - 110, -1, false);
        this.fontRendererObj.drawString("Release    : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Release)), this.width - 95, this.height - 100, -1, false);
        this.fontRendererObj.drawString("Gate Close : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Close)), this.width - 95, this.height - 90, -1, false);
        this.fontRendererObj.drawString("Yaw Reset  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)), this.width - 95, this.height - 70, -1, false);
        this.fontRendererObj.drawString("Dismount   : " + Keyboard.getKeyName(settings.keyBindSneak.getKeyCode()), this.width - 95, this.height - 60, -1, false);
    }

    public void onGuiClosed() {
        if (this.towingCar.riddenByEntity == null) {
            this.towingCar.getDataWatcher().updateObject(TowingDataFlag.GuiOpen.flagID, 0);
        }
        this.towingCar.DataFlagUpdateSync(TowingDataFlag.Brake, 0);
        if (Keyboard.isKeyDown(this.mc.gameSettings.keyBindSneak.getKeyCode())) {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Dismount, 1);
        } else {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Dismount, 0);
        }
    }

    @Override
    protected void keyTyped(char p_73869_1_, int p_73869_2_) {
        if (p_73869_2_ == 1) {
            this.mc.displayGuiScreen(new GuiIngameMenu());//closeScreen();
            Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor();
            return;
        }
        //移動などのMinecraft通常の処理を呼び出すための処置
        GameSettings settings = Minecraft.getMinecraft().gameSettings;
        if (Keyboard.isKeyDown(settings.keyBindSneak.getKeyCode())) {
            this.player.setInvisible(false);
            this.player.mountEntity(null);
            this.player.closeScreen();
            return;
        }

        if (Keyboard.getEventKeyState()) {
            int keyID = Keyboard.getEventKey();
            if (keyID == settings.keyBindCommand.getKeyCode()) {
                KeyBinding.onTick(keyID);
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GuiOpen, 1);
                this.player.closeScreen();
                Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor();
                return;
            }


            List<Integer> keyIDs = new ArrayList<Integer>(Arrays.asList(
                    settings.keyBindInventory.getKeyCode(),
                    settings.keyBindChat.getKeyCode(),
                    settings.keyBindTogglePerspective.getKeyCode(),
                    settings.keyBindSmoothCamera.getKeyCode(),
                    settings.field_152395_am.getKeyCode()
            ));
            if (keyIDs.contains(keyID)) {
                KeyBinding.onTick(keyID);
                if (keyID == settings.keyBindInventory.getKeyCode() || keyID == settings.keyBindChat.getKeyCode()) {
                    this.towingCar.getDataWatcher().updateObject(TowingDataFlag.GuiOpen.flagID, 1);
                    Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor();
                }
                return;
            }

            if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_PickUp)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.PickUp.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Release)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.Release.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Open)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.GateOpen.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Close)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.GateClose.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)) {
                this.towingCar.riddenByEntity.rotationYaw = this.towingCar.rotationYaw;
                this.towingCar.riddenByEntity.rotationPitch = this.towingCar.rotationPitch;
            }
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        boolean flag = Display.isActive();
        if (flag) {
            this.mc.mouseHelper.mouseXYChange();
            float f1 = this.mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
            float f2 = f1 * f1 * f1 * 8.0F;
            float f3 = (float) this.mc.mouseHelper.deltaX * f2;
            float f4 = (float) this.mc.mouseHelper.deltaY * f2;
            byte b0 = 1;

            if (this.mc.gameSettings.invertMouse) {
                b0 = -1;
            }
            this.mc.thePlayer.setAngles(f3, f4 * (float) b0);
        }
        if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake))) {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Brake, 1);
        } else {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Brake, 0);
        }
        GameSettings settings = Minecraft.getMinecraft().gameSettings;
        List<Integer> keyIDStates = new ArrayList<Integer>(Arrays.asList(
                settings.keyBindForward.getKeyCode(),
                settings.keyBindBack.getKeyCode(),
                settings.keyBindRight.getKeyCode(),
                settings.keyBindLeft.getKeyCode(),
                settings.keyBindPlayerList.getKeyCode()
        ));
        for (int stateKey : keyIDStates) {
            KeyBinding.setKeyBindState(stateKey, Keyboard.isKeyDown(stateKey));
        }
    }

    public boolean doesGuiPauseGame() {
        return false;
    }
}
