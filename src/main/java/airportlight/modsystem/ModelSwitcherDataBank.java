package airportlight.modsystem;

import airportlight.util.IUseWeightModel;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

import java.util.ArrayList;
import java.util.HashMap;


public class ModelSwitcherDataBank {
    private static final ArrayList<IUseWeightModel> listModelclass = new ArrayList<IUseWeightModel>();
    private static final HashMap<ResourceLocation, IModelCustom> mapModelCustom = new HashMap<ResourceLocation, IModelCustom>();

    public static <T extends IUseWeightModel> T registerModelClass(T model) {
        listModelclass.add(model);
        return model;
    }

    public static void switchModel(boolean UseWeightModel) {
        for (IUseWeightModel model : listModelclass) {
            model.readModel(UseWeightModel);
        }
    }

    public static IModelCustom loadModel(ResourceLocation location) {
        if (mapModelCustom.containsKey(location)) {
            return mapModelCustom.get(location);
        }
        IModelCustom model = AdvancedModelLoader.loadModel(location);
        mapModelCustom.put(location, model);
        return model;
    }
}
