package airportlight.modsystem.apmdatamanager

import airportlight.modsystem.navigation.NavRadioData
import airportlight.modsystem.navigation.frequency.FrequencyID
import airportlight.modsystem.navigation.ils.ILSData
import airportlight.util.Vec3I
import net.minecraftforge.common.DimensionManager
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.xml.sax.SAXException
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Boolean
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException
import javax.xml.transform.*
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import kotlin.Int
import kotlin.String
import kotlin.assert

object FrequencyNavDataFileManager {
    var olddefaultFileName = "LandPointData.xml"
    var defaultFileName = "NavPointData.xml"
    fun loadFreqNavData(): HashMap<Int, HashMap<Vec3I, NavRadioData>> {
        // 1. DocumentBuilderFactoryのインスタンスを取得する
        val factory = DocumentBuilderFactory.newInstance()
        // 2. DocumentBuilderのインスタンスを取得する
        var builder: DocumentBuilder? = null
        try {
            builder = factory.newDocumentBuilder()
        } catch (e: ParserConfigurationException) {
            e.printStackTrace()
        }

        // 3. DocumentBuilderにXMLを読み込ませ、Documentを作る
        var document: Document? = null
        try {
            val oldDir = File(DimensionManager.getCurrentSaveRootDirectory(), "AirpotMod/")
            val saveDir = defaultSaveDir
            if (oldDir.exists()) {
                oldDir.renameTo(saveDir)
            }

            saveDir.mkdirs()
            val fileName = defaultFileName
            var dataFile = File(saveDir, fileName)
            if (!dataFile.exists()) {
                val oldDataFile = File(saveDir, olddefaultFileName)
                if (oldDataFile.exists()) {
                    dataFile = oldDataFile
                } else {
                    writeFreqNavData(saveDir, fileName, emptyData)
                }
            }
            assert(builder != null)
            document = builder!!.parse(dataFile)
        } catch (e: SAXException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        assert(document != null)
        // 4. Documentから、ルート要素(Data)を取得する
        val tagTree = document!!.documentElement

        //5. File Format Version を取得する
        val formatVersionTag = tagTree.getElementsByTagName("FileFormat")
        val formatVersion = (formatVersionTag.item(0) as Element).getAttribute("Version").toInt()

        // 5. FreqencyList、FreqencyUseData
        val freqUseDataList =
            (tagTree.getElementsByTagName("FreqencyList").item(0) as Element).getElementsByTagName("FreqencyUseData")
        val ilsUseMap = HashMap<Int, HashMap<Vec3I, NavRadioData>>()


        // 6. 取得したFreqencyUseData要素でループする
        for (i in 0 until freqUseDataList.length) {
            // 7. ILS要素をElementにキャストする
            val freqUseData = freqUseDataList.item(i) as Element

            // 8. ILS要素の属性値と、テキストノードの値を取得する
            val freqencyID = freqUseData.getAttribute("freqencyID").toInt()
            val posX = freqUseData.getAttribute("posX").toInt()
            val posY = freqUseData.getAttribute("posY").toInt()
            val posZ = freqUseData.getAttribute("posZ").toInt()
            val name = freqUseData.textContent
            val isILSData = Boolean.parseBoolean(freqUseData.getAttribute("isILSData"))
            val navRadioData: NavRadioData = if (isILSData || formatVersion < 2) {
                val localizerAngStr = if (formatVersion < 3) {
                    freqUseData.getAttribute("Ang")
                } else {
                    freqUseData.getAttribute("localizerAng")
                }
                val glideSlopeAngStr = if (formatVersion < 3) {
                    "5"
                } else {
                    freqUseData.getAttribute("localizerAng")
                }

                val localizerAngDeg = localizerAngStr.toDouble()
                val glideSlopeAng = glideSlopeAngStr.toDouble()
                ILSData(
                    name, posX.toDouble(), posY.toDouble(), posZ.toDouble(), localizerAngDeg, glideSlopeAng, FrequencyID(freqencyID)
                )
            } else {
                NavRadioData(name, posX.toDouble(), posY.toDouble(), posZ.toDouble(), FrequencyID(freqencyID))
            }
            var freqDatas = ilsUseMap[freqencyID]
            if (freqDatas == null) {
                freqDatas = HashMap()
                ilsUseMap[freqencyID] = freqDatas
            }
            freqDatas[Vec3I(posX, posY, posZ)] = navRadioData
        }
        return ilsUseMap
    }

    fun <T : NavRadioData> writeFreqNavData(ilsUseMap: HashMap<Int, HashMap<Vec3I, T>>) {
        writeFreqNavData(defaultSaveDir, defaultFileName, ilsUseMap)
    }

    fun <T : NavRadioData> writeFreqNavData(
        saveDir: File, fileName: String?, ilsUseMap: HashMap<Int, HashMap<Vec3I, T>>
    ) {
        val factory = DocumentBuilderFactory.newInstance()
        var builder: DocumentBuilder? = null
        try {
            builder = factory.newDocumentBuilder()
        } catch (e: ParserConfigurationException) {
            e.printStackTrace()
        }
        assert(builder != null)
        val domImpl = builder!!.domImplementation
        val document = domImpl.createDocument("", "Data", null)
        val tagList = document.documentElement
        val elementFileFormat = document.createElement("FileFormat")
        elementFileFormat.setAttribute("Version", "3")
        tagList.appendChild(elementFileFormat)
        val elementILSList = document.createElement("FreqencyList")
        //ILSDataの内容を書き込み
        for ((freqencyID, value) in ilsUseMap) {
            for ((pos, data) in value) {
                val elementILS = document.createElement("FreqencyUseData")
                elementILS.setAttribute("freqencyID", freqencyID.toString())
                elementILS.setAttribute("posX", pos.x.toString())
                elementILS.setAttribute("posY", pos.y.toString())
                elementILS.setAttribute("posZ", pos.z.toString())
                elementILS.appendChild(document.createTextNode(data.name))
                val isILSData = data is ILSData
                elementILS.setAttribute("isILSData", isILSData.toString())
                if (isILSData) {
                    elementILS.setAttribute("localizerAng", (data as ILSData).localizerAngDeg.toString())
                    elementILS.setAttribute("glideSlopeAng", (data as ILSData).localizerAngDeg.toString())
                }
                elementILSList.appendChild(elementILS)
            }
        }
        tagList.appendChild(elementILSList)
        val transFactory = TransformerFactory.newInstance()
        var transformer: Transformer? = null
        try {
            transformer = transFactory.newTransformer()
            transformer.setOutputProperty(OutputKeys.INDENT, "yes")
            transformer.setOutputProperty(OutputKeys.METHOD, "xml")
        } catch (e: TransformerConfigurationException) {
            e.printStackTrace()
        }
        saveDir.mkdirs()
        val source = DOMSource(document)
        val newXML = File(saveDir, fileName)
        var os: FileOutputStream? = null
        newXML.createNewFile()
        try {
            os = FileOutputStream(newXML)
            val result = StreamResult(os)
            try {
                assert(transformer != null)
                transformer!!.transform(source, result)
            } catch (e: TransformerException) {
                e.printStackTrace()
            }

            os.flush()
            os.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    private val emptyData: HashMap<Int, HashMap<Vec3I, ILSData>> = object : HashMap<Int, HashMap<Vec3I, ILSData>>() {}


    val defaultSaveDir = File(DimensionManager.getCurrentSaveRootDirectory(), "AirportMod/")
}
