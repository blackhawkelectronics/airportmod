package airportlight.modsystem.navigation.autopilot

import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf

class MessageAPCourseSetting : IMessage, IMessageHandler<MessageAPCourseSetting, IMessage> {
    var courseA: Int = 0
    var courseB: Int = 0

    constructor() {}

    constructor(courseA: Int, courseB: Int) {
        this.courseA = courseA
        this.courseB = courseB
    }

    override fun fromBytes(buf: ByteBuf) {
        this.courseA = buf.readInt()
        this.courseB = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.courseA)
        buf.writeInt(this.courseB)
    }

    override fun onMessage(message: MessageAPCourseSetting, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val pilot = ctx.serverHandler.playerEntity
            val aircraft = ParentEntityGetter.getParent(pilot)
            AutopilotManager.startAutopilot(aircraft, pilot)
            val apSettingEntity = AutopilotManager.apSettings[aircraft]
            if (apSettingEntity != null) {
                apSettingEntity.courseA = message.courseA
                apSettingEntity.courseB = message.courseB
                apSettingEntity.update = true
//                return MessageAPCourseSetting(
//                    apSettingEntity.courseA,
//                    apSettingEntity.courseB
//                )
            }
            return null
        } else {
            //Client side
            AutopilotSettingCli._courseA = message.courseA
            AutopilotSettingCli._courseB = message.courseB

            AutopilotSettingCli.update = true
        }
        return null
    }
}
