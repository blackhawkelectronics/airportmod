package airportlight.modsystem.navigation.autopilot

import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft

class MessageAPRollSetting : IMessage, IMessageHandler<MessageAPRollSetting, IMessage> {
    var rollMode: EnumRollMode =
        EnumRollMode.HDGSEL
    var bankMode: EnumHedingBank =
        EnumHedingBank.A_20
    var headingActive: Int = 0
    var headingReady: Int = 0

    constructor() {}

    constructor(
        mode: EnumRollMode,
        bankMode: EnumHedingBank,
        hedingActive: Int,
        headingReady: Int
    ) {
        this.rollMode = mode
        this.bankMode = bankMode
        this.headingActive = hedingActive
        this.headingReady = headingReady
    }

    override fun fromBytes(buf: ByteBuf) {
        this.rollMode =
            EnumRollMode.getFromMode(buf.readInt())
        this.bankMode =
            EnumHedingBank.getFromMode(buf.readInt())
        this.headingActive = buf.readInt()
        this.headingReady = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.rollMode.mode)
        buf.writeInt(this.bankMode.mode)
        buf.writeInt(this.headingActive)
        buf.writeInt(this.headingReady)
    }

    override fun onMessage(message: MessageAPRollSetting, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val pilot = ctx.serverHandler.playerEntity
            val aircraft = ParentEntityGetter.getParent(pilot)
            AutopilotManager.startAutopilot(aircraft, pilot)
            val apSettingEntity = AutopilotManager.apSettings[aircraft]
            if (apSettingEntity != null) {
                apSettingEntity.rollMode = message.rollMode
                apSettingEntity.bankMode = message.bankMode
                apSettingEntity.headingActive = message.headingActive
                apSettingEntity.headingReady = message.headingReady
                apSettingEntity.update = true
                return MessageAPRollSetting(
                    apSettingEntity.rollMode,
                    apSettingEntity.bankMode,
                    apSettingEntity.headingActive,
                    apSettingEntity.headingReady
                )
            }
            return null
        } else {
            //Client side
            val player = Minecraft.getMinecraft().thePlayer
            AutopilotSettingCli._rollMode = message.rollMode
            AutopilotSettingCli._bankMode = message.bankMode
            AutopilotSettingCli._headingActive = message.headingActive
            AutopilotSettingCli.headingReady = message.headingReady

            AutopilotSettingCli.update = true
        }
        return null
    }
}
