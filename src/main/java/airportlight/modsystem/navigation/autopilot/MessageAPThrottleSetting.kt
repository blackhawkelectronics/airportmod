package airportlight.modsystem.navigation.autopilot

import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft

class MessageAPThrottleSetting : IMessage, IMessageHandler<MessageAPThrottleSetting, IMessage> {
    var throttleMode: EnumEngageThrottleMode =
        EnumEngageThrottleMode.NON
    var ias: Int = 2

    constructor()

    constructor(
        throttleMode: EnumEngageThrottleMode,
        ias: Int
    ) {
        this.throttleMode = throttleMode
        this.ias = ias
    }

    override fun fromBytes(buf: ByteBuf) {
        this.throttleMode =
            EnumEngageThrottleMode.getFromMode(buf.readInt())
        this.ias = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.throttleMode.mode)
        buf.writeInt(this.ias)
    }

    override fun onMessage(message: MessageAPThrottleSetting, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val pilot = ctx.serverHandler.playerEntity
            val aircraft = ParentEntityGetter.getParent(pilot)
            AutopilotManager.startAutopilot(aircraft, pilot)
            val apSettingEntity = AutopilotManager.apSettings[aircraft]
            if (apSettingEntity != null) {
                apSettingEntity.throttleMode = message.throttleMode
                apSettingEntity.ias = message.ias
                apSettingEntity.update = true
                return MessageAPThrottleSetting(apSettingEntity.throttleMode, apSettingEntity.ias)
            }
            return null
        } else {
            //Client side
            val player = Minecraft.getMinecraft().thePlayer
            AutopilotSettingCli._throttleMode = message.throttleMode
            AutopilotSettingCli._ias = message.ias

            AutopilotSettingCli.update = true
        }
        return null
    }
}
