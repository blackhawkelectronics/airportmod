package airportlight.modsystem.navigation.autopilot;

public enum EnumEngageThrottleMode {
    NON(0), N1(1), SPEED(2), CMD(3), CWS(4);
    public final int mode;

    EnumEngageThrottleMode(int mode) {
        this.mode = mode;
    }

    public static EnumEngageThrottleMode getFromMode(int mode) {
        return values()[mode];
    }
}
