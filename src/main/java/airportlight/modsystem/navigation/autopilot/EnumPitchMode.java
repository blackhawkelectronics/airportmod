package airportlight.modsystem.navigation.autopilot;

public enum EnumPitchMode {
    VNAV(0), VS(1), ALTHOLD(2), LVLCHG(3), APP(4);
    public final int mode;

    EnumPitchMode(int mode) {
        this.mode = mode;
    }

    public static EnumPitchMode getFromMode(int mode) {
        return values()[mode];
    }
}
