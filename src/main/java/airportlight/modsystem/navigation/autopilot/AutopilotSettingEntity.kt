package airportlight.modsystem.navigation.autopilot

import airportlight.modcore.PacketHandlerAPM
import airportlight.modsystem.navigation.frequency.FrequencyID
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayerMP

class AutopilotSettingEntity : IAutopilotSetting {
    constructor()
    constructor(aircraft: Entity, pilot: EntityPlayerMP) {
        this.aircraft = aircraft
        this.pilot = pilot
    }

    lateinit var aircraft: Entity
    lateinit var pilot: EntityPlayerMP

    var carrier: EntityAutopilotCarrier? = null

    var freqActive: FrequencyID? =
        FrequencyID(0)
    var freqStandby: FrequencyID? =
        FrequencyID(0)

    //EngageD
    override fun checkAndManageAPEngage() {
        if (engage) {
            AutopilotManager.engage(aircraft, pilot)
        } else {
            AutopilotManager.disEngage(aircraft)
        }
    }

    var _fd: Boolean = false
    override var fd: Boolean
        set(value) {
            _fd = value
            checkAndManageAPEngage()
        }
        get() = _fd

    var _autoThrottle: Boolean = false
    override var autoThrottle: Boolean
        set(value) {
            _autoThrottle = value
            checkAndManageAPEngage()
        }
        get() = _autoThrottle

    var _cmdA: Boolean = false
    override var cmdA: Boolean
        set(value) {
            _cmdA = value
            checkAndManageAPEngage()
        }
        get() = _cmdA

    var _cmdB: Boolean = false
    override var cmdB: Boolean
        set(value) {
            _cmdB = value
            checkAndManageAPEngage()
        }
        get() = _cmdB

    fun setEngageData(
        fd: Boolean = this._fd,
        autoThrottle: Boolean = this._autoThrottle,
        cmdA: Boolean = this._cmdA,
        cmdB: Boolean = this._cmdB
    ) {
        _fd = fd
        _autoThrottle = autoThrottle
        _cmdA = cmdA
        _cmdB = cmdB

        checkAndManageAPEngage()
    }

    //パワーモード
    override var throttleMode: EnumEngageThrottleMode =
        EnumEngageThrottleMode.SPEED
    override var ias: Int = 2

    //ロールモード
    override var rollMode = EnumRollMode.HDGSEL
    override var bankMode = EnumHedingBank.A_20
    override var headingActive = 360
        set(value) {
            field = value
            val carrier = AutopilotManager.apSettings[aircraft]?.carrier
            carrier?.setRotation(value, carrier.rotationPitch.toInt())
        }
    override var headingReady = 360

    override var courseA: Int = 360
    override var courseB: Int = 360


    //ピッチモード
    override var pitchMode: EnumPitchMode =
        EnumPitchMode.ALTHOLD
    override var altitudeActive: Int = 70

    fun syncAllData() {
        syncFreqDatabase()
        syncEngageData()
        syncPitchData()
        syncRollData()
        syncCourseData()
        syncThrottleData()
    }

    fun syncFreqDatabase(
        active: FrequencyID? = this.freqActive,
        standby: FrequencyID? = this.freqStandby
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageEntityUseFreqData(active, standby), pilot)
    }

    fun syncEngageData(
        fd: Boolean = this.fd,
        autoThrottle: Boolean = this.autoThrottle,
        cmdA: Boolean = this.cmdA,
        cmdB: Boolean = this.cmdB
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageAPEngageSetting(fd, autoThrottle, cmdA, cmdB), pilot)
    }

    fun syncPitchData(
        pitchMode: EnumPitchMode = this.pitchMode,
        altitude: Int = this.altitudeActive
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageAPPitchSetting(pitchMode, altitude), pilot)
    }

    fun syncRollData(
        rollMode: EnumRollMode = this.rollMode,
        bankMode: EnumHedingBank = this.bankMode,
        headingActive: Int = this.headingActive,
        hedingReady: Int = this.headingReady
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageAPRollSetting(rollMode, bankMode, headingActive, hedingReady), pilot)
    }

    fun syncCourseData(
        courseA: Int = this.courseA,
        courseB: Int = this.courseB
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageAPCourseSetting(courseA, courseB), pilot)
    }

    fun syncThrottleData(
        throttleMode: EnumEngageThrottleMode = this.throttleMode,
        ias: Int = this.ias
    ) {
        PacketHandlerAPM.sendPacketEPM(MessageAPThrottleSetting(throttleMode, ias), pilot)
    }
}