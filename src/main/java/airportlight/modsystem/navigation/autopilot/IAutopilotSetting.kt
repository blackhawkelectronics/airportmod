package airportlight.modsystem.navigation.autopilot

import airportlight.util.MathHelperAirPort

abstract class IAutopilotSetting {
    var update: Boolean = false
    fun isUpdate(): Boolean {
        if (update) {
            update = false
            return true
        }
        return false
    }

    var _engagedFlag: Boolean = false

    //Engage
    val engage: Boolean
        get() {
            val ret = fd && (cmdA || cmdB)
            return ret
        }

    abstract fun checkAndManageAPEngage()

    abstract var fd: Boolean
    abstract var autoThrottle: Boolean
    abstract var cmdA: Boolean
    abstract var cmdB: Boolean

    //パワーモード
    abstract var throttleMode: EnumEngageThrottleMode
    abstract var ias: Int

    //ロールモード
    abstract var rollMode: EnumRollMode
    abstract var bankMode: EnumHedingBank

    abstract var courseA: Int
    abstract var courseB: Int

    fun canUseVOR(): Boolean {
        return true//(rollMode == EnumRollMode.VORLOC || rollMode == EnumRollMode.APP)
    }

    //LNAVを使う場合は、そちらで実装する。
    fun getVORCourseAngDeg(useA: Boolean): Int {
        when (rollMode) {
            EnumRollMode.VORLOC, EnumRollMode.APP, EnumRollMode.HDGSEL -> {
                return if (useA) {
                    MathHelperAirPort.floor360(courseA)
                } else {
                    MathHelperAirPort.floor360(courseB)
                }
            }
            EnumRollMode.LNAV -> {
                throw IllegalStateException("When using VORCourseAng, AP-RollMode must be VORLOC or APP.")
            }
        }
    }

    abstract var headingActive: Int
    abstract var headingReady: Int


    //ピッチモード
    abstract var pitchMode: EnumPitchMode
    abstract var altitudeActive: Int
}