package airportlight.modsystem.navigation.autopilot;

public enum EnumRollMode {
    HDGSEL(0), LNAV(1), VORLOC(2), APP(3);
    public final int mode;

    EnumRollMode(int mode) {
        this.mode = mode;
    }

    public static EnumRollMode getFromMode(int mode) {
        return values()[mode];
    }
}
