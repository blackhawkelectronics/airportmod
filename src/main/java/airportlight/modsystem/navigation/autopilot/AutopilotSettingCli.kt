package airportlight.modsystem.navigation.autopilot

import airportlight.modcore.PacketHandlerAPM

object AutopilotSettingCli : IAutopilotSetting() {
    override fun checkAndManageAPEngage() {
    }

    var _fd: Boolean = false
    override var fd: Boolean
        set(value) {
            syncEngageData(fd = value)
        }
        get() = _fd

    var _autoThrottle: Boolean = false
    override var autoThrottle: Boolean
        set(value) {
            syncEngageData(autoThrottle = value)
        }
        get() = _autoThrottle

    var _cmdA: Boolean = false
    override var cmdA: Boolean
        set(value) {
            syncEngageData(cmdA = value)
        }
        get() = _cmdA

    var _cmdB: Boolean = false
    override var cmdB: Boolean
        set(value) {
            syncEngageData(cmdB = value)
        }
        get() = _cmdB

    fun syncEngageData(
        fd: Boolean = _fd,
        autoThrottle: Boolean = _autoThrottle,
        cmdA: Boolean = _cmdA,
        cmdB: Boolean = _cmdB
    ) {
        PacketHandlerAPM.sendPacketServer(MessageAPEngageSetting(fd, autoThrottle, cmdA, cmdB))
    }


    //パワーモード
    var _throttleMode = EnumEngageThrottleMode.SPEED
    override var throttleMode: EnumEngageThrottleMode
        set(value) {
            syncThrottleData(throttleMode = value)
        }
        get() = _throttleMode

    var _ias: Int = 0
    override var ias: Int
        set(value) {
            syncThrottleData(ias = value)
        }
        get() = _ias

    fun syncThrottleData(
        throttleMode: EnumEngageThrottleMode = _throttleMode,
        ias: Int = _ias
    ) {
        PacketHandlerAPM.sendPacketServer(MessageAPThrottleSetting(throttleMode, ias))
    }

    //ロールモード
    var _rollMode = EnumRollMode.HDGSEL
    override var rollMode: EnumRollMode
        set(value) {
            syncRollData(rollMode = value)
        }
        get() = _rollMode

    var _bankMode = EnumHedingBank.A_20
    override var bankMode: EnumHedingBank
        set(value) {
            syncRollData(bankMode = value)
        }
        get() = _bankMode

    var _headingActive = 360
    override var headingActive: Int
        set(value) {
            syncRollData(headingActive = value)
        }
        get() = _headingActive

    override var headingReady = 360

    fun syncRollData(
        rollMode: EnumRollMode = _rollMode,
        bankMode: EnumHedingBank = _bankMode,
        headingActive: Int = _headingActive,
        headingReady: Int = this.headingReady
    ) {
        PacketHandlerAPM.sendPacketServer(MessageAPRollSetting(rollMode, bankMode, headingActive, headingReady))
    }

    var _courseA: Int = 360
    override var courseA: Int
        set(value) {
            _courseA = value
            syncCourseData(courseA = value)
        }
        get() = _courseA

    var _courseB: Int = 360
    override var courseB: Int
        set(value) {
            syncCourseData(courseB = value)
        }
        get() = _courseB

    fun syncCourseData(
        courseA: Int = _courseA,
        courseB: Int = _courseB
    ) {
        PacketHandlerAPM.sendPacketServer(MessageAPCourseSetting(courseA, courseB))
    }

    //ピッチモード
    var _pitchMode: EnumPitchMode =
        EnumPitchMode.ALTHOLD
    override var pitchMode: EnumPitchMode
        set(value) {
            syncPitchData(pitchMode = value)
        }
        get() = _pitchMode

    var _altitudeActive: Int = 70
    override var altitudeActive: Int
        set(value) {
            syncPitchData(altitude = value)
        }
        get() = _altitudeActive
    var altitudeReady: Int = 70

    fun syncPitchData(
        pitchMode: EnumPitchMode = _pitchMode,
        altitude: Int = _altitudeActive
    ) {
        PacketHandlerAPM.sendPacketServer(MessageAPPitchSetting(pitchMode, altitude))
    }
}