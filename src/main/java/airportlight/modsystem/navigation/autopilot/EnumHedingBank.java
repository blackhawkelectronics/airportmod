package airportlight.modsystem.navigation.autopilot;

public enum EnumHedingBank {
    A_10(0, 10), A_15(1, 15), A_20(2, 20), A_25(3, 25), A_30(4, 30);
    public final int mode;
    public final int bank;

    EnumHedingBank(int mode, int bank) {
        this.mode = mode;
        this.bank = bank;
    }

    public static EnumHedingBank getFromMode(int mode) {
        return values()[mode];
    }
}
