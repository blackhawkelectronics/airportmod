package airportlight.modsystem.navigation.autopilot

import airportlight.modsystem.navigation.ils.NavFreqManager
import airportlight.util.MathHelperAirPort
import airportlight.util.ParentEntityGetter
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.entity.Entity
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.MathHelper
import net.minecraft.world.World
import kotlin.math.*

class EntityAutopilotCarrier(p_i1582_1_: World) : Entity(p_i1582_1_) {
    constructor(p_i1582_1_: World, apSetting: IAutopilotSetting) : this(p_i1582_1_) {
        this.apSetting = apSetting
    }

    var apSetting: IAutopilotSetting? = null

    companion object {
        val dwidSpd = 10
        val dwidVecYaw = 11
        val dwidVecPitch = 12
        val dwidRotYaw = 13
        val dwidRotPitch = 14
    }

    var carringEntity: Entity? = null

    var spd: Double = 0.0
    var vecYawRad: Double = 0.0
    var vecPitchRad: Double = 0.0

    init {
        yOffset = 0.0f
        width = 0.6f
        height = 0.01f
        ySize = 0.0f
        isInvisible = true
        renderDistanceWeight = 0.0
    }

    @SideOnly(Side.CLIENT)
    override fun getShadowSize(): Float {
        return 0.0f
    }

    override fun entityInit() {
        this.dataWatcher.addObject(dwidSpd, 0.0f)
        this.dataWatcher.addObject(dwidVecYaw, 0.0f)
        this.dataWatcher.addObject(dwidVecPitch, 0.0f)
        this.dataWatcher.addObject(dwidRotYaw, 0.0f)
        this.dataWatcher.addObject(dwidRotPitch, 0.0f)
        isInvisible = true
    }

    /**
     * Call From Side.Server Only
     * */
    fun setVelHorizontal(parent: Entity) {
        this.spd =
            sqrt(parent.motionX * parent.motionX + parent.motionY * parent.motionY + parent.motionZ * parent.motionZ)
        this.apSetting!!.ias = MathHelper.floor_double(spd * 10)
        (this.apSetting as AutopilotSettingEntity).syncThrottleData()

        this.vecYawRad = -atan2(parent.motionX, parent.motionZ)
        this.rotationYaw = Math.toDegrees(this.vecYawRad).toFloat()
        this.vecPitchRad = 0.0
        this.rotationPitch = Math.toDegrees(this.vecPitchRad).toFloat()

        this.dataWatcher.updateObject(dwidSpd, this.spd.toFloat())
        this.dataWatcher.updateObject(dwidVecYaw, this.vecYawRad.toFloat())
        this.dataWatcher.updateObject(dwidVecPitch, this.vecPitchRad.toFloat())
        this.dataWatcher.updateObject(dwidRotYaw, this.rotationYaw.toFloat())
        this.dataWatcher.updateObject(dwidRotPitch, this.rotationPitch.toFloat())
    }

    override fun onUpdate() {
        super.onUpdate()
        if (this.riddenByEntity == null) {
            if (carringEntity == null && !this.worldObj.isRemote) {
                this.setDead()
                return
            }
            if (carringEntity != null) {
                carringEntity!!.setLocationAndAngles(
                    posX,
                    posY - carringEntity!!.yOffset + 0.05,
                    posZ,
                    this.rotationYaw,
                    this.rotationPitch
                )
                carringEntity!!.updateRiderPosition()
                carringEntity = null
                this.apSetting?.cmdA = false
                this.apSetting?.cmdB = false
                this.apSetting?.checkAndManageAPEngage()
                if (!this.worldObj.isRemote) {
                    (apSetting as AutopilotSettingEntity).syncEngageData()
                }
            }
        } else if (this.worldObj.isRemote && this.carringEntity == null) {
            this.carringEntity = this.riddenByEntity
        }
        if (this.dataWatcher.hasChanges()) {
            this.spd = this.dataWatcher.getWatchableObjectFloat(dwidSpd).toDouble()
            this.vecYawRad = this.dataWatcher.getWatchableObjectFloat(dwidVecYaw).toDouble()
            this.vecPitchRad = this.dataWatcher.getWatchableObjectFloat(dwidVecPitch).toDouble()
            this.rotationYaw = this.dataWatcher.getWatchableObjectFloat(dwidRotYaw)
            this.rotationPitch = this.dataWatcher.getWatchableObjectFloat(dwidRotPitch)
        }


        if (!worldObj.isRemote && this.apSetting != null) {
            when (apSetting!!.pitchMode) {
                EnumPitchMode.VNAV -> {
                }
                EnumPitchMode.VS -> {
                }
                EnumPitchMode.ALTHOLD -> {
                }
                EnumPitchMode.LVLCHG -> {
                    this.vecPitchRad = 0.0
                }
                EnumPitchMode.APP -> {
                    val aircraft = (this.apSetting as AutopilotSettingEntity).aircraft
                    (this.apSetting as AutopilotSettingEntity).carrier?.let { aircraft.onGround = it.onGround }
                    assert(aircraft == this.carringEntity)
                    if (ParentEntityGetter.isOnGrownd(aircraft, true)) {
                        this.rotationPitch = 0.0f
                        this.vecPitchRad = 0.0
                        this.apSetting!!.pitchMode =
                            EnumPitchMode.ALTHOLD
                        this.apSetting!!.rollMode =
                            EnumRollMode.HDGSEL
                        this.apSetting!!.cmdA = false
                        this.apSetting!!.cmdB = false
                        this.apSetting!!.checkAndManageAPEngage()
                        (this.apSetting as AutopilotSettingEntity).syncEngageData()
                        (this.apSetting as AutopilotSettingEntity).syncPitchData()
                        (this.apSetting as AutopilotSettingEntity).syncRollData()
                    } else {
                        val useDataSet =
                            NavFreqManager.getActiveILS(
                                aircraft,
                                (this.apSetting as AutopilotSettingEntity).freqActive,
                                this.apSetting as AutopilotSettingEntity,
                            )
                        if (useDataSet == null) {
                            this.apSetting!!.pitchMode =
                                EnumPitchMode.ALTHOLD
                            (this.apSetting as AutopilotSettingEntity).syncPitchData()
                            this.apSetting!!.rollMode =
                                EnumRollMode.HDGSEL
                            this.apSetting!!.headingActive = this.rotationYaw.roundToInt() + 180
                            this.apSetting!!.headingReady = this.apSetting!!.headingActive
                            (this.apSetting as AutopilotSettingEntity).syncRollData()
                        } else {
                            val approachPitchAng = useDataSet.ilsApproachAngDeg
                            val approachYawAng = -MathHelper.wrapAngleTo180_double(useDataSet.localizerAngDeg + 180)

                            //roll
                            if (abs(useDataSet.diffPosYaw) <= 0.0436) {
                                //5度以内のとき誘導
                                this.vecYawRad = Math.toRadians(approachYawAng) + useDataSet.diffPosYaw.toFloat() * 3
                                this.rotationYaw =
                                    approachYawAng.toFloat() + Math.toDegrees(useDataSet.diffPosYaw).toFloat() * 3
                            } else if (
                                abs(useDataSet.diffYaw) < 90 &&
                                ((useDataSet.diffPosYaw > 0 && this.rotationYaw > approachYawAng + Math.toDegrees(
                                    useDataSet.diffPosYaw
                                ))
                                        || (useDataSet.diffPosYaw < 0 && this.rotationYaw < approachYawAng + Math.toDegrees(
                                    useDataSet.diffPosYaw
                                )))
                            ) {
                                //5度以上でコースにインターセプトできるとき
                                //HDGHLD
                                val i = 0
                            } else {
                                this.vecYawRad = Math.toRadians(approachYawAng) + useDataSet.diffPosYaw.toFloat() * 4
                                this.rotationYaw =
                                    approachYawAng.toFloat() + Math.toDegrees(useDataSet.diffPosYaw).toFloat() * 4

                                //インターセプトできないとき、アプローチモードをキャンセルして
//                                AutopilotSettingCli.rollMode = EnumRollMode.HDGSEL
//                                val hdg = MathHelperAirPort.floor360(this.rotationYaw)
//                                AutopilotSettingCli.syncRollData(
//                                    EnumRollMode.HDGSEL,
//                                    AutopilotSettingCli.bankMode,
//                                    hdg,
//                                    hdg
//                                )
//                                AutopilotSettingCli.pitchMode = EnumPitchMode.ALTHOLD
                            }

                            //this.rotationYaw = approachYawAng.toFloat() + Math.toDegrees(useDataSet.diffPosYaw).toFloat() * 3


                            //pitchMode
                            var flareMode = false
                            if (this.posY < this.worldObj.getHeightValue(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posZ)) + 17) {
                                val iX = MathHelper.floor_double(posX)
                                val iY = MathHelper.floor_double(posY)
                                val iZ = MathHelper.floor_double(posZ)
                                for (y in iY - 18..iY) {
                                    val block = worldObj.getBlock(iX, y, iZ)
                                    if (block.canCollideCheck(worldObj.getBlockMetadata(iX, y, iZ), false) && block.isCollidable) {
                                        flareMode = true
                                        break
                                    }
                                }
                            }

                            if (flareMode) {
                                this.rotationPitch = -5f
                            } else {
                                this.rotationPitch = -3f - Math.toDegrees(useDataSet.diffPosPitch).toFloat() * 1.5f
                            }
                            this.vecPitchRad = -Math.toRadians(approachPitchAng) + useDataSet.diffPosPitch.toFloat() * 2.5f
                        }
                    }
                }
            }

            when (apSetting!!.rollMode) {
                EnumRollMode.HDGSEL -> {
                }
                EnumRollMode.LNAV -> {
                }
                EnumRollMode.VORLOC -> {
                    val aircraft = (this.apSetting as AutopilotSettingEntity).aircraft
                    assert(aircraft == this.carringEntity)

                    val useDataSet =
                        NavFreqManager.getActiveNav(
                            aircraft,
                            (this.apSetting as AutopilotSettingEntity).freqActive,
                            this.apSetting as AutopilotSettingEntity,
                            true
                        )
                    if (useDataSet == null) {
                        this.apSetting!!.pitchMode =
                            EnumPitchMode.ALTHOLD
                        this.apSetting!!.rollMode =
                            EnumRollMode.HDGSEL
                        (this.apSetting as AutopilotSettingEntity).syncPitchData()
                        (this.apSetting as AutopilotSettingEntity).syncRollData()
                    } else {
                        val approachYawAng = -MathHelper.wrapAngleTo180_double(useDataSet.localizerAngDeg + 180)

                        //roll
                        if (abs(useDataSet.diffPosYaw) <= 0.0436) {
                            //5度以内のとき誘導
                            this.vecYawRad = Math.toRadians(approachYawAng) + useDataSet.diffPosYaw.toFloat() * 3
                            this.rotationYaw =
                                approachYawAng.toFloat() + Math.toDegrees(useDataSet.diffPosYaw).toFloat() * 3
                        } else if (
                            abs(useDataSet.diffYaw) < 90 &&
                            ((useDataSet.diffPosYaw > 0 && this.rotationYaw > approachYawAng + Math.toDegrees(
                                useDataSet.diffPosYaw
                            ))
                                    || (useDataSet.diffPosYaw < 0 && this.rotationYaw < approachYawAng + Math.toDegrees(
                                useDataSet.diffPosYaw
                            )))
                        ) {
                            //5度以上でコースにインターセプトできるとき
                            //HDGHLD
                            val i = 0
                        } else {
                            this.vecYawRad = Math.toRadians(approachYawAng) + useDataSet.diffPosYaw.toFloat() * 4
                            this.rotationYaw =
                                approachYawAng.toFloat() + Math.toDegrees(useDataSet.diffPosYaw).toFloat() * 4

                            //インターセプトできないとき、アプローチモードをキャンセルして
//                                AutopilotSettingCli.rollMode = EnumRollMode.HDGSEL
//                                val hdg = MathHelperAirPort.floor360(this.rotationYaw)
//                                AutopilotSettingCli.syncRollData(
//                                    EnumRollMode.HDGSEL,
//                                    AutopilotSettingCli.bankMode,
//                                    hdg,
//                                    hdg
//                                )
//                                AutopilotSettingCli.pitchMode = EnumPitchMode.ALTHOLD
                        }
                        this.vecYawRad = Math.toRadians(this.rotationYaw.toDouble())
                    }
                }
                EnumRollMode.APP -> {
                    //pitchのところで処理実装済み
                }
            }
            this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidVecYaw, this.vecYawRad.toFloat())
            this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidVecPitch, this.vecPitchRad.toFloat())
            this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidRotYaw, this.rotationYaw.toFloat())
            this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidRotPitch, this.rotationPitch.toFloat())
        }


        motionY = sin(vecPitchRad) * this.spd
        val motionXZ = cos(vecPitchRad) * this.spd
        motionX = -sin(vecYawRad) * motionXZ
        motionZ = cos(vecYawRad) * motionXZ

        if (!this.worldObj.isRemote) {
            if (apSetting == null) {
                this.setDead()
                return
            } else {
                if (this.apSetting!!.isUpdate()) {
                    if (this.apSetting!!.throttleMode == EnumEngageThrottleMode.SPEED) {
                        this.spd = this.apSetting!!.ias / 10.toDouble()
                        this.dataWatcher.updateObject(dwidSpd, this.spd.toFloat())
                    }
                }
                if (this.apSetting!!.pitchMode == EnumPitchMode.LVLCHG) {
                    if (MathHelper.floor_double(this.posY) < apSetting!!.altitudeActive) {
                        this.motionY += 1.0
                    } else if (MathHelper.floor_double(this.posY) == apSetting!!.altitudeActive) {
                        this.vecPitchRad = 0.0
                        this.apSetting!!.pitchMode =
                            EnumPitchMode.ALTHOLD
                        (this.apSetting as AutopilotSettingEntity).syncPitchData()
                        this.motionY = 0.0
                    } else {
                        this.motionY -= 1.0
                    }
                }
            }
        }

        this.moveEntity(motionX, motionY, motionZ)
    }

    override fun updateRiderPosition() {
        if (riddenByEntity != null) {
            riddenByEntity.motionX = motionX
            riddenByEntity.motionY = motionY
            riddenByEntity.motionZ = motionZ
            riddenByEntity.posX = posX
            riddenByEntity.posY = posY + 0.05
            riddenByEntity.posZ = posZ
            riddenByEntity.rotationYaw = this.rotationYaw
            riddenByEntity.rotationPitch = this.rotationPitch
            riddenByEntity.updateRiderPosition()
        }
    }

    fun setRotation(yaw: Int, pitch: Int) {
        setRotation(MathHelperAirPort.floor360(yaw - 180).toFloat(), pitch.toFloat())
        this.vecYawRad = Math.toRadians(this.rotationYaw.toDouble())
        this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidVecYaw, this.vecYawRad.toFloat())
        this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidRotYaw, this.rotationYaw)
        this.dataWatcher.updateObject(EntityAutopilotCarrier.dwidVecPitch, this.vecPitchRad.toFloat())
    }

    override fun readEntityFromNBT(p_70037_1_: NBTTagCompound) {
    }

    override fun writeEntityToNBT(p_70014_1_: NBTTagCompound) {
    }
}
