package airportlight.modsystem.navigation;

public enum EnumNavMode {
    OFF(0), NAV(1), Command(2);
    public final int mode;

    EnumNavMode(int mode) {
        this.mode = mode;
    }

    public static EnumNavMode getFromMode(int mode) {
        return values()[mode];
    }
}
