package airportlight.modsystem.navigation.vordme;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.normal.ModelBaseNormalLight;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class VORDMEModel extends ModelBaseNormalLight<TileVORDME> implements IUseWeightModel {
    protected final ResourceLocation textureWhite;
    protected IModelCustom modelbody;

    public VORDMEModel() {
        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");

        IModelCustom model;
        if (APMConfig.UseWeightModel) {
            model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/vordme.obj"));
        } else {
            model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightvordme.obj"));
        }
        this.modelbody = model;
    }

    @Override
    protected void ModelBodyRender() {
    }

    @Override
    public void readModel(boolean UseWeightModel) {
        IModelCustom model;
        if (UseWeightModel) {
            model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/vordme.obj"));
        } else {
            model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightvordme.obj"));
        }
        this.modelbody = model;
    }

    @Override
    public void render(@NotNull TileVORDME tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);

        if (DisplayListIDs.VORDME == -1) {
            DisplayListIDs.VORDME = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.VORDME, GL11.GL_COMPILE);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);
            this.modelbody.renderPart("DME");
            for (int i = 0, m = 20; i < m; i++) {
                this.modelbody.renderPart("VOR");
                GL11.glRotated(18, 0, 1, 0);
            }
            GL11.glEndList();
        }

        GL11.glCallList(DisplayListIDs.VORDME);
        GL11.glPopMatrix();
    }
}