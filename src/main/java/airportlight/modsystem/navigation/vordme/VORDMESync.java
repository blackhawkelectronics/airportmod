package airportlight.modsystem.navigation.vordme;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class VORDMESync extends TileEntityMessage implements IMessageHandler<VORDMESync, IMessage> {
    String vordmeName;
    FrequencyID navFreqID;

    public VORDMESync() {
        super();
    }

    public VORDMESync(TileVORDME tile, String vordmeName, FrequencyID navFreqID) {
        super(tile);
        this.vordmeName = vordmeName;
        this.navFreqID = navFreqID;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.vordmeName = tag.getString("vordmeName");
        this.navFreqID = new FrequencyID(tag.getInteger("navFreqID"));
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("vordmeName", this.vordmeName);
        tag.setInteger("navFreqID", this.navFreqID.ID);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(VORDMESync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileVORDME) {
            ((TileVORDME) tile).setDatas(message.vordmeName, message.navFreqID, ctx.side);
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        } else {
            NavFreqManager.Companion.setUserData(message.getPos(), new NavRadioData(vordmeName, message.getPos().x, message.getPos().y, message.getPos().z, message.navFreqID), ctx.side);
        }
        return null;
    }
}
