package airportlight.modsystem.navigation.vordme;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockVORDME extends BlockNormal {
    public BlockVORDME() {
        super();
        setBlockName("VORDME");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":vordme");
        setBlockBounds(0.2F, 0F, 0.2F, 0.8F, 2.0F, 0.8F);
    }

    @Override
    public void onBlockAdded(World p_149726_1_, int p_149726_2_, int p_149726_3_, int p_149726_4_) {
        super.onBlockAdded(p_149726_1_, p_149726_2_, p_149726_3_, p_149726_4_);
    }


    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ - 0.52, p_149633_3_, (double) p_149633_4_ - 0.52, (double) p_149633_2_ + 1.52, (double) p_149633_3_ + 2.5, (double) p_149633_4_ + 1.52);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        player.openGui(ModAirPortLight.instance, GuiID.VORDMESetting.getID(), world, x, y, z);
        return true;
    }

    @Override
    public void breakBlock(World worldObj, int x, int y, int z, Block block, int metadata) {
        ((TileVORDME) worldObj.getTileEntity(x, y, z)).NavFreqUnUse();
        super.breakBlock(worldObj, x, y, z, block, metadata);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TileVORDME();
    }
}
