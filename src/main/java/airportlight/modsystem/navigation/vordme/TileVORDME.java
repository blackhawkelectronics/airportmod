package airportlight.modsystem.navigation.vordme;

import airportlight.modcore.normal.TileNormal;
import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.modsystem.navigation.frequency.IFrequencyContainer;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.util.Vec3I;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.nbt.NBTTagCompound;


public class TileVORDME extends TileNormal implements IFrequencyContainer {
    private String vordmeName = "";
    private FrequencyID freqencyID = new FrequencyID(40);


    public TileVORDME() {
    }

    public String getVORDMEName() {
        return this.vordmeName;
    }

    public FrequencyID getFreqencyID() {
        return this.freqencyID;
    }

    public boolean setDatas(String vordmeName, FrequencyID freqencyID, Side runSide) {
        boolean isChanged = false;
        if (!this.vordmeName.equals(vordmeName)) {
            this.vordmeName = vordmeName;
            isChanged = true;
        }
        if (this.freqencyID.ID != freqencyID.ID) {
            NavFreqUnUse(runSide);
            this.freqencyID = freqencyID;
            isChanged = true;
        }
        if (isChanged) {
            NavFreqManager.Companion.setUserData(new Vec3I(this.xCoord, this.yCoord, this.zCoord), new NavRadioData(this.vordmeName, this.xCoord, this.yCoord, this.zCoord, this.freqencyID), runSide);
            this.markDirty();
        }
        return isChanged;
    }

    public void NavFreqUnUse() {
        this.NavFreqUnUse(this.worldObj.isRemote ? Side.CLIENT : Side.SERVER);
    }

    public void NavFreqUnUse(Side runSide) {
        NavFreqManager.Companion.unUse(this.freqencyID, new Vec3I(this.xCoord, this.yCoord, this.zCoord), runSide);
    }

    @Override
    public void markDirty() {
        super.markDirty();
        this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
    }


    @Override
    public void writeToNBT(NBTTagCompound nbtTag) {
        super.writeToNBT(nbtTag);
        nbtTag.setString("vordmeName", this.vordmeName);
        nbtTag.setInteger("frequencyID", freqencyID.ID);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTag) {
        super.readFromNBT(nbtTag);
        this.vordmeName = nbtTag.getString("vordmeName");
        this.freqencyID = new FrequencyID(nbtTag.getInteger("frequencyID"));
        NavFreqManager.Companion.setUserData(new Vec3I(this.xCoord, this.yCoord, this.zCoord), new NavRadioData(this.vordmeName, this.xCoord, this.yCoord, this.zCoord, freqencyID), (this.worldObj == null || !this.worldObj.isRemote) ? Side.SERVER : Side.CLIENT);
    }

}
