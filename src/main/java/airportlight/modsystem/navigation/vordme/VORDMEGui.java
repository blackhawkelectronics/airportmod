package airportlight.modsystem.navigation.vordme;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.RotatingKnob;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.util.Consumer;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class VORDMEGui extends GuiContainer {
    private static final int biVORDMEName = 30;
    private static final int biFreqKnob = 31;


    private TileVORDME tile;
    private int inputID;
    private StringInputGuiButton buttonVORDMEName;
    private RotatingKnob buttonKnob;

    private String scrapString;

    private String tempVORDMEName;
    private FrequencyID tempFreID;

    public VORDMEGui() {
        super(new ContainerAirPort());
    }

    public VORDMEGui(TileVORDME tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempVORDMEName = tile.getVORDMEName();
        this.tempFreID = tile.getFreqencyID();
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        boolean isChanged = this.tile.setDatas(this.tempVORDMEName, this.tempFreID, (this.tile.getWorldObj().isRemote ? Side.CLIENT : Side.SERVER));
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new VORDMESync(this.tile, this.tempVORDMEName, this.tempFreID));
        }
    }


    @Override
    public void initGui() {
        buttonVORDMEName = new StringInputGuiButton(biVORDMEName, this.width / 2 - 70, this.height / 2 - 35, tile.getVORDMEName());
        buttonKnob = new RotatingKnob(biFreqKnob, this.width / 2 + 100, this.height / 2 + 40, 40, new Consumer() {
            @Override
            public void accept(int i) {
                tempFreID = tempFreID.Add(i);
            }
        });
        this.buttonList.add(buttonVORDMEName);
        this.buttonList.add(buttonKnob);
    }

    protected void actionPerformed(GuiButton button) {
        if (button.id == biVORDMEName) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        }
    }

    private void EnterSettings() {
        if (this.inputID == biVORDMEName) {
            if (scrapString.isEmpty()) {
                this.buttonVORDMEName.displayString = tile.getVORDMEName();
            } else {
                this.buttonVORDMEName.displayString = scrapString;
            }
            this.tempVORDMEName = this.buttonVORDMEName.displayString;
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.buttonKnob.doWheel(mouseX, mouseY, Mouse.getDWheel());
        this.fontRendererObj.drawString("VOR/DME Name  : ", this.width / 2 - 140, this.height / 2 - 30, -1, false);
        this.fontRendererObj.drawString("Frequency    :  " + String.format("%.2f", this.tempFreID.getFreqency()), this.width / 2 - 140, this.height / 2 + 30, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biVORDMEName) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                this.tempVORDMEName = scrapString;
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (Character.isLetter(eventChar) || Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonVORDMEName.displayString = scrapString;
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    public int parse36(String input) {
        int i = Integer.parseInt(input);
        return floor36(i);
    }

    public int floor36(int ang36) {
        int ret = ang36 % 36;
        if (ret < 0) {
            ret += 36;
        }
        if (ret == 0) {
            ret = 36;
        }
        return ret;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
