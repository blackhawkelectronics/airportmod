package airportlight.modsystem.navigation

import airportlight.modsystem.navigation.frequency.FrequencyID

open class NavRadioData(
    @JvmField val name: String,
    @JvmField val x: Double = 0.0,
    @JvmField val y: Double = 0.0,
    @JvmField val z: Double = 0.0,
    @JvmField var frequencyID: FrequencyID
) {
    var haveRadioWideRange = false
        private set

    var radioWideRangeRad = 0.35
        protected set(value) {
            field = value
            haveRadioWideRange = true
        }

    var radioLongRangeSqrt = (2000 * 2000).toDouble()
}
