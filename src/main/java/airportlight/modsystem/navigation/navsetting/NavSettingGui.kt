package airportlight.modsystem.navigation.navsetting

import airportlight.ModAirPortLight
import airportlight.modcore.gui.ContainerAirPort
import airportlight.modcore.gui.GuiContainerAirPort
import airportlight.modcore.gui.Indicator.BarIndicator
import airportlight.modcore.gui.Indicator.MAIndicator
import airportlight.modcore.gui.custombutton.*
import airportlight.modsystem.navigation.ClientNavSetting
import airportlight.modsystem.navigation.EnumNavMode
import airportlight.modsystem.navigation.autopilot.*
import airportlight.modsystem.navigation.autopilot.EnumPitchMode.*
import airportlight.modsystem.navigation.ils.NavFreqManager
import airportlight.util.Consumer
import airportlight.util.MathHelperAirPort
import airportlight.util.ParentEntityGetter
import net.minecraft.client.gui.GuiButton
import net.minecraft.util.MathHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class NavSettingGui : GuiContainerAirPort(ContainerAirPort()) {
    companion object {
        private const val biFrequency = 30
        private const val biSwitch = 31
        private const val biModeSelect = 32
        private const val biHeading = 33
        private const val biRollMode_HeadingSel = 34
        private const val biRollMode_LNAV = 35
        private const val biRollMode_VORLOC = 36
        private const val biRollMode_APP = 37
        private const val biFDL = 38
        private const val biFDR = 39
        private const val biAP_CMDA = 40
        private const val biATARM = 41
        private const val biN1 = 42
        private const val biSpeed = 43
        private const val biAP_CMDB = 44
        private const val biAP_CWSA = 45
        private const val biAP_CWSB = 46
        private const val biPitchMode_VNAV = 47
        private const val biPitchMode_LVLCHG = 48
        private const val biPitchMode_ALTHLD = 47
        private const val biPitchMode_VS = 48
        private const val biAPCourseL = 49
        private const val biAPCourseR = 50
        private const val biAPIAS = 51
        private const val biALTKnob = 52
        private const val biVSKnob = 53
        private const val biDisEngage = 54
        private const val biCO = 55
        private const val biSPDINTV = 56
        private const val biALTINTV = 57


        private val biRollModes =
            ArrayList(listOf(biRollMode_HeadingSel, biRollMode_LNAV, biRollMode_VORLOC, biRollMode_APP))
        private val biPitchModes =
            ArrayList(listOf(biPitchMode_VNAV, biPitchMode_LVLCHG, biPitchMode_ALTHLD, biPitchMode_VS))

        private val texturePanel = ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/ilspanel.png")

        private val textureAutopilotPanel = ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/autopilot_panel.png")
    }

    private lateinit var buttonFreqKnob: RotatingKnob
    private lateinit var buttonSwitch: SwitchButton
    private lateinit var buttonSelectKnob: SelectKnob


    private lateinit var buttonFDL: ToggleButton
    private lateinit var buttonFDR: ToggleButton
    private lateinit var indicatorMAL: MAIndicator
    private lateinit var indicatorMAR: MAIndicator

    private lateinit var buttonCourseL: CourseKnob
    private lateinit var buttonCourseR: CourseKnob

    private lateinit var indicatorThrottle: BarIndicator
    private lateinit var buttonAutoThrottle: ToggleButton

    private lateinit var buttonN1: ThrottleModeButton
    private lateinit var buttonSpeed: ThrottleModeButton

    private lateinit var buttonIASKnob: IASKnob
    private lateinit var buttonCO: BlackButton
    private lateinit var buttonSPDINTV: BlackButton

    private lateinit var buttonPitch_VNAV: PitchModeButton
    private lateinit var buttonPitch_LVLCHG: PitchModeButton

    private lateinit var buttonHeadingKnob: HeadingKnob
    private lateinit var buttonRoll_HDGSEL: RollModeButton
    private lateinit var buttonRoll_LNAV: RollModeButton
    private lateinit var buttonRoll_VORLOC: RollModeButton
    private lateinit var buttonRoll_APP: RollModeButton

    private lateinit var buttonALTKnob: AltKnob
    private lateinit var buttonPitch_ALTHLD: PitchModeButton
    private lateinit var buttonPitch_VS: PitchModeButton
    private lateinit var buttonALTINTV: BlackButton

    private lateinit var buttonVSKnob: VSKnob

    private lateinit var buttonAP_CMDA: ThrottleModeButton
    private lateinit var buttonAP_CMDB: ThrottleModeButton
    private lateinit var buttonAP_CWSA: ThrottleModeButton
    private lateinit var buttonAP_CWSB: ThrottleModeButton

    private lateinit var buttonAP_DisEnagage: DisEngageButton


    private var navMode: EnumNavMode


    init {
        navMode = ClientNavSetting.navMode
    }

    override fun onGuiClosed() {
        ClientNavSetting.navMode = navMode
    }

    override fun initGui() {
        super.initGui()

        //ILS ・ VOR FreqSet
        buttonFreqKnob = RotatingKnob(biFrequency, width / 2 + 41, height / 2 + 73, 45, object : Consumer() {
            override fun accept(i: Int) {
                NavSettingCli._navFreqStandByID = NavSettingCli.navFreqStandByID.Add(i)
            }
        })
        buttonSwitch = SwitchButton(biSwitch, width / 2 - 12, height / 2 + 20, 23)
        buttonSelectKnob = SelectKnob(
            biModeSelect,
            width / 2 + 100,
            height / 2 + 73,
            35,
            arrayOf("OFF", "NAV", "Com"),
            ClientNavSetting.navMode.mode,
            object : Consumer() {
                override fun accept(valIndex: Int) {
                    navMode = EnumNavMode.getFromMode(valIndex)
                }
            })

        buttonList.add(buttonFreqKnob)
        buttonList.add(buttonSwitch)
        buttonList.add(buttonSelectKnob)
        doWheels.add(buttonFreqKnob)
        doWheels.add(buttonSelectKnob)


        //FD
        buttonFDL = ToggleButton(biFDL, width / 2 - 157.0, height / 2 - 63.0, 10.0)
        buttonFDR = ToggleButton(biFDR, width / 2 + 154.0, height / 2 - 63.0, 10.0)
        indicatorMAL = MAIndicator(width / 2 - 158.0, height / 2 - 83.0, 15.0)
        indicatorMAR = MAIndicator(width / 2 + 153.0, height / 2 - 83.0, 15.0)
        buttonList.add(buttonFDL)
        buttonList.add(buttonFDR)
        buttonList.add(indicatorMAL)
        buttonList.add(indicatorMAR)

        //COURSE
        buttonCourseL = CourseKnob(biAPCourseL, width / 2 - 172, height / 2 - 73, 14, object : Consumer() {
            override fun accept(i: Int) {
                AutopilotSettingCli.courseA = MathHelperAirPort.floor360(AutopilotSettingCli.courseA + i)
            }
        })
        buttonCourseL.setKnobAngle((AutopilotSettingCli.courseA % 20) * 18)
        buttonCourseR = CourseKnob(biAPCourseR, width / 2 + 173, height / 2 - 73, 14, object : Consumer() {
            override fun accept(i: Int) {
                //AutopilotSettingCli.courseB = MathHelperAirPort.floor360(AutopilotSettingCli.courseB + i)
            }
        })
        buttonCourseR.enabled = false
        buttonList.add(buttonCourseL)
        buttonList.add(buttonCourseR)
        doWheels.add(buttonCourseL)
        doWheels.add(buttonCourseR)

        //AP Throttle
        indicatorThrottle = BarIndicator(width / 2 - 134.0, height / 2 - 98.0, 9.0)
        buttonAutoThrottle =
            ToggleButton(biATARM, width / 2 - 133.5, height / 2 - 97.0, 18.0, false,
                object : Consumer() {
                    override fun accept(i: Int) {
                        if (i == 0) {
                            indicatorThrottle.indicator = false
                        } else if (i == 1) {
                            indicatorThrottle.indicator = true
                        }
                    }
                })
        buttonN1 =
            ThrottleModeButton(
                biN1,
                width / 2 - 143,
                height / 2 - 63,
                15,
                EnumEngageThrottleMode.N1
            ).setEnabled(false)
        buttonSpeed =
            ThrottleModeButton(
                biSpeed,
                width / 2 - 124,
                height / 2 - 63,
                15,
                EnumEngageThrottleMode.SPEED
            ).setEnabled(
                false
            )
        buttonList.add(indicatorThrottle)
        buttonList.add(buttonAutoThrottle)
        buttonList.add(buttonN1)
        buttonList.add(buttonSpeed)


        //IAS
        buttonIASKnob = IASKnob(biAPIAS, width / 2 - 89, height / 2 - 70, 14, object : Consumer() {
            override fun accept(i: Int) {
                var ias = AutopilotSettingCli.ias
                ias += i
                if (ias < 0) {
                    ias = 0
                } else if (20 < ias) {
                    ias = 20
                }
                AutopilotSettingCli.ias = ias
            }
        })
        buttonCO = BlackButton(biCO, width / 2 - 109.0, height / 2 - 79.5, 7.0).setEnabled(false)
        buttonSPDINTV = BlackButton(biSPDINTV, width / 2 - 73.5, height / 2 - 79.5, 7.0).setEnabled(false)
        buttonList.add(buttonIASKnob)
        buttonList.add(buttonCO)
        buttonList.add(buttonSPDINTV)
        doWheels.add(buttonIASKnob)


        //AP Pitch
        buttonPitch_VNAV =
            PitchModeButton(biPitchMode_VNAV, width / 2 - 70, height / 2 - 104, 15, VNAV).setEnabled(false)
        buttonPitch_LVLCHG = PitchModeButton(biPitchMode_LVLCHG, width / 2 - 70, height / 2 - 63, 15, LVLCHG)
        buttonList.add(buttonPitch_VNAV)
        buttonList.add(buttonPitch_LVLCHG)


        //AP Roll
        buttonHeadingKnob = HeadingKnob(
            biHeading,
            width / 2 - 35,
            height / 2 - 77,
            22,
            AutopilotSettingCli.bankMode.mode,
            object : Consumer() {
                override fun accept(i: Int) {
                    //Out
                    AutopilotSettingCli.bankMode =
                        EnumHedingBank.getFromMode(i)
                }
            },
            object : Consumer() {
                override fun accept(i: Int) {
                    //In
                    AutopilotSettingCli.headingReady += i
                    AutopilotSettingCli.headingReady = MathHelperAirPort.floor360(AutopilotSettingCli.headingReady)
                    AutopilotSettingCli.headingActive = AutopilotSettingCli.headingReady
                }
            })

        buttonRoll_HDGSEL =
            RollModeButton(
                biRollMode_HeadingSel,
                width / 2 - 43,
                height / 2 - 63,
                15,
                EnumRollMode.HDGSEL
            )
        buttonRoll_LNAV =
            RollModeButton(
                biRollMode_LNAV,
                width / 2 - 16,
                height / 2 - 104,
                15,
                EnumRollMode.LNAV
            ).setEnabled(false)
        buttonRoll_VORLOC = RollModeButton(
            biRollMode_VORLOC,
            width / 2 - 16,
            height / 2 - 83,
            15,
            EnumRollMode.VORLOC
        )
        buttonRoll_APP = RollModeButton(
            biRollMode_APP,
            width / 2 - 16,
            height / 2 - 63,
            15,
            EnumRollMode.APP
        )
        selectRollMode(AutopilotSettingCli.rollMode)
        buttonList.add(buttonHeadingKnob)
        buttonList.add(buttonRoll_HDGSEL)
        buttonList.add(buttonRoll_LNAV)
        buttonList.add(buttonRoll_VORLOC)
        buttonList.add(buttonRoll_APP)
        doWheels.add(buttonHeadingKnob)

        buttonALTKnob = AltKnob(biALTKnob, width / 2 + 24, height / 2 - 77, 13, object : Consumer() {
            override fun accept(i: Int) {
                AutopilotSettingCli.altitudeReady += i
                if (AutopilotSettingCli.altitudeReady < 0) {
                    AutopilotSettingCli.altitudeReady = 0
                }
            }
        })
        buttonPitch_ALTHLD = PitchModeButton(biPitchMode_ALTHLD, width / 2 + 16, height / 2 - 63, 15, ALTHOLD)
        buttonPitch_VS = PitchModeButton(biPitchMode_VS, width / 2 + 38, height / 2 - 63, 15, VS).setEnabled(false)
        buttonALTINTV = BlackButton(biALTINTV, width / 2 + 45.0, height / 2 - 77.0, 8.0).setEnabled(false)
        buttonList.add(buttonALTKnob)
        buttonList.add(buttonPitch_ALTHLD)
        buttonList.add(buttonPitch_VS)
        buttonList.add(buttonALTINTV)
        doWheels.add(buttonALTKnob)

        buttonVSKnob = VSKnob(biVSKnob, width / 2 + 78, height / 2 - 67, 25, object : Consumer() {
            override fun accept(i: Int) {
                //AutopilotSettingCli.vertspeed
            }
        }).setEnabled(false)
        buttonList.add(buttonVSKnob)
        doWheels.add(buttonVSKnob)

        // A/P Engage
        buttonAP_CMDA = ThrottleModeButton(
            biAP_CMDA,
            width / 2 + 101,
            height / 2 - 99,
            15,
            EnumEngageThrottleMode.CMD
        )
        buttonAP_CMDB = ThrottleModeButton(
            biAP_CMDB,
            width / 2 + 124,
            height / 2 - 99,
            15,
            EnumEngageThrottleMode.CMD
        )
        buttonAP_CWSA =
            ThrottleModeButton(
                biAP_CWSA,
                width / 2 + 101,
                height / 2 - 80,
                15,
                EnumEngageThrottleMode.CWS
            ).setEnabled(
                false
            )
        buttonAP_CWSB =
            ThrottleModeButton(
                biAP_CWSB,
                width / 2 + 125,
                height / 2 - 80,
                15,
                EnumEngageThrottleMode.CWS
            ).setEnabled(
                false
            )
        buttonList.add(buttonAP_CMDA)
        buttonList.add(buttonAP_CMDB)
        buttonList.add(buttonAP_CWSA)
        buttonList.add(buttonAP_CWSB)

        buttonAP_DisEnagage = DisEngageButton(biDisEngage, width / 2 + 103.0, height / 2 - 59.0, 35.0)
        buttonList.add(buttonAP_DisEnagage)

        updateButtonInfoAuto()
    }

    fun updateButtonInfoAuto() {
        buttonFDL.switchUp = AutopilotSettingCli.fd
        buttonFDR.switchUp = AutopilotSettingCli.fd
        indicatorMAL.indicator = AutopilotSettingCli.fd
        indicatorMAR.indicator = AutopilotSettingCli.fd

        buttonCourseL.setKnobAngle((AutopilotSettingCli.courseA % 20) * 18)
        buttonCourseR.setKnobAngle((AutopilotSettingCli.courseB % 20) * 18)

        indicatorThrottle.indicator = AutopilotSettingCli.autoThrottle
        buttonAutoThrottle.switchUp = AutopilotSettingCli.autoThrottle
        selectThrottleMode(AutopilotSettingCli.throttleMode)

        selectPitchMode(AutopilotSettingCli.pitchMode)
        buttonHeadingKnob.setOutValIndex(AutopilotSettingCli.bankMode.mode)
        selectRollMode(AutopilotSettingCli.rollMode)

        buttonAP_CMDA.lightMode = EnumButtonLightMode.getFromBoolean(AutopilotSettingCli.cmdA)
        buttonAP_CMDB.lightMode = EnumButtonLightMode.getFromBoolean(AutopilotSettingCli.cmdB)

        tmpEngage(AutopilotSettingCli.fd)
    }

    fun tmpEngage(engage: Boolean) {
        buttonAutoThrottle.switchUp = engage
    }

    override fun actionPerformed(button: GuiButton) {
        val ID = button.id

        if (biRollModes.contains(ID) && button is RollModeButton) {
            selectRollMode(button)
        } else if (biPitchModes.contains(ID) && button is PitchModeButton) {
            selectPitchMode(button)
        } else {
            when (ID) {
                //ILS Panel
                biSwitch -> {
                    val freqBuff = NavSettingCli.navFreqActiveID
                    NavSettingCli._navFreqActiveID = NavSettingCli.navFreqStandByID
                    NavSettingCli._navFreqStandByID = freqBuff
                    NavSettingCli.syncFreqData()
                }

                //AutoPilot
                biFDL, biFDR -> {
                    val fd = !buttonFDL.switchUp
                    buttonFDL.switchUp = fd
                    buttonFDR.switchUp = fd
                    indicatorMAL.indicator = fd
                    indicatorMAR.indicator = fd
                    AutopilotSettingCli.fd = fd
                }

                biATARM -> {
                    AutopilotSettingCli.autoThrottle = !buttonAutoThrottle.switchUp
                }

                biAP_CMDA -> {
                    AutopilotSettingCli.cmdA = !buttonAP_CMDA.lightMode.engage
                }

                biAP_CMDB -> {
                    AutopilotSettingCli.cmdB = !buttonAP_CMDB.lightMode.engage
                }

                biDisEngage -> {
                    AutopilotSettingCli.syncEngageData(cmdA = false, cmdB = false)
                }
            }
        }
    }

    fun selectThrottleMode(button: ThrottleModeButton) {
        val select = button.mode
        if (AutopilotSettingCli.throttleMode == select) {
            AutopilotSettingCli.throttleMode =
                EnumEngageThrottleMode.NON
        } else {
            AutopilotSettingCli.throttleMode = select
        }
        selectThrottleMode(AutopilotSettingCli.throttleMode)
    }

    fun selectThrottleMode(rollMode: EnumEngageThrottleMode) {
        buttonN1.lightMode =
            if (rollMode == EnumEngageThrottleMode.N1) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
        buttonSpeed.lightMode =
            if (rollMode == EnumEngageThrottleMode.SPEED) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
    }

    fun selectRollMode(button: RollModeButton) {
        var select = button.mode
        if (select == EnumRollMode.APP) {
            if (button.lightMode == EnumButtonLightMode.StandBy) {
                AutopilotSettingCli.rollMode =
                    EnumRollMode.APP
                AutopilotSettingCli.pitchMode =
                    APP
                selectPitchMode(APP)
            } else {
                select = AutopilotSettingCli.rollMode
            }
        } else if (select == EnumRollMode.VORLOC) {
            if (button.lightMode == EnumButtonLightMode.StandBy) {
                AutopilotSettingCli.rollMode =
                    EnumRollMode.VORLOC
            } else {
                select = AutopilotSettingCli.rollMode
            }
        } else {
            if (AutopilotSettingCli.rollMode == EnumRollMode.APP) {
                AutopilotSettingCli.pitchMode = ALTHOLD
                selectPitchMode(ALTHOLD)
            }
            AutopilotSettingCli.rollMode = select
        }
        selectRollMode(select)
    }

    fun selectRollMode(rollMode: EnumRollMode) {
        buttonRoll_HDGSEL.lightMode =
            if (rollMode == EnumRollMode.HDGSEL) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
        buttonRoll_LNAV.lightMode =
            if (rollMode == EnumRollMode.LNAV) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
        buttonRoll_VORLOC.lightMode =
            if (rollMode == EnumRollMode.VORLOC) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
        buttonRoll_APP.lightMode =
            if (rollMode == EnumRollMode.APP) {
                EnumButtonLightMode.ON
            } else {
                EnumButtonLightMode.OFF
            }
    }

    fun selectPitchMode(button: PitchModeButton) {
        val select = button.mode
        AutopilotSettingCli.pitchMode = select
        selectPitchMode(select)
        when (select) {
            LVLCHG, VS -> {
                AutopilotSettingCli.syncPitchData(select, AutopilotSettingCli.altitudeReady)
            }

            ALTHOLD -> {
                AutopilotSettingCli.syncPitchData(
                    ALTHOLD,
                    MathHelper.floor_double(ParentEntityGetter.getParent(this.mc.thePlayer).posY)
                )
            }

            APP -> TODO()
            VNAV -> TODO()
        }
        if (AutopilotSettingCli.rollMode == EnumRollMode.APP) {
            AutopilotSettingCli.rollMode =
                EnumRollMode.HDGSEL
        }
    }

    fun selectPitchMode(pitchMode: EnumPitchMode) {
        buttonPitch_VNAV.lightMode = if (pitchMode == VNAV) {
            EnumButtonLightMode.ON
        } else {
            EnumButtonLightMode.OFF
        }
        buttonPitch_LVLCHG.lightMode = if (pitchMode == LVLCHG) {
            EnumButtonLightMode.ON
        } else {
            EnumButtonLightMode.OFF
        }
        buttonPitch_ALTHLD.lightMode = if (pitchMode == ALTHOLD) {
            EnumButtonLightMode.ON
        } else {
            EnumButtonLightMode.OFF
        }
        buttonPitch_VS.lightMode = if (pitchMode == VS) {
            EnumButtonLightMode.ON
        } else {
            EnumButtonLightMode.OFF
        }
    }

    override fun drawWorldBackground(p_146270_1_: Int) {
        if (AutopilotSettingCli.isUpdate()) {
            updateButtonInfoAuto()
        }

        val useDataSet = NavFreqManager.getActiveILS(
            this.mc.thePlayer,
            NavSettingCli.navFreqActiveID,
            AutopilotSettingCli
        )

        if (buttonRoll_APP.lightMode != EnumButtonLightMode.ON) {
            if (useDataSet != null) {
                buttonRoll_APP.lightMode = EnumButtonLightMode.StandBy
            } else {
                buttonRoll_APP.lightMode = EnumButtonLightMode.OFF
            }
        }

        if (buttonRoll_VORLOC.lightMode != EnumButtonLightMode.ON) {
            if (useDataSet != null
                || NavFreqManager.getActiveNav(
                    this.mc.thePlayer,
                    NavSettingCli.navFreqActiveID,
                    AutopilotSettingCli,
                    true
                ) != null
            ) {
                buttonRoll_VORLOC.lightMode = EnumButtonLightMode.StandBy
            } else {
                buttonRoll_VORLOC.lightMode = EnumButtonLightMode.OFF
            }
        }

        //drawGradientRect(40, 40, width - 40, height - 40, -804253680, -804253680)
        mc.textureManager.bindTexture(texturePanel)
        var x = width / 2 - 128
        var y = height / 2 - 5 //-55
        GL11.glTranslated(x.toDouble(), y.toDouble(), 0.0)
        drawTexturedModalRect(0, 0, 0, 0, 255, 110)
        GL11.glTranslated(-x.toDouble(), -y.toDouble(), 0.0)
        mc.textureManager.bindTexture(textureAutopilotPanel)
        x = width / 2
        y = height / 2 - 110
        GL11.glTranslated(x.toDouble(), y.toDouble(), 0.0)
        GL11.glScaled(1.60, 1.60, 1.0)
        GL11.glTranslated(-128.0, 0.0, 0.0)
        drawTexturedModalRect(0, 0, 0, 0, 255, 40)
        GL11.glTranslated(128.0, 0.0, 0.0)
        GL11.glScaled(1 / 1.6, 1 / 1.6, 1.0)
        GL11.glTranslated(-x.toDouble(), -y.toDouble(), 0.0)
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, mouseX: Int, mouseY: Int) {
        super.drawGuiContainerBackgroundLayer(p_146976_1_, mouseX, mouseY)
        GL11.glPushMatrix()
        GL11.glTranslated(width / 2 - 85.toDouble(), height / 2 + 19.toDouble(), 0.0)
        GL11.glScaled(2.0, 2.0, 2.0)
        fontRendererObj.drawString(String.format("%.2f", NavSettingCli.navFreqActiveID.freqency), 0, 0, 0xf3bd63, false)
        GL11.glTranslated(65.0, 0.0, 0.0)
        fontRendererObj.drawString(
            String.format("%.2f", NavSettingCli.navFreqStandByID.freqency),
            0,
            0,
            0xf3bd63,
            false
        )
        GL11.glTranslated(-44.5, -60.5, 0.0)
        GL11.glScaled((1 / 2.0) * 1.5, (1 / 2.0) * 1.5, 1 / 2.0)
        var color: Int
        val parent = ParentEntityGetter.getParent(this.mc.thePlayer)
        val carrier = (parent.ridingEntity as? EntityAutopilotCarrier)
        color = when {
            AutopilotSettingCli.headingActive != AutopilotSettingCli.headingReady -> {
                0xe600ff
            }

            MathHelperAirPort.floor360(parent.rotationYaw + 180) == AutopilotSettingCli.headingReady -> {
                0xf3bd63
            }

            else -> {
                0xffffff
            }
        }
        fontRendererObj.drawString(String.format("%03d", AutopilotSettingCli.headingReady), 0, 0, color, false)

        GL11.glTranslated(40.0, 0.0, 0.0)

        color = when {
            AutopilotSettingCli.altitudeActive != AutopilotSettingCli.altitudeReady -> {
                0xe600ff
            }

            MathHelper.floor_double(parent.posY) == AutopilotSettingCli.altitudeReady -> {
                0xf3bd63
            }

            else -> {
                0xffffff
            }
        }
        fontRendererObj.drawString(String.format("%03d", AutopilotSettingCli.altitudeReady), 0, 0, color, false)

        GL11.glTranslated(-80.0, 0.0, 0.0)

        color = when {
            carrier?.spd?.let { MathHelper.floor_double(it) } == AutopilotSettingCli.ias -> {
                0xf3bd63
            }

            else -> {
                0xffffff
            }
        }
        fontRendererObj.drawString(String.format("%03d", AutopilotSettingCli.ias), 0, 0, color, false)

        GL11.glTranslated(-46.0, 0.0, 0.0)

        fontRendererObj.drawString(String.format("%03d", AutopilotSettingCli.courseA), 0, 0, 0xf3bd63, false)
        GL11.glScaled(1 / 1.5, 1 / 1.5, 1.0)
        GL11.glPopMatrix()
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }

}
