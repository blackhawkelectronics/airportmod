package airportlight.modsystem.navigation.navsetting

import airportlight.modsystem.navigation.autopilot.AutopilotManager.getAPpSetting
import airportlight.modsystem.navigation.autopilot.AutopilotManager.startAutopilot
import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf

class MessageReqNavSetting() : IMessage, IMessageHandler<MessageReqNavSetting, IMessage> {

    override fun fromBytes(buf: ByteBuf) {
    }

    override fun toBytes(buf: ByteBuf) {
    }

    override fun onMessage(message: MessageReqNavSetting, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val player = ctx.serverHandler.playerEntity
            val aircraft = ParentEntityGetter.getParent(player)
            startAutopilot(aircraft, player)
            val apSetting = getAPpSetting(aircraft)
            apSetting?.syncAllData()
            return null
        }
        return null
    }
}
