package airportlight.modsystem.navigation.navsetting

import airportlight.modcore.PacketHandlerAPM
import airportlight.modsystem.navigation.autopilot.MessageEntityUseFreqData
import airportlight.modsystem.navigation.frequency.FrequencyID
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
object NavSettingCli {
    var _navFreqActiveID: FrequencyID =
        FrequencyID(0)
    var navFreqActiveID: FrequencyID
        set(value) {
            _navFreqActiveID = value
            syncFreqData(active = navFreqActiveID)
        }
        get() = _navFreqActiveID

    var _navFreqStandByID = FrequencyID(0)
    var navFreqStandByID
        set(value) {
            _navFreqStandByID = value
            syncFreqData(standby = navFreqStandByID)
        }
        get() = _navFreqStandByID

    fun syncFreqData() {
        syncFreqData(navFreqActiveID, navFreqStandByID)
    }

    private fun syncFreqData(
        active: FrequencyID = navFreqActiveID,
        standby: FrequencyID = navFreqStandByID
    ) {
        PacketHandlerAPM.sendPacketServer(MessageEntityUseFreqData(active, standby))
    }
}