package airportlight.modsystem.navigation.waypointmanager;

import airportlight.util.Vec3I;

public class VORData {
    public final int freqency;
    public final String name;
    public final Vec3I pos;

    public VORData(int freqency, String name, Vec3I pos) {
        this.freqency = freqency;
        this.name = name;
        this.pos = pos;
    }
}
