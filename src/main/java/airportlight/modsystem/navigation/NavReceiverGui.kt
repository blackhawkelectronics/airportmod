package airportlight.modsystem.navigation

import airportlight.modsystem.navigation.autopilot.AutopilotSettingCli
import airportlight.modsystem.navigation.ils.CommandILSManager
import airportlight.modsystem.navigation.ils.ILSUseDataSet
import airportlight.modsystem.navigation.ils.NavFreqManager
import airportlight.modsystem.navigation.ils.NavUseDataSet
import airportlight.modsystem.navigation.navsetting.NavSettingCli
import airportlight.util.MathHelperAirPort
import airportlight.util.ParentEntityGetter
import airportlight.util.Vec3D
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.ScaledResolution
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.MathHelper
import org.lwjgl.opengl.GL11
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.sqrt

class NavReceiverGui : GuiScreen() {
    var centerX = 0
    var centerY = 0
    var scaleFactor = -1
    var diffPosYaw = 0.0
    var diffPosPitch = 0.0
    var diffYaw = 0.0
    override fun doesGuiPauseGame(): Boolean {
        return false
    }

    //    @Override
    //    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
    //    }
    fun isDrawGui(player: EntityPlayer): NavUseDataSet? {
        if (CommandILSManager.approaching && ParentEntityGetter.onGround(player)) {
            CommandILSManager.approaching = false
            ClientNavSetting.navMode = EnumNavMode.OFF
        }
        if (ClientNavSetting.navMode == EnumNavMode.OFF) {
            return null
        }
        if (mc == null) {
            mc = Minecraft.getMinecraft()
        }
        if (mc != null) {
            if (!mc.inGameHasFocus && !CommandILSManager.force) {
                return null
            }
            val scaledresolution = ScaledResolution(mc, mc.displayWidth, mc.displayHeight)
            scaleFactor = scaledresolution.scaleFactor
            if (!mc.gameSettings.hideGUI) {
                width = mc.displayWidth / scaleFactor
                height = mc.displayHeight / scaleFactor - 30
                centerX = width / 2
                centerY = height / 2
            }
            if (ClientNavSetting.navMode == EnumNavMode.NAV) {
                return NavFreqManager.getActiveNav(player, NavSettingCli.navFreqActiveID, AutopilotSettingCli)
            } else if (ClientNavSetting.navMode == EnumNavMode.Command) {
                return getCenterPosCommandILSData(player)
            }
        }
        return null
    }

    //Use From Command Mode
    private fun getCenterPosCommandILSData(player: EntityPlayer): ILSUseDataSet? {
        val diffX = CommandILSManager.centerX - player.posX
        val diffY = CommandILSManager.centerY - player.posY
        val diffZ = CommandILSManager.centerZ - player.posZ
        var d0 = MathHelperAirPort.wrapAngleToPI_double(
            Math.atan2(
                diffZ,
                diffX
            ) - Math.PI / 2 - CommandILSManager.localizerAngRad
        )
        var d1 = MathHelperAirPort.wrapAngleToPI_double(
            Math.atan2(
                diffZ,
                diffX
            ) + Math.PI / 2 + CommandILSManager.localizerAngRad
        )
        val _diffPosYaw: Double
        val _diffYaw: Double
        _diffPosYaw = if (Math.abs(d0) < Math.abs(d1)) {
            d0
        } else {
            d1
        }
        val _diffPosPitch =
            CommandILSManager.ilsApproachAngRad - Math.atan2(
                diffY,
                Math.sqrt(diffX * diffX + diffZ * diffZ)
            )
        d0 = MathHelper.wrapAngleTo180_double(player.rotationYaw - CommandILSManager.localizerAngDeg + 180)
        d1 = MathHelper.wrapAngleTo180_double(player.rotationYaw + CommandILSManager.localizerAngDeg + 180)
        _diffYaw = if (Math.abs(d0) < Math.abs(d1)) {
            d0
        } else {
            d1
        }
        return if (-CommandILSManager.ilsWideAngRad < _diffPosYaw && _diffPosYaw < CommandILSManager.ilsWideAngRad && -CommandILSManager.ilsWideAngRad < _diffPosPitch && _diffPosPitch < +CommandILSManager.ilsWideAngRad) {
            ILSUseDataSet(
                Vec3D(
                    CommandILSManager.centerX,
                    CommandILSManager.centerY,
                    CommandILSManager.centerZ
                ),
                CommandILSManager.localizerAngDeg,
                CommandILSManager.ilsApproachAngDeg,
                _diffPosYaw,
                _diffPosPitch,
                _diffYaw
            )
        } else {
            null
        }
    }

    fun drawGui(player: EntityPlayer, isThirdPersonView: Boolean) {
        if (isThirdPersonView) {
            return
        }
        val dataSet = isDrawGui(player) ?: return
        val localizerAngDeg = dataSet.localizerAngDeg
        val ilsApproachAngDeg = if (dataSet is ILSUseDataSet) {
            dataSet.ilsApproachAngDeg
        } else {
            CommandILSManager.ilsApproachAngDeg
        }
        if (abs(dataSet.diffPosYaw) < 0.25) {
            diffPosYaw = dataSet.diffPosYaw
        } else {
            diffPosYaw = (0.25 + sqrt((abs(dataSet.diffPosYaw) - 0.25) / 15)) * if (dataSet.diffPosYaw >= 0) {
                1
            } else {
                -1
            }
        }
        diffYaw = dataSet.diffYaw
        GL11.glPushMatrix()
        GL11.glEnable(GL11.GL_BLEND)
        GL11.glDisable(GL11.GL_TEXTURE_2D)
        val parent = ParentEntityGetter.getParent(player)
        val motionX = parent.motionX
        val motionY = parent.motionY
        val motionZ = parent.motionZ
        val motionAngYaw = Math.toDegrees(Math.atan2(-motionX, motionZ))
        val motionXZ = Math.sqrt(motionX * motionX + motionZ * motionZ)
        val motionAngPitch = Math.toDegrees(Math.atan2(motionY, motionXZ))
        val diffmotionPitch: Double
        val d0 = MathHelper.wrapAngleTo180_double(localizerAngDeg + 180 - motionAngYaw)
        val d1 = MathHelper.wrapAngleTo180_double(localizerAngDeg + 180 + motionAngYaw)
        var diffmotionYaw: Double
        diffmotionYaw = if (Math.abs(d0) < Math.abs(d1)) {
            d0
        } else {
            d1
        }

        //TODO
        if (motionX == 0.0 && motionZ == 0.0) {
            diffmotionYaw = 0.0
            diffmotionPitch = if (motionY == 0.0 || ParentEntityGetter.onGround(player)) {
                0.0
            } else {
                motionAngPitch//ilsApproachAngDeg + motionAngPitch
            }
        } else {
            diffmotionPitch = motionAngPitch//ilsApproachAngDeg + motionAngPitch
        }

        //基準線
        GL11.glColor3d(1.0, 1.0, 1.0)
        GL11.glLineWidth((scaleFactor * 5).toFloat())
        GL11.glBegin(1)
        GL11.glVertex2d(centerX.toDouble(), 0.0)
        GL11.glVertex2d(centerX.toDouble(), height / 15.0)
        GL11.glVertex2d(centerX.toDouble(), height / 15.0 * 14)
        GL11.glVertex2d(centerX.toDouble(), height.toDouble())
        GL11.glVertex2d(0.0, centerY.toDouble())
        GL11.glVertex2d(width / 15.0, centerY.toDouble())
        GL11.glVertex2d(width / 15.0 * 14, centerY.toDouble())
        GL11.glVertex2d(width.toDouble(), centerY.toDouble())
        GL11.glEnd()
        GL11.glLineWidth((scaleFactor * 3).toFloat())
        GL11.glBegin(1)
        GL11.glVertex2d(
            centerX.toDouble() - CommandILSManager.ilsWideAngRad * 280,
            height / 20.0 * 19
        )
        GL11.glVertex2d(
            centerX.toDouble() - CommandILSManager.ilsWideAngRad * 280,
            height.toDouble()
        )
        GL11.glVertex2d(
            centerX.toDouble() + CommandILSManager.ilsWideAngRad * 280,
            height / 20.0 * 19
        )
        GL11.glVertex2d(
            centerX.toDouble() + CommandILSManager.ilsWideAngRad * 280,
            height.toDouble()
        )
        GL11.glEnd()

        //Yaw・Pitch線
        GL11.glLineWidth((scaleFactor * 6).toFloat())
        GL11.glBegin(1)
        GL11.glVertex2d(centerX + diffmotionYaw - 3, centerY - diffmotionPitch)
        GL11.glVertex2d(centerX + diffmotionYaw + 3, centerY - diffmotionPitch)
        GL11.glEnd()
        GL11.glLineWidth(scaleFactor.toFloat())
        GL11.glColor3d(0.92, 0.03, 1.0)
        GL11.glBegin(1)
        GL11.glVertex2d(centerX + diffPosYaw * 300, 0.0)
        GL11.glVertex2d(centerX + diffPosYaw * 300, height.toDouble())

        if (dataSet.isILSData()) {
            diffPosPitch = (dataSet as ILSUseDataSet).diffPosPitch
            GL11.glVertex2d(0.0, centerY - diffPosPitch * 400)
            GL11.glVertex2d(width.toDouble(), centerY - diffPosPitch * 400)
        }
        GL11.glEnd()
        GL11.glLineWidth((scaleFactor * 2).toFloat())
        GL11.glColor3d(1.0, 0.25, 0.25)
        GL11.glBegin(GL11.GL_TRIANGLES)
        GL11.glVertex2d(centerX - diffYaw, height / 11.0)
        GL11.glVertex2d(centerX - diffYaw - scaleFactor * 2, height / 11.0 + scaleFactor * 5)
        GL11.glVertex2d(centerX - diffYaw + scaleFactor * 2, height / 11.0 + scaleFactor * 5)
        GL11.glEnd()
        GL11.glEnable(GL11.GL_TEXTURE_2D)

        if (!dataSet.isILSData()) {
            val pos = dataSet.pos
            val dist = player.getDistance(pos.x, pos.y, pos.z)

            GL11.glColor3d(1.0, 0.25, 0.25)
            mc.fontRenderer.drawString(
                String.format("[%dm]", dist.roundToInt()),
                centerX + 5,
                height - mc.fontRenderer.FONT_HEIGHT,
                0xf3bd63
            )
        }

        GL11.glDisable(GL11.GL_BLEND)
        GL11.glPopMatrix()
    }
}
