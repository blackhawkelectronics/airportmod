package airportlight.modsystem.navigation.ils

import airportlight.util.Vec3D

open class NavUseDataSet(val pos: Vec3D, val localizerAngDeg: Double, val diffPosYaw: Double, val diffYaw: Double) {
    open fun isILSData(): Boolean {
        return false
    }
}
