package airportlight.modsystem.navigation.ils;

import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.autopilot.IAutopilotSetting;
import net.minecraft.entity.player.EntityPlayer;

import java.util.HashMap;

public class CommandILSManager {
    public static boolean force = false;
    public static double centerX = 0.5, centerY = 65.62, centerZ = 0.5;
    public static double localizerAngDeg = 0;
    public static double localizerAngRad = 0;
    public static boolean approaching = false;
    public static double ilsApproachAngDeg = 5;
    public static double ilsApproachAngRad = 0.0873;
    public static double ilsWideAngRad = 0.35;
    public static HashMap<String, NavRadioData> centerList = new HashMap<String, NavRadioData>();
    public static NavRadioData activeILSData = null;

    public static void setPosAndAng(EntityPlayer player) {
        setPosAndAng(
                floor_double_05(player.posX),
                floor_double_05(player.posY),
                floor_double_05(player.posZ),
                floor_double_15(player.rotationYaw + 180),
                ilsApproachAngDeg
        );
    }

    //Use From Command Only
    public static void setActiveNavData(NavRadioData data, IAutopilotSetting apSetting) {
        boolean isILSData = data instanceof ILSData;
        double targetPosYawDeg;
        double targetPosPitchDeg;
        if (isILSData) {
            targetPosYawDeg = ((ILSData) data).localizerAngDeg;
            targetPosPitchDeg = ((ILSData) data).glideSlopeAngDeg;
        } else {
            if (!apSetting.canUseVOR()) {
                //TODO
                return;
            }
            targetPosYawDeg = apSetting.getVORCourseAngDeg(true);
            targetPosPitchDeg = 0;
        }
        setPosAndAng(data.x, data.y, data.z, targetPosYawDeg, targetPosPitchDeg);
        activeILSData = data;
    }

    public static void setPosAndAng(double x, double y, double z, double angYawDeg, double glideSlopeAngDeg) {
        centerX = x;
        centerY = y;
        centerZ = z;
        localizerAngDeg = angYawDeg;
        localizerAngRad = Math.toRadians(localizerAngDeg);
        ilsApproachAngDeg = glideSlopeAngDeg;
        ilsApproachAngRad = Math.toRadians(glideSlopeAngDeg);
        activeILSData = null;
    }

    public static double floor_double_05(double f) {
        return ((int) (f / 0.5)) / 2.0;
    }

    public static double floor_double_15(double f) {
        return Math.floor((f + 22.5) / 15) * 15 % 360;
    }
}
