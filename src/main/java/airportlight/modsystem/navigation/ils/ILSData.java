package airportlight.modsystem.navigation.ils;

import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.frequency.FrequencyID;

public class ILSData extends NavRadioData {
    public final double localizerAngDeg;
    public final double localizerAngRad;
    public final double glideSlopeAngDeg;
    public final double glideSlopeAngRad;

    public ILSData(String name, double x, double y, double z, double localizerAngDeg, double glideSlopeAngDeg, FrequencyID frequencyID) {
        super(name, x, y, z, frequencyID);
        this.localizerAngDeg = localizerAngDeg;
        this.localizerAngRad = Math.toRadians(this.localizerAngDeg);
        this.glideSlopeAngDeg = glideSlopeAngDeg;
        this.glideSlopeAngRad = Math.toRadians(this.glideSlopeAngDeg);
        setRadioWideRangeRad(0.35);
    }

    public static String makeILSName(String airportName, int runwayDegree, EnumRunwaySide side) {
        return airportName + "-" + runwayDegree + ((side == EnumRunwaySide.NON) ? "" : side.name());
    }
}

