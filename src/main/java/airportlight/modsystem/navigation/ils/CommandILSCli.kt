package airportlight.modsystem.navigation.ils

import airportlight.modcore.config.APMConfig
import airportlight.modsystem.apmdatamanager.CommandILSDataFileManager
import airportlight.modsystem.navigation.ClientNavSetting
import airportlight.modsystem.navigation.EnumNavMode
import airportlight.modsystem.navigation.autopilot.AutopilotSettingCli
import airportlight.modsystem.navigation.frequency.FrequencyID
import airportlight.modsystem.navigation.navsetting.NavSettingCli
import net.minecraft.command.CommandBase
import net.minecraft.command.ICommandSender
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.ChatComponentText
import java.lang.Boolean
import kotlin.Array
import kotlin.Int
import kotlin.String

class CommandILSCli : CommandBase() {
    override fun getCommandName(): String {
        return "ILS"
    }

    override fun getCommandUsage(p_71518_1_: ICommandSender): String {
        return "APM-ILS:commandUsage."
    }

    override fun getRequiredPermissionLevel(): Int {
        return 0
    }

    override fun processCommand(p_71515_1_: ICommandSender, args: Array<String>) {
        if (args.isNotEmpty()) {
            if ("Engage" == args[0]) {
                ClientNavSetting.navMode = EnumNavMode.Command
                p_71515_1_.addChatMessage(ChatComponentText("ILS set Engage."))
            } else if ("Disengage" == args[0]) {
                ClientNavSetting.navMode = EnumNavMode.OFF
                p_71515_1_.addChatMessage(ChatComponentText("ILS set Disengage."))
            } else if ("Approach" == args[0]) {
                CommandILSManager.approaching = true
                ClientNavSetting.navMode = EnumNavMode.Command
                p_71515_1_.addChatMessage(ChatComponentText("ILS set Enabled, and set Approaching."))
            } else if ("SetNowPos" == args[0]) {
                val player = p_71515_1_ as EntityPlayer
                CommandILSManager.setPosAndAng(player)
                ClientNavSetting.navMode = EnumNavMode.Command
                p_71515_1_.addChatMessage(
                    ChatComponentText(
                        "ILS set Pos [" + CommandILSManager.centerX + ", " + CommandILSManager.centerY + ", " + CommandILSManager.centerZ + " : " + Math.toDegrees(
                            CommandILSManager.localizerAngRad
                        ) + "]"
                    )
                )
            } else if ("SetPos" == args[0]) {
                if (args.size == 6) {
                    CommandILSManager.setPosAndAng(
                        args[1].toDouble(),
                        args[2].toDouble(),
                        args[3].toDouble(),
                        args[4].toDouble(),
                        args[5].toDouble()
                    )
                } else if (args.size == 5) {
                    CommandILSManager.setPosAndAng(
                        args[1].toDouble(),
                        args[2].toDouble(),
                        args[3].toDouble(),
                        args[4].toDouble(),
                        CommandILSManager.ilsApproachAngDeg
                    )
                } else if (args.size == 2) {
                    val center = CommandILSManager.centerList[args[1]]
                    if (center != null) {
                        CommandILSManager.setActiveNavData(
                            center,
                            AutopilotSettingCli
                        )
                    }
                }
                p_71515_1_.addChatMessage(
                    ChatComponentText(
                        "ILS set Pos [" + CommandILSManager.centerX + ", " + CommandILSManager.centerY + ", " + CommandILSManager.centerZ + " : " + Math.toDegrees(
                            CommandILSManager.localizerAngRad
                        ) + "]"
                    )
                )
            } else if ("ApproachAng" == args[0]) {
                if (args.size == 2) {
                    APMConfig.ApproachAng = args[1].toDouble().toInt()
                    ClientNavSetting.navMode = EnumNavMode.Command
                    return
                }
                p_71515_1_.addChatMessage(ChatComponentText("ILS Approach Ang set " + CommandILSManager.ilsApproachAngDeg.toInt() + " Deg."))
            } else if ("RegisterThisPos" == args[0]) {
                if (args.size == 2) {
                    val player = p_71515_1_ as EntityPlayer
                    CommandILSManager.centerList[args[1]] =
                        ILSData(
                            args[1],
                            CommandILSManager.floor_double_05(player.posX),
                            CommandILSManager.floor_double_05(player.posY),
                            CommandILSManager.floor_double_05(player.posZ),
                            CommandILSManager.floor_double_15((player.rotationYaw - 180).toDouble()),
                            CommandILSManager.ilsApproachAngDeg,
                            FrequencyID(-1)
                        )
                    p_71515_1_.addChatMessage(ChatComponentText("Register this pos : " + args[1]))
                    CommandILSDataFileManager.writeILSData(CommandILSManager.centerList)
                    return
                }
                p_71515_1_.addChatMessage(ChatComponentText(" Not Register this pos."))
            } else if ("DeletePos" == args[0]) {
                if (args.size == 2) {
                    if (CommandILSManager.centerList.remove(args[1]) != null) {
                        p_71515_1_.addChatMessage(ChatComponentText("Delete pos : " + args[1]))
                        CommandILSDataFileManager.writeILSData(CommandILSManager.centerList)
                        return
                    }
                    p_71515_1_.addChatMessage(ChatComponentText("Not Delete pos."))
                }
            } else if ("force" == args[0]) {
                if (args.size == 2) {
                    CommandILSManager.force =
                        Boolean.parseBoolean(args[1])
                }
                p_71515_1_.addChatMessage(ChatComponentText("ILS force " + CommandILSManager.force + "."))
            } else if ("?" == args[0]) {
                p_71515_1_.addChatMessage(ChatComponentText("§2-----APM Command Help-----"))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS <Engage|Disengage> -> <Engage|Disengage> ILS"))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS Approach -> Enabled ILS only before touch down."))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS SetNowPos -> ILS centerPos set to Now Player Pos and YawAng."))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS SetPos [posName] -> ILS centerPos set to [posName] Pos and YawAng."))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS ApproachAng [angDeg] -> ILS approach ang set to [angDeg](Degree)."))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS RegisterThisPos [posName] -> Register now Player Pos with [posName]."))
                p_71515_1_.addChatMessage(ChatComponentText("/ILS DeletePos [posName] -> Delete registered pos with [posName]."))
            }
        } else {
            if (ClientNavSetting.navMode == EnumNavMode.NAV) {
                p_71515_1_.addChatMessage(ChatComponentText("ILS Engage. Frequency : " + NavSettingCli.navFreqActiveID.freqency))
            } else if (ClientNavSetting.navMode == EnumNavMode.Command) {
                p_71515_1_.addChatMessage(
                    ChatComponentText(
                        "ILS Engage. Pos [" + CommandILSManager.centerX + ", " + CommandILSManager.centerY + ", " + CommandILSManager.centerZ + " : " + Math.toDegrees(
                            CommandILSManager.localizerAngRad
                        ) + "]"
                    )
                )
            } else {
                p_71515_1_.addChatMessage(ChatComponentText("ILS Disengage."))
            }
        }
    }

    override fun addTabCompletionOptions(p_71516_1_: ICommandSender, p_71516_2_: Array<String>): List<*>? {
        if (p_71516_2_.size == 1) {
            return getListOfStringsMatchingLastWord(
                p_71516_2_,
                "Engage",
                "Disengage",
                "Approach",
                "SetNowPos",
                "SetPos",
                "ApproachAng",
                "RegisterThisPos",
                "DeletePos",
                "?"
            )
        } else if (p_71516_2_.size == 2) {
            if (p_71516_2_[0] == "SetPos" || p_71516_2_[0] == "DeletePos") {
                return getListOfStringsMatchingLastWord(
                    p_71516_2_,
                    *CommandILSManager.centerList.keys.toTypedArray()
                )
            }
        }
        return null
    }
}
