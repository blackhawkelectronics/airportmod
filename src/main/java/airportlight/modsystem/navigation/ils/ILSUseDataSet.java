package airportlight.modsystem.navigation.ils;

import airportlight.util.Vec3D;

public class ILSUseDataSet extends NavUseDataSet {
    public final double ilsApproachAngDeg;
    public final double diffPosPitch;

    public ILSUseDataSet(Vec3D pos, double localizerAngDeg, double ilsApproachAngDeg, double diffPosYaw, double diffPosPitch, double diffYaw) {
        super(pos, localizerAngDeg, diffPosYaw, diffYaw);
        this.ilsApproachAngDeg = ilsApproachAngDeg;
        this.diffPosPitch = diffPosPitch;
    }

    @Override
    public boolean isILSData() {
        return true;
    }
}
