package airportlight.modsystem.navigation.ils

import airportlight.modcore.PacketHandlerAPM.sendPacketAll
import airportlight.modcore.PacketHandlerAPM.sendPacketEPM
import airportlight.modsystem.apmdatamanager.FrequencyNavDataFileManager
import airportlight.modsystem.navigation.NavRadioData
import airportlight.modsystem.navigation.autopilot.IAutopilotSetting
import airportlight.modsystem.navigation.frequency.FrequencyID
import airportlight.modsystem.navigation.frequency.IFrequencyContainer
import airportlight.modsystem.navigation.frequency.SendToFreqManagerData
import airportlight.util.MathHelperAirPort
import airportlight.util.Pair
import airportlight.util.Vec3D
import airportlight.util.Vec3I
import cpw.mods.fml.common.eventhandler.SubscribeEvent
import cpw.mods.fml.relauncher.Side
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.util.MathHelper
import net.minecraft.world.ChunkCoordIntPair
import net.minecraftforge.event.world.ChunkEvent
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.sqrt

class NavFreqManager {
    @SubscribeEvent
    fun onChunkLoad(e: ChunkEvent.Load) {
        val worldObj = e.chunk.worldObj
        if (!worldObj.isRemote && checkLists.isNotEmpty()) {
            val checkList = checkLists[e.chunk.chunkCoordIntPair]
            if (checkList != null) {
                val removeList = ArrayList<Pair<Int, Vec3I>>()
                for (pair in checkList) {
                    val pos = pair.value
                    if (worldObj.getTileEntity(
                            pos.x,
                            pos.y,
                            pos.z
                        ) !is IFrequencyContainer
                    ) {
                        removeList.add(pair)
                    }
                }
                for (item in removeList) {
                    val map = frequencyNavUseMapSer[item.key]
                    map?.remove(item.value)
                }
                checkLists.remove(e.chunk.chunkCoordIntPair)
                if (!removeList.isEmpty()) {
                    saveFreqNavData()
                }
            }
        }
    }

    companion object {
        var INSTANCE = NavFreqManager()
        private var frequencyNavUseMapCli = HashMap<Int, HashMap<Vec3I, NavRadioData>>()
        private val checkLists = HashMap<ChunkCoordIntPair, ArrayList<Pair<Int, Vec3I>>>()
        private var frequencyNavUseMapSer = HashMap<Int, HashMap<Vec3I, NavRadioData>>()
        fun loadUseMapSerFromFile() {
            frequencyNavUseMapSer = FrequencyNavDataFileManager.loadFreqNavData()
            checkLists.clear()
            for ((frequencyID, value) in frequencyNavUseMapSer) {
                for ((pos) in value) {
                    val coord = ChunkCoordIntPair(pos.x shr 4, pos.z shr 4)
                    var list = checkLists[coord]
                    if (list == null) {
                        list = ArrayList()
                        checkLists[coord] = list
                    }
                    list.add(Pair(frequencyID, pos))
                }
            }
        }

        fun setUserData(
            pos: Vec3I,
            navData: NavRadioData,
            runSide: Side
        ) {
            val frequencyNavUseMap: HashMap<Int, HashMap<Vec3I, NavRadioData>> = if (runSide.isClient) {
                frequencyNavUseMapCli
            } else {
                frequencyNavUseMapSer
            }
            val freqID = navData.frequencyID.ID
            var map = frequencyNavUseMap[freqID]
            if (map == null) {
                map = HashMap()
                frequencyNavUseMap[freqID] = map
            }
            map[pos] = navData
            if (runSide.isServer) {
                saveFreqNavData()
                sendPacketAll(
                    SendToFreqManagerData(
                        frequencyNavUseMapSer
                    )
                )
            }
        }

        fun unUse(
            navFreqID: FrequencyID,
            pos: Vec3I,
            runSide: Side
        ) {
            val frequencyNavUseMap: HashMap<Int, HashMap<Vec3I, NavRadioData>> = if (runSide.isClient) {
                frequencyNavUseMapCli
            } else {
                frequencyNavUseMapSer
            }
            val map = frequencyNavUseMap[navFreqID.ID]
            map?.remove(pos)
            if (runSide.isServer) {
                saveFreqNavData()
                sendPacketAll(
                    SendToFreqManagerData(
                        frequencyNavUseMapSer
                    )
                )
            }
        }

        /**
         * 呼び出しはサーバー側からのみ
         */
        fun saveFreqNavData() {
            FrequencyNavDataFileManager.writeFreqNavData(frequencyNavUseMapSer)
        }

        fun getActiveILS(
            aircraft: Entity,
            ilsFreqID: FrequencyID?,
            apSetting: IAutopilotSetting
        ): ILSUseDataSet? {
            val retData = getActiveNav(aircraft, ilsFreqID, apSetting, needILS = true, needVORTo = true)
            return retData as ILSUseDataSet?
        }

        fun getActiveNav(
            aircraft: Entity,
            navFreqID: FrequencyID?,
            apSetting: IAutopilotSetting
        ): NavUseDataSet? {
            return getActiveNav(aircraft, navFreqID, apSetting, needILS = false, needVORTo = false)
        }

        fun getActiveNav(
            aircraft: Entity,
            navFreqID: FrequencyID?,
            apSetting: IAutopilotSetting,
            needVORTo: Boolean
        ): NavUseDataSet? {
            return getActiveNav(aircraft, navFreqID, apSetting, false, needVORTo)
        }

        fun getActiveNav(
            aircraft: Entity,
            navFreqID: FrequencyID?,
            apSetting: IAutopilotSetting,
            needILS: Boolean,
            needVORTo: Boolean
        ): NavUseDataSet? {
            val frequencyNabUseMap: HashMap<Int, HashMap<Vec3I, NavRadioData>> = if (aircraft.worldObj.isRemote) {
                frequencyNavUseMapCli
            } else {
                frequencyNavUseMapSer
            }
            if (navFreqID == null) {
                return null
            }
            var diffPosYaw = 0.0
            var diffPosPitch = 0.0
            var diffYaw = 0.0
            var targetPosYawDegBuff: Double = Double.MAX_VALUE
            var targetPosPitchDegBuff: Double = Double.MAX_VALUE
            var diffPosYawBuff: Double = Double.MAX_VALUE
            var diffPosPitchBuff: Double = Double.MAX_VALUE
            var diffYawBuff: Double = Double.MAX_VALUE
            var retNavRadioData: NavRadioData? = null
            val freqDatas = frequencyNabUseMap[navFreqID.ID] ?: return null
            for (navdata in freqDatas.values) {
                val diffX = navdata.x + 0.5 - aircraft.posX
                val diffY = navdata.y + 1.5 - aircraft.posY
                val diffZ = navdata.z + 0.5 - aircraft.posZ
                val isILSData = navdata is ILSData
                var targetPosYawDeg: Double
                var targetPosYawRad: Double
                var targetPosPitchDeg: Double
                if (isILSData) {
                    targetPosYawDeg = -(navdata as ILSData).localizerAngDeg
                    targetPosYawRad = -navdata.localizerAngRad
                    targetPosPitchDeg = navdata.glideSlopeAngDeg
                } else {
                    if (needILS || !apSetting.canUseVOR()) {
                        break
                    }
                    targetPosYawDeg = -apSetting.getVORCourseAngDeg(true).toDouble()
                    targetPosYawRad = Math.toRadians(targetPosYawDeg)
                    targetPosPitchDeg = 0.0
                }
                var d0 = MathHelperAirPort.wrapAngleToPI_double(Math.atan2(diffZ, diffX) + Math.PI / 2 + targetPosYawRad)
                val d1 = MathHelperAirPort.wrapAngleToPI_double(Math.atan2(diffZ, diffX) + Math.PI / 2 + targetPosYawRad)
                diffPosYaw = if (abs(d0) < abs(d1)) {
                    d0
                } else {
                    d1
                }
                val diffXZ2 = diffX * diffX + diffZ * diffZ
                val diffXYZ2 = diffXZ2 + diffY * diffY

                val targetPosPitchRad = if (isILSData) {
                    (navdata as ILSData).glideSlopeAngRad
                } else {
                    0.0
                }
                diffPosPitch = targetPosPitchRad + atan2(diffY, sqrt(diffXZ2))

                aircraft.rotationYaw %= 360f
                d0 = MathHelper.wrapAngleTo180_double(
                    MathHelperAirPort.wrapAngleTo360_double(aircraft.rotationYaw.toDouble()) + MathHelperAirPort.wrapAngleTo360_double(
                        targetPosYawDeg
                    ) - 180
                )
                diffYaw = d0
                if (diffXYZ2 < navdata.radioLongRangeSqrt
                    && ((!needVORTo && !navdata.haveRadioWideRange) || -navdata.radioWideRangeRad < diffPosYaw && diffPosYaw < navdata.radioWideRangeRad
                            && (!isILSData || -CommandILSManager.ilsWideAngRad < diffPosPitch && diffPosPitch < +CommandILSManager.ilsWideAngRad))
                ) {
                    if (abs(diffPosYaw + diffPosPitch) < abs(diffPosYawBuff + diffPosPitchBuff)) {
                        targetPosYawDegBuff = targetPosYawDeg
                        targetPosPitchDegBuff = targetPosPitchDeg
                        diffPosYawBuff = diffPosYaw
                        diffPosPitchBuff = diffPosPitch
                        diffYawBuff = diffYaw
                        retNavRadioData = navdata
                    }
                }
            }
            return if (retNavRadioData != null) {
                if (retNavRadioData is ILSData) {
                    ILSUseDataSet(
                        Vec3D(retNavRadioData.x, retNavRadioData.y, retNavRadioData.z),
                        targetPosYawDegBuff,
                        targetPosPitchDegBuff,
                        diffPosYawBuff,
                        diffPosPitchBuff,
                        diffYawBuff
                    )
                } else {
                    NavUseDataSet(
                        Vec3D(retNavRadioData.x, retNavRadioData.y, retNavRadioData.z),
                        targetPosYawDegBuff,
                        diffPosYawBuff,
                        diffYawBuff
                    )
                }
            } else {
                null
            }
        }

        fun sendUseDataToPlayer(player: EntityPlayerMP?) {
            sendPacketEPM(
                SendToFreqManagerData(frequencyNavUseMapSer),
                player!!
            )
        }

        fun setUseDataCliFromServer(useData: HashMap<Int, HashMap<Vec3I, NavRadioData>>) {
            frequencyNavUseMapCli = useData
        }

        fun getNavInfo(): HashMap<Int, HashMap<Vec3I, NavRadioData>> {
            return frequencyNavUseMapCli
        }
    }
}
