package airportlight.modsystem.navigation.frequency;

import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.ils.ILSData;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.util.Vec3I;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;

import java.util.HashMap;
import java.util.Map;

public class SendToFreqManagerData implements IMessage, IMessageHandler<SendToFreqManagerData, IMessage> {
    HashMap<Integer, HashMap<Vec3I, NavRadioData>> data;

    public SendToFreqManagerData() {
    }

    public SendToFreqManagerData(HashMap<Integer, HashMap<Vec3I, NavRadioData>> data) {
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.data = new HashMap<Integer, HashMap<Vec3I, NavRadioData>>();
        int entrySize = buf.readInt();
        for (int e = 0; e < entrySize; e++) {
            int entryDataSize = buf.readInt();
            for (int ed = 0; ed < entryDataSize; ed++) {
                NBTTagCompound nbt = ByteBufUtils.readTag(buf);
                int freqID = nbt.getInteger("freqID");
                String name = nbt.getString("name");
                int x = nbt.getInteger("x");
                int y = nbt.getInteger("y");
                int z = nbt.getInteger("z");
                Vec3I pos = new Vec3I(x, y, z);
                NavRadioData navRadioData;
                boolean isILS = nbt.getBoolean("isILS");
                if (isILS) {
                    double localizerAngDeg = nbt.getDouble("localizerAngDeg");
                    double glideSlopeAngDeg = nbt.getDouble("glideSlopeAngDeg");
                    navRadioData = new ILSData(name, x, y, z, localizerAngDeg, glideSlopeAngDeg, new FrequencyID(freqID));
                } else {
                    navRadioData = new NavRadioData(name, x, y, z, new FrequencyID(freqID));
                }
                HashMap<Vec3I, NavRadioData> map = data.computeIfAbsent(freqID, k -> new HashMap<Vec3I, NavRadioData>());

                map.put(pos, navRadioData);
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(data.size());
        for (Map.Entry<Integer, HashMap<Vec3I, NavRadioData>> entry : data.entrySet()) {
            buf.writeInt(entry.getValue().size());
            int freqID = entry.getKey();
            for (Map.Entry<Vec3I, NavRadioData> entryData : entry.getValue().entrySet()) {
                Vec3I pos = entryData.getKey();
                NBTTagCompound nbt = new NBTTagCompound();
                nbt.setInteger("freqID", freqID);
                nbt.setInteger("x", pos.x);
                nbt.setInteger("y", pos.y);
                nbt.setInteger("z", pos.z);

                if (entryData.getValue() instanceof ILSData) {
                    nbt.setBoolean("isILS", true);
                    ILSData ilsData = ((ILSData) entryData.getValue());
                    nbt.setDouble("localizerAngDeg", ilsData.localizerAngDeg);
                    nbt.setDouble("glideSlopeAngDeg", ilsData.glideSlopeAngDeg);
                    nbt.setString("name", ilsData.name);
                    ByteBufUtils.writeTag(buf, nbt);
                } else {
                    nbt.setBoolean("isILS", false);
                    ByteBufUtils.writeTag(buf, nbt);
                }
            }
        }
    }

    @Override
    public IMessage onMessage(SendToFreqManagerData message, MessageContext ctx) {
        NavFreqManager.Companion.setUseDataCliFromServer(message.data);
        return null;
    }
}
