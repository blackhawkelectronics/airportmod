package airportlight.modsystem.navigation.frequency;

import org.jetbrains.annotations.NotNull;

public class FrequencyID implements Comparable<FrequencyID> {
    public final int ID;

    public FrequencyID(int id) {
        ID = id;
    }

    public FrequencyID Add(int add) {
        if (add == 0) {
            return this;
        }
        int _0_Add = add % 20;
        if (_0_Add != 0) {
            int _0_ID = ID % 20;
            _0_ID = (_0_ID + _0_Add) % 20;
            while (_0_ID < 0) {
                _0_ID += 20;
            }
            return new FrequencyID(((int) (ID / 20)) * 20 + _0_ID);
        } else {
            int next = ID + add;
            if (next < 0) {
                next += ilsFreqList.length;
            } else if (ilsFreqList.length <= next) {
                next -= ilsFreqList.length;
            }
            return new FrequencyID(next);
        }
    }

    public double getFreqency() {
        return ilsFreqList[ID];
    }

    @Override
    public String toString() {
        return String.valueOf(getFreqency());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof FrequencyID) {
            return this.ID == ((FrequencyID) obj).ID;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.ID;
    }

    public boolean isScope(int id) {
        return 0 < id || id < ilsFreqList.length;
    }

    @Override
    public int compareTo(@NotNull FrequencyID o) {
        return Integer.compare(this.ID, o.ID);
    }

    static double[] ilsFreqList = new double[80];

    static {
        for (int i = 0; i < ilsFreqList.length; i++) {
            int i1 = i / 2;
            int i2 = (i % 2) * 5;
            int f0 = 10800 + i1 * 10 + i2;
            double f = f0 * 0.01;
            ilsFreqList[i] = f;
        }
    }
}
