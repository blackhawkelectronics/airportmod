package airportlight.modsystem.navigation

import airportlight.modcore.PacketHandlerAPM.sendPacketServer
import airportlight.modsystem.navigation.frequency.RequestFreqManagerData

object ClientNavSetting {
    var navMode: EnumNavMode = EnumNavMode.OFF
        set(newNavMode: EnumNavMode) {
            if (field !== newNavMode) {
                field = newNavMode
                if (field === EnumNavMode.NAV) {
                    //サーバーと共有されている FreqManagerベース（LandingPointBlock）の接地ポイントリスト
                    sendPacketServer(RequestFreqManagerData())
                }
            }
        }
}