package airportlight.font.robotobold_h30_1_0;

import airportlight.font.lwjgfont.abstractH30Font;

public class RobotoBoldH30FontBlack extends abstractH30Font {
    public RobotoBoldH30FontBlack() {
        super("robotoboldh30font_0.png");
        setColor(0, 0, 0);
    }

}

