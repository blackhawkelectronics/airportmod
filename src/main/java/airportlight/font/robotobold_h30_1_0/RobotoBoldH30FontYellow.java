package airportlight.font.robotobold_h30_1_0;

import airportlight.font.lwjgfont.abstractH30Font;

public class RobotoBoldH30FontYellow extends abstractH30Font {
    public RobotoBoldH30FontYellow() {
        super("robotoboldh30font_0.png");
        setColor(1, 0.73f, 0);
    }
}

