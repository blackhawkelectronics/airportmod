/**
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2014 momokan (http://lwjgfont.chocolapod.net)
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package airportlight.font.lwjgfont.texture;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.model.obj.TextureCoordinate;
import net.minecraftforge.client.model.obj.Vertex;

import static airportlight.font.lwjgfont.texture.FontAlphaBlend.AlphaBlend;
import static org.lwjgl.opengl.GL11.*;

public class FontTexture {
    private final int target;
    private int textureID;

    private int width;
    private int height;

    private int textureWidth;
    private int textureHeight;

    private float red;
    private float green;
    private float blue;
    private FontAlphaBlend alphaBlend;
    private float alpha;
    private boolean isAlphaPremultiplied;

    public FontTexture(int target, int textureID) {
        this.target = target;
        this.textureID = textureID;
        this.alpha = 1f;
        this.alphaBlend = AlphaBlend;
        this.isAlphaPremultiplied = true;
        this.red = 1f;
        this.green = 1f;
        this.blue = 1f;
    }

    public void draw(float dstX1, float dstY1) {
        draw(dstX1, dstY1, dstX1 + width, dstY1 - height, 0, 0, 0, width, height, 0);
    }

    public void draw(float dstX1, float dstY1, float dstX2, float dstY2, float dstZ, float srcX1, float srcY1, float srcX2, float srcY2, int bright) {
        float halfWidth = (dstX2 - dstX1) / 2;
        float halfHeight = (dstY1 - dstY2) / 2;

        // store the current model matrix
        glPushMatrix();

        // bind to the appropriate texture for this sprite
        bind();

        // translate to the right location and prepare to draw
        glTranslatef(dstX1 + halfWidth, dstY1 - halfHeight, dstZ * -1);
//		glTranslatef(dstX, dstY, 0);

        glEnable(GL_BLEND);

        alphaBlend.config();

        if (bright >= 0) {
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setBrightness(bright);
            tessellator.setColorRGBA_F(red, green, blue, alpha);

            float tx1 = srcX1 / textureWidth;
            float tx2 = srcX2 / textureWidth;
            float ty1 = srcY1 / textureHeight;
            float ty2 = srcY2 / textureHeight;
            float averageU = (tx1 + tx2) / 2;
            float averageV = (ty1 + ty2) / 2;

            TextureCoordinate[] textureCoordinates = new TextureCoordinate[4];
            textureCoordinates[0] = new TextureCoordinate(tx1, ty1);
            textureCoordinates[1] = new TextureCoordinate(tx1, ty2);
            textureCoordinates[2] = new TextureCoordinate(tx2, ty2);
            textureCoordinates[3] = new TextureCoordinate(tx2, ty1);

            Vertex[] vertices = new Vertex[4];
            vertices[0] = new Vertex(halfWidth * -1, halfHeight);
            vertices[1] = new Vertex(halfWidth * -1, halfHeight * -1);
            vertices[2] = new Vertex(halfWidth, halfHeight * -1);
            vertices[3] = new Vertex(halfWidth, halfHeight);

            //normalize
            Vec3 v1 = Vec3.createVectorHelper(vertices[1].x - vertices[0].x, vertices[1].y - vertices[0].y, vertices[1].z - vertices[0].z);
            Vec3 v2 = Vec3.createVectorHelper(vertices[2].x - vertices[0].x, vertices[2].y - vertices[0].y, vertices[2].z - vertices[0].z);
            Vec3 normalVector = v1.crossProduct(v2).normalize();
            tessellator.setNormal((float) normalVector.xCoord, (float) normalVector.yCoord, (float) normalVector.zCoord);


            float offsetU, offsetV;
            for (int i = 0; i < vertices.length; ++i) {
                offsetU = 0;
                offsetV = 0;

                if (textureCoordinates[i].u > averageU) {
                    offsetU = -offsetU;
                }
                if (textureCoordinates[i].v > averageV) {
                    offsetV = -offsetV;
                }

                tessellator.addVertexWithUV(vertices[i].x, vertices[i].y, vertices[i].z, textureCoordinates[i].u + offsetU, textureCoordinates[i].v + offsetV);
            }

            tessellator.draw();
        } else {

            //	透過率を設定する
            if (isAlphaPremultiplied) {
                //	透過イメージを表示する (pre-multipled)
                //	Premultiplied な画像である PNG を半透明表示する場合、 RGB のそれぞれについて alpha 値をかける
                glColor4f(red * alpha, green * alpha, blue * alpha, alpha);
            } else {
                //	透過イメージを表示する (not pre-multipled)
                glColor4f(red, green, blue, alpha);
            }

            // draw a quad textured to match the sprite
            glBegin(GL_QUADS);
            {
                float tx1 = srcX1 / textureWidth;
                float tx2 = srcX2 / textureWidth;
                float ty1 = srcY1 / textureHeight;
                float ty2 = srcY2 / textureHeight;

                glTexCoord2f(tx1, ty1);
                glVertex2f(halfWidth * -1, halfHeight);

                glTexCoord2f(tx1, ty2);
                glVertex2f(halfWidth * -1, halfHeight * -1);

                glTexCoord2f(tx2, ty2);
                glVertex2f(halfWidth, halfHeight * -1);

                glTexCoord2f(tx2, ty1);
                glVertex2f(halfWidth, halfHeight);
            }
            glEnd();
        }
        // restore the model view matrix to prevent contamination
        glPopMatrix();
    }

    public void point(int srcX, int srcY) {
        float tx = 1.0f * srcX / textureWidth;
        float ty = 1.0f * srcY / textureHeight;

        glTexCoord2f(tx, ty);
    }

    void setTextureHeight(int texHeight) {
        this.textureHeight = texHeight;
    }

    void setTextureWidth(int texWidth) {
        this.textureWidth = texWidth;
    }

    int getTextureWidth() {
        return textureWidth;
    }

    int getTextureHeight() {
        return textureHeight;
    }

    public int getWidth() {
        return width;
    }

    void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    void setHeight(int height) {
        this.height = height;
    }

    public boolean isAlphaPremultiplied() {
        return isAlphaPremultiplied;
    }

    void setAlphaPremultiplied(boolean isAlphaPremultiplied) {
        this.isAlphaPremultiplied = isAlphaPremultiplied;
    }

    public void dispose() {
        if (0 < textureID) {
            glDeleteTextures(textureID);
            textureID = -1;
        }
    }

    public void bind() {
        glBindTexture(target, textureID);
    }

    public void setAlphaBlend(FontAlphaBlend alphaBlend) {
        this.alphaBlend = alphaBlend;
    }

    public void setColor(float red, float green, float blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

}
