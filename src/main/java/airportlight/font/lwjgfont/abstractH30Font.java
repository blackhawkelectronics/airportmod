package airportlight.font.lwjgfont;

public abstract class abstractH30Font extends LWJGFont {

    private static final FontMap map = new FontMap();

    protected FontMap getFontMap() {
        return map;
    }

    public abstractH30Font(String filename) {
        map.addImageFile(0, filename);
        configMap0(map);
    }

    private static void configMap0(FontMap var0) {
        var0.addCharacter(new MappedCharacter('A', 0, 5, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('B', 0, 35, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('C', 0, 64, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('D', 0, 94, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('E', 0, 124, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('F', 0, 151, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('G', 0, 177, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('H', 0, 207, 28, 28, 8, 21, 0));
        var0.addCharacter(new MappedCharacter('I', 0, 238, 28, 28, 8, 9, 0));
        var0.addCharacter(new MappedCharacter('J', 0, 257, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('K', 0, 284, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('L', 0, 313, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('M', 0, 339, 28, 28, 8, 26, 0));
        var0.addCharacter(new MappedCharacter('N', 0, 375, 28, 28, 8, 21, 0));
        var0.addCharacter(new MappedCharacter('O', 0, 406, 28, 28, 8, 21, 0));
        var0.addCharacter(new MappedCharacter('P', 0, 437, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('Q', 0, 466, 28, 28, 8, 21, 0));
        var0.addCharacter(new MappedCharacter('R', 0, 497, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('S', 0, 526, 28, 28, 8, 18, 0));
        var0.addCharacter(new MappedCharacter('T', 0, 554, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('U', 0, 583, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('V', 0, 613, 28, 28, 8, 20, 0));
        var0.addCharacter(new MappedCharacter('W', 0, 643, 28, 28, 8, 26, 0));
        var0.addCharacter(new MappedCharacter('X', 0, 679, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('Y', 0, 708, 28, 28, 8, 19, 0));
        var0.addCharacter(new MappedCharacter('Z', 0, 737, 28, 28, 8, 18, 0));
        var0.addCharacter(new MappedCharacter('a', 0, 765, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('b', 0, 791, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('c', 0, 818, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('d', 0, 844, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('e', 0, 871, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('f', 0, 897, 28, 28, 8, 11, 0));
        var0.addCharacter(new MappedCharacter('g', 0, 918, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('h', 0, 945, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('i', 0, 972, 28, 28, 8, 8, 0));
        var0.addCharacter(new MappedCharacter('j', 0, 990, 28, 28, 8, 8, 0));
        var0.addCharacter(new MappedCharacter('k', 0, 1008, 28, 28, 8, 16, 0));
        var0.addCharacter(new MappedCharacter('l', 0, 1034, 28, 28, 8, 8, 0));
        var0.addCharacter(new MappedCharacter('m', 0, 1052, 28, 28, 8, 26, 0));
        var0.addCharacter(new MappedCharacter('n', 0, 1088, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('o', 0, 1115, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('p', 0, 1142, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('q', 0, 1169, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('r', 0, 1196, 28, 28, 8, 11, 0));
        var0.addCharacter(new MappedCharacter('s', 0, 1217, 28, 28, 8, 15, 0));
        var0.addCharacter(new MappedCharacter('t', 0, 1242, 28, 28, 8, 10, 0));
        var0.addCharacter(new MappedCharacter('u', 0, 1262, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('v', 0, 1289, 28, 28, 8, 15, 0));
        var0.addCharacter(new MappedCharacter('w', 0, 1314, 28, 28, 8, 22, 0));
        var0.addCharacter(new MappedCharacter('x', 0, 1346, 28, 28, 8, 15, 0));
        var0.addCharacter(new MappedCharacter('y', 0, 1371, 28, 28, 8, 15, 0));
        var0.addCharacter(new MappedCharacter('z', 0, 1396, 28, 28, 8, 15, 0));
        var0.addCharacter(new MappedCharacter('0', 0, 1421, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('1', 0, 1448, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('2', 0, 1475, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('3', 0, 1502, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('4', 0, 1529, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('5', 0, 1556, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('6', 0, 1583, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('7', 0, 1610, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('8', 0, 1637, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('9', 0, 1664, 28, 28, 8, 17, 0));
        var0.addCharacter(new MappedCharacter('０', 0, 1691, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('１', 0, 1714, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('２', 0, 1737, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('３', 0, 1760, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('４', 0, 1783, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('５', 0, 1806, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('６', 0, 1829, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('７', 0, 1852, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('８', 0, 1875, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('９', 0, 1898, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('←', 0, 1921, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('→', 0, 1944, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('↑', 0, 1967, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('↓', 0, 1990, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter(' ', 0, 2013, 28, 28, 8, 7, 0));
        var0.addCharacter(new MappedCharacter('　', 0, 2030, 28, 28, 8, 13, 0));
        var0.addCharacter(new MappedCharacter('■', 0, 2053, 28, 28, 8, 13, 0));
    }

    public int getDefaultLineHeight() {
        return 30;
    }
}

