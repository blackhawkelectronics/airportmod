package airportlight.font.fontobj;

import airportlight.ModAirPortLight;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;

public class FontModel {
    static WavefrontObject[] modelNumbers = new WavefrontObject[10];

    public static void render(int num) {
        WavefrontObject model = getModel(num);
        model.renderAll();
    }

    public static void render(int num, int brightness) {
        WavefrontObject model = getModel(num);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(GL11.GL_TRIANGLES);
        tessellator.setBrightness(brightness);
        tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
        model.tessellateAll(tessellator);
        tessellator.draw();
    }

    private static WavefrontObject getModel(int num) {
        WavefrontObject model = modelNumbers[num];
        if (model == null) {
            modelNumbers[num] = (WavefrontObject) AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/numbers_robot/" + num + ".obj"));
            model = modelNumbers[num];
        }
        return model;
    }
}
