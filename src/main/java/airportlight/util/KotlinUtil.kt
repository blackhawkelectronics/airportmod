package airportlight.util

import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiScreen
import net.minecraft.world.World

val MessageContext.currentWorld: World
    get() = when (side!!) {
        Side.SERVER -> serverHandler.playerEntity.worldObj
        Side.CLIENT -> Minecraft.getMinecraft().theWorld
    }
val MessageContext.guiScreen: GuiScreen?
    get() = when (side!!) {
        Side.SERVER -> null
        Side.CLIENT -> Minecraft.getMinecraft().currentScreen
    }

fun Double.floorStep(step: Int): Int {
    return (this / step).toInt() * step
}

fun Float.roundStepDeg(step: Int): Int {
    var ang = this
    ang += step / 2
    ang %= 360
    if (ang < 0) {
        ang += 360
    }
    return ((ang) / step).toInt() * step
}
