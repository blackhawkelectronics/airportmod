package airportlight.util

import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World

abstract class TileEntityMessage : IMessage {
    protected constructor()
    protected constructor(tile: TileEntity) {
        pos = Vec3I(tile.xCoord, tile.yCoord, tile.zCoord)
        this.tile = tile
    }

    lateinit var tile: TileEntity
    protected lateinit var pos: Vec3I

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(tile.xCoord)
        buf.writeInt(tile.yCoord)
        buf.writeInt(tile.zCoord)
        write(buf)
    }

    abstract fun write(buf: ByteBuf)

    override fun fromBytes(buf: ByteBuf) {
        pos = Vec3I(buf.readInt(), buf.readInt(), buf.readInt())
        read(buf)
    }

    abstract fun read(buf: ByteBuf)

    protected fun getWorld(ctx: MessageContext): World {
        return ctx.currentWorld
    }

    fun getTileEntity(ctx: MessageContext): TileEntity? {
        return getWorld(ctx).getTileEntity(pos.x, pos.y, pos.z)
    }
}
