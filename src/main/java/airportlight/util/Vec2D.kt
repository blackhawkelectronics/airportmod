package airportlight.util

import net.minecraft.tileentity.TileEntity

class Vec2D {
    @JvmField
    val x: Double

    @JvmField
    val y: Double

    constructor(x: Double, y: Double) {
        this.x = x
        this.y = y
    }

    constructor(tile: TileEntity) {
        x = tile.xCoord.toDouble()
        y = tile.zCoord.toDouble()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Vec3I) {
            return x == other.x.toDouble() && y == other.y.toDouble()
        }
        return false
    }

    operator fun plus(other: Vec2D): Vec2D {
        return Vec2D(x + other.x, y + other.y)
    }

    operator fun minus(other: Vec2D): Vec2D {
        return Vec2D(x - other.x, y - other.y)
    }

    operator fun times(other: Double): Vec2D {
        return Vec2D(x * other, y * other)
    }

    operator fun div(other: Double): Vec2D {
        return Vec2D(x / other, y / other)
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }
}