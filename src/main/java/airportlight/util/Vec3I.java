package airportlight.util;

import java.io.Serializable;

public class Vec3I implements Serializable {
    public final int x, y, z;

    public Vec3I(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public int hashCode() {
        return x << 8 + y << 4 + z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vec3I) {
            Vec3I objI = (Vec3I) obj;
            return this.x == objI.x && this.y == objI.y && this.z == objI.z;
        }
        return false;
    }
}
