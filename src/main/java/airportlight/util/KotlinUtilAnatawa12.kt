package airportlight.util

/**
 * Kotlin Util
 * (c) 2020 anatawa12
 */

import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import io.netty.buffer.ByteBuf
import java.io.*
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

fun OutputStream.gzip(size: Int = 512, syncFlush: Boolean = false) = GZIPOutputStream(this, size, syncFlush)
fun InputStream.gunzip(size: Int = 512) = GZIPInputStream(this, size)

fun OutputStream.forData() = DataOutputStream(this)
fun InputStream.toData() = DataInputStream(this)


inline fun <T> DataInput.readList(block: DataInput.() -> T): List<T> = readCollectionTo(ArrayList(), block)

inline fun <T> DataOutput.writeList(list: List<T>, block: DataOutput.(T) -> Unit) {
    writeCollection(list, block)
}

inline fun <T, C : MutableCollection<T>> DataInput.readCollectionTo(collection: C, block: DataInput.() -> T): C {
    val size = readInt()
    repeat(size) {
        collection.add(block())
    }
    return collection
}

inline fun <T> DataOutput.writeCollection(list: Collection<T>, block: DataOutput.(T) -> Unit) {
    writeInt(list.size)
    for (element in list) {
        block(element)
    }
}

fun ByteBuf.outputStream(): OutputStream = ByteBufOutputStream(this)

private class ByteBufOutputStream(val buf: ByteBuf) : OutputStream() {
    override fun write(b: Int) {
        buf.writeByte(b)
    }

    override fun write(b: ByteArray, off: Int, len: Int) {
        buf.writeBytes(b, off, len)
    }
}

fun DataInput.readVec3D(): Vec3D {
    return Vec3D(
        readDouble(),
        readDouble(),
        readDouble()
    )
}

fun DataOutput.writeVec3D(vec: Vec3D) {
    writeDouble(vec.x)
    writeDouble(vec.y)
    writeDouble(vec.z)
}

@Suppress("unused")
inline fun <reified Packet : IMessage> IMessageHandler<Packet, *>.packetErrorLog(message: String): Nothing? {
    Logger.error("${Packet::class.simpleName}: $message")
    return null
}