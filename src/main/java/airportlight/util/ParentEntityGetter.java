package airportlight.util;

import airportlight.modsystem.navigation.autopilot.EntityAutopilotCarrier;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class ParentEntityGetter {
    private static final HashMap<String, String> parentGetMethodMap = new HashMap<String, String>();
    private static final HashMap<String, String> parentGetFiledMap = new HashMap<String, String>();

    public static void registerNormalData() {
        registerMethodMap("entity.mcheli.MCH.E.Seat.name", "getParent");
        registerFieldMap("entity.mcheli.MCH.E.PSeat.name", "parent");
    }

    public static void registerMethodMap(String entityName, String parentEntityGetMethodName) {
        parentGetMethodMap.put(entityName, parentEntityGetMethodName);
    }

    public static void registerFieldMap(String entityName, String parentEntityFieldName) {
        parentGetFiledMap.put(entityName, parentEntityFieldName);
    }

    public static boolean isOnGrownd(Entity entity, Boolean checkLoose) {
        return entity.onGround || _isOnGrownd(entity, checkLoose);
    }

    private static boolean _isOnGrownd(Entity entity, Boolean checkLoose) {
        double d1 = -0.001;
        double d2 = -0.001;
        World worldObj = entity.worldObj;
        int px = MathHelper.floor_double(entity.posX);
        int yOffset;
        if (checkLoose) {
            yOffset = 2;
        } else {
            yOffset = 1;
        }
        int py = MathHelper.floor_double(entity.posY - yOffset);
        int pz = MathHelper.floor_double(entity.posZ);
        Block block = worldObj.getBlock(px, py, pz);
        AxisAlignedBB aabb = block.getCollisionBoundingBoxFromPool(worldObj, px, py, pz);
        if (aabb != null) {
            return entity.boundingBox.minY - yOffset <= aabb.maxY;
        }
        return false;
    }

    public static Entity getParent(EntityPlayer player) {
        Entity ridingEntity = player.ridingEntity;
        if (ridingEntity == null || ridingEntity instanceof EntityAutopilotCarrier) {
            return player;
        }
        return getParent(ridingEntity);
    }

    public static Entity getParent(Entity entity) {
        Entity parentEntity = entity;

        while (true) {
            String methodName = parentGetMethodMap.get(parentEntity.getCommandSenderName());
            if (methodName != null) {
                try {
                    Method method = parentEntity.getClass().getMethod(methodName);
                    Object parent = method.invoke(parentEntity);
                    if (parent == null) {
                        return parentEntity;
                    }
                    if (parent instanceof Entity) {
                        return (Entity) parent;
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
                return parentEntity;
            }
            String fieldName = parentGetFiledMap.get(parentEntity.getCommandSenderName());
            if (fieldName != null) {
                try {
                    Field field = parentEntity.getClass().getField(fieldName);
                    Object parent = field.get(parentEntity);
                    if (parent == null) {
                        return parentEntity;
                    }
                    if (parent instanceof Entity) {
                        return (Entity) parent;
                    }
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (parentEntity.ridingEntity != null && !(parentEntity.ridingEntity instanceof EntityAutopilotCarrier)) {
                parentEntity = parentEntity.ridingEntity;
            } else {
                return parentEntity;
            }
        }
    }

    public static boolean onGround(EntityPlayer player) {
        return onGround(player, false);
    }

    public static boolean onGround(EntityPlayer player, Boolean checkLoose) {
        if (player.onGround) {
            return true;
        }
        Entity ridingEntity = player.ridingEntity;
        if (ridingEntity == null) {
            return false;
        }
        if (ridingEntity.onGround || _isOnGrownd(ridingEntity, checkLoose)) {
            return true;
        }

        Entity parent = getParent(player);

        if (parent != player) {
            return parent.onGround || _isOnGrownd(parent, checkLoose);
        }
        return false;
    }
}
