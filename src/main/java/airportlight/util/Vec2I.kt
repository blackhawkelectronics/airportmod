package airportlight.util

import net.minecraft.tileentity.TileEntity

class Vec2I {
    @JvmField
    val x: Int

    @JvmField
    val y: Int

    constructor(x: Int, y: Int) {
        this.x = x
        this.y = y
    }

    constructor(tile: TileEntity) {
        x = tile.xCoord
        y = tile.zCoord
    }

    override fun equals(other: Any?): Boolean {
        if (other is Vec3I) {
            return x == other.x && y == other.y
        }
        return false
    }

    operator fun plus(other: Vec2I): Vec2I {
        return Vec2I(x + other.x, y + other.y)
    }

    operator fun minus(other: Vec2I): Vec2I {
        return Vec2I(x - other.x, y - other.y)
    }

    operator fun times(other: Int): Vec2I {
        return Vec2I(x * other, y * other)
    }

    operator fun div(other: Int): Vec2I {
        return Vec2I(x / other, y / other)
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }
}