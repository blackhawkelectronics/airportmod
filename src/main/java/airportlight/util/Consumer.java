package airportlight.util;

public abstract class Consumer {
    public abstract void accept(int i);
}
