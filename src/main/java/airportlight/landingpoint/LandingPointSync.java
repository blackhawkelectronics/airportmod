package airportlight.landingpoint;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.modsystem.navigation.ils.ILSData;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class LandingPointSync extends TileEntityMessage implements IMessageHandler<LandingPointSync, IMessage> {
    String airportName;
    int runwayDegree;
    EnumRunwaySide side;

    double glideSlopeDegree;
    FrequencyID navFreqID;

    public LandingPointSync() {
        super();
    }

    public LandingPointSync(TileLandingPoint tile, String airportName, int runwayDegree, EnumRunwaySide side, double glideSlopeDegree, FrequencyID navFreqID) {
        super(tile);
        this.airportName = airportName;
        this.runwayDegree = runwayDegree;
        this.side = side;
        this.glideSlopeDegree = glideSlopeDegree;
        this.navFreqID = navFreqID;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.airportName = tag.getString("airportName");
        this.runwayDegree = tag.getInteger("runwayDegree");
        this.side = EnumRunwaySide.getEnum(tag.getInteger("side"));
        this.glideSlopeDegree = tag.getDouble("glideSlopeDegree");
        this.navFreqID = new FrequencyID(tag.getInteger("navFreqID"));
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("airportName", this.airportName);
        tag.setInteger("runwayDegree", this.runwayDegree);
        tag.setInteger("side", this.side.ID);
        tag.setDouble("glideSlopeDegree", this.glideSlopeDegree);
        tag.setInteger("navFreqID", this.navFreqID.ID);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(LandingPointSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileLandingPoint) {
            ((TileLandingPoint) tile).setDatas(message.airportName, message.runwayDegree, message.side, message.glideSlopeDegree, message.navFreqID, ctx.side);
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        } else {
            String ilsName = ILSData.makeILSName(message.airportName, message.runwayDegree, side);
            NavFreqManager.Companion.setUserData(message.pos, new ILSData(ilsName, message.pos.x, message.pos.y, message.pos.z, message.runwayDegree, message.glideSlopeDegree, message.navFreqID), ctx.side);
        }
        return null;
    }
}
