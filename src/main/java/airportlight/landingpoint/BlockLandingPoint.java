package airportlight.landingpoint;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Random;

public class BlockLandingPoint extends BlockContainer {
    public BlockLandingPoint() {
        super(Material.rock);
        setCreativeTab(ModAirPortLight.AirPortLightTabs);
        setBlockName("LandingPoint");
        setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.15F, 1.0F);
        setBlockTextureName(ModAirPortLight.DOMAIN + ":landingpoint");
        setTickRandomly(true);
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random rand) {
        setVisibleFlg(world, x, y, z);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random random) {
        setVisibleFlg(world, x, y, z);
    }

    boolean isVisible = false;

    private void setVisibleFlg(World world, int i, int j, int k) {
        if (world.isRemote) {
            ItemStack is = FMLClientHandler.instance().getClient().thePlayer.getCurrentEquippedItem();
            boolean visible = false;
            if (is != null) {
                Item item = is.getItem();
                if (((item instanceof ItemBlock)) &&
                        (Block.getBlockFromItem(item) == this)) {
                    visible = true;
                }
            }
            if (this.isVisible != visible) {
                world.markBlockForUpdate(i, j, k);
                this.isVisible = visible;
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int x, int y, int z) {
        //boolean isVisible = p_149633_1_.getBlockMetadata(x, y, z) == 1;
        return this.isVisible ?
                AxisAlignedBB.getBoundingBox(0.0F, 0.0F, 0.0F, 1.0F, 0.15F, 1.0F) :
                AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
    }

    public void setBlockBoundsBasedOnState(IBlockAccess p_149719_1_, int p_149719_2_, int p_149719_3_, int p_149719_4_) {
        if (this.isVisible) {
            setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.15F, 1.0F);
        } else {
            setBlockBounds(0, 0, 0, 0, 0, 0);
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World p_149668_1_, int p_149668_2_, int p_149668_3_, int p_149668_4_) {
        return null;
    }

    @Override
    public void setBlockBoundsForItemRender() {
        setBlockBounds(0.0F, 0F, 0.0F, 1.0F, 0.15F, 1.0F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if (isVisible) {
            player.openGui(ModAirPortLight.instance, GuiID.LandingPoint.getID(), world, x, y, z);
        }
        return true;
    }

    @Override
    public void breakBlock(World worldObj, int x, int y, int z, Block block, int metadata) {
        ((TileLandingPoint) worldObj.getTileEntity(x, y, z)).ILSFreqUnUse();
        super.breakBlock(worldObj, x, y, z, block, metadata);
    }


    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return blockIcon;
    }

    @SideOnly(Side.CLIENT)
    public String getItemIconName() {
        return this.getTextureName();
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_) {
        this.blockIcon = p_149651_1_.registerIcon(this.getTextureName());
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileLandingPoint();
    }
}
