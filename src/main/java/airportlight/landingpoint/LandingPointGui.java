package airportlight.landingpoint;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.RotatingKnob;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.util.Consumer;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class LandingPointGui extends GuiContainer {
    private static final int biAirportName = 30;
    private static final int biRunwayName = 31;
    private static final int biFreqKnob = 32;
    private static final int biSides = 40;
    private static final int biGlideSlopeDegree = 33;


    private TileLandingPoint tile;
    private int inputID;
    private StringInputGuiButton buttonAirportName;
    private StringInputGuiButton buttonRunwayNumber;
    private GuiButton[] buttonSide;
    private StringInputGuiButton buttonGlideSlopeDegree;
    private RotatingKnob buttonKnob;

    private String scrapString;

    private String tempAirportName;
    private int tempRunwayDegree;
    private EnumRunwaySide tempRunwaySide;

    private double tempGlideSlopeDegree;
    private FrequencyID tempILSFreID;

    public LandingPointGui() {
        super(new ContainerAirPort());
    }

    public LandingPointGui(TileLandingPoint tile) {
        super(new ContainerAirPort());
        this.tile = tile;
        this.tempAirportName = tile.getAirportName();
        this.tempRunwayDegree = floor360(tile.getRunwayDegree());
        this.tempRunwaySide = tile.getRunwaySide();
        this.tempGlideSlopeDegree = tile.getGlideSlopeDegree();
        this.tempILSFreID = tile.getILSFreqID();
    }

    @Override
    public void onGuiClosed() {
        EnterSettings();
        boolean isChanged = this.tile.setDatas(this.tempAirportName, this.tempRunwayDegree, this.tempRunwaySide, this.tempGlideSlopeDegree, this.tempILSFreID, (this.tile.getWorldObj().isRemote ? Side.CLIENT : Side.SERVER));
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new LandingPointSync(this.tile, this.tempAirportName, this.tempRunwayDegree, this.tempRunwaySide, this.tempGlideSlopeDegree, this.tempILSFreID));
        }
    }


    @Override
    public void initGui() {
        buttonAirportName = new StringInputGuiButton(biAirportName, this.width / 2 - 70, this.height / 2 - 65, tile.getAirportName());
        buttonRunwayNumber = new StringInputGuiButton(biRunwayName, this.width / 2 - 70, this.height / 2 - 35, 80, 20, String.format("%02d", floor360(tile.getRunwayDegree())));

        buttonSide = new GuiButton[6];
        buttonSide[0] = new GuiButton(biSides, this.width / 2 + 20, this.height / 2 - 35, 20, 20, "Non");
        buttonSide[1] = new GuiButton(biSides + 1, this.width / 2 + 45, this.height / 2 - 35, 20, 20, "L");
        buttonSide[2] = new GuiButton(biSides + 2, this.width / 2 + 65, this.height / 2 - 35, 20, 20, "LC");
        buttonSide[3] = new GuiButton(biSides + 3, this.width / 2 + 85, this.height / 2 - 35, 20, 20, "C");
        buttonSide[4] = new GuiButton(biSides + 4, this.width / 2 + 105, this.height / 2 - 35, 20, 20, "CR");
        buttonSide[5] = new GuiButton(biSides + 5, this.width / 2 + 125, this.height / 2 - 35, 20, 20, "R");
        this.buttonSide[tile.getRunwaySide().ID].enabled = false;

        buttonGlideSlopeDegree = new StringInputGuiButton(biGlideSlopeDegree, this.width / 2 - 70, this.height / 2 - 5, 80, 20, String.format("%02.1f", tile.getGlideSlopeDegree()));

        buttonKnob = new RotatingKnob(biFreqKnob, this.width / 2 + 100, this.height / 2 + 40, 40, new Consumer() {
            @Override
            public void accept(int i) {
                tempILSFreID = tempILSFreID.Add(i);
            }
        });
        this.buttonList.add(buttonAirportName);
        this.buttonList.add(buttonRunwayNumber);
        for (GuiButton button : buttonSide) {
            this.buttonList.add(button);
        }
        this.buttonList.add(buttonGlideSlopeDegree);
        this.buttonList.add(buttonKnob);
    }

    protected void actionPerformed(GuiButton button) {
        if (button.id == biAirportName || button.id == biRunwayName || button.id == biGlideSlopeDegree) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        } else if (biSides <= button.id && button.id <= biSides + 5) {
            for (GuiButton bs : buttonSide) {
                bs.enabled = true;
            }
            button.enabled = false;
            tempRunwaySide = EnumRunwaySide.getEnum(button.id - biSides);
        }
    }

    private void EnterSettings() {
        if (this.inputID == biAirportName) {
            if (scrapString.isEmpty()) {
                this.buttonAirportName.displayString = tile.getAirportName();
            } else {
                this.buttonAirportName.displayString = scrapString;
            }
            this.tempAirportName = this.buttonAirportName.displayString;
        } else if (this.inputID == biRunwayName) {
            if (scrapString.isEmpty()) {
                this.buttonRunwayNumber.displayString = String.format("%03d", floor360(tile.getRunwayDegree()));
            } else {
                this.buttonRunwayNumber.displayString = String.format("%03d", parse360(scrapString));
            }
            this.tempRunwayDegree = parse360(this.buttonRunwayNumber.displayString);
        } else if (this.inputID == biGlideSlopeDegree) {
            if (scrapString.isEmpty()) {
                this.buttonGlideSlopeDegree.displayString = String.format("%02.1f", tile.getGlideSlopeDegree());
            } else {
                double temp = Double.parseDouble(scrapString);
                if (temp <= 0) {
                    temp = 5;
                } else if (temp >= 90) {
                    temp = 89;
                }
                this.buttonGlideSlopeDegree.displayString = String.format("%02.1f", temp);
            }
            this.tempGlideSlopeDegree = Double.parseDouble(this.buttonGlideSlopeDegree.displayString);
        }
    }


    @Override
    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(40, 40, this.width - 40, this.height - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int mouseX, int mouseY) {
        GL11.glPushMatrix();
        this.buttonKnob.doWheel(mouseX, mouseY, Mouse.getDWheel());
        this.fontRendererObj.drawString("Airport Name  : ", this.width / 2 - 140, this.height / 2 - 60, -1, false);
        this.fontRendererObj.drawString("Runway Number : ", this.width / 2 - 140, this.height / 2 - 30, -1, false);
        this.fontRendererObj.drawString("Glide Slope Ang : ", this.width / 2 - 145, this.height / 2, -1, false);
        this.fontRendererObj.drawString("Frequency    :  " + String.format("%.2f", this.tempILSFreID.getFreqency()), this.width / 2 - 140, this.height / 2 + 30, -1, false);
        GL11.glPopMatrix();
    }

    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biAirportName) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                this.tempAirportName = scrapString;
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (Character.isLetter(eventChar) || Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonAirportName.displayString = scrapString;
        } else if (inputID == biRunwayName) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.trim().length() > 0) {
                    this.tempRunwayDegree = parse360(scrapString);
                    scrapString = String.format("%03d", tempRunwayDegree);
                } else {
                    this.tempRunwayDegree = 0;
                    scrapString = "0";
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (scrapString.length() < 3 && Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            this.buttonRunwayNumber.displayString = scrapString;
        } else if (inputID == biGlideSlopeDegree) {
            if (keyID == Keyboard.KEY_RETURN) {
                this.inputID = 0;
                if (scrapString.trim().length() > 0) {
                    double temp = Double.parseDouble(scrapString);
                    if (temp <= 0) {
                        temp = 5;
                    } else if (temp >= 90) {
                        temp = 89;
                    }
                    this.tempGlideSlopeDegree = temp;
                    scrapString = String.format("%02.1f", tempGlideSlopeDegree);
                } else {
                    this.tempGlideSlopeDegree = 0;
                    scrapString = "5.0";
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                if (scrapString.contains(".")) {
                    if (scrapString.endsWith(".") && Character.isDigit(eventChar)) {
                        scrapString += eventChar;
                    }
                } else {
                    if (scrapString.length() > 0 && keyID == Keyboard.KEY_DECIMAL) {
                        scrapString += eventChar;
                    } else if (scrapString.length() < 2 && Character.isDigit(eventChar)) {
                        scrapString += eventChar;
                    }
                }
            }
            this.buttonGlideSlopeDegree.displayString = scrapString;
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    public int parse360(String input) {
        int i = Integer.parseInt(input);
        return floor360(i);
    }

    public int floor360(int ang360) {
        int ret = ang360 % 360;
        if (ret < 0) {
            ret += 360;
        }
        if (ret == 0) {
            ret = 360;
        }
        return ret;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
