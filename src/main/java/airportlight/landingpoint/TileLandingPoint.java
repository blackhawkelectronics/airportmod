package airportlight.landingpoint;

import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modcore.normal.TileNormal;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.modsystem.navigation.frequency.IFrequencyContainer;
import airportlight.modsystem.navigation.ils.ILSData;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.util.Vec3I;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.nbt.NBTTagCompound;

import static airportlight.util.MathHelperAirPort.floor360;

public class TileLandingPoint extends TileNormal implements IFrequencyContainer {
    private String airportName = "";
    private int runwayDegree = 0;
    private EnumRunwaySide runwaySide = EnumRunwaySide.NON;

    private double glideSlopeDegree = 5.0;
    private FrequencyID ilsFreqID = new FrequencyID(0);

    public TileLandingPoint() {
    }

    public String getAirportName() {
        return this.airportName;
    }

    public int getRunwayDegree() {
        return this.runwayDegree;
    }

    public EnumRunwaySide getRunwaySide() {
        return this.runwaySide;
    }

    public double getGlideSlopeDegree() {
        return this.glideSlopeDegree;
    }

    public FrequencyID getILSFreqID() {
        return this.ilsFreqID;
    }

    public boolean setDatas(String airportName, int runwayDegree, EnumRunwaySide runwaySide, double glideSlopeDegree, FrequencyID ilsFreqID, Side runSide) {
        boolean isChanged = false;
        if (!this.airportName.equals(airportName)) {
            this.airportName = airportName;
            isChanged = true;
        }
        if (this.runwayDegree != runwayDegree) {
            this.runwayDegree = floor360(runwayDegree);
            isChanged = true;
        }
        if (this.runwaySide != runwaySide) {
            this.runwaySide = runwaySide;
            isChanged = true;
        }
        if (this.glideSlopeDegree != glideSlopeDegree) {
            if (glideSlopeDegree <= 0) {
                glideSlopeDegree = 5;
            } else if (glideSlopeDegree >= 90) {
                glideSlopeDegree = 89;
            }
            if (this.glideSlopeDegree != glideSlopeDegree) {
                this.glideSlopeDegree = glideSlopeDegree;
                isChanged = true;
            }
        }
        if (this.ilsFreqID.ID != ilsFreqID.ID) {
            ILSFreqUnUse(runSide);
            this.ilsFreqID = ilsFreqID;
            isChanged = true;
        }
        if (isChanged) {
            String ilsName = ILSData.makeILSName(this.airportName, this.runwayDegree, this.runwaySide);
            NavFreqManager.Companion.setUserData(new Vec3I(this.xCoord, this.yCoord, this.zCoord), new ILSData(ilsName, this.xCoord, this.yCoord, this.zCoord, this.runwayDegree, this.glideSlopeDegree, this.ilsFreqID), runSide);
            this.markDirty();
        }
        return isChanged;
    }

    public void ILSFreqUnUse() {
        this.ILSFreqUnUse(this.worldObj.isRemote ? Side.CLIENT : Side.SERVER);
    }

    public void ILSFreqUnUse(Side runSide) {
        NavFreqManager.Companion.unUse(this.ilsFreqID, new Vec3I(this.xCoord, this.yCoord, this.zCoord), runSide);
    }

    @Override
    public void markDirty() {
        super.markDirty();
        this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
    }


    @Override
    public void writeToNBT(NBTTagCompound nbtTag) {
        super.writeToNBT(nbtTag);
        nbtTag.setString("airportName", this.airportName);
        nbtTag.setInteger("runwayDegree", this.runwayDegree);
        nbtTag.setInteger("runwaySide", this.runwaySide.ID);
        nbtTag.setDouble("glideSlopeDegree", this.glideSlopeDegree);
        nbtTag.setInteger("frequencyID", ilsFreqID.ID);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTag) {
        super.readFromNBT(nbtTag);
        this.airportName = nbtTag.getString("airportName");
        this.runwayDegree = floor360(nbtTag.getInteger("runwayDegree"));
        this.runwaySide = EnumRunwaySide.getEnum(nbtTag.getInteger("runwaySide"));
        this.glideSlopeDegree = nbtTag.getDouble("glideSlopeDegree");
        if (this.glideSlopeDegree <= 0) {
            this.glideSlopeDegree = 5.0;
        } else if (this.glideSlopeDegree >= 90) {
            this.glideSlopeDegree = 89.0;
        }
        this.ilsFreqID = new FrequencyID(nbtTag.getInteger("frequencyID"));
        String ilsName = ILSData.makeILSName(this.airportName, this.runwayDegree, this.runwaySide);
        NavFreqManager.Companion.setUserData(new Vec3I(this.xCoord, this.yCoord, this.zCoord), new ILSData(ilsName, this.xCoord, this.yCoord, this.zCoord, this.runwayDegree, this.glideSlopeDegree, this.ilsFreqID), (this.worldObj == null || !this.worldObj.isRemote) ? Side.SERVER : Side.CLIENT);
    }
}
