package airportlight.items;

import airportlight.ModAirPortLight;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;

import java.util.List;

public class ItemLamp extends Item
{
    public static final String[] colors = new String[] {"red","white","green"};
    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    public ItemLamp()
    {
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
        this.setCreativeTab(ModAirPortLight.AirPortLightTabs);
        this.setTextureName(ModAirPortLight.MOD_ID + ":lamp");
        this.setUnlocalizedName("APLLamp");
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int damage)
    {
        int j = MathHelper.clamp_int(damage, 0, 2);
        return this.icons[j];
    }

    public String getUnlocalizedName(ItemStack p_77667_1_)
    {
        int i = MathHelper.clamp_int(p_77667_1_.getItemDamage(), 0, 2);
        return super.getUnlocalizedName() + "." + colors[i];
    }
    
    @SideOnly(Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tabs, List items)
    {
        for (int i = 0; i < 3; ++i)
        {
            items.add(new ItemStack(item, 1, i));
        }
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister register)
    {
        this.icons = new IIcon[colors.length];

        for (int i = 0; i < colors.length; ++i)
        {
            this.icons[i] = register.registerIcon(this.getIconString() + "_" + colors[i]);
        }
    }
}