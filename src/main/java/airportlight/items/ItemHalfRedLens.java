package airportlight.items;

import airportlight.ModAirPortLight;
import net.minecraft.item.Item;

public class ItemHalfRedLens extends Item {
    public ItemHalfRedLens()
    {
        this.setCreativeTab(ModAirPortLight.AirPortLightTabs);
        this.setTextureName(ModAirPortLight.MOD_ID + ":lens");
        this.setUnlocalizedName("APLLens");
    }
}
