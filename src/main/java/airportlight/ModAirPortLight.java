package airportlight;

import airportlight.blocks.facility.asde.TileASDE;
import airportlight.blocks.facility.glideslope.GlideSlopeTile;
import airportlight.blocks.facility.localizer.LocalizerTile;
import airportlight.blocks.facility.pbb.PBBDriverSeat;
import airportlight.blocks.facility.pbb.TilePBB;
import airportlight.blocks.facility.psr.TilePSR;
import airportlight.blocks.light.aerodromebeacon.TileAerodromeBeacon;
import airportlight.blocks.light.approachlight.ApproachLightTile;
import airportlight.blocks.light.apronlighting.ApronLightingTile;
import airportlight.blocks.light.endlight.TileEndLight;
import airportlight.blocks.light.obstaclelight.TileObstacleLight;
import airportlight.blocks.light.overrunareaedgelight.TileOverrunAreaEdgeLight;
import airportlight.blocks.light.papi.TilePAPI;
import airportlight.blocks.light.runwaycenterlinelight.TileRunwayCenterLineLight;
import airportlight.blocks.light.runwaydistancemarkerlight.TileRunwayDistanceMarkerLight;
import airportlight.blocks.light.runwayedgelight.TileRunwayEdgeLight;
import airportlight.blocks.light.runwaytouchdownzonelight.TileRunwayTouchdownZoneLight;
import airportlight.blocks.light.taxiwaycenterlinelight.TileTaxiwayCenterLineLight;
import airportlight.blocks.light.taxiwayedgelight.TileTaxiwayEdgeLight;
import airportlight.blocks.light.winddirectionindicatorlight.TileWindDirectionIndicatorLight;
import airportlight.blocks.markings.gloundsine.GroundSineTile;
import airportlight.blocks.markings.guidepanel.TileGuidePanel;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsTile;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsTile;
import airportlight.blocks.markings.runwaynumber.TileRunwayNumber;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsTile;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsTile;
import airportlight.landingpoint.TileLandingPoint;
import airportlight.modcore.*;
import airportlight.modcore.command.CommandAPL;
import airportlight.modcore.config.APMConfig;
import airportlight.modcore.config.APMKeyConfig;
import airportlight.modcore.gui.ModAirPortGuiHandler;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.ItemBlockAngleLight;
import airportlight.modcore.proxy.APLProxy;
import airportlight.modsystem.apmdatamanager.CommandILSDataFileManager;
import airportlight.modsystem.navigation.autopilot.EntityAutopilotCarrier;
import airportlight.modsystem.navigation.ils.CommandILSManager;
import airportlight.modsystem.navigation.ils.NavFreqManager;
import airportlight.modsystem.navigation.vordme.TileVORDME;
import airportlight.radar.artsdisplay.ArtsDisplayTile;
import airportlight.radar.system.RadarSystemClient;
import airportlight.radar.system.RadarSystemServer;
import airportlight.towingcar.CommandTowingCar;
import airportlight.towingcar.EntityTowingCar;
import airportlight.util.Logger;
import airportlight.util.ParentEntityGetter;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLMissingMappingsEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import kotlin.Suppress;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.event.world.WorldEvent;


/**
 * Mod   メイン
 */
@Mod(modid = ModAirPortLight.MOD_ID, name = "AirPortLight", version = Constants.version, guiFactory = "airportlight.modcore.config.APMGuiFactory")
public class ModAirPortLight {
    public static final String MOD_ID = "AirPortLight";

    @Mod.Instance("AirPortLight")
    public static ModAirPortLight instance;

    @SidedProxy(clientSide = "airportlight.modcore.proxy.APLClientProxy", serverSide = "airportlight.modcore.proxy.APLCommonProxy")
    public static APLProxy proxy;

    /*クリエイティブタブ*/
    public static CreativeTabs AirPortLightTabs = null;

    /**
     * textureなどのためのドメイン名
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static final String DOMAIN = "airportlight";

    /**
     * コマンド登録
     **/
    @Mod.EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandAPL());
        event.registerServerCommand(new CommandTowingCar());
    }

    @SubscribeEvent
    public void WorldLoadEvent(WorldEvent.Load event) {
        if (event.world.isRemote) {
            //クライアント側のみで処理されているコマンドベースの接地ポイントリスト
            CommandILSDataFileManager.LoadILSData();
            RadarSystemClient.stripList.clear();
        } else {
            //サーバー側で FreqManagerベース（LandingPointBlock）の接地ポイントリストを読み込み
            NavFreqManager.Companion.loadUseMapSerFromFile();
            RadarSystemServer.stripList.clear();
        }
    }

    @SubscribeEvent
    public void WorldUnloadEvent(WorldEvent.Unload event) {
        if (event.world.isRemote) {
            CommandILSDataFileManager.writeILSData(CommandILSManager.centerList);
        }
    }


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ForgeChunkManager.addConfigProperty(instance, "maximumTicketCount", String.valueOf(4000), Property.Type.INTEGER);
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(KeyInputObserver.INSTANCE);
        MinecraftForge.EVENT_BUS.register(NavFreqManager.Companion.getINSTANCE());
        APMConfig.loadConfig(event);
        Logger.registry(event);
        ParentEntityGetter.registerNormalData();

        PacketHandlerAPM.init();
        AirPortLightTabs = new APLCreativeTab("AirPortLighting");
        registerBlock(InstanceList.blockASDE, "ASDE");
        registerBlock(InstanceList.blockGlideSlope, "GlideSlope");
        registerBlock(InstanceList.blockLocalizer, "Localizer");
        registerBlock(InstanceList.blockPBB, "PBB");
        registerBlock(InstanceList.blockPSR, "PSR");

        registerBlock(InstanceList.blockAerodromeBeacon, "AerodromeBeacon");
        registerBlock(InstanceList.blockApproachLight, "ApproachLightBlock");
        registerBlock(InstanceList.blockApronLighting, "ApronLighting");
        registerBlock(InstanceList.blockLightAir, "LightAir");
        registerBlock(InstanceList.blockEndLight, "EndLight");
        registerBlock(InstanceList.blockObstacleLight, "ObstacleLight");
        registerBlock(InstanceList.blockOverrunAreaEdgeLight, "OverrunAreaEdgeLight");
        registerBlock(InstanceList.blockPAPIAuto, "PAPI");
        registerBlock(InstanceList.blockPAPIL, "PAPIL");
        registerBlock(InstanceList.blockPAPILC, "PAPILC");
        registerBlock(InstanceList.blockPAPICR, "PAPICR");
        registerBlock(InstanceList.blockPAPIR, "PAPIR");
        InstanceList.itemBlockRunwayCenterLineLight =
                registerBlock(InstanceList.blockRunwayCenterLineLight, "RunwayCenterLineLight");
        registerBlock(InstanceList.blockRunwayDistanceMarkerLight, "RunwayDistanceMarkerLight");
        registerBlock(InstanceList.blockRunwayEdgeLight, "RunwayEdgeLight");
        registerBlock(InstanceList.blockRunwayTouchdownZoneLight, "RunwayTouchdownZoneLight");
        registerBlock(InstanceList.blockTaxiwayCenterLineLight, "TaxiwayCenterLineLight");
        registerBlock(InstanceList.blockTaxiwayEdgeLight, "TaxiwayEdgeLight");
        registerBlock(InstanceList.blockWindDirectionIndicatorLight, "WindDirectionIndicatorLight");

        registerBlock(InstanceList.blockGroundSine, "GroundSine");
        registerBlock(InstanceList.blockGuidePanel, "GuidePanel");
        registerBlock(InstanceList.blockRunwayAimingPointMarkings, "RunwayAimingPointMarkings");
        registerBlock(InstanceList.blockRunwayHoldPositionMarkings, "RunwayHoldPositionMarkings");
        registerBlock(InstanceList.blockRunwayNumber, "RunwayNumber");
        registerBlock(InstanceList.blockRunwayThresholdMarkings, "RunwayThresholdMarkings");
        registerBlock(InstanceList.blockRunwayTouchdownZoneMarkingsBlock, "RunwayTouchdownZoneMarkings");

        registerBlock(InstanceList.blockLandingPoint, "LandingPoint");
        registerBlock(InstanceList.blockVORDME, "VORDME");
        registerBlock(InstanceList.blockArtsDisplay, "ArtsDisplay");

        GameRegistry.registerItem(InstanceList.itemTowingCar, "itemTowingCar");
        GameRegistry.registerItem(InstanceList.itemLamp, "itemLamp");
        GameRegistry.registerItem(InstanceList.itemHalfRedLens, "itemLens");

        EntityRegistry.registerModEntity(EntityTowingCar.class, "MototokSPACER250", 0, this, 30000000, 1, true);
        EntityRegistry.registerModEntity(EntityAutopilotCarrier.class, "AutopilotCarrier", 1, this, 30000000, 1, true);
        EntityRegistry.registerModEntity(PBBDriverSeat.class, "PBBDriverSeat", 2, this, 30000000, 1, true);
    }

    @Suppress(names = "UNUSED_PARAMETER")
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        //region TileEntityの登録

        GameRegistry.registerTileEntity(TileASDE.class, "TilieASDE");
        GameRegistry.registerTileEntity(GlideSlopeTile.class, "GlideSlopeTile");
        GameRegistry.registerTileEntity(LocalizerTile.class, "LocalizerTile");
        GameRegistry.registerTileEntity(TilePBB.class, "TilePBB");
        GameRegistry.registerTileEntity(TilePSR.class, "TilePSR");

        GameRegistry.registerTileEntity(TileAerodromeBeacon.class, "TileAerodromeBeacon");
        GameRegistry.registerTileEntity(ApproachLightTile.class, "ApproachLightTile");
        GameRegistry.registerTileEntity(ApronLightingTile.class, "ApronLightTile");
        GameRegistry.registerTileEntity(TileEndLight.class, "TileEndLight");
        GameRegistry.registerTileEntity(TileObstacleLight.class, "TileObstacleLight");
        GameRegistry.registerTileEntity(TileOverrunAreaEdgeLight.class, "TileOverrunAreaEdgeLight");
        GameRegistry.registerTileEntity(TilePAPI.class, "TilePAPI");
        GameRegistry.registerTileEntity(TileRunwayCenterLineLight.class, "TileCenterLineLight");
        GameRegistry.registerTileEntity(TileRunwayDistanceMarkerLight.class, "TileRunwayDistanceMarkerLight");
        GameRegistry.registerTileEntity(TileRunwayEdgeLight.class, "TileRunwayEdgeLight");
        GameRegistry.registerTileEntity(TileRunwayTouchdownZoneLight.class, "TileRunwayTouchdownZoneLight");
        GameRegistry.registerTileEntity(TileTaxiwayCenterLineLight.class, "TileTaxiwayCenterLineLight");
        GameRegistry.registerTileEntity(TileTaxiwayEdgeLight.class, "TileTaxiwayEdgeLight");
        GameRegistry.registerTileEntity(TileWindDirectionIndicatorLight.class, "WindDirectionIndicatorLightTile");
        GameRegistry.registerTileEntity(TileLandingPoint.class, "TileLandingPoint");
        GameRegistry.registerTileEntity(TileVORDME.class, "TileVORDME");
        GameRegistry.registerTileEntity(ArtsDisplayTile.class, "ArtsDisplayTile");


        GameRegistry.registerTileEntity(GroundSineTile.class, "GroundSineTile");
        GameRegistry.registerTileEntity(TileGuidePanel.class, "TileGuidePanel");
        GameRegistry.registerTileEntity(RunwayAimingPointMarkingsTile.class, "RunwayAimingPointMarkingsTile");
        GameRegistry.registerTileEntity(RunwayHoldPositionMarkingsTile.class, "RunwayHoldPositionMarkingsTile");
        GameRegistry.registerTileEntity(TileRunwayNumber.class, "TileRunwayNumber");
        GameRegistry.registerTileEntity(RunwayThresholdMarkingsTile.class, "RunwayThresholdMarkingsTile");
        GameRegistry.registerTileEntity(RunwayTouchdownZoneMarkingsTile.class, "RunwayTouchdownZoneMarkingsTile");

        new APLRecipes().registerRecipes();


        //GUIハンドラーの登録
        NetworkRegistry.INSTANCE.registerGuiHandler(ModAirPortLight.instance, new ModAirPortGuiHandler());

        //Tickイベント Minecraftイベント登録
        FMLCommonHandler.instance().bus().register(AirportEventManager.INSTANCE);
        MinecraftForge.EVENT_BUS.register(AirportEventManager.INSTANCE);
        FMLCommonHandler.instance().bus().register(APMConfig.EventHandler.INSTANCE);
        MinecraftForge.EVENT_BUS.register(APMConfig.EventHandler.INSTANCE);

        APMKeyConfig.setKeyConfigDefaults();

        // レシピの登録 現在はサンプルが入っているだけで使っていない
        //ASMRecipe.addRecipe();

        if (FMLCommonHandler.instance().getSide() == Side.CLIENT) {
            proxy.registerRenderer();
        }


        //region 各種機能初期化
        proxy.init();
    }

    @Suppress(names = "UNUSED_PARAMETER")
    @Mod.EventHandler
    public static void FMLMissingMappingsEvent(FMLMissingMappingsEvent e) {
        for (FMLMissingMappingsEvent.MissingMapping map : e.get()) {
            if ("AirPortLight:Aerodrome".equals(map.name)) {
                if (map.type == GameRegistry.Type.BLOCK) {
                    map.ignore();//.remap(InstanceList.blockAerodromeBeacon);
                } else if (map.type == GameRegistry.Type.ITEM) {
                    map.ignore();//.remap(InstanceList.itemBlockRunwayCenterLineLight);
                }
            } else if ("AirPortLight:CenterLineLight".equals(map.name)) {
                if (map.type == GameRegistry.Type.BLOCK) {
                    map.ignore();//.remap(InstanceList.blockRunwayCenterLineLight);
                } else if (map.type == GameRegistry.Type.ITEM) {
                    map.ignore();//.remap(InstanceList.itemBlockRunwayCenterLineLight);
                }
            } else if ("AirPortLight:PBB".equals(map.name)) {
                if (map.type == GameRegistry.Type.BLOCK) {
                    map.ignore();
                } else if (map.type == GameRegistry.Type.ITEM) {
                    map.ignore();
                }
            }
        }
    }

    public static Item registerBlock(Block block, String name) {
        if (block instanceof BlockAngleLightNormal) {
            GameRegistry.registerBlock(block, ItemBlockAngleLight.class, name);
            return GameRegistry.findItem(MOD_ID, name);
        } else {
            GameRegistry.registerBlock(block, name);
        }
        return null;
    }
}
